$NOMOD51
; E3000E0.ASM
; ORIGINAL SEED - E202EA-C.H51  5/1/2020
; =============================================================================
; E3000E0 USES: C8051f962, CYPRESS NVRAM, PCM12, DM11, EM11
;
; FILES:  E200EA-C.H51 = SOURCE CODE
;
; -----------------------------------------------------------------------------
; E3000E0 - 2020-05-01 -     - FIRST VERSION FOR DS2 Chip with NVRAM and EEPROM


?BOOT:		DS 1400h   ;This makes room for the bootloader


;EXTRN CODE (MSG9)
;------------------------------------------------------------------------------
; INCLUDES
;------------------------------------------------------------------------------
$include (SI_C8051F960_Defs.inc)
$include (E300EA-M.INC)
$include (DS2HDR.INC)


;------------------------------------------------------------------------------
; EQUATES
;------------------------------------------------------------------------------

WDADDR          EQU   0       ; EEPROM WORD ADDRESS
;                                                       OLD  NEW
WBANK0          EQU   0A0H     ; FIRST CHIP FIRST BANK   8    0
WBANK1          EQU   0A2H     ; FIRST CHIP SECOND BANK  A    8
WBANK2          EQU   0A8H     ; SECOND CHIP FIRST BANK  C    4
WBANK3          EQU   0AAH     ; SECOND CHIP SECOND BANK E    C

RBANK0          EQU   0A1H     ; FIRST CHIP FIRST BANK   9    1
RBANK1          EQU   0A3H     ; FIRST CHIP SECOND BANK  B    9
RBANK2          EQU   0A9H     ; SECOND CHIP FIRST BANK  D    5
RBANK3          EQU   0ABH     ; SECOND CHIP SECOND BANK F    D

TRNSTART        EQU   100H    ; START ADDRESS OF TRANSACTIONS. 256 BYTES RESERVED
LASTSENT        EQU   0000H   ; NVRAM ADDRESS END BYTE LAST TRANS SENT
LASTRECD        EQU   0002H   ; NVRAM ADDRESS END BYTE LAST TRANSACTION RECORD

DPTRSAV   EQU   DATEIMG
READ      EQU   DATEIMG+3       ; READ BANK ADDRESS
WRITE     EQU   DATEIMG+4       ; WRITE BANK ADDRESS
BANK      EQU   DATEIMG+5       ; WORKING BANK FROM NVADD 02H
TEMP1     EQU   CGALS           ; NEED 10 BYTES TEMP



;------------------------------------------------------------------------------
; RESET and INTERRUPT VECTORS
;------------------------------------------------------------------------------
            ; Reset Vector
            PUBLIC  START

            CSEG AT 	1400h
           	JMP	 START                  ; Locate a jump to the start of
                                       ; code at the reset vector.


START:
		MOV     P1,#00000000B
		 CLR     STATUS_LED; Turn on DS2 status LED to indicate software is running
         MOV     SP,#STACK
         CALL    INIT
		 SETB	 STATUS_LED
         CALL    GETBANK

         MOV     DPTR,#DIVISOR
         CALL    SENDDIV               ; SEND DIVISOR TO EM


         CALL    SETEMCFG
         MOV     DPTR,#EMCONFIG
         CALL    SENDDIV               ; SEND CONFIG TO EM

         CALL    CHKFR
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     AIRSOL,#0xF7			; CLR AIRSOL AIRSOL CLEAR = OPTIC WET
         ANL     EMCOUNT,#0x7F			; CLR EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page


MAIN:

         CALL    DOEMLOAD

         CALL    SETCMP                ; CAN'T HAVE ENOUGH
         MOV     DPTR,#DELFLG
         MOVX    A,@DPTR
         JZ      GOOD
         MOV     DPTR,#POWFAIL
         MOV     A,#1
         MOVX    @DPTR,A               ; SET PF FLAG IF EC BOOTS WITH DELFLG SET

         MOV     DPTR,#STATUS+1        ; UPDATE T-STATUS1 BIT 1
         MOVX    A,@DPTR
         ORL     A,#01H
         MOVX    @DPTR,A               ; SET POW FAIL BIT

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#3,M3               ; IF NOT 'NO PRINTER' THEN SHOW "ERR PRESS PRINT"
         JMP     DEL10                 ; OTHERWISE SKIP TO FINALIZING PREVIOUS DELIVERY

M3:
         MOV     DPTR,#MSG63
         CALL    SHOW
         CALL    KPRESS
         JNC     M1
         JMP     DEL10
M1:      MOV     DPTR,#MSG95
         CALL    SHOW
         CALL    KPRESS
         JNC     M2
         JMP     DEL10
M2:      MOV     DPTR,#MSG2
         CALL    SHOW
         CALL    KPRESS
         JNC     M3
         JMP     DEL10                 ; GO RIGHT TO FINALIZE AND PRINT

GOOD:    CALL    CHKDEC
         JZ      MAINY
         MOV     DPTR,#MONEYFLG        ; CHECK ON DECIMAL OFF/MONEY OFF
         MOV     A,#0                  ; PRICING OFF IF DECIMAL IS OFF (DECIMAL: 0=ON,1=OFF)
         MOVX    @DPTR,A

MAINY:   CALL    SELECTHST             ; ALWAYS READY FOR COMMAND
         CALL    CLRCGALS              ; MAKE STATUS VOLUME ZERO
         CALL    DMODMSG               ; DELIVERY MODE MESSAGE

MAINX:
;         CALL    CON2HOST
         CALL    SELECTHST
;         MOV     A,#'X'
;         CALL    OUTA

         CALL    CLRCGALS
         MOV     DPTR,#PLSCTR
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR AUTO RESET COUNTER

         MOV     DPTR,#HPCTR
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR HOSE PACK COUNTER
         INC     DPTR
         MOVX    @DPTR,A

         CALL    DOEMLOAD

MAINZ:
         CALL    SETCMP                ; SET COMP FLAG
         CALL    LASTVOL               ; ALWAYS COME BACK TO LAST VOLUME

         MOV     DPTR,#FLSHLGFLG       ; RESET HOST DISPLAY FLAG, JUST IN CASE....
         MOV     A,#0
         MOVX    @DPTR,A

MAIN4N:
		 CALL	 BootloaderEvent	   ; CHECK FOR PROGRAM SWITCH TO JUMP TO BOOTLOADER FLASH
         ; IF EMULATEFLG=PA21=2 THEN DO INPA AND CHECK FOR CHARS FROM LAPTOP/EC PRINT BOX, ELSE DO NODEL_INPA
         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#2,MAIN4N_E0_X      ; 2=DO PA21 INPA, ELSE CONTINUE
         CALL    INPA
         JNC     MAIN4X_SJMP           ; NO CHAR, LOOK FOR OTHER INPUTS

MAIN4N_S0:
         CJNE    A,#'P',MAIN4N_S0X      ; SLSMODE, P=HOST REQUESTING SLS PRINT LINE OF TEXT
         CALL    OUTA
         CALL    CRLF

         JMP     DOSLSPLINE            ; SLSMODE, PASS THROUGH PRINT LINE, JUMPS TO MAINZ WHEN DONE

MAIN4N_S0X:
         CJNE    A,#'I',MAIN4N_S1      ; IT'S NOT AN I ASKING FOR DATA = BAIL
         CALL    OUTA
         CALL    CRLF                  ; SEND DELIVERY DATA IN PA21 FORMAT = 82 CHARS

         CALL    SENDIDEL              ; SENDS PIPE+CRLF AT END OF DELIVERY DATA
         JMP     MAIN4N

MAIN4X_SJMP:
         JMP     MAIN4NEXT             ; SJUMP OUT OF RANGE
MAIN4N_E0_X:
         JMP     MAIN4N_E0             ; SJUMP OUT OF RANGE


MAIN4N_S1:
         CJNE    A,#'+',MAIN4N_S2      ; SLSMODE, '+' IS EC REMOTE PRINT FOR SLS MODE

         CALL    LF0000
         MOV     DPTR,#MSG231          ; 'REMOTE' = REMOTE PRINT BOX SAID REMOTE PRINT
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD

         CALL    WAIT3
         CALL    WAIT3
         CALL    WAIT3
         CALL    WAIT3

         JMP     M28A                  ; JUMP TO CODE WHERE <PRINT> IS PRINT DUPLICATE

MAIN4N_S2:
         CJNE    A,#'*',MAIN4N_S3      ; SLSMODE, '*' IS FAKE INSERTION FOR NO DEL, FOR SLS MODE

         CALL    LF0000
         MOV     DPTR,#MSG40           ; 'TICKET' = REMOTE PRINT BOX SAID TICKET INSERTED
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD

         MOV     A,#'X'                ; NEED TO TELL SLS HOST TICKET WAS INSERTED
         CALL    OUTA
         CALL    CRLF

         JMP     MAIN4N

MAIN4N_S3:
         CJNE    A,#'^',MAIN4N_S4      ; SLSMODE, '^' IS SHIFT PRINT INSERTION FOR NO DEL, FOR SLS MODE

         CALL    LF0000
         MOV     DPTR,#MSG47           ; 'SHIFT ' = REMOTE PRINT BOX SAID PRINT SHIFT
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD

         JMP     PRSHT                 ; JUMP TO <SHIFT PRINT> FROM DEL MENU

MAIN4N_S4:
         CJNE    A,#'@',MAIN4NEXT      ; SLSMODE, '@' IS RESET, FOR SLS MODE

         CALL    LF0000
         MOV     DPTR,#MSG72           ; 'RESET ' = REMOTE PRINT BOX SAID RESET
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD

         MOV     DPTR,#RFLAG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     MAIN40                ; JUMP TO <START/STOP> IN DELIVERY MODE SND SKIP ALL RESET SECURITY CHECKS


MAIN4N_E0:
         CALL    INPA
         JC      TSTHSTFXM             ; GOT CHAR, JUMP TO TILDE TESTS
MAIN4NEXT:
         JMP     MAIN10                ; LOOK FOR OTHER INPUTS

TSTHSTFXM:
         CALL    DOHOSTFIX
         JC      MAINISMC                ; GOT TILDE CHARS OK OR HOST FIX OFF, JUMP TO CHECK IF MULTI CHAR COMMAND
         JMP     MAIN4N                ; HOST FIX ON AND GOT INVALID CHAR(S), SKIP CHARS ... LOOK FOR OTHER INPUTS

MAINISMC:
        CJNE    A,#'=',MAIN60          ;CHECK IF IT IS A MULTI CHARACTER COMMAND STARTING WITH '='
        CALL    MCIN                   ;READ IN THE NEXT CHARACTER.
        JNC     MAINMCEND

MAINMC01:
        CJNE    A,#'Y',MAINMC02       ; HOST REQUESTS SEND DUPLICATE OF PREVIOUS DELIVERY TICKET TO HOST (HOST OR PUMP&PRINT)
        CALL    OUTA
        MOV     DPTR,#SNDDUP          ; SET FLAG TO SEND DATA TO HOST INSTEAD OF PRINTER
        MOV     A,#1
        MOVX    @DPTR,A
        MOV     DPTR,#DUPFLG
        MOV     A,#1
        MOVX    @DPTR,A               ; FLAG DUPLICATE MESSAGE
        MOV     DPTR,#DUPTFLG
        MOVX    A,@DPTR
        JZ      MAINMC01A
        MOV     A,#1
        MOV     DPTR,#TORFLG
        MOVX    @DPTR,A               ; SET TIMER OVERRIDE FLAG CORRECTLY

MAINMC01A:
        MOV     DPTR,#HDPFLG          ; SET FLAG TO DO PIPE SINCE NOT HOST MODE BUT IN HOST CMD
        MOV     A,#1
        MOVX    @DPTR,A
        JMP     PRTFINAL              ; CALL #1: PRTFINAL JUMPS TO MAIN
                                      ; THIS IS PRINT DUPLICATE COMMAND FROM HOST
                                      ; DON'T WORRY ABOUT SLSMODE

MAINMC02:
        CJNE    A,#'T',MAINMCEND         ; HOST REQUESTS DELIVERY DATA WITH 12 DIGIT TOTALIZER
        CALL    OUTA
		MOV		DPTR,#TOTDIGITS			;TODO TEMP FOR TESTING
		MOV		A,#12
		MOVX	@DPTR,A
		CALL    SENDDEL
		JMP     MAIN4N

MAINMCEND:
         CLR     C                     ; IGNORE CHAR IN A
         JMP     NOCMDMAIN


MCIN:    CALL    OUTA                  ;ECHO '='
         MOV     R1,#0
         MOV     R2,#6                 ; 5*256 IS ABOUT 45 MS
MCIN1:   CALL    INPA                  ; WAIT FOR CHAR #2
         JC      MCINY                 ; GOT CHAR2, JUMP TO EXIT
         DJNZ    R1,MCIN1
         DJNZ    R2,MCIN1              ; LOOK FOR R2*256 (SINCE R1=0, 0-1=256) TIMES
         JMP     MCINN                 ; ERR: TIMEOUT WAITING FOR CHAR2
MCINY:
         SETB    C                     ; CHARACTER RECEIVED
         JMP     MCINEX                ; JUMP TO EXIT
MCINN:
         CLR     C                     ; NO CHARACTER  = DON'T PROCEED (CLEAR CARRY)
MCINEX:
         RET

MAIN60:  CJNE    A,#'A',MAIN21         ; PRESET / HOST MODE COMMAND FOR 00000.0 - 99999.9 (E177 AND NEWER), THIS IS FOR PRESET/HOST PRIOR TO DELIVERY
         MOV     B,A
         MOV     DPTR,#PS6FLG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     M36C

MAIN21:  CJNE    A,#'B',MAIN27         ; HOST SENDING PUMP & PRINT FOOTER TEXT, 100 BYTES
         CALL    OUTA
         CALL    CHKPND                ; IF TICKET PENDING THEN DISALLOW
         JC      M21
         CALL    GETFTR                ; GET 100 BYTES FOR PUMP & PRINT FOOTER
         JMP     MAINZ
M21:     MOV     R0,#100               ; IGNORE (BUT EAT) 100 BYTES
         CALL    GTBLKR0
         JMP     MAINZ

MAIN27:  CJNE    A,#'C',MAIN22         ; HOST SENDING TIMER TEMP OVERRIDE SETTING FOR NEXT DELIVERY
         CALL    OUTA
M27A:    CALL    INPA
         JNC     M27A
         CALL    OUTA
         CJNE    A,#'0',M27B
         MOV     A,#0
         JMP     M27C
M27B:    MOV     A,#1
M27C:    MOV     DPTR,#TORFLG
         MOVX    @DPTR,A
M27D:    CALL    SENDPIPE
         JMP     MAIN4N

MAIN22:  CJNE    A,#'D',MAIN36         ; HOST SENDING NEXTEL DELIVERY SETUP DATA
         CALL    OUTA
         CALL    GETBLOCK
         JMP     MAINX                 ; FIXES J CMD PROBLEM IF J SENT AFTER D

MAIN36:  CJNE    A,#'E',MAIN20         ; PRESET / HOST MODE COMMAND FOR 0000.0 - 9999.9 (E176 AND OLDER, NEED TO KEEP BACKWARDS COMPATIBLE)
         MOV     B,A                   ; SAVE E
         MOV     DPTR,#PS6FLG
         MOV     A,#0
         MOVX    @DPTR,A               ; ASSUME REGULAR PRESET UNTIL 'A' COMMAND
M36C:
         MOV     A,B                   ; GET E BACK
         CALL    CHKPND
         JC      M36
         CALL    OUTA
         MOV     DPTR,#PS6FLG
         MOVX    A,@DPTR
         JZ      M36D
         CALL    LD6PRST
         JMP     M36E
M36D:    CALL    LOADPRST              ; LOADS PRCOD,PRESET,TIMER
M36E:    MOV     DPTR,#HOST
         MOV     A,#1
         MOVX    @DPTR,A
M36A:    JMP     MAINZ                 ; NOW IN HOST MODE
M36:     CALL    OUTA
         CALL    SEND0PIP
         JMP     MAINZ

MAIN20:  CJNE    A,#'F',MAIN24         ; HOST SENDING LABELS AND AMOUNTS OF ALL FIXED CHARGES
         CALL    OUTA
         CALL    GETFIXED              ; GET FIXED CHARGES
         JMP     MAINZ

MAIN24:  CJNE    A,#'G',MAIN16         ; HOST REQUESTING 600 BYTES OF CALIBRATION DATA, FOR ALL PRODUCTS
         CALL    OUTA
         CALL    CHKPND
         JC      M24
         CALL    SENDCAL
M24:     JMP     MAINZ

; J CAN BE DETECTED AS H DURING REMOTE PRINT
MAIN16:  CJNE    A,#'H',MAIN39         ; HOST SENDING PUMP & PRINT HEADER TEXT, 250 BYTES
         CALL    OUTA
         CALL    CHKPND                ; IF TICKET PENDING THEN DISALLOW
         JC      M16
         CALL    GETHDR                ; GET 250 BYTES FOR PUMP & PRINT HEADER
         JMP     MAINZ

M16:     MOV     R0,#250               ; IGNORE (BUT EAT) 250 BYTES
         CALL    GTBLKR0
         JMP     MAINZ

MAIN39:  CJNE    A,#'I',MAIN34         ; HOST REQUESTING CURRENT PRINTER STATUS
         CALL    OUTA                  ; ECOUNT MODE NEEDS TO DO THESE - ; THIS IS *NOT* FOR SLSMODE
         CALL    CHKPRTR               ; EVENTUALLY JMPS TO MAIN
         JMP     MAINZ

MAIN34:  CJNE    A,#'J',MAIN15         ; HOST REQUESTS STATUS DATA
         CALL    SENDSTAT
         CALL    SENDVOL
         CALL    CHKSUM
         JMP     MAIN4N

; K ONLY VALID DURING THE DELIVERY = CANCELS THE COMMAND; ONLY FOR U, W COMMANDS...

MAIN15:  CJNE    A,#'L',MAIN18         ; HOST SENDING PRODUCT LABELS FOR ALL 99 PC
         CALL    OUTA
         CALL    CHKPND                ; IF TICKET PENDING THEN DISALLOW
         JC      M15
         CALL    GETLABEL
M15:     JMP     MAINZ

MAIN18:  CJNE    A,#'M',MAIN65         ; SEND PRICING FOR ALL PRODUCTS TO ECOUNT
         CALL    OUTA
         CALL    GETPRMX               ; GET PRICING MATRIX
         CALL    GETPCPR
         CALL    CONV2BCD
         JMP     MAINZ

; N ONLY VALID DURING THE DELIVERY = ENDS THE DELIVERY

MAIN65:  CJNE    A,#'O',MAIN14         ; HOST SENDING FLEET TIMEOUT COMMAND
         CALL    OUTA
         CALL    GETFLT
         JMP     MAINZ

MAIN14:  CJNE    A,#'P',MAIN12         ; HOST REQUESTING LIST OF VALID PRODUCTS, 2BYTES/PC x 99 PC = 198 BYTES
         CALL    OUTA                  ; ECOUNT MODE NEEDS TO DO THESE
         CALL    PRODST                ; THIS IS *NOT* FOR SLSMODE
         JMP     M27D                  ; SENDPIPE + JMP MAIN4

MAIN12:  CJNE    A,#'Q',MAIN13         ; HOST REQUESTING NEXTEL DELIVERY DATA FROM ECOUNT
         CALL    OUTA
         CALL    SENDBLK               ; SEND DATA TO HOST
M12X1:   CALL    SENDPIPE
M12X2:   JMP     MAINZ

MAIN13:  CJNE    A,#'R',MAIN25         ; HOST REQUESTING RESET TO BEGIN DELIVERY
         MOV     B,A                   ; SAVE R
         MOV     A,B                   ; GET R BACK
         CALL    CHKPND
         JC      M13
         CALL    OUTA
         MOV     DPTR,#RFLAG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     MAIN40

M13:     CALL    OUTA
         JMP     M12X1                 ; SENDPIPE + JMP MAINZ

MAIN25:  CJNE    A,#'S',MAIN33         ; HOST SENDING 600 BYTES OF CALIBRATION DATA, FOR ALL PRODUCTS
         CALL    OUTA
         CALL    GETCALDAT             ; THIS WILL EAT(IGNORE) DATA AS NECESSARY, AND ALSO ECHO INVALID STATUS AS NECESSARY
M25X:    JMP     MAINZ

MAIN33:  CJNE    A,#'T',MAIN35         ; HOST REQUESTING DELIVERY DATA
         CALL    OUTA
		 MOV	 DPTR,#TOTDIGITS			;TODO TEMP FOR TESTING
		 MOV	 A,#8
		 MOVX	 @DPTR,A
         CALL    SENDDEL
         JMP     MAIN4N

MAIN35:  CJNE    A,#'U',MAIN17         ; HOST SENDING 'BEFORE' BYTES
         CALL    OUTA
         CALL    GETBBYTE
         JMP     MAIN4N

MAIN17:  CJNE    A,#'V',MAIN37         ; HOST REQUESTING VERSION + INFORMATION STRING
         CALL    OUTA
         CALL    SENDVER
         JMP     MAIN4N

MAIN37:  CJNE    A,#'W',MAIN38         ; HOST SENDING 'AFTER' BYTES
         CALL    OUTA
         CALL    GETABYTE
         JMP     MAIN4N

MAIN38:  CJNE    A,#'X',MAIN23         ; HOST REQUESTING PRINTING OF HOST-MODE DELIVERY TICKET
         CALL    OUTA
         JMP     PRTHOST               ; EVENTUALLY JMPS TO MAIN

MAIN23:  CJNE    A,#'Y',MAIN26         ; HOST REQUESTS PRINT DUPLICATE OF PREVIOUS DELIVERY TICKET (HOST OR PUMP&PRINT)
         CALL    OUTA
         CALL    CHKTKT                ; CALL #1 = PRINT DUPLICATE
         CALL    CHKPND
         JC      M25X                  ; WAS A JUMP TO MAIN10, BUT SHOULD PROBABLY RETURN TO TOP (M25X = JMP TO MAINZ)
         MOV     DPTR,#DUPFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG DUPLICATE MESSAGE
         MOV     DPTR,#DUPTFLG
         MOVX    A,@DPTR
         JZ      M23A
         MOV     A,#1
         MOV     DPTR,#TORFLG
         MOVX    @DPTR,A               ; SET TIMER OVERRIDE FLAG CORRECTLY

; I DON'T THINK THIS NEEDS TO BE CHANGED/SET HERE ..
;         MOV     DPTR,#ETFLGBIN
;         MOVX    @DPTR,A               ; SET ELAPSED TIMER FLAG CORRECTLY

M23A:    MOV     DPTR,#HDPFLG          ; SET FLAG TO DO PIPE SINCE NOT HOST MODE BUT IN HOST CMD
         MOV     A,#1
         MOVX    @DPTR,A

         JMP     PRTFINAL              ; CALL #1: PRTFINAL JUMPS TO MAIN
                                       ; THIS IS PRINT DUPLICATE COMMAND FROM HOST
                                       ; DON'T WORRY ABOUT SLSMODE

MAIN26:  CJNE    A,#'Z',MAIN30         ; HOST REQUESTING DUMP OF A LOT OF DATA, MOSTLY FOR MATRIX
         CALL    OUTA
         CALL    DUMP
         JMP     MAINZ

MAIN30:  CJNE    A,#'a',MAIN31         ; HOST SENDING FLAGS FOR TICKET PRINTING LINES AS SETUP VIA MATRIX
         CALL    OUTA
         CALL    GETLINE
         JMP     MAINZ

MAIN31:  CJNE    A,#'b',MAIN32         ; HOST SENDING ADDITIONAL PRODUCT LABEL LINE FLAGS (START LINE# AND END LINE# = 2/PC X 99 PC = 198 BYTES)
         CALL    OUTA
         CALL    GETPLC                ; ADDTIONAL PRODUCT LABEL LINE FLAGS = 2X99=198 BYTES
         JMP     MAINZ

MAIN32:  CJNE    A,#'c',MAIN47         ; HOST SENDING ADDITIONAL PRODUCT LABELS
         CALL    OUTA
         CALL    GETADPL               ; ADDITIONAL PRODUCT LABEL 100 X 25 = 2500
M32X:    JMP     MAINZ

MAIN47:  CJNE    A,#'d',MAIN48         ; HOST REQUESTS PRINTING OF CALIBRATION TICKET
         CALL    OUTA
         CALL    CHKPND
         JC      M32X
         MOV     A,#1
         MOV     DPTR,#CALLED
         MOVX    @DPTR,A               ; SET CALLED FLAG FOR RETURN
         JMP     PRTCAL                ; WHEN THIS IS DONE, IT JUMPS TO M47X

M47X:    CALL    CON2HOST              ; THERE ARE 4 PLACES THAT RETURN TO M47X, DON'T CHANGE THIS
         CALL    SELECTHST
         JMP     M12X1                 ; SENDPIPE + JMP MAINZ

MAIN48:  CJNE    A,#'e',MAIN49         ; HOST REQUESTS PRINTING OF SHIFT TICKET
         CALL    OUTA
         CALL    CHKPND
         JC      M32X
         MOV     A,#1
         MOV     DPTR,#CALLED
         MOVX    @DPTR,A               ; SET CALLED FLAG FOR RETURN
         JMP     PRSHT                 ; WHEN THIS IS DONE, IT JUMPS TO M47X

MAIN49:  CJNE    A,#'f',MAIN50         ; HOST SENDING NEW QOB VALUES FOR ALL 99 PC
         CALL    OUTA
         CALL    CHKPND
         JC      M49
         CALL    GETAQOB               ; GET QOBS FROM HOST
M49:     JMP     MAINZ

MAIN50:  CJNE    A,#'g',MAIN53         ; HOST REQUESTING QOBS FOR ALL 99 PC
         CALL    OUTA
         CALL    CHKPND
         JC      M50                   ; DONT SEND IF TICKET PENDING
         CALL    SENDQOB               ; SEND QOBS TO HOST
M50:     JMP     MAINZ

MAIN53:  CJNE    A,#'h',MAIN54         ; HOST SENDING TAX LABELS AND TAX2 SUBJECT TO TAX1 FLAG
         CALL    OUTA
         CALL    GETTXLB
         JMP     MAINZ

MAIN54:  CJNE    A,#'i',MAIN61         ; HOST SENDING HOST MODE PRINTING FLAGS
         CALL    OUTA
         CALL    GETFLAGS              ; SET LINE PRINTING CONTROL FLAGS
         JMP     MAIN4N

MAIN61:  CJNE    A,#'j',MAIN62         ; HOST REQUESTING CAL TICKET TEXT
         CALL    OUTA
         CALL    SETHCMDFLG
         JMP     CALRPT                ; EVENTUALLY JUMPS TO MAIN61X

MAIN61X: CALL    CLRHCMDFLG
         JMP     M12X1                 ; SENDPIPE + JMP MAINZ

MAIN62:  CJNE    A,#'k',MAIN51         ; HOST REQUESTING SHIFT TICKET TEXT
         CALL    OUTA
         CALL    SETHCMDFLG
         JMP     SHFTRPT               ; EVENTUALLY JUMPS TO MAIN61X

MAIN51:  CJNE    A,#'l',MAIN64         ; HOST REQUESTING #LINES PRINTED ON LAST TICKET
         CALL    OUTA
         MOV     DPTR,#LINES           ; SEND # OF PRINTED LINES TO HOST
         MOVX    A,@DPTR
         CALL    OUTA
         JMP     M12X1                 ; SENDPIPE + JMP MAINZ

MAIN64:  CJNE    A,#'m',MAIN70A        ; HOST REQUESTING PC+PRESET 176- (00-99 + 00000-99999 IN TENTHS)
         CALL    OUTA
         MOV     DPTR,#PS6GET          ; CHECKED IN SENDPCPR TO KNOW IF MSB OF PRESET NEEDS TO BE SENT
         MOV     A,#0                  ; 0 = DON'T SEND 6TH DIGIT
MAIN64A: MOVX    @DPTR,A
         CALL    SENDPCPR
         JMP     M12X1                 ; SENDPIPE + JMP MAINZ

MAIN70A: CJNE    A,#'n',MAIN70         ; HOST REQUESTING PC+PRESET 177+ (00-99 + 000000-999999 IN TENTHS)
         CALL    OUTA
         MOV     DPTR,#PS6GET          ; CHECKED IN SENDPCPR TO KNOW IF MSB OF PRESET NEEDS TO BE SENT
         MOV     A,#1                  ; 1 = SEND 6TH DIGIT
         JMP     MAIN64A

MAIN70:  CJNE    A,#'o',MAIN52         ; PASS-THROUGH PRINT START TICKET (LOGO, MLACK MARK FF, LABEL-START)
         CALL    OUTA
         CALL    CHKPND                ; SKIP IF TICKET PENDING (METER TICKET MUST PRINT 1ST)
         JC      M70
         CALL    HSTPRNTST             ; START, RETURN SUCCESS OR ERR
M70:     JMP     MAINZ

MAIN52:  CJNE    A,#'p',MAIN72         ; HOST REQUESTING SINGLE PRODUCT DESCRIPTION
         CALL    OUTA
         CALL    SENDPD                ; SEND PRODUCT DESCRIPTION
         JMP     M27D                  ; SENDPIPE + JMP MAIN4N

MAIN72:  CJNE    A,#'q',MAIN74         ; PASS-THROUGH PRINT 25 ASCII CHARS
         CALL    OUTA
         CALL    CHKPND                ; SKIP IF TICKET PENDING (METER TICKET MUST PRINT 1ST)
         JC      M73
         CALL    HSTPRTLN              ; READ 25 CHARS/PRINT, RETURN SUCCESS OR ERR
         JMP     MAIN4N
M73:     MOV     R0,#25                ; IGNORE (BUT EAT) 25 BYTES
         CALL    GTBLKR0
         JMP     MAIN4N

MAIN74:  CJNE    A,#'r',MAIN59         ; PASS-THROUGH PRINT FINISH TICKET
         CALL    OUTA
         CALL    CHKPND                ; SKIP IF TICKET PENDING (METER TICKET MUST PRINT 1ST)
         JC      M74
         CALL    HSTPRNTFN             ; FINISH, RETURN SUCCESS OR ERR
M74:     JMP     MAINZ

;MAIN75:  CJNE    A,#'s',MAIN59         ; LT ONLY<< GET NEW HOSE-SELECTION TEXT, ADDED 413_REV0
;         CALL    OUTA
;         CALL    GETHOSETX              ; FINISH, RETURN SUCCESS OR ERR
;         JMP     MAINZ

MAIN59:  CJNE    A,#'t',MAIN58         ; HOST SENDING NEW CURRENT NET TOTALZIER VALUE
         CALL    OUTA
         CALL    GETTTLZR
         JMP     MAINZ

MAIN58:  CJNE    A,#'u',MAIN55         ; HOST REQUESTING PRINTER TYPE SETTING
         CALL    OUTA
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         ORL     A,#30H                ; MAKE ASCII
         CALL    OUTA
         JMP     M27D                  ; SENDPIPE + JMP MAIN4

MAIN55:  CJNE    A,#'v',MAIN56         ; HOST REQUESTING CURRENT TIMEDATE
         CALL    OUTA
         CALL    SHTIME                ; SEND CURRENT TIMEDATE TO HOST
         JMP     MAINZ

MAIN56:  CJNE    A,#'w',MAIN57A        ; HOST SENDING NEW TIMEDATE
         CALL    OUTA
         CALL    GHTIME                ; GET NEW TIMEDATE FROM HOST
         JMP     MAINZ

; SW AUTHORIZATION
MAIN57A: CJNE    A,#'x',MAIN57B        ; FMS ARM COMMAND ALLOWS SSKEY
         CALL    OUTA
         MOV     DPTR,#RSTARM
         MOV     A,#1                  ; NOTE NOT ASCII
         MOVX    @DPTR,A
         JMP     M12X1                 ; SENDPIPE + JMP MAINZ

MAIN57B: CJNE    A,#'y',MAIN57C        ; FMS DISARM COMMAND DISALLOWS SSKEY
         CALL    OUTA
         MOV     DPTR,#RSTARM
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     M12X1                 ; SENDPIPE + JMP MAINZ

MAIN57C: CJNE    A,#'z',MAIN57D        ; HOST REQUESTING HEADER AND FOOTER TEXT
         CALL    OUTA
         CALL    DUMPHF
         JMP     M27D                  ; SENDPIPE + JMP MAIN4

MAIN57D: CJNE    A,#'!',MAIN57E       ; E200E0 = GET ALL STORED TRANSACTIONS AND SEND TO HOST
;         CALL    OUTA                ; NO ECHO FOR EXCEL SS
         CALL    DALL                 ; DUMP ALL TRANSACTIONS
         JMP     MAINZ

MAIN57E: CJNE    A,#'@',MAIN57F       ; E200E0 = GET LAST STORED TRANSACTION AND SEND TO HOST
;         CALL    OUTA
         CALL    OUTLAST
         JMP     MAINZ

MAIN57F: CJNE    A,#'#',MAIN63B       ; E200E0 = INITIALIZE NVRAM
         CALL    OUTA                 ; NEED '##' TO MAKE IT HAPPEN

         MOV     R1,#0
         MOV     R2,#2                 ; 2*256 IS ABOUT 15 MS
MAIN57E1:CALL    INPA                  ; WAIT FOR CHAR #2
         JC      MAIN57E2              ; GOT CHAR2, JUMP TO CH3
         DJNZ    R1,MAIN57E1
         DJNZ    R2,MAIN57E1           ; LOOK FOR R2*256 (SINCE R1=0, 0-1=256) TIMES

MAIN57EX:MOV     A,#'X'                ; TELL HOST TIMEOUT/INVALID WAITING FOR CHAR
         CALL    OUTA
         JMP     M27D                  ; SENDPIPE + JMP MAIN4

MAIN57E2:CJNE    A,#'#',MAIN57EX
         CALL    OUTA
         CALL    NVRINIT
         JMP     M27D

         MOV     R1,#0
         MOV     R2,#2                 ; 2*256 IS ABOUT 15 MS
MAIN57E3:CALL    INPA                  ; WAIT FOR CHAR #2
         JC      MAIN57E4              ; GOT CHAR2, JUMP TO CH3
         DJNZ    R1,MAIN57E3
         DJNZ    R2,MAIN57E3           ; LOOK FOR R2*256 (SINCE R1=0, 0-1=256) TIMES
         JMP     MAIN57EX

; <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
MAIN57E4:CJNE    A,#'#',MAIN57EX
         CALL    OUTA

		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     DATAOUT,#0x10			; SETB DATAOUT
         CALL    WAIT3
         CALL    WAIT3
         ANL     DATAOUT,#0xEF			; CLR DATAOUT
		 MOV     SFRPAGE,#00H          ;Switch SFR page

         MOV     DPTR,#MSG31
         CALL    SHOW
         MOV     R1,#35
MAIN57E5:CALL    WAIT1000
         MOV     A,#'.'
         CALL    OUTA
         DJNZ    R1,MAIN57E5
         CALL    SENDPIPE
         JMP     MAINZ

; NOTE: THE SETHCMDFLG IS INTERPRETED BY THE PRINTING FUNCTIONS FOR THE DELVIERY TICKET AS THE PASS THROUGH PRINTING
; WE'RE GOING TO HAVE TO USE A NEW VARIABLE TO SEND TICKET TEXT TO HOST INSTEAD OF TO PRINTER
; SKIPPING THIS FOR NOW
;MAIN63A: CJNE    A,#'$',MAIN63B        ; E180+ HOST REQUESTING PREVIOUS DELIVERY TICKET TEXT
;         CALL    OUTA
;         CALL    SETHCMDFLG
;         JMP     DEL36B                ; PRINT DELIVERY, EVENTUALLY JUMPS TO MAIN61X, WORKS LIKE CAL & SHIFT RPT CMDS

MAIN63B: CJNE    A,#'%',MAIN63C        ; E180+ HOST REQUESTING MULTIPOINT CAL DATA
         CALL    OUTA
         CALL    SENDMPCDAT
         JMP     MAINZ                 ; PIPE ALREADY SENT

MAIN63C: CJNE    A,#'^',NOCMDMAIN      ; E180+ HOST SENDING MULTIPOINT CAL DATA
         CALL    OUTA
         CALL    GETMPCDAT
         JMP     MAINZ                 ; PIPE ALREADY SENT


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
NOCMDMAIN:                             ; NOMORE COMMAND CHARS TO TEST, IF HOSTFIX ON
         MOV     R0,A                  ; SAVE COMMAND CHAR FOR REPORTING IF HOSTFIX ON
         CALL    ISHOSTFIXON           ; SEE IF DOING HOSTFIX
         JNC     MAIN10                ; NC = SKIP HOST FIX
         CLR     C                     ; IGNORE CHAR IN A
         JMP     MAIN4N

MAIN10:
         CALL    CHKPND
         JNC     MAIN10Z

         MOV     DPTR,#FLSHLGFLG       ; IF HOST DISPLAY OFF TURN IT ON
         MOVX    A,@DPTR
         CJNE    A,#0,MAIN10Z
         MOV     A,#1
         MOVX    @DPTR,A               ; TURN HOST DISPLAY ON TO TELL DRIVER THAT WE ARE WAITING FOR X CMD

         CALL    LF0000
         MOV     DPTR,#MSG93           ; 'HOST' = WAITING FOR HOST TO SEND X COMMAND..
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD

MAIN10Z:
         MOV     DPTR,#BROKNVLV        ; BROKEN VALVE DETECTION SETTING ADDED IN 178, 0=OFF(DO NOT CHECK), 1=ON(CHECK)
         MOVX    A,@DPTR
         CJNE    A,#1,DEL10A           ; IF NOT 1 THEN NEED TO SKIP BROKEN VALVE DETECTION

         CALL    CHKPND
         JC      DEL10A
         //Since P4 is not bit addressable, we have to use TEMPBIT to address bit
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         MOV     TEMPBIT, P4
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         JNB     TEMPBIT.2,DEL10A      ; ;P4.2 is EMPULSE
         CALL    CHKCTR
         MOV     A,B
         CJNE    A,#15,DEL10A          ; UPPED BROKEN VALVE PULSES

         CALL    RTIME                 ; CALL 1 = BROKEN VALVE DETECTION START
         MOV     DPTR,#STTIME          ; SAVE CURRENT DATETIME
         CALL    SAVTIME
         MOV     DPTR,#FTIME           ; SET FINISH SAME AS START
         CALL    SAVTIME

         CALL    CLRTMT                ; CLEAR 3 MINUTE TIMER
         CALL    CLR10U                ; CLEAR 10 UNIT FLAG (TEN)
         MOV     DPTR,#STOPFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; FLAG DELIVER NOT STOPPED
         MOV     DPTR,#FIRST
         MOV     A,#0
         MOVX    @DPTR,A

         CALL    BUMPTKT
         CALL    LATCH
         JMP     DEL99                 ; AUTO START IF STRAY PULSE


DEL10A:
         CALL    CHKHPC                ; CHECK HOSE PACK COUNTER

         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JNC     MAIN102

         MOV     A,#1
         MOV     DPTR,#CALFLG
         MOVX    @DPTR,A               ; SET FLAG TO ALLOW CAL LOADS
         MOV     DPTR,#TTLZRFLG
         MOVX    @DPTR,A               ; SET FLAG TO ALLOW TOTALIZER UPDATES
         CALL    CLROUT
         CALL    CLRDEL                ; START CLEAN GOING IN
         JMP     PROVE                 ; TO AVOID COUNT LOCKUP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
MAIN102:
         CALL    KPRESS
         JC      MAIN19
         JMP     MAIN4N

MAIN19:  CJNE    A,#SSKEY,MAIN28

MAIN109:

         MOV     DPTR,#SWAUTHFLG       ; CHECK SOFTWARE AUTHORIZATION FLAG FIRST
         MOVX    A,@DPTR
         CJNE    A,#1,MAIN109AA        ; IF TURNED OFF THEN CHECK SSRST

         MOV     DPTR,#RSTARM          ; IF SW AUTH ON THEN SEE IF ARMED
         MOVX    A,@DPTR
         CJNE    A,#0,MAIN109AA        ; IF ARMED CHECK SSRST

         MOV     DPTR,#MSG213          ; SOFTWARE AUTHORIZATION (FLG)
         CALL    SHOW
         MOV     DPTR,#MSG24           ; OFF
         CALL    SHOW
         JMP     MAINX                 ; ON BUT NOT ARMED GET OUT

MAIN109AA:
         MOV     DPTR,#CMDFLAG+7       ; BYTE7(0-9) = HOST OVERRIDE SSRST SETTING
         MOVX    A,@DPTR
         CJNE    A,#'0',MAIN109A0      ; E179_A: LET SSRST OVERRIDE FROM HOST
         JMP     MAIN109A1             ; ITS 0 = NOT OVERRIDEN, CHECK SETTING
MAIN109A0:
         CJNE    A,#'1',MAIN110        ; SSRST HOST OVERRIDE: 0=USE SSRSTFLG, 1=OFF, 2=ON, 3=NSSOHM:NO S/S TO RE-OPEN VALVES IN HOST MODE
         JMP     MAIN109B              ; HOST SAYS NO
MAIN109A1:
         MOV     DPTR,#SSRSTFLG        ; SSRST: 0=ON, 1=OFF, 2=NSSOHM:NO S/S TO OPEN VALVES IN HOST MODE
         MOVX    A,@DPTR
         CJNE    A,#1,MAIN110
MAIN109B:
         MOV     DPTR,#MSG117          ; 'SS KEY' ...  0=OFF, S/S RESET NOT ALLOWED
         CALL    SHOW
         MOV     DPTR,#MSG24           ; OFF
         CALL    SHOW
         JMP     MAINX

MAIN110: MOV     DPTR,#RETURN
         MOVX    A,@DPTR
         JZ      MAIN40                ; CONTINUE TO PRINTER CHECK
         JMP     MAIN4N                ; IGNORE = HOST MODE (DISALLOW IF RETURN WITH HOST)

MAIN40:  MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#3,MAIN40A          ; IF NOT 'NO PRINTER' THEN GOTO TEST NEXT
         JMP     MAIN6                 ; NO PRINTER, GO TO DELIVERY
MAIN40A: CJNE    A,#12,MAIN9           ; IF NOT 'PRINTEK RT43 BT' THEN GOTO TEST TICKET
         JMP     MAIN6                 ; 'PRINTEK RT43 BT' MIGHT BE OFF AND CAN'T TEST FOR PAPER ANYWAY, SO SKIP TO DELIVERY, WILL FORCE THEM TO WAKE IT UP TO PRINT WHEN DONE

MAIN28:  CJNE    A,#PRINTKEY,MAIN45    ; PRINT 1
; 01/15/2014 PPBTNENBL: FLAG FOR PRESET+PRINT BUTTONS ENABLED: 0=BOTH(00), 1=PRINT ONLY(01), 2=PRESET ONLY(10), 3=NEITHER(11)
;E300EA - switched logic so 0 = enabled so defaults work better JC
         MOV     DPTR,#PPBTNENBL       ; CHECK IF PRINT BUTTON ENABLED
         MOVX    A,@DPTR
         ANL     A,#02H                ; AND WITH 2, IF ZERO SET THEN PRINT DISABLED
         JNZ      MAIN28A               ; PRINT DISABLED, IGNORE AND JUMP TO END OF KEY-CHECKING ... MAIN45->MAIN1->MAIN5->MAIN11A->MAIN11
         JMP     MAIN46                ; PRINT ENABLED, DO IT
MAIN28A: JMP     MAIN11                ; (LONG JUMP)
MAIN45:  JMP     MAIN1                 ; JUMP TO NEXT KEY TO CHECK
MAIN46:  MOV     DPTR,#RETURN
         MOVX    A,@DPTR
         JZ      M28A                  ; DISALLOW IF RETURN WITH HOST
         JMP     MAIN4N                ; WAS JUMPING TO MAIN41, BUT WE'RE ALREADY SHOWING HOST SO JUST BAIL
M28A:    CALL    CHKTKT                ; CALL #2: PRINT DUPLICATE SINCE TICKET READY ON <PRINT> PRESS
         JC      MAIN29
         CALL    SELECTP0
         MOV     A,#0FFH
         CALL    OUTA                  ; SET PCM NOT BUSY
         JMP     MAIN                  ; GET THE HELL OUT

MAIN29:
         MOV     DPTR,#DUPFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG DUPLICATE MESSAGE

         MOV     DPTR,#DUPTFLG
         MOVX    A,@DPTR
         JZ      MAIN29A
         MOV     DPTR,#TORFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; SET TIMER OVERRIDE FLAG CORRECTLY

; I DON'T THINK THIS NEEDS TO BE CHANGED/SET HERE ..
;         MOV     DPTR,#ETFLGBIN
;         MOV     A,#1
;         MOVX    @DPTR,A               ; SET ELAPSED TIMER FLAG CORRECTLY

MAIN29A:
         JMP     PRTFINAL              ; CALL #2: PRTFINAL JUMPS TO MAIN
                                       ; THIS IS <PRINT> BEING USED TO PRINT DUPLICATE
                                       ; DON'T WORRY ABOUT SLSMODE

MAIN9:
         CALL    CHKTKT                ; CALL #3: START OF DELIVERY
         JC      MAIN6                 ; CALL 3 = BEGINNING OF DELIVERY CHECK FOR TICKET
                                       ; IF THE PRINTER ISN'T READY NEED TO TELL HOST COMMAND IS COMPLETE
         CALL    FINHSTRSET            ; NEED TO FINISH HOST COMMS IF CMD STARTED BY HOST
         JMP     MAIN

FINHSTRSET:
         CALL    SELECTP0
         MOV     A,#0FFH
         CALL    OUTA                  ; SET PCM NOT BUSY
         MOV     DPTR,#RFLAG
         MOVX    A,@DPTR
         JZ      FINHSTRSX
         CALL    CON2HOST
         CALL    SELECTHST             ; ALLOW FOR COMM FROM HOST
         CALL    SENDPIPE              ; SIGNAL HOST ALL DONE
         MOV     DPTR,#RFLAG
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET RFLAG
FINHSTRSX:
         RET


MAIN6:
         CALL    LATCH

MAIN6A:  MOV     DPTR,#MSG35           ; DELIVER MESSAGE
         CALL    SHOW
         CALL    RTIME                 ; CALL 2 = NORMAL DELIVERY START
         MOV     DPTR,#STTIME          ; SAVE CURRENT DATETIME
         CALL    SAVTIME

         CALL    RTIME                 ; SET FINISH TIME TO SAME AS START TIME
         MOV     DPTR,#FTIME
         CALL    SAVTIME

         MOV     A,#0

         MOV     DPTR,#FLSHLGFLG       ; RESET HOST DISPLAY FLAG USED IN HOST MODE
         MOVX    @DPTR,A

         MOV     DPTR,#DUPTFLG
         MOVX    @DPTR,A               ; CLEAR DUPLICATE FLAG FOR TIMER OVERRIDE PRINT

         CALL    CLRTMT                ; CLEAR 3 MINUTE TIMER
         CALL    CLR10U                ; CLEAR 10 UNIT FLAG (TEN)
         MOV     DPTR,#STOPFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; FLAG DELIVER NOT STOPPED
         MOV     DPTR,#FIRST
         MOVX    @DPTR,A
         MOV     DPTR,#PSRQD
         MOVX    A,@DPTR
         JZ      MAIN42
         MOV     DPTR,#LASTPRST
         MOV     A,#0
         MOVX    @DPTR,A
         INC     DPTR
         MOVX    @DPTR,A
         INC     DPTR
         MOVX    @DPTR,A               ; CLEAR LAST PRESET IF PRESET REQUIRED
MAIN44:  MOV     DPTR,#REPRFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG TO INCLUDE FIXED CHARGES

         CALL    GETPRST
         MOV     A,#0
         ORL     A,PRESET
         ORL     A,PRESET+1
         ORL     A,PRESET+2
         JZ      MAIN44                ; DON'T ALLOW ZERO PRESET

MAIN42:  CALL    BUMPTKT
         JMP     DEL9                  ; S/S GOES RIGHT TO DELIVERY

MAIN1A:  JMP     MAIN11                ; (LONG JUMP)

MAIN1:
         CJNE    A,#PRSETKEY,MAIN5     ; PRESET KEY 1
; 01/15/2014 PPBTNENBL: FLAG FOR PRESET+PRINT BUTTONS ENABLED: 0=BOTH(00), 1=PRINT ONLY(01), 2=PRESET ONLY(10), 3=NEITHER(11)
;E300EA - switched logic so 0 = enabled so defaults work better JC
         MOV     DPTR,#PPBTNENBL       ; CHECK IF PRESET BUTTON ENABLED
         MOVX    A,@DPTR
         ANL     A,#01H                ; AND WITH 1, IF ZERO SET THEN PRESET DISABLED
         JNZ      MAIN1A                ; PRESET DISABLED, IGNORE AND JUMP TO END OF KEY-CHECKING ... MAIN5->MAIN11A->MAIN11

M3B:     MOV     DPTR,#PSRQD           ; PRESET KEY, PRE-DELIVERY
         MOVX    A,@DPTR
         JZ      MAIN43
         JMP     MAINZ
MAIN43:  MOV     DPTR,#RETURN
         MOVX    A,@DPTR
         JZ      M3A
         JMP     MAIN4N                ; IGNORE = HOST MODE
M3A:     MOV     DPTR,#MSG3
         CALL    SHOW                  ; PRESET MESSAGE
         MOV     DPTR,#REPRFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG TO INCLUDE FIXED
         CALL    GETPRST
         JMP     MAINZ

MAIN5:   CJNE    A,#MODEKEY,MAIN11A
         MOV     DPTR,#RETURN
         MOVX    A,@DPTR
         JZ      M5A
         CALL    WAIT3000
         CALL    KPRESS
         MOV     A,KEY
         JZ      MAIN11                ; HOLD DOWN MODE TO PRINT AFTER 3 SEC

         MOV     DPTR,#STATUS+1        ; UPDATE T-STATUS1 BIT 2
         MOVX    A,@DPTR
         ORL     A,#02H
         MOVX    @DPTR,A               ; FLAG MODE KEY HELD 3 SECS TO BAIL FROM HOST MODE

         MOV     DPTR,#KILLPIPE
         MOV     A,#1
         MOVX    @DPTR,A

         JMP     PRTFINAL              ; CALL #3: PRTFINAL JUMPS TO MAIN
                                       ; THIS IS HOST MODE EVERRIDE = PRESS <MODE> WHILE WAITING FOR X
                                       ; DON'T WORRY ABOUT SLSMODE



M5A:     JMP     DELMODE               ; PRCOD,SHTOT,DRIVER,SETUP

MAIN11A: CJNE    A,#RIGHTKEY,MAIN11
         MOV     DPTR,#MONEYFLG
         MOVX    A,@DPTR               ; DON'T ALLOW REPRICING IF
         JZ      MAIN11                ; MONEY=0=OFF(1=PRICE CHK,2=ALWAYS)
         MOV     DPTR,#HOST            ; NOT IN HOST MODE
         MOVX    A,@DPTR               ; IN HOST BUT NO TICKET PENDING
         JZ      MAIN11
         MOV     DPTR,#TKTPND
         MOVX    A,@DPTR
         JZ      MAIN11
         MOV     DPTR,#MSG78
         CALL    SHOW
         CALL    GETPCPR
         CALL    CONV2BCD
         CALL    GETPCE
         CALL    LASTVOL

MAIN11:  JMP     MAIN4N

; ***************************************************
; SUBROUTINES FROM HERE DOWN.....SUBS
; ***************************************************
; NEXT IS NVRAM STUFF. TEMO IS ONLY 1 BYTE BUT OK TO
; USE A EW MORE. WILL OVERWRIT COMMAND,DIGIT1,ETC.

; CLEARS AND INITILIZES EEPROM(S)
NVRINIT:
          MOV    A,#'1'
          CALL   OUTA
          MOV    WRITE,#WBANK1
          CALL   ALRMEM
          CALL   WAIT500

          MOV    A,#'2'
          CALL   OUTA
          MOV    WRITE,#WBANK2
          CALL   ALRMEM
          CALL   WAIT500

          MOV    A,#'3'
          CALL   OUTA
          MOV    WRITE,#WBANK3
          CALL   ALRMEM
          CALL   WAIT500

         CLR   WP
         MOV   TEMP,#00H    ; INIT FIRST TRANSACTION ADDRESS ONE TIME
         MOV   TEMP+1,#10H  ; LOCATION WILL RETAIN NEXT ADDR TO WRITE
         MOV   TEMP+2,#01H  ; BANK
         MOV   R1,#TEMP
         MOV   R2,#3
         MOV   DPH,#00H
         MOV   DPL,#00H
         MOV   WRITE,#WBANK1
         CALL  TMP2MEM1

         MOV   TEMP,#00H    ; INIT FIRST TRANSACTION ADDRESS ONE TIME
         MOV   TEMP+1,#10H  ; LOCATION WILL HOLD LAST TRANSACTION ADDR
         MOV   TEMP+2,#01H  ; NV BANK
         MOV   R1,#TEMP
         MOV   R2,#3
         MOV   DPH,#00H
         MOV   DPL,#03H
         MOV   WRITE,#WBANK1
         CALL  TMP2MEM1

         SETB  WP
         RET


ALRMEM:
          CLR    WP
          MOV    DPH,#00H   ; FROM ZERO
          MOV    DPL,#00H


ALRMEM2:
          MOV    R0,#2     ; 2X 256 PAGES=512
          MOV    R1,#0     ; 256 PAGES
ALRMEM4:  MOV    R2,#128   ; 128 BYTES/PAGE
          CALL   ESTART
          MOV    A,WRITE
          CALL   SHOUT
          MOV    A,DPH     ; ADDRESS HIGH
          CALL   SHOUT
          MOV    A,DPL     ; ADDRESS LOW
          CALL   SHOUT
ALRMEM3:  MOV    A,#'0'      ; DATA
          CALL   SHOUT
          INC    DPTR     ; <<<<<
          DJNZ   R2,ALRMEM3
          CALL   EESTOP
ALRMEM1:  CALL   ESTART
          MOV    A,WRITE
          CALL   SHOUT
          JC     ALRMEM1   ; CARRY SET WHEN DONE WRITING
          DJNZ   R1,ALRMEM4
          DJNZ   R0,ALRMEM4

          SETB   WP
          RET


OUTLAST:

          MOV     WRITE,#WBANK1
          MOV     READ,#RBANK1
          MOV     DPH,#00H
          MOV     DPL,#03H       ; LAST TRANS ADDRESS
          MOV     R1,#TEMP
          MOV     R2,#2          ; 2 BYTES
          CALL    MEM2TMP1
          MOV     DPH,TEMP
          MOV     DPL,TEMP+1     ; NOW HAVE ADDRESS AND BANK OF LAST
          MOV     A,DPH
;          CALL    OUTA
          MOV     A,DPL
;          CALL    OUTA
          CALL    GETZANK        ; DPTR PUSHED AND POPPED THERE

          MOV     R3,#100
OUTL1:    MOV     R2,#1
          MOV     R1,#TEMP
          CALL    MEM2TMP1       ; 100 BYTES
          MOV     A,TEMP
          CALL    OUTA
          INC     DPTR
          DJNZ    R3,OUTL1

		  MOV     TEMP,#'|'		 ; //SEND '|' to indicate end of transmission
          MOV     A,TEMP
          CALL    OUTA
          RET



; USE GETBANK TO GET BANK OF NEXT TRANSACTION
GETBANK:
         PUSH    DPH
         PUSH    DPL
         MOV     R1,#BANK
         MOV     R2,#1
         MOV     DPH,#00H
         MOV     DPL,#02H        ; BANK ADDRESS
         MOV     READ,#RBANK1
         MOV     WRITE,#WBANK1
         CALL    MEM2TMP1

         MOV     A,BANK           ; CHECK FOR 1,2,3
         CJNE    A,#1,GB1
         MOV     WRITE,#WBANK1    ; LOAD READ/WRITE WITH NV ADDRESS
         MOV     READ,#RBANK1
         JMP     GBDONE
GB1:     CJNE    A,#2,GB2
         MOV     WRITE,#WBANK2
         MOV     READ,#RBANK2
         JMP     GBDONE
GB2:     CJNE    A,#3,GB3         ; SHOULD BE 1,2,3 BUT IF NOT
         MOV     WRITE,#WBANK3    ; WILL DEFAULT BACK TO 1 SO
         MOV     READ,#RBANK3     ; WE HAVE A VALID BANK#
         JMP     GBDONE
GB3:     MOV     WRITE,#WBANK1
         MOV     READ,#RBANK1
GBDONE:
         POP     DPL
         POP     DPH
         RET

; USE GETZANK FOR BANK OF LAST TRANSACTION
GETZANK:
         PUSH    DPH
         PUSH    DPL
         MOV     R1,#BANK
         MOV     R2,#1
         MOV     DPH,#00H
         MOV     DPL,#05H        ; BANK ADDRESS FOR LAST TRANS
         MOV     READ,#RBANK1
         MOV     WRITE,#WBANK1
         CALL    MEM2TMP1

         MOV     A,BANK           ; CHECK FOR 1,2,3
         CJNE    A,#1,GZ1
         MOV     WRITE,#WBANK1    ; LOAD READ/WRITE WITH NV ADDRESS
         MOV     READ,#RBANK1
         JMP     GZDONE
GZ1:     CJNE    A,#2,GZ2
         MOV     WRITE,#WBANK2
         MOV     READ,#RBANK2
         JMP     GZDONE
GZ2:     CJNE    A,#3,GZ3         ; SHOULD BE 1,2,3 BUT IF NOT
         MOV     WRITE,#WBANK3    ; WILL DEFAULT BACK TO 1 SO
         MOV     READ,#RBANK3     ; WE HAVE A VALID BANK#
         JMP     GZDONE
GZ3:     MOV     WRITE,#WBANK1
         MOV     READ,#RBANK1
GZDONE:
         POP     DPL
         POP     DPH
         RET



; *****************************************************
; DATA COMES FROM @R1. ADDRESS INCREMENTED EACH BYTE.
; ONE WRITE CYCLE PER BYTE.

TMP2MEM:
          CLR    WP
          MOV    R2,#100    ; 100 BYTES
          MOV    R1,#TEMP

TMP2MEM1:
          CLR    WP
          CALL   ESTART
          MOV    A,WRITE
          CALL   SHOUT
TMP2MEM3: MOV    A,DPH     ; ADDRESS HIGH
          CALL   SHOUT
          MOV    A,DPL     ; ADDRESS LOW
          CALL   SHOUT
          MOV    A,@R1     ; DATA
          CALL   SHOUT
          CALL   EESTOP


TMP2MEM2: CALL   ESTART
          MOV    A,WRITE
          CALL   SHOUT
          JC     TMP2MEM2   ; CARRY SET WHEN DONE WRITING

          INC    DPTR
          INC    R1
          DJNZ   R2,TMP2MEM1
TMP2MEM5: SETB   WP
          RET

; ********************************************************
; DAATA RETURNED IN @R1

MEM2TMP:
        MOV     R2,#100
        MOV     R1,#TEMP
MEM2TMP1:
        CALL    ESTART
        MOV     A,WRITE       ; DUMMY WRITE
        CALL    SHOUT
        MOV     A,DPH
        CALL    SHOUT
        MOV     A,DPL
        CALL    SHOUT
        CALL    ESTART
        MOV     A,READ
        CALL    SHOUT


READ1:  CALL    SHIN
        CALL    ACK
        MOV     @R1,A
        INC     R1
        DJNZ    R2,READ1
        CALL    SHIN
        CALL    NAK
        CALL    EESTOP
        RET


DALL:
;         CALL    SELECTP3

          MOV     DPH,#00H
          MOV     DPL,#10H
DALL5:    MOV     READ,#RBANK1
          MOV     WRITE,#WBANK1
          MOV     R2,#1
          MOV     R1,#TEMP
          CALL    MEM2TMP1
          MOV     A,TEMP
          CALL    OUTA
          INC     DPTR
          MOV     A,DPH
          CJNE    A,#0FFH,DALL5  ; GET NEXT 100


          MOV     DPH,#00H
          MOV     DPL,#10H
DALL6:    MOV     READ,#RBANK2
          MOV     WRITE,#WBANK2
          MOV     R2,#1
          MOV     R1,#TEMP
          CALL    MEM2TMP1
          MOV     A,TEMP
          CALL    OUTA
          INC     DPTR
          MOV     A,DPH
          CJNE    A,#0FFH,DALL6

          MOV     DPH,#00H
          MOV     DPL,#10H
DALL7:    MOV     READ,#RBANK3
          MOV     WRITE,#WBANK3
          MOV     R2,#1
          MOV     R1,#TEMP
          CALL    MEM2TMP1
          MOV     A,TEMP
          CALL    OUTA
          INC     DPTR
          MOV     A,DPH
          CJNE    A,#0FFH,DALL7

DALL8:    MOV     TEMP,#'|'
          MOV     A,TEMP
          CALL    OUTA
          RET


TDATOUT:
 ;        CALL    CON2HOST
 ;        CALL    SELECTHSTS

         CALL    CONV2ASC              ; SEND DELIVERY DATA TO HOST
         CALL    SELECTP3

ST3:      MOV   DPTRSAV,#0
          MOV   DPTRSAV+1,#0
          MOV   DPTRSAV+2,#0


          MOV   DPH,#00H        ; GET NEXT WRITE ADDR.
          MOV   DPL,#00H        ; AND SAVE FOR LAST TRANS OUTPUT.
          MOV   R2,#3           ; GET 3 BYTES WITH BANK#
          MOV   R1,#DPTRSAV
          MOV   READ,#RBANK1
          MOV   WRITE,#WBANK1
          CALL  MEM2TMP1

          MOV   DPH,#00H        ; PUT IT IN SAVE ADDR
          MOV   DPL,#03H        ; AND SAVE FOR LAST TRANS OUTPUT.
          MOV   R2,#3           ; GET 3 BYTES WITH BANK#
          MOV   R1,#DPTRSAV
          MOV   READ,#RBANK1
          MOV   WRITE,#WBANK1
          CALL  TMP2MEM1


         MOV     DPTR,#STORFLG        ; SEE IF COMING IN FROM TNKNUM:
         MOVX    A,@DPTR
         JZ      TDAT1                ; FIGURE OUT LATER

         CALL    OVWLAST              ; BACKS UP ONE TRANSACTION ADDRESS.   FOOLS STORE:


TDAT1:
         MOV     DPTR,#SDATFIX
         MOV     R6,DPL
         MOV     R7,DPH
         MOV     DPTR,#STIMFIX
         MOV     R4,DPL
         MOV     R5,DPH
         MOV     DPTR,#ASTIME
         CALL    FIXDT                ; REFORMAT DATETIME FOR EXCEL DUMP

         MOV     DPTR,#FDATFIX
         MOV     R6,DPL
         MOV     R7,DPH
         MOV     DPTR,#FTIMFIX
         MOV     R4,DPL
         MOV     R5,DPH
         MOV     DPTR,#AFTIME
         CALL    FIXDT

         MOV     DPTR,#ATANKID
         MOV     R1,#6
         CALL    SEND
;         MOV     DPTR,#ASTIME
;         MOV     R1,#10
;         CALL    SEND
;         MOV     DPTR,#AFTIME
;         MOV     R1,#10
;         CALL    SEND

         MOV      DPTR,#SDATFIX
         MOV      R1,#8
         CALL     SEND
         MOV      DPTR,#STIMFIX
         MOV      R1,#4
         CALL     SEND
         MOV      DPTR,#FDATFIX
         MOV      R1,#8
         CALL     SEND
         MOV      DPTR,#FTIMFIX
         MOV      R1,#4
         CALL     SEND

         MOV     DPTR,#APRCOD
         MOV     R1,#2
         CALL    SEND
         MOV     DPTR,#ATRUCK
         MOV     R1,#4
         CALL    SEND
         MOV     DPTR,#ADRIVER
         MOV     R1,#4
         CALL    SEND
         MOV     DPTR,#ATKTNO
         MOV     R1,#6
         CALL    SEND
         MOV     DPTR,#ANVOL
         MOV     R1,#8
         CALL    SEND
         MOV     DPTR,#AGVOL
         MOV     R1,#8
         CALL    SEND
         MOV     DPTR,#ATNVOL
         MOV     R1,#8
         CALL    SEND
         MOV     DPTR,#ATGVOL
         MOV     R1,#8
         CALL    SEND
         MOV     DPTR,#ACOMPST
         MOV     R1,#1
         CALL    SEND
;         MOV     DPTR,#STATUS
;         MOV     R1,#3
;         CALL    SEND
         CALL    SENDP
         RET

FIXDT:
         PUSH    DPH
         PUSH    DPL
         MOV     R1,#8
         MOV     R0,#ECGALS
FIXD2:   MOVX    A,@DPTR
         MOV     @R0,A
         INC     DPTR
         INC     R0
         DJNZ    R1,FIXD2       ; GET IT TO RAM

         MOV     DATEIMG,#'2'
         MOV     DATEIMG+1,#'0'
         MOV     DATEIMG+2,ECGALS+4
         MOV     DATEIMG+3,ECGALS+5
         MOV     DATEIMG+4,ECGALS
         MOV     DATEIMG+5,ECGALS+1
         MOV     DATEIMG+6,ECGALS+2
         MOV     DATEIMG+7,ECGALS+3

         MOV     R1,#8
         MOV     R0,#DATEIMG
         MOV     DPL,R6
         MOV     DPH,R7
FIXD1:   MOV     A,@R0
         MOVX    @DPTR,A
         INC     DPTR
         INC     R0
         DJNZ    R1,FIXD1       ; PUT BACK IN NEW VARIALBE

         POP     DPL
         POP     DPH            ; BACK TO DATETIME

         MOV     R1,#6
FIXD5:   INC     DPTR
         DJNZ    R1,FIXD5      ; SHIFT OVER TO TIME

         MOV     R1,#4
         MOV     R0,#DATEIMG
FIXD3:   MOVX    A,@DPTR
         MOV     @R0,A
         INC     DPTR
         INC     R0
         DJNZ    R1,FIXD3

         MOV     R1,#4
         MOV     R0,#DATEIMG
         MOV     DPL,R4
         MOV     DPH,R5
FIXD4:   MOV     A,@R0
         MOVX    @DPTR,A
         INC     DPTR
         INC     R0
         DJNZ    R1,FIXD4
         RET


OVWLAST:                         ; OVERWRITE LAST WITH TANKID FIELD POPULATED
                                 ; FOOL INDATA BY SWAPPING NEXT WITH LAST ADDRESS
          MOV     WRITE,#WBANK1
          MOV     READ,#RBANK1
          MOV     DPH,#00H
          MOV     DPL,#03H
          MOV     R1,#TEMP
          MOV     R2,#3          ; DPH,DPL,BANK
          CALL    MEM2TMP1

          MOV     WRITE,#WBANK1  ; SHOULD REALLY BE UNCHANGED
          MOV     READ,#RBANK1
          MOV     DPH,#00H
          MOV     DPL,#00H
          MOV     R1,#TEMP
          MOV     R2,#3
          CALL    TMP2MEM1      ; UNDOES WHAT HAPPENS IN STORE:
          RET

OUTVAR:   RET
          MOV   PRESET,#0
          MOV   PRESET+1,#0
          MOV   PRESET+2,#0

          MOV   A,DPH
          CALL  OUTA
          MOV   A,DPL
          CALL  OUTA

          MOV   DPH,#00H        ; GET NEXT WRITE ADDR.
          MOV   DPL,#00H        ; AND SAVE FOR LAST TRANS OUTPUT.
          MOV   R2,#3           ; GET 3 BYTES WITH BANK#
          MOV   R1,#PRESET
          MOV   READ,#RBANK1
          MOV   WRITE,#WBANK1
          CALL  MEM2TMP1

          MOV   A,PRESET
          CALL  OUTA
          MOV   A,PRESET+1
          CALL  OUTA
          MOV   A,PRESET+1
          CALL  OUTA

          MOV   A,BANK
          CALL  OUTA
          RET

STORE:
          MOV   DPH,#00H        ; GET NEXT WRITE ADDR.
          MOV   DPL,#00H
          MOV   R2,#2           ; GET 2 BYTES
          MOV   R1,#DPTRSAV
          MOV   READ,#RBANK1
          MOV   WRITE,#WBANK1
          CALL  MEM2TMP1
          MOV   DPH,DPTRSAV     ; AND PUT IT BACK IN DPTR
          MOV   DPL,DPTRSAV+1

          CALL  GETBANK
          MOV   R1,#TEMP        ; START OF T DATA
          MOV   R2,RES          ; 100 CHARS TO STORE WAS R1
          MOV   A,RES
          CALL  TMP2MEM1        ; PUT IT AWAY

          MOV   A,DPH
          CJNE  A,#0FFH,TDATA14  ; END OF BANK MINUS 256 OR LESS BYTES
          MOV   DPH,#00H
          MOV   DPL,#10H        ; START OVER FROM TOP WASTING A FEW BYTES IN DPL
          MOV   A,BANK
          CJNE  A,#3,TDATA15    ; INCREMENT DPTR OR IF BANK=3 MAKE A 1
          MOV   BANK,#1
          JMP   TDATA14
TDATA15:  INC   BANK


TDATA14:  MOV   TEMP,DPH
          MOV   TEMP+1,DPL    ; TEMP STORE DPTR

          MOV   DPH,#00H
          MOV   DPL,#00H        ; ADDRESSS OF LAST TRANSACTION
          MOV   R1,#TEMP
          MOV   R2,#2
          MOV   READ,#RBANK1
          MOV   WRITE,#WBANK1
          CALL  TMP2MEM1        ; PUT IT AWAY

          CALL  SAVBANK
          RET

SAVBANK: CLR   WP               ; EACH TIME BACK TO MAIN STORE BANK# IN NVRAM
         MOV   R1,#BANK
         MOV   R2,#1
         MOV   DPH,#00H
         MOV   DPL,#02H
         MOV   WRITE,#WBANK1
         CALL  TMP2MEM1
         SETB  WP
         RET

; MOVES @DPTR TO TEMP. R1 HAS # OF BYTES
SEND:
         MOV     R0,#TEMP
         MOV     RES,R1       ; USE LATER
SEND1:   MOVX    A,@DPTR
         MOV     @R0,A
         INC     DPTR
         INC     R0
         DJNZ    R1,SEND1
         MOV     A,#','       ; ADD COMMA
         MOV     @R0,A        ; R0 ALREADY INCREMENTED
         INC     RES          ; STORE NEEDS ONE MORE BYTE
         CALL    STORE
         RET

SENDP:   MOV     R1,#5
         MOV     RES,R1
         MOV     A,#'*'
         MOV     R0,#TEMP
SENDP1:  MOV     @R0,A
         INC     R0
         DJNZ    R1,SENDP1
         MOV     A,#0DH      ; CR
         MOV     @R0,A
         INC     R0
         MOV     A,#0AH      ; LF
         MOV     @R0,A
         INC     RES
         INC     RES         ; BUMP TWICE FOR STORE
         CALL    STORE
         RET


SENDPIPE:
         MOV     A,#'|'
         CALL    OUTA
SPOK:    RET



; *****************************************
; ISHOSTFIXON CHECKS HOSTFIX VARIABLE
; RETURNS: CARRY SET IF HOSTFIX=MATRIX =OR= HOSTFIX=ALL
;          CARRY NOT SET IF HOSTFIX=OFF
; *****************************************
ISHOSTFIXON:
         CLR     C                     ; CLEAR CARRY
         MOV     R0,A                  ; SAVE A
         MOV     DPTR,#HOSTFIX         ; SETS CARRY IF HOST FIX ENABLED
         MOVX    A,@DPTR
         JZ      ISHOSTFN
         JMP     ISHOSTFY
ISHOSTFXALL:
         CLR     C                     ; CLEAR CARRY
         MOV     R0,A                  ; SAVE A
         MOV     DPTR,#HOSTFIX         ; SETS CARRY IF HOST FIX IS ALL
         MOVX    A,@DPTR
         CJNE    A,#02H,ISHOSTFN       ; HOSTFIX NOT ALL
         JMP     ISHOSTFY
ISHOSTFXMTX:
         CLR     C                     ; CLEAR CARRY
         MOV     R0,A                  ; SAVE A
         MOV     DPTR,#HOSTFIX         ; SETS CARRY IF HOST FIX IS MATRIX ONLY
         MOVX    A,@DPTR
         CJNE    A,#01H,ISHOSTFN       ; HOSTFIX NOT MATRIX ONLY
         JMP     ISHOSTFY
ISHOSTFY:
         SETB    C                     ; SET CARRY
ISHOSTFN:
         MOV     A,R0                  ; RESTORE A
         RET


; *******************************************************************
; DOHOSTFIX CHECKS FOR 1 TILDE CHAR FROM HOST
; AT START OF SERIAL COMMANDS DEPENDING ON 'HOSTFX' SETTING:
;  OFF    = NO TILDE REQUIRED
;  MATRIX = REQUIRED FOR B,D,F,H,L,M,S,a,b,c,h,s,t
;  ALL    = REQUIRED BEFORE ALL COMMANDS
;
; RETURNS: CARRY SET IF HOSTFIX=ON && TILDE AS REQUIRED, =OR= IF HOSTFIX=OFF, =OR= IF HOSTFIX=ON && TILE NOT REQUIRED FOR COMMAND
;          CARRY NOT SET IF HOSTFIX=ON && TILDES *NOT* OK
; *******************************************************************
DOHOSTFIX:
         MOV     R0,A                  ; SAVE A
                                       ; BYTE 2 = ERR RESULT: (TILDE TILDE SENT IF NO ERR)
                                       ;  1=CH1 NOT TILDE && TILDE REQUIRED,
                                       ;  2=TIMEOUT WAITING FOR CH2,
                                       ;  3=CH2 NOT VALID CMD WHEN DELIVERY NOT ACTIVE, ERR# IN R1, nn IN R2
                                       ;  4=CH2 NOT VALID CMD WHEN DELIVERY ACTIVE AND NO PRODUCT FLOW, ERR# IN R1, nn IN R2
                                       ;  5=CH2 NOT VALID CMD WHEN DELIVERY ACTIVE AND PRODUCT FLOWING, ERR# IN R1, nn IN R2
         CALL    ISHOSTFIXON           ; SEE IF DOING HOSTFIX
         JNC     HOSTFT0AX             ; NC = SKIP HOST FIX, JUMP TO EXIT (RETURN OK)
         MOV     A,R0                  ; RESTORE A
         CJNE    A,#'~',HOSTFT0A       ; IF CHAR1 NOT TILDE SEE IF CMD IS MATRIX-ONLY
         JMP     HOSTFT1A
HOSTFT0A:
         CALL    ISHOSTFXMTX           ; SEE IF HOSTFIX = MATRIX-ONLY
         JNC     HOSTFT0A1             ; NC = HOSTFIX=ALL, SINCE CHAR1 NOT TILDE JUMP TO EXIT (RETURN ERR 1)
                                       ; CS = SEE IF CMD IS MATRIX-ONLY
         JMP     HOSTFT0B1             ; HOSTFIX=MATRIX, TEST TO SEE IF CHAR IS MATRIX-ONLY
HOSTFT0A1:
         MOV     A,#'*'                ; TELL HOST HOSTFIX=ALL && TILDE REQUIRED FOR CMD && NOT TILDE RECEIVED
         CALL    OUTA
         JMP     DOHOSTEXN
HOSTFT0AX:
         JNC     DOHOSTEXX            ; LONG JUMP TO EXIT (RETURN OK)

;'BDFHLMSabchst'            ; HOST COMMANDS THAT REQUIRE TILDE IF HOSTFX=MATRIX
HOSTFT0B1:
         CJNE    A,#'B',HOSTFT0B2      ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B2:
         CJNE    A,#'D',HOSTFT0B3      ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B3:
         CJNE    A,#'F',HOSTFT0B4      ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B4:
         CJNE    A,#'H',HOSTFT0B5      ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B5:
         CJNE    A,#'L',HOSTFT0B6      ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B6:
         CJNE    A,#'M',HOSTFT0B7      ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B7:
         CJNE    A,#'S',HOSTFT0B8      ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B8:
         CJNE    A,#'a',HOSTFT0B9      ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B9:
         CJNE    A,#'b',HOSTFT0B10     ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B10:
         CJNE    A,#'c',HOSTFT0B11     ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B11:
         CJNE    A,#'h',HOSTFT0B12     ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)
HOSTFT0B12:
         CJNE    A,#'t',HOSTFT0BOK     ; IF CHAR IS NOT MATRIX-ONLY, TEST NEXT
         JMP     HOSTFT0BEX            ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)

HOSTFT0BOK:
         JMP     DOHOSTEXX             ; HOSTFIX=MATRIX && CMD=NOT MATRIX && TILDE NOT REQUIRED, JUMP TO EXIT (RETURN OK)
HOSTFT0BEX:
         MOV     A,#'!'                ; TELL HOST HOSTFIX=MATRIX && TILDE REQUIRED FOR CMD && NOT TILDE RECEIVED
         CALL    OUTA
         JMP     DOHOSTEXN             ; CHAR IS MATRIX-ONLY && HOSTFIX=MATRIX & && CHAR1 NOT TILDE, JUMP TO EXIT (RETURN ERR 1)

HOSTFT1A:MOV     R1,#0
         MOV     R2,#2                 ; 2*256 IS ABOUT 15 MS
HOSTFT1B:CALL    INPA                  ; WAIT FOR CHAR #2
         JC      DOHOSTEXY             ; GOT CHAR2, JUMP TO EXIT
         DJNZ    R1,HOSTFT1B
         DJNZ    R2,HOSTFT1B           ; LOOK FOR R2*256 (SINCE R1=0, 0-1=256) TIMES
; E178, THIS DASH MIGHT BE CONFUSING, DON'T SEND IT (WE'LL SEE WHO COMPLAINS)
;         MOV     A,#'-'                ; TELL HOST CHAR AFTER TILDE EXPECTED AND CHAR NOT RECEIVED
;         CALL    OUTA
         JMP     DOHOSTEXN             ; ERR: TIMEOUT WAITING FOR CHAR2
DOHOSTEXX:
         MOV     A,R0                  ; HOST FIX OFF, MAKE SURE ORIGINAL CHAR IS IN A BEFORE LEAVING
DOHOSTEXY:
         SETB    C                     ; HOST FIX ON & GOT 1 TILDE CHARS **OR** HOST FIX OFF = PROCEED (SET CARRY)
         JMP     DOHOSTEXZ             ; JUMP TO EXIT
DOHOSTEXN:
         CLR     C                     ; HOST FIX ON & GOT INVALID CHARS = DON'T PROCEED (CLEAR CARRY)
DOHOSTEXZ:
         RET



; SETS C IF V1 OR V2 IS SET
TESTV1V2:
         MOV     C,V1ST                ; CHECK VALVES CLOSED
         ORL     C,V2ND
         RET

OPNV1AUX:
         SETB    V1ST
         SETB    VAUX
         RET

OPNV2AUX:
         SETB    V2ND
         SETB    VAUX
         RET

OPNV1V2AUX:
         SETB    V1ST
         SETB    V2ND
         SETB    VAUX
         RET


CLOSVLVS:
         CLR     V1ST
         CLR     V2ND
         CLR     VAUX
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     AIRSOL,#0xF7			; CLR AIRSOL AIRSOL CLEAR = OPTIC WET
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         RET

SHWVOPEN:
         MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JNZ     SHWVOPE1              ; 1=HOST,BAIL
         MOV     DPTR,#MSG28           ; VALVES
         CALL    SHOW
         MOV     DPTR,#MSG29           ; OPEN
         CALL    SHOW
SHWVOPE1:RET

SHWVCLSD:
         MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JNZ     SHWVCLS1              ; 1=HOST,BAIL
         MOV     DPTR,#MSG28           ; VALVES
         CALL    SHOW
         MOV     DPTR,#MSG30           ; CLOSED
         CALL    SHOW
SHWVCLS1:RET


LATCH:                                 ; POWER-LATCHING FOR DELIVERY IN PCM12
         CALL    SELECTP0
         MOV     DPTR,#REGNUM          ; USES NEW COMMANDS ONLY IN PCM12
         MOVX    A,@DPTR
         CJNE    A,#01H,LATCHA
         MOV     A,#2
         CALL    OUTA
         CALL    WAIT1
         MOV     A,#1FH
         CALL    OUTA
         MOV     A,#22
         CALL    OUTA
         JMP     LATCHB
LATCHA:  MOV     A,#4
         CALL    OUTA
         CALL    WAIT1
         MOV     A,#1FH
         CALL    OUTA
         MOV     A,#24
         CALL    OUTA
LATCHB:  RET

UNLATCH:                               ; POWER-UNLATCHING FOR AFTER DELIVERY IN PCM12
         CALL    SELECTP0
         MOV     DPTR,#REGNUM          ; USES NEW COMMANDS ONLY IN PCM12
         MOVX    A,@DPTR
         CJNE    A,#01H,UNLATCHA
         MOV     A,#1
         CALL    OUTA
         CALL    WAIT1
         MOV     A,#1FH
         CALL    OUTA
         MOV     A,#21
         CALL    OUTA
         JMP     UNLATCHB
UNLATCHA:MOV     A,#3
         CALL    OUTA
         CALL    WAIT1
         MOV     A,#1FH
         CALL    OUTA
         MOV     A,#23
         CALL    OUTA
UNLATCHB:RET


CHKPND:  MOV     B,A
         CLR     C
         MOV     DPTR,#TKTPND          ; SETS CARRY IF TICKET PENDING
         MOVX    A,@DPTR
         JZ      CHKPND1
         SETB    C
CHKPND1: MOV     A,B
         RET

//CHKCTR is only called from MAIN10Z as part of broken valve. It has already deteced that there is pulses and this adds to PULSECTR and acknowledges
//Not sure about the WAIT500 between low to high of EMCOUNT as it appears to work with or without it
//Removed check of number of pulses (9/7/21) because MAIN10Z was changed to compare pulses to 15 (1.5) for both gallons and liters.
CHKCTR:
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     EMCOUNT,#0x80			; SETB EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
		 CALL    WAIT500U              ;TODO Guessing at timer value, maybe less. Old timer was DJNZ from 100
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     EMCOUNT,#0x7F			; CLR EMCOUNT RESET EM PLSOUT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
		 CALL    WAIT20U               ;Wait for EM added jc

;         CLR     PSW_F0
         MOV     DPTR,#PLSCTR
         MOVX    A,@DPTR
         ADD     A,#1                  ; BUMP PULSE COUNTER
         MOVX    @DPTR,A               ; PUT IT BACK
         MOV     B,A                   ; TEMP SAVE
;         MOV     DPTR,#UNITS           ; CHECK LITRES OR GALLONS
;         MOVX    A,@DPTR
;         JZ      CHKCT1
;         MOV     A,B
;         CJNE    A,#4,CHKCT2           ; LITRES
;         JMP     CHKCT3
;CHKCT1:  MOV     A,B
;         CJNE    A,#11,CHKCT2          ; GALLONS
;CHKCT3:  SETB    PSW_F0
CHKCT2:  RET

CHKHPC:
         CLR     C
         MOV     DPTR,#HPCTR           ; BUMP HOSE PACK COUNTER
         MOVX    A,@DPTR
         ADD     A,#1                  ; BUMP PULSE COUNTER
         MOVX    @DPTR,A               ; PUT IT BACK
         INC     DPTR
         MOVX    A,@DPTR
         ADDC    A,#0
         MOVX    @DPTR,A
         CJNE    A,#0FFH,CHKHP1			;TIMER FOR HOSE PACK, SHOULD BE AROUND 6-7 SECONDS
         CLR     V2ND
CHKHP1:  RET


CLRTMT:  MOV     DPTR,#TMT
         MOV     R1,#6
         MOV     A,#0
CLRTMT1: MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,CLRTMT1
         RET

CLR10U:  MOV     DPTR,#TEN
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR TEN UNIT FLAG
         RET


DASCOUTX4:
         CALL    DASCOUT
         CALL    DASCOUT
DASCOUTX2:
         CALL    DASCOUT
         CALL    DASCOUT
         RET

DASCOUT:
         CALL    DECDPTR
ASCOUT:  MOVX    A,@DPTR
         CALL    MAKEASC
         CALL    OUTA
         MOV     A,B
         CALL    OUTA
         RET

; DECREMENT ASCII BUILD FOR PRINT ROUTINES
DASCBLD: CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC
         INC     R0
         MOV     @R0,A
         MOV     A,B
         INC     R0
         MOV     @R0,A
         RET

ASCBLD:  MOVX    A,@DPTR
         CALL    MAKEASC
         MOV     @R0,A
         MOV     A,B
         INC     R0
         MOV     @R0,A
         RET

PRTVOL:
         MOV     DPTR,#PRMSG11         ; SALES NUMBER
         CALL    MOVMES
         MOV     DPTR,#TKTNO+2
         MOV     R0,#ECGALS+19
         CALL    ASCBLD
         CALL    DASCBLD
         CALL    DASCBLD
         CALL    PRTLINE

         MOV     DPTR,#SERPRT
         MOVX    A,@DPTR
         JZ      SERX
         RET

SERX:    MOV     DPTR,#B2FLG
         MOVX    A,@DPTR
         JNZ     SKIP3

         CALL    PFEED

SKIP3:
         CALL    CHKDEC
         JZ      DEC3                  ; DECIMAL ON
         CALL    GETUNIT

         MOV     DPTR,#PRMSG2A         ; GALLONS START NO DECIMAL
         MOV     R1,#25
         CALL    SHDP20
         JNB     PSW_F0,U24
         MOV     DPTR,#PRMSG34A        ; LITRES START NO DECIMAL
         MOV     R1,#25
         CALL    SHDP20
         JMP     U24

DEC3:    CALL    GETUNIT

         MOV     DPTR,#TRUNC
         MOVX    A,@DPTR
         JZ      DEC4
         MOV     DPTR,#PRMSG2D
         JMP     DEC5                  ; TENTHS TRUNCATED

DEC4:    MOV     DPTR,#PRMSG2          ; GALLONS START 1 DECIMAL
DEC5:    MOV     R1,#25
         CALL    SHDP20
         JNB     PSW_F0,U24
         MOV     DPTR,#PRMSG34         ; LITRES START 1 DECIMAL
         MOV     R1,#25
         CALL    SHDP20
U24:     CALL    BLAST
         MOV     R1,#25

PRTVOL1: CLR     A
         MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRTVOL1
         CALL    PLINEEND             ; START LINE

         CALL    GETUNIT
         JNB     PSW_F0,U25

         CALL    CKPGRSPGRS            ; CKPGRSPGRS: PSW_F0=SET=PRINT GROSS VOLUME
         MOV     DPTR,#PRMSG36         ; LITRES FINISH (INSTEAD OF LITRES GROSS)
         MOV     R1,#18
         CALL    SHDP20
         JNB     PSW_F0,U25A
         MOV     DPTR,#PRMSG35         ; LITRES NET
         JMP     U25A
U25:
         CALL    CKPGRSPGRS            ; CKPGRSPGRS: PSW_F0=SET=PRINT GROSS VOLUME
         MOV     DPTR,#PRMSG3A         ; GALLONS FINISH
         MOV     R1,#18
         CALL    SHDP20
         JNB     PSW_F0,U25A
         MOV     DPTR,#PRMSG3          ; GALLONS NET

U25A:    CALL    BLAST

         PUSH    DPL
         PUSH    DPH
         MOV     DPTR,#TRUNC
         MOVX    A,@DPTR
         POP     DPH
         POP     DPL
         JZ      TRUNC1
         MOV     R1,#20
         JMP     PRTVOL2

TRUNC1:  MOV     R1,#18
PRTVOL2: CLR     A
         MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRTVOL2

         MOV     DPTR,#NVOL+2
         CALL    ASCOUT
         CALL    DASCOUT
         CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC
         CALL    OUTA

         MOV     DPTR,#TRUNC
         MOVX    A,@DPTR
         JNZ     DECA6

         CALL    CHKDEC
         JNZ     DEC1
         MOV     A,#'.'
         CALL    OUTA
DEC1:    MOV     A,B
         CALL    OUTA

DECA6:   CALL    PLINEEND

         CALL    CKPGRSPGRS            ; CKPGRSPGRS: PSW_F0=CLR=OFF, PSW_F0=SET=ONE OR MORE GROSS FIELDS TO PRINT
         JNB     PSW_F0,PRTVOL6            ; PSW_F0=CLEAR = SKIP ALL GROSS PRINTING FUNCTIONS

DEC1A:                                 ; PRINT GROSS FIELD(S)
         CALL    GETUNIT
         JNB     PSW_F0,U26

         CALL    CKPGRSPGRS            ; CKPGRSPGRS: PSW_F0=CLR=OFF, PSW_F0=SET=ONE OR MORE GROSS FIELDS TO PRINT

         MOV     DPTR,#PRMSG36         ; LITRES FINISH
         MOV     R1,#18
         CALL    SHDP20
         JNB     PSW_F0,U26A
         MOV     DPTR,#PRMSG37         ; LITRES GROSS
         JMP     U26A

U26:
         CALL    CKPGRSPGRS            ; CKPGRSPGRS: PSW_F0=CLR=OFF, PSW_F0=SET=ONE OR MORE GROSS FIELDS TO PRINT

         MOV     DPTR,#PRMSG3A         ; GALLONS FINISH
         MOV     R1,#18
         CALL    SHDP20
         JNB     PSW_F0,U26A

         MOV     DPTR,#PRMSG4          ; GALLONS GROSS

U26A:    CALL    BLAST
         MOV     R1,#18
PRTVOL3: CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRTVOL3

         MOV     DPTR,#GVOL+2
         CALL    ASCOUT
         CALL    DASCOUT
         CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC

         CALL    OUTA
         CALL    CHKDEC
         JNZ     DEC8
         MOV     A,#'.'
         CALL    OUTA
DEC8:    MOV     A,B
         CALL    OUTA
         CALL    PLINEEND

;MOVED THE JUMP POINT FOR THE BLANK LINE BELOW THE VOLUME TO CHECK THE FLAG
;ALSO ADDED THE CHECK FOR 6501PF FROM THE REMOVED CALL FOR THE B3FLG IN DOPRINT,
; IF PRICING ON AND PRTGRS ON THEN WOULD GET 2 BLANKS IF B3FLG WAS ON
PRTVOL6:
         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JNZ     SKIP15                ; IF THE PR65FLG !=0 THEN WE ARE IN 6501 PRINT FORMAT & NEED TO SKIP THE BLANK BELOW THE VOLUME

         MOV     DPTR,#B3FLG           ; PRINT FLAG FOR BLANK LINE BELOW VOLUME
         MOVX    A,@DPTR
         JNZ     SKIP15

         CALL    PFEED

SKIP15:
         MOV     DPTR,#MONEYFLG
         MOVX    A,@DPTR               ; CHECK IF MONEY IS ON: 0=OFF, 1=PRICE CHK (WAS ON), 2=ALWAYS
         JZ      X26

         CALL    CONVERT
         CALL    PRICECHK
         JZ      X26
         CALL    VOLUMCHK
         JZ      X26
         MOV     DPTR,#REPRFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; MAKE SURE FIXED NOT SKIPPED FROM REPRESET
         CALL    DOPRINT

PR65K:   CALL    DODISCA
         CALL    PRTDR1

         MOV     DPTR,#B7FLG
         MOVX    A,@DPTR
         JNZ     SKIP14

         CALL    PFEED

SKIP14:
X26:
         CALL    PCMPTR                ; GET PRODUCT MESSAGE POINTER
         CALL    PRTMSG
         CALL    ADCPTR                ; ADDITIONAL LABEL CODE
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOVX    A,@DPTR               ; SHIFT RAMS AND GET LINE NUMBER
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY - FOR PRCOD
         JZ      SKIP18                ; SKIP IF ZERO
         INC     DPTR                  ; GOIN TO PRINT, GET LINES#
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOVX    A,@DPTR
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         MOV     DPTR,#ADDLNUM         ; STORE AS ADDITIONAL LINE NUMBER
         MOVX    @DPTR,A               ; DONE
         MOV     DPTR,#ADDLCTR
         MOV     A,#0
         MOVX    @DPTR,A               ; START LINE COUNTER AT 0


X26A:    CALL    ADCPTR                ; GET PRCOD POINTER AGAIN
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOVX    A,@DPTR               ; ACTUAL LINE NUMBER TO START
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         MOV     TEMP,A
         MOV     DPTR,#ADDLCTR         ; COMPUTE OFFSET FROM START LINE
         MOVX    A,@DPTR               ; GET LINE# OFFSET
         CLR     C
         ADD     A,TEMP
         CALL    ADLPTR
         CALL    BLAST
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - DPTR HAS BEGINNING LINE #
         MOV     R1,#25
PRTLBL1: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRTLBL1
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         CALL    PLINEEND

         MOV     DPTR,#ADDLCTR
         MOVX    A,@DPTR
         INC     A                     ; ADD 1 FOR LINE PRINTED
         MOVX    @DPTR,A               ; PUT IT BACK, WILL POINT TO NEXT

         MOV     DPTR,#ADDLNUM
         MOVX    A,@DPTR
         DEC     A                     ; BUMP LINE NUMBER DOWN 1
         MOVX    @DPTR,A               ; PUT IT BACK

         CJNE    A,#0,X26A

SKIP18:                                ; LOAD CORRECT COMP MSG BASED ON PRINTER

         MOV     DPTR,#PRTFLG          ; IF THE PRINTEK MOBILE RT43 BLUETOOTH IS BEING USED THE DEGREE SYMBOL IS WRONG, FIX IT
         MOVX    A,@DPTR
         CJNE    A,#12,U29A

         MOV     A,COMP
         JZ      PRTVOL4
         CALL    GETUNIT
         MOV     DPTR,#PRMSG8A         ; 60F COMP MESSAGE FOR PRINTEK RT43 PRINTER
         JNB     PSW_F0,U29Y
         MOV     DPTR,#PRMSG38A        ; VOL COR TO 15 FOR PRINTEK RT43 PRINTER
         JMP     U29Y

U29A:
         MOV     A,COMP
         JZ      PRTVOL4
         CALL    SETBLDG1FLG           ; SET THE BLASTER DEGREE SYMBOL FLAG, IT WILL BE CLEARED IN BLAST
         CALL    GETUNIT
         MOV     DPTR,#PRMSG8          ; 60F COMP MESSAGE
         JNB     PSW_F0,U29Y
         MOV     DPTR,#PRMSG38         ; VOL COR TO 15

U29Y:
         CALL    PRTCMSG
         CALL    CLRBLDGFLG            ; CLEAR THE BLASTER DEGREE SYMBOL FLAG, IT WILL BE CLEARED IN BLAST THIS IS INSURANCE

         MOV     DPTR,#ATCFLG
         MOVX    A,@DPTR
         JZ      U29Z                  ; ATC OK
         MOV     DPTR,#PRMSG48         ; ATC FAILURE
         CALL    PRTCMSG

U29Z:
         CALL    CKPGRSPGRS
         JNB     PSW_F0,PRTVOL4            ; PSW_F0=CLR IF GRSFLG=0(OFF) OR 3(TEMP)

         MOV     DPTR,#UNITS           ; CHECK LITRES OR GALLONS
         MOVX    A,@DPTR               ; ONLY PRINT TABLE INFO IF LITRES
         JZ      PRTVOL4               ; THIS IS TO PREVENT ASTM54 / TABLE 6 COMPLAINTS
                                       ;  BETTER TO PRINT NOTHING THAN PRINT 'WRONG' IF GALLONS
         CALL    PCTPTR                ; GET TABLE INFO - LINE 1
         CALL    PRTCMSG                ; PRINT TABLE INFO - LINE 1
         CALL    PCTPTR                ; GET TABLE INFO - LINE 1
         MOV     R1,#25
U29Z1:   INC     DPTR                  ; MOVE OVER 25 CHARS FOR LINE 2
         DJNZ    R1,U29Z1
         CALL    PRTCMSG                ; PRINT TABLE INFO - LINE 2

PRTVOL4: MOV     DPTR,#ETFLG
         MOVX    A,@DPTR
         JZ      PRTVOL5

         MOV     DPTR,#TORFLG
         MOVX    A,@DPTR
         JZ      PRTVOL5

         CALL    PFEED

         MOV     DPTR,#PRMSG26         ; MULTI-TANK MESSAGE
         CALL    PRTCMSG
         MOV     DPTR,#PRMSG27
         CALL    PRTCMSG

         MOV     DPTR,#DUPTFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; SET DUPLICATE FLAG FOR TIMER OVERRIDE PRINT

PRTVOL5:
         MOV     DPTR,#B8FLG           ; SELECTIVE PRINT OPTIONS
         MOVX    A,@DPTR
         JNZ     SKIP4

         CALL    PFEED

SKIP4:
         MOV     DPTR,#TOTFLG          ; NOTE NO MENU ITEM TO CHANGE????
         MOVX    A,@DPTR
         JNZ     SKIP5
         CALL    PRTTOT                ; PRINT BOTH START AND FINISH TOTALIZER IF TOTFLG ON
SKIP5:   MOV     DPTR,#REGFLG
         MOVX    A,@DPTR
         JNZ     SKIP6
         CALL    PRTSER
SKIP6:   RET

PRTTR1:
         MOV     TEMP,#0
         MOV     DPTR,#TAX1BCD
         MOVX    A,@DPTR
         ORL     TEMP,A
         INC     DPTR
         MOVX    A,@DPTR
         ORL     TEMP,A
         INC     DPTR
         MOVX    A,@DPTR
         ORL     TEMP,A
         MOV     A,TEMP
         JNZ     PRTR1G
         JMP     PRTR1H
PRTR1G:  MOV     ZERO,#0
         MOV     B,#0                  ; FLAG TO PRINT %
         CALL    BLAST
         MOV     A,#'@'
         CALL    OUTA
         MOV     A,#' '
         CALL    OUTA
         MOV     DPTR,#TAX1T
         MOVX    A,@DPTR               ; TAXTYPE FROM NOTAX
         CJNE    A,#'$',PRTR1A
         MOV     A,#'$'
         CALL    OUTA
         MOV     B,#1                  ; FLAG $ PRINTED
PRTR1A:  MOV     DPTR,#TAX1
         MOVX    A,@DPTR
         CALL    OUTAZ
         INC     DPTR
         MOVX    A,@DPTR
         CALL    OUTAZ
         MOV     A,#'.'
         CALL    OUTA
         MOV     R0,#4
PRTR1B:  INC     DPTR
         MOVX    A,@DPTR
         CALL    OUTA
         DJNZ    R0,PRTR1B

PRTR1F:  MOV     A,B
         JNZ     PRTR1C
         MOV     A,#'%'
         CALL    OUTA
PRTR1C:  CALL    PLINEEND
PRTR1H:  RET

PRTTR2:
         MOV     TEMP,#0
         MOV     DPTR,#TAX2BCD
         MOVX    A,@DPTR
         ORL     TEMP,A
         INC     DPTR
         MOVX    A,@DPTR
         ORL     TEMP,A
         INC     DPTR
         MOVX    A,@DPTR
         ORL     TEMP,A
         MOV     A,TEMP
         JNZ     PRTR2G
         JMP     PRTR2H

PRTR2G:  MOV     ZERO,#0
         MOV     B,#0                  ; FLAG TO PRINT %
         CALL    BLAST
         MOV     A,#'@'
         CALL    OUTA
         MOV     A,#' '
         CALL    OUTA
         MOV     DPTR,#TAX2T
         MOVX    A,@DPTR               ; TAXTYPE FROM NOTAX
         CJNE    A,#'$',PRTR2A
         MOV     A,#'$'
         CALL    OUTA
         MOV     B,#1                  ; FLAG $ PRINTED
PRTR2A:  MOV     DPTR,#TAX2
         MOVX    A,@DPTR
         CALL    OUTAZ
         INC     DPTR
         MOVX    A,@DPTR
         CALL    OUTAZ
         MOV     A,#'.'
         CALL    OUTA
         MOV     R0,#4
PRTR2B:  INC     DPTR
         MOVX    A,@DPTR
         CALL    OUTA
         DJNZ    R0,PRTR2B
PRTR2F:  MOV     A,B
         JNZ     PRTR2C
         MOV     A,#'%'
         CALL    OUTA
PRTR2C:  CALL    PLINEEND
PRTR2H:  RET

PRTDR1:
         MOV     TEMP,#0
         MOV     DPTR,#DSCBCD
         MOVX    A,@DPTR
         ORL     TEMP,A
         INC     DPTR
         MOVX    A,@DPTR
         ORL     TEMP,A
         INC     DPTR
         MOVX    A,@DPTR
         ORL     TEMP,A
         MOV     A,TEMP
         JNZ     PRTD1G
         JMP     PRTD1H

PRTD1G:  MOV     ZERO,#0
         MOV     B,#0                  ; FLAG TO PRINT %
         CALL    BLAST
         MOV     A,#'@'
         CALL    OUTA
         MOV     A,#' '
         CALL    OUTA
         MOV     DPTR,#DISCAKEY
         MOVX    A,@DPTR               ; TAXTYPE FROM NOTAX
         CJNE    A,#'$',PRTD1A
         MOV     A,#'$'
         CALL    OUTA
         MOV     B,#1                  ; FLAG $ PRINTED
PRTD1A:  MOV     DPTR,#DISCRATE
         MOVX    A,@DPTR
         CALL    OUTAZ
         INC     DPTR
         MOVX    A,@DPTR
         CALL    OUTAZ
         MOV     A,#'.'
         CALL    OUTA
         MOV     R0,#4
PRTD1B:  INC     DPTR
         MOVX    A,@DPTR
         CALL    OUTA
         DJNZ    R0,PRTD1B
PRTD1F:  MOV     A,B
         JNZ     PRTD1C
         MOV     A,#'%'
         CALL    OUTA
PRTD1C:  CALL    PLINEEND
PRTD1H:  RET


PRTTOTLA:
         MOV     R1,#25
         CALL    SHDP20
         CALL    BLAST

         PUSH    DPL
         PUSH    DPH
         MOV     DPTR,#TRUNC
         MOVX    A,@DPTR
         POP     DPH
         POP     DPL
         JZ      TRUNC2
         MOV     R1,#14
         JMP     PRTTOTLL

TRUNC2:  MOV     R1,#12
PRTTOTLL:CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRTTOTLL
         RET

PRTTOTLB:
         CALL    ASCOUT
         CALL    DASCOUTX4
         CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC
         CALL    OUTA
         MOV     DPTR,#TRUNC
         MOVX    A,@DPTR
         JNZ     PRTOTX
         CALL    CHKDEC
         JNZ     PRTTOTLBB
         MOV     A,#'.'
         CALL    OUTA
PRTTOTLBB:
         MOV     A,B
         CALL    OUTA
PRTOTX:  CALL    PLINEEND
         RET


PRTTOT:
         MOV     DPTR,#PRMSG28D        ; START TOTALIZER
         CALL    PRTTOTLA
         MOV     DPTR,#GRTOTSTRT+5
         CALL    PRTTOTLB
PRTTOTFONLY:                           ; SHIFT REPORT AND CAL REPORT JUMP TO HERE
         MOV     DPTR,#PRMSG28E        ; FINISH TOTALIZER
         CALL    PRTTOTLA
         MOV     DPTR,#GRTOTN+5
         CALL    PRTTOTLB
         RET


PRSHT:   CALL    CLRLINES
         CALL    SAYPRNT
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#3,PRSHT12          ; IF PRINTER = 'NONE' GET OUT, ELSE CONTINUE
         JMP     PRSHT11
PRSHT12:
         CALL    CHKTKT                ; CALL #4: PRINT SHIFT TICKET #A
         JC      SHFTRPT

PRSHT9:  MOV     DPTR,#CALLED
         MOVX    A,@DPTR
         JZ      PRSHT11
         MOV     A,#0
         MOVX    @DPTR,A               ; IF CALLED BY 'f' THEN CLEAR FLAG
         JMP     M47X                  ; AND RETURN TO f COMMAND

PRSHT11: CALL    DISCON
         JMP     MAIN

SHFTRPT:
         CALL    SELECTP2

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#13,SHFTRP00
         CALL    PFEED                 ; NEED ONE BLANK LINE AT TOP OF TICKET FOR DATAMAX M4TE
SHFTRP00:
         MOV     DPTR,#PRMSG32         ; SHIFT REPORT MESSAGE
         CALL    PRTCMSG

         CALL    PFEED

         MOV     DPTR,#HDRTYPFL        ; HEADER TYPE FLAG, 0=DELTICKET, 1=SHIFTTICKET, 2=CALTICKET
         MOV     A,#1
         MOVX    @DPTR,A
         CALL    DOPRTHDR

         MOV     DPTR,#PRMSG6A
         CALL    MOVMES
         CALL    RTIME                 ; PRINT CURRENT TIME
         CALL    PRTHDR2               ; CURRENT TIME

         CALL    LODHCMDFLG
         JNZ     PRSHT99

         MOV     DPTR,#LSTSHFT         ; LAST SHIFT TIME
         CALL    SAVTIME

PRSHT99:
         CALL    PFEED

         CALL    PRTTOTFONLY           ; PURPOSEFULLY ONLY PRINTING FINAL ON SHIFT

         CALL    PRTSER

         MOV     BCDM,PRCOD            ; SAVE CURRENT PRCOD
         MOV     PRCOD,#1

PRSHT3:
         MOV     DPTR,#PCNVOL
         CALL    PCVPTR
         CALL    CHKTOTAL              ; SEE IF ZERO
         JNZ     PRSHT1
         JMP     PRSHT4                ; SKIP PRINT, INC PRCOD

PRSHT1:  PUSH    DPH
         PUSH    DPL
         CALL    BLAST
         MOV     DPTR,#NMSG            ; NET MESSAGE
         MOV     R1,#5
PRSHT2:  CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRSHT2
         MOV     A,PRCOD
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     A,TEMP
         CALL    MAKEASC
         CALL    OUTA
         MOV     A,B
         CALL    OUTA                  ; PRCOD OUT

         MOV     DPTR,#NMSG+8          ; NET MESSAGE
         MOV     R1,#6
PRSHT5:  CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRSHT5
         MOV     A,#' '
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA

         POP     DPL
         POP     DPH

         INC     DPTR
         INC     DPTR
         INC     DPTR
         CALL    ASCOUT                ; 1ST 2 BYTES
         CALL    DASCOUTX2             ; 2ND AND 3RD SETS OF 2 BYTES
         CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC
         CALL    OUTA

         CALL    CHKDEC
         JNZ     DEC6
         MOV     A,#'.'
         CALL    OUTA
DEC6:    MOV     A,B
         CALL    OUTA
         CALL    PLINEEND

         CALL    LODHCMDFLG
         JNZ     NEXT1

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#0,CHKT10
         JMP     CHKT1
CHKT10:  CJNE    A,#1,NEXT1            ; REVISED FOR TM295 AND MIDCOM ONLY
CHKT1:   CALL    CHKTKT                ; CALL #5: PRINT SHIFT TICKET CALL #B
         JNC     CHKT1
         CALL    SELECTP2

NEXT1:   MOV     DPTR,#PCGVOL          ; NOW FOR GROSS
         CALL    PCVPTR                ; GET POINTER

         PUSH    DPH                   ; SAVE THE POINTER
         PUSH    DPL
         CALL    BLAST
         MOV     DPTR,#GMSG            ; GROSS MESSAGE
         MOV     R1,#5
PRSHT7:  CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRSHT7

         MOV     A,PRCOD               ; STICK IN PRCOD
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     A,TEMP
         CALL    MAKEASC
         CALL    OUTA
         MOV     A,B
         CALL    OUTA                  ; PRCOD OUT

         MOV     DPTR,#GMSG+8
         MOV     R1,#6
PRSHT8:  CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRSHT8
         MOV     A,#' '
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA

         POP     DPL
         POP     DPH

         INC     DPTR
         INC     DPTR
         INC     DPTR
         CALL    ASCOUT
         CALL    DASCOUTX2
         CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC

         CALL    OUTA
         CALL    CHKDEC
         JNZ     DEC7
         MOV     A,#'.'
         CALL    OUTA
DEC7:    MOV     A,B
         CALL    OUTA
         CALL    PLINEEND



         MOV     DPTR,#QOBMODE         ; SKIP QOB PRINTING IF QOBMODE = 0 = OFF
         MOVX    A,@DPTR
         JZ      DECY

         CALL    LODHCMDFLG
         JNZ     NEXT2

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#0,CHKT11
         JMP     CHKT2
CHKT11:  CJNE    A,#1,NEXT2
CHKT2:   CALL    CHKTKT                ; CALL #6: PRINT SHIFT TICKET CALL #C
         JNC     CHKT2
         CALL    SELECTP2

NEXT2:   CALL    BLAST
         MOV     DPTR,#PRMSG53
         MOV     R1,#18
PRTQOB2: CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRTQOB2

         CALL    QOBPTR
         INC     DPTR
         INC     DPTR
         CALL    ASCOUT
         CALL    DASCOUT
         CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC
         CALL    OUTA
         CALL    CHKDEC
         JNZ     DECX
         MOV     A,#'.'
         CALL    OUTA
DECX:    MOV     A,B
         CALL    OUTA
         CALL    PLINEEND

DECY:    CALL    LODHCMDFLG
         JNZ     PRSHT4

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#0,CHKT12
         JMP     CHKT3
CHKT12:  CJNE    A,#1,PRSHT4
CHKT3:   CALL    CHKTKT                ; CALL #7: PRINT SHIFT TICKET CALL #D
         JNC     CHKT3
         CALL    SELECTP2

PRSHT4:  INC     PRCOD
         MOV     A,PRCOD
         CJNE    A,#100,PRSHT6
         JMP     PRSHTN
PRSHT6:  JMP     PRSHT3
PRSHTN:  MOV     PRCOD,BCDM            ; RESTORE CURRENT PRCOD

         CALL    PFEED

         CALL    LODHCMDFLG
         JNZ     NEXT3

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#0,CHKT13
         JMP     CHKT4
CHKT13:  CJNE    A,#1,NEXT3
CHKT4:   CALL    CHKTKT                ; CALL #8: PRINT SHIFT TICKET CALL #F
         JNC     CHKT4
         CALL    SELECTP2

NEXT3:   MOV     DPTR,#PRMSG33
         CALL    PRTCMSG

;THIS IS EXTRA, THE OTHER TICKETS END WITH TEXT (DEL, DUP, CAL, POWFAIL, ETC) SO IT MAKES THE SPACING LOOK WRONG
;         CALL    PFEED

NEXT3A:  CALL    LODHCMDFLG
         JZ      PRSHT1X
         JMP     PRSHT1Y

PRSHT1X: MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#2,PRSH10A          ; 2 = GENERIC THERMAL
PRSHMFCT:CALL    MFEED
         JMP     PRSH10Z               ; JUMP TO CUT

PRSH10A: CJNE    A,#4,PRSH10B          ; 4 = ZEBRA RW-420 V1, 3 BLANK LINES
         CALL    DO3BLNKLNS
         JMP     PRSHT10               ; JUMP TO AFTER CUT/BMFF

PRSH10B: CJNE    A,#5,PRSH10C          ; 5 = BLASTER V1, PRINT 3 BLANK LINES FOR BLASTER V1
         CALL    DO3BLNKLNS
         JMP     PRSHT10               ; JUMP TO AFTER CUT/BMFF

PRSH10C: CJNE    A,#6,PRSH10D          ; 6 = EPSON TM-220
         CALL    MFEED
         JMP     PRSHT10               ; JUMP TO AFTER CUT/BMFF

PRSH10D:CJNE     A,#7,PRSH10E
         CALL    PR460BMF              ; 7 = FUJITSU FP-460 BLACK MARK FF, DOES 1 SEC WAIT
         JMP     PRSHT10               ; JUMP TO AFTER CUT/BMFF

PRSH10E: CJNE    A,#8,PRSH10F          ; 8 = CITIZEN CT-S310, SAME AS THERMAL
         JMP     PRSHMFCT

PRSH10F:CJNE     A,#9,PRSH10G          ; 9 = CITIZEN CT-S601/651, SAME AS THERMAL
         JMP     PRSHMFCT

PRSH10G:CJNE     A,#10,PRSH10H         ; 10 = EPSON TM-88, SAME AS THERMAL
         JMP     PRSHMFCT

PRSH10H:CJNE     A,#11,PRSH10I         ; 11 = BLASTER V2 = BMFF
         CALL    PRBLSTBMFF            ; BLASTER BLACK MARK FF
         JMP     PRSHT10               ; JUMP TO AFTER CUT/BMFF

PRSH10I:CJNE     A,#12,PRSH10J         ; 12 = PRINTEK MOBILE RT43 BLUETOOTH
         CALL    MFEED                 ; NO CUT (TEAR ONLY) AFTER 7 LFS
         JMP     PRSHT10               ; JUMP TO AFTER CUT/BMFF

PRSH10J: CJNE    A,#13,PRSH10K         ; 13 = DATAMAX M4TE
         CALL    PFEED                 ; NO CUT (TEAR ONLY) AFTER 2 LFS
         CALL    PFEED
         JMP     PRSHT10               ; JUMP TO AFTER CUT/BMFF

PRSH10K: CJNE    A,#14,PRSH10L         ;  14 = ZEBRA RW-420 V2 = DO BMFF
         CALL    PR295FF               ; RW-420 BMFF USES SAME FF CMD AS TM-295
         JMP     PRSHT10               ; JUMP TO AFTER CUT/BMFF

PRSH10L: CJNE    A,#15,PRSH10M         ; 15 = CUSTOM Q3 = DO BMFF + CUT
         CALL    PRQ3BMFF              ; Q3 BMFF
         JMP     PRSH10Z               ; JUMP TO CUT

PRSH10M: CJNE    A,#16,PRSHT10         ; 16 = STAR TSP700II = DO BMFF + CUT
         CALL    PR295FF               ; STAR TSP700II/743 FEED TO BLACK MARK SENSOR
         JMP     PRSH10Z               ; JUMP TO CUT

PRSH10Z:
         CALL    THRMPRTCUT

PRSHT10: CALL    CLRSHFT
         CALL    DISCON

         MOV     R0,#CGALS
         MOV     R1,#5
         MOV     A,#0
PRSHTZ:  MOV     @R0,A
         INC     R0
         DJNZ    R1,PRSHTZ

PRSHT1Y: CALL    LODHCMDFLG
         JZ      PRSHTW
         JMP     MAIN61X

PRSHTW:  MOV     DPTR,#CALLED
         MOVX    A,@DPTR
         JZ      PRSHTX
         MOV     A,#0
         MOVX    @DPTR,A               ; IF CALLED BY 'f' THEN CLEAR FLAG
         JMP     M47X                  ; AND RETURN TO f COMMAND
PRSHTX:  JMP     MAIN

; *************************************
; E179_REVA: CLEARED IN TWO PLACES, MAKE SUB
; *************************************
CLRCMNDFLG:
         MOV     A,#'0'
         MOV     DPTR,#CMDFLAG         ; CLEAR HOST 'i' COMMAND FLAGS
         MOV     R0,#10                ; MAKE SURE TO ALWAYS CLEAR ALL 10
CLRCMNDFLL:MOVX  @DPTR,A
         INC     DPTR
         DJNZ    R0,CLRCMNDFLL
         RET


; *****************************************
; CHKTOTAL CHECKS FOR SHIFT TOTAL = 0
; *****************************************
CHKTOTAL:
         MOV     A,#0
         PUSH    DPH
         PUSH    DPL                   ; NEED TO SAVE ON RETURN
         MOV     R2,#4                 ; 4 BYTES WORTH
CHKTOT2: MOVX    A,@DPTR               ; GET IT
         JNZ     CHKTOT1
         INC     DPTR
         DJNZ    R2,CHKTOT2
CHKTOT1: POP     DPL
         POP     DPH                   ; GET EM' BACK
         RET

DMODMSG: MOV     DPTR,#MSG35           ; SPLASHES "DELIVERY MODE"
         CALL    SHOW
         MOV     DPTR,#MSG4
         CALL    SHOW
         RET

DELMODE: MOV     DPTR,#DMODEFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; START WITH PRCOD SELECTION

DELMD000:MOV     DPTR,#DMODEFLG
         MOVX    A,@DPTR               ; CHECK CURRENT SELECTION

         CJNE    A,#0,DELMD001
         MOV     DPTR,#HOSEPKFLG
         MOVX    A,@DPTR
         JNZ     DELMD00A

         MOV     DPTR,#DMODEFLG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     DELMD001

DELMD00A:MOV     DPTR,#MSG101          ; HOSE PACK MESSAGE
         JMP     DELMOD99

DELMD001:CJNE    A,#1,DELMD002
         MOV     DPTR,#MSG15           ; PRCOD MESSAGE
         JMP     DELMOD99

DELMD002:CJNE    A,#2,DELMD003
         MOV     DPTR,#QOBMODE         ; SKIP IF QOB FLAG = 0 = OFF
         MOVX    A,@DPTR
         JZ      DELMD02A
         MOV     DPTR,#MSG100          ; QOB MESSAGE
         JMP     DELMOD99
DELMD02A:MOV     DPTR,#DMODEFLG
         MOV     A,#3
         MOVX    @DPTR,A

DELMD003:CJNE    A,#3,DELMD004
         MOV     DPTR,#MONEYFLG
         MOVX    A,@DPTR
         JNZ     DELMD003A             ; 0=OFF, 1=PRICE CHK, 2=ALWAYS
         MOV     DPTR,#DMODEFLG
         MOV     A,#4
         MOVX    @DPTR,A
         JMP     DELMD004
DELMD003A:MOV    DPTR,#MSG77           ; PRICE MESSAGE
         JMP     DELMOD99

DELMD004:CJNE    A,#4,DELMD005
         MOV     DPTR,#ETFLG           ; IF ETFLG=0=TIMER OFF SKIP TIMER OVERRIDE
         MOVX    A,@DPTR
         JZ      DELMD04A
         MOV     DPTR,#MSG89           ; TIMOVR MESSAGE
         JMP     DELMOD99
DELMD04A:MOV     DPTR,#DMODEFLG
         MOV     A,#5
         MOVX    @DPTR,A

DELMD005:CJNE    A,#5,DELMD006
         MOV     DPTR,#MSG47           ; SHIFT  MESSAGE
         JMP     DELMOD99

DELMD006:CJNE    A,#6,DELMD007
         MOV     DPTR,#MSG46           ; DRIVER MESSAGE
         JMP     DELMOD99

DELMD007:CJNE    A,#7,DELMD008
         MOV     DPTR,#MSG49           ; TOTAL MESSAGE
         JMP     DELMOD99

DELMD008:CJNE    A,#8,DELMD009
         MOV     DPTR,#MSG25           ; DISVOL MESSAGE
         JMP     DELMOD99

DELMD009:CJNE    A,#9,DELMD010
         MOV     DPTR,#MSG11           ; TEMPF MESSAGE
         JMP     DELMOD99

DELMD010:CJNE    A,#10,DELMD011
         MOV     DPTR,#MSG10           ; TEMPC MESSAGE
         JMP     DELMOD99

DELMD011:CJNE    A,#11,DELMD012
         MOV     DPTR,#MSG60           ; CALTKT MESSAGE - UP ARROW JUMPS HERE
         JMP     DELMOD99

DELMD012:CJNE    A,#12,DELMD113
         MOV     DPTR,#MSG220          ; TANK ID MESSAGE
         JMP     DELMOD99

DELMD113:CJNE    A,#13,DELMD114
         MOV     DPTR,#MSG41           ; SETUP MESSAGE - LEFT ARROW JUMPS HERE
         JMP     DELMOD99

DELMD114:CJNE    A,#14,DELMOD99
         MOV     DPTR,#MSG48           ; EXIT MESSAGE - RIGHT ARROW JUMPS HERE
         JMP     DELMOD99

DELMOD99:CALL    SHOW

DELMOD7: CALL    KPRESS
         MOV     A,KEY
         JZ      DELMOD7               ; USE KEY != 0 TO SCROLL

         CJNE    A,#MODEKEY,DELMOD7A
         MOV     DPTR,#DMODEFLG
         MOVX    A,@DPTR
         JMP     DELMOD7B
DELMOD7A:JMP     DELMOD13

DELMOD7B:MOV     TEMP,#0
         MOV     R2,#15
DELX3:   CJNE    A,TEMP,DELX1
         INC     A
         CJNE    A,#15,DELX2
         MOV     A,#0
         JMP     DELX2
DELX1:   INC     TEMP
         DJNZ    R2,DELX3
DELX2:   JMP     DELMOD7C

DELMOD7C:MOV     DPTR,#DMODEFLG
         MOVX    @DPTR,A
         JMP     DELMD000

DELMOD13:CJNE    A,#SSKEY,DELMOD14     ; BACK TO TOP
         MOV     DPTR,#DMODEFLG
         MOVX    A,@DPTR               ; GET CURRENT SELECTION
         JMP     DELMD150

DELMOD14:CJNE    A,#RIGHTKEY,DELMOD15
         MOV     DPTR,#DMODEFLG
         MOV     A,#14
         MOVX    @DPTR,A
         JMP     DELMD000              ; QUICK TO EXIT WITH RIGHT ARROW

DELMOD15:CJNE    A,#LEFTKEY,DELMOD15A
         MOV     DPTR,#DMODEFLG
         MOV     A,#13
         MOVX    @DPTR,A
         JMP     DELMD000              ; QUICK TO SETUP WITH LEFT ARROW

DELMOD15A:
         CJNE    A,#UPKEY,DELMOD16
         MOV     DPTR,#DMODEFLG
         MOV     A,#11
         MOVX    @DPTR,A
         JMP     DELMD000              ; QUICK TO CALTKT WITH UP ARROW

DELMOD16:CJNE    A,#PRINTKEY,DELMOD16Z

         MOV     DPTR,#DMODEFLG
         MOVX    A,@DPTR               ; CHECK CURRENT SELECTION
         CJNE    A,#5,DELMOD16A        ; IF SHIFT TKT PRINT IT
         JMP     DELMD150              ; IF CAL OR SHIFT TICKET THIS WILL PRINT IT
DELMOD16A:
         CJNE    A,#11,DELMOD16Z       ; IF CAL TKT PRINT IT
         JMP     DELMD150              ; IF CAL OR SHIFT TICKET THIS WILL PRINT IT
DELMOD16Z:
         JMP    DELMD000


DELMD150:CJNE    A,#0,DELMD151
DEL57A:  CALL    KPRESS                ; OPEN ONLY SECOND FOR HOSE PACK
         MOV     A,KEY
         JZ      DEL57A
         CJNE    A,#SSKEY,DEL57A
         MOV     DPTR,#MSG108          ; PACK
         CALL    SHOW
         SETB    V2ND
         CALL    WAIT500
         JMP     MAINX

DELMD151:CJNE    A,#1,DELMD152
         CALL    GETPRCODE
         CALL    SETCMP
         JMP     MAIN

DELMD152:CJNE    A,#2,DELMD153
         CALL    GETQOB
         JMP     MAIN

DELMD153:CJNE    A,#3,DELMD154
         JMP     PRCMODE

DELMD154:CJNE    A,#4,DELMD155
         CALL    SETTOVR
         JMP     MAIN

DELMD155:CJNE    A,#5,DELMD156
         JMP     PRSHT

DELMD156:CJNE    A,#6,DELMD157
         CALL    GETDRV
         JMP     MAIN

DELMD157:CJNE    A,#7,DELMD158
         CALL    SHOWTOT
         JMP     MAIN

DELMD158:CJNE    A,#8,DELMD159
         JMP     DISPNG                ; RETURNS TO MAIN

DELMD159:CJNE    A,#9,DELMD160
DELMD159A:
         CALL    TEMPF
         CALL    WAIT1000
         CALL    KPRESS
         MOV     A,KEY
         JZ      DELMD159A
         CJNE    A,#SSKEY,DELMD159A
         JMP     MAIN

DELMD160:CJNE    A,#10,DELMD161
DELMD160A:
         CALL    TEMPC
         CALL    WAIT1000
         CALL    KPRESS
         MOV     A,KEY
         JZ      DELMD160A
         CJNE    A,#SSKEY,DELMD160A
         JMP     MAIN

DELMD161:CJNE    A,#11,DELMD164
         JMP     PRTCAL                ; JUMPS TO MAIN

DELMD164:CJNE    A,#12,DELMD162
         CALL    TNKNUM
         JMP     MAIN

DELMD162:CJNE    A,#13,DELMD163
         CALL    GETCODE
         JC      DELMD162A
         JMP     MAIN
DELMD162A:
         JMP     SETUP

DELMD163:CJNE    A,#14,DELMD199
         JMP     MAIN



DELMD199:JMP     DELMD000


; PRICE MODE
PRCMODE: MOV     DPTR,#PMODEFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; START WITH PRCOD SELECTION

PRCMOD1: MOV     DPTR,#PMODEFLG
         MOVX    A,@DPTR               ; CHECK CURRENT SELECTION

         CJNE    A,#0,PRCMOD35
         MOV     DPTR,#MSG78           ; PRICE MESSAGE
         JMP     PRCMOD6

PRCMOD35:CJNE    A,#1,PRCMOD2
         MOV     DPTR,#MSG79           ; TAX 1 MESSAGE
         JMP     PRCMOD6

PRCMOD2: CJNE    A,#2,PRCMOD3
         MOV     DPTR,#MSG80           ; TXTYP1 MESSAGE
         JMP     PRCMOD6

PRCMOD3: CJNE    A,#3,PRCMOD4
         MOV     DPTR,#MSG81           ; TAX 2 MESSAGE
         JMP     PRCMOD6

PRCMOD4: CJNE    A,#4,PRCMOD39
         MOV     DPTR,#MSG82           ; TXTYP2 MESSAGE
         JMP     PRCMOD6

PRCMOD39:CJNE    A,#5,PRCMOD5
         MOV     DPTR,#MSG112          ; TAXSUB MESSAGE
         JMP     PRCMOD6

PRCMOD5: CJNE    A,#6,PRCMOD21
         MOV     DPTR,#MSG83           ; DISCNT  MESSAGE
         JMP     PRCMOD6

PRCMOD21:CJNE    A,#7,PRCMOD25
         MOV     DPTR,#MSG84           ; DSCTYP MESSAGE
         JMP     PRCMOD6

PRCMOD25:CJNE    A,#8,PRCMOD14
         MOV     DPTR,#MSG85           ; DSCDAY MESSAGE
         JMP     PRCMOD6

PRCMOD14:CJNE    A,#9,PRCMOD42
         MOV     DPTR,#MSG114          ; PRICE ADJUSTMENT MESSAGE
         JMP     PRCMOD6

PRCMOD42:CJNE    A,#10,PRCMOD6
         MOV     DPTR,#MSG48           ; EXIT MESSAGE
         JMP     PRCMOD6

PRCMOD6: CALL    SHOW

PRCMOD7: CALL    KPRESS
         MOV     A,KEY
         JZ      PRCMOD7               ; USE KEY != 0 TO SCROLL

         CJNE    A,#MODEKEY,PRCMOD13
         MOV     DPTR,#PMODEFLG
         MOVX    A,@DPTR

         MOV     TEMP,#0
         MOV     R2,#11
PRCX3:   CJNE    A,TEMP,PRCX1
         INC     A
         CJNE    A,#11,PRCX2
         MOV     A,#0
         JMP     PRCX2
PRCX1:   INC     TEMP
         DJNZ    R2,PRCX3
PRCX2:   JMP     PRCMOD15

PRCMOD15:MOV     DPTR,#PMODEFLG
         MOVX    @DPTR,A
         JMP     PRCMOD1

PRCMOD13:CJNE    A,#SSKEY,PRCMOD33     ; BACK TO TOP
         MOV     DPTR,#PMODEFLG
         MOVX    A,@DPTR               ; GET CURRENT SELECTION
         JMP     PRCMOD34

PRCMOD33:CJNE    A,#RIGHTKEY,PRCMOD38
         MOV     DPTR,#PMODEFLG
         MOV     A,#10
         MOVX    @DPTR,A
         JMP     PRCMOD1               ; QUICK TO EXIT WITH RIGHT ARROW
PRCMOD38:JMP     PRCMOD30

PRCMOD34:CJNE    A,#0,PRCMOD16
         CALL    GETPCPR
         CALL    GETPCE
         CALL    PUTPCPR
         JMP     PRCMOD1

PRCMOD16:CJNE    A,#1,PRCMOD17
         CALL    GETPCPR
         CALL    GETTAX1
         CALL    PUTPCPR
         JMP     PRCMOD1

PRCMOD17:CJNE    A,#2,PRCMOD18
         CALL    GETPCPR
         CALL    SETTYP1
         CALL    PUTPCPR
         JMP     PRCMOD1

PRCMOD18:CJNE    A,#3,PRCMOD19
         CALL    GETPCPR
         CALL    GETTAX2
         CALL    PUTPCPR
         JMP     PRCMOD1

PRCMOD19:CJNE    A,#4,PRCMOD41
         CALL    GETPCPR
         CALL    SETTYP2
         CALL    PUTPCPR
         JMP     PRCMOD1

PRCMOD41:CJNE    A,#5,PRCMOD20
         CALL    SETTXSUB
         JMP     PRCMOD1

PRCMOD20:CJNE    A,#6,PRCMOD27
         CALL    GETPCPR
         CALL    GETDSC
         CALL    PUTPCPR
         JMP     PRCMOD1

PRCMOD27:CJNE    A,#7,PRCMOD32
         CALL    GETPCPR
         CALL    SETDSC2
         CALL    PUTPCPR
         JMP     PRCMOD1

PRCMOD32:CJNE    A,#8,PRCMOD30
         CALL    GETPCPR
         CALL    GETDAYS
         CALL    PUTPCPR
         JMP     PRCMOD1

PRCMOD30:CJNE    A,#10,PRCMOD44
;        CALL    PUTPCPR
         JMP     MAIN

PRCMOD44:CJNE    A,#9,PRCMOD37
         CALL    GETADJ
         JMP     PRCMOD1

PRCMOD37:JMP     PRCMOD1


SETUP:   MOV     DPTR,#SMODEFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; START WITH CODE SELECTION

SUPMD000:MOV     DPTR,#SMODEFLG
         MOVX    A,@DPTR               ; CHECK CURRENT SELECTION

         CJNE    A,#0,SUPMD001
         MOV     DPTR,#MSG58           ; CODE MESSAGE
         JMP     SUPMD100

SUPMD001:CJNE    A,#1,SUPMD002
         MOV     DPTR,#MSG50           ; STAGE1 MESSAGE
         JMP     SUPMD100

SUPMD002:CJNE    A,#2,SUPMD003
         MOV     DPTR,#MSG51           ; STAGE2 MESSAGE
         JMP     SUPMD100

SUPMD003:CJNE    A,#3,SUPMD004
         MOV     DPTR,#MSG52           ; TIME MESSAGE
         JMP     SUPMD100

SUPMD004:CJNE    A,#4,SUPMD005
         MOV     DPTR,#MSG53           ; DATE MESSAGE
         JMP     SUPMD100

SUPMD005:CJNE    A,#5,SUPMD006
         MOV     DPTR,#MSG54           ; SALE MESSAGE
         JMP     SUPMD100

SUPMD006:CJNE    A,#6,SUPMD007
         MOV     DPTR,#MSG55           ; TRUCK MESSAGE
         JMP     SUPMD100

SUPMD007:CJNE    A,#7,SUPMD008
         MOV     DPTR,#MSG90           ; CURRENCY MESSAGE
         JMP     SUPMD100

SUPMD008:CJNE    A,#8,SUPMD009
         MOV     DPTR,#MSG73           ; REGNUM MESSAGE = UP ARROW
         JMP     SUPMD100

SUPMD009:CJNE    A,#9,SUPMD010
         MOV     DPTR,#MSG64           ; PRINTER MESSAGE = LEFT ARROW
         JMP     SUPMD100

SUPMD010:CJNE    A,#10,SUPMD011
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#2,SUPMDEA          ; CHECK FOR THERMAL
         JMP     SUPMDEE
SUPMDEA: CJNE    A,#4,SUPMDEB          ; ZEBRA RW-420 V1 = NO BMFF
         JMP     SUPMDEE
SUPMDEB: CJNE    A,#5,SUPMDEC          ; BLASTER
         JMP     SUPMDEE
SUPMDEC: CJNE    A,#7,SUPMDED          ; FUJITSU FP-460 = LOGO+BMFF
         JMP     SUPMDEE
SUPMDED: CJNE    A,#8,SUPMDEF          ; CITIZEN CTS-310
         JMP     SUPMDEE
SUPMDEF: CJNE    A,#9,SUPMDEG          ; CITIZEN CTS-601/651
         JMP     SUPMDEE
SUPMDEG: CJNE    A,#10,SUPMDEH         ; EPSON TM-88
         JMP     SUPMDEE
SUPMDEH: CJNE    A,#11,SUPMDEI         ; BLASTER V2
         JMP     SUPMDEE
SUPMDEI: CJNE    A,#12,SUPMDEJ         ; PRINTEK MOBILE RT43 BLUETOOTH
         JMP     SUPMDEE
SUPMDEJ: CJNE    A,#13,SUPMDEK         ; DATAMAX M4TE
         JMP     SUPMDEE
SUPMDEK: CJNE    A,#14,SUPMDEL         ; ZEBRA RW-420 V2 = DO BMFF+CUT
         JMP     SUPMDEE
SUPMDEL: CJNE    A,#15,SUPMDEM         ; CUSTOM Q3 = DO LOGO+BMFF
         JMP     SUPMDEE
SUPMDEM: CJNE    A,#16,SUPMDEZ         ; STAR TSP700II = DO LOGO+BMFF
         JMP     SUPMDEE

SUPMDEE: MOV     DPTR,#MSG71           ; COPIES MESSAGE
         JMP     SUPMD100

SUPMDEZ: MOV     DPTR,#SMODEFLG
         MOV     A,#11
         MOVX    @DPTR,A

SUPMD011:CJNE    A,#11,SUPMD012
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#15,SUPMD11X        ; IF NOT Q3 THEN SKIP LOGO HEIGHT
         MOV     DPTR,#MSG228          ; LOGOHT MESSAGE
         JMP     SUPMD100

SUPMD11X:MOV     DPTR,#SMODEFLG
         MOV     A,#12
         MOVX    @DPTR,A

SUPMD012:CJNE    A,#12,SUPMD012A
         MOV     DPTR,#MSG117          ; 'SS KEY' MESSAGE
         JMP     SUPMD100

SUPMD012A:CJNE    A,#13,SUPMD013
         MOV     DPTR,#MSG226          ; "EMULAT"
         JMP     SUPMD100

SUPMD013:CJNE    A,#14,SUPMD014
         MOV     DPTR,#MSG213          ; SWAUTH = SOFTWARE AUTHORIZATION MESSAGE FOR 'x' / 'y' CMDS
         JMP     SUPMD100

SUPMD014:CJNE    A,#15,SUPMD015
         MOV     DPTR,#MSG229          ; "QOB MD" ENABLE/DISABLE
         JMP     SUPMD100

SUPMD015:CJNE    A,#16,SUPMD016
         MOV     DPTR,#MSG134          ; HOSTFX MESSAGE
         JMP     SUPMD100

SUPMD016:CJNE    A,#17,SUPMD017
         MOV     DPTR,#MSG214          ; PRINT+PRESET BUTTONS ENABLED (P KEYS) MESSAGE
         JMP     SUPMD100

SUPMD017:CJNE    A,#18,SUPMD018
         MOV     DPTR,#MSG94           ; PRESET REQUIRED MESSAGE
         JMP     SUPMD100

SUPMD018:CJNE    A,#19,SUPMD019
         MOV     DPTR,#MSG101          ; PKHOSE MESSAGE
         JMP     SUPMD100

SUPMD019:CJNE    A,#20,SUPMD020
         MOV     DPTR,#MSG159          ; INITNV MESSAGE
         JMP     SUPMD100

SUPMD020:CJNE    A,#21,SUPMD021
         MOV     DPTR,#MSG160          ; TRNCAT MESSAGE
         JMP     SUPMD100

SUPMD021:CJNE    A,#22,SUPMD022
         MOV     DPTR,#MSG121           ;6501PF
         JMP     SUPMD100

SUPMD022:CJNE    A,#23,SUPMD100
         MOV     DPTR,#MSG48           ; EXIT MESSAGE = RIGHT ARROW
         JMP     SUPMD100

SUPMD100:CALL    SHOWS


SUPMD7:  CALL    KPRESS
         MOV     A,KEY
         JZ      SUPMD7                ; USE KEY != 0 TO SCROLL

         CJNE    A,#MODEKEY,SUPMD70
         MOV     DPTR,#SMODEFLG
         MOVX    A,@DPTR
         JMP     SUPMD71
SUPMD70: JMP     SUPMD13

SUPMD71: CJNE    A,#23,SUPMD71A        ; 1 OF 2 PLACES HAVE TO CHANGE THIS MAGIC NUMBER
         MOV     A,#0
         JMP     SUPMD15
SUPMD71A:INC     A
         JMP     SUPMD15

SUPMD15: MOV     DPTR,#SMODEFLG
         MOVX    @DPTR,A
         JMP     SUPMD000

SUPMD13: CJNE    A,#SSKEY,SUPMD30
         MOV     DPTR,#SMODEFLG
         MOVX    A,@DPTR               ; GET CURRENT SELECTION
         JMP     SUPMD030

SUPMD30: CJNE    A,#RIGHTKEY,SUPMD32
         MOV     DPTR,#SMODEFLG
         MOV     A,#23                 ; 2 OF 2 PLACES HAVE TO CHANGE THIS MAGIC NUMBER
         MOVX    @DPTR,A
SUPMD63: JMP     SUPMD000              ; QUICK TO EXIT WITH RIGHT ARROW

SUPMD32: CJNE    A,#LEFTKEY,SUPMD32A
         MOV     DPTR,#SMODEFLG
         MOV     A,#9
         MOVX    @DPTR,A
         JMP     SUPMD000              ; QUICK TO PRINTER WITH LEFT ARROW

SUPMD32A:CJNE    A,#UPKEY,SUPMD63
         MOV     DPTR,#SMODEFLG
         MOV     A,#8
         MOVX    @DPTR,A
         JMP     SUPMD000              ; QUICK TO REGNUM WITH UP ARROW

SUPMD030:CJNE    A,#0,SUPMD031
         CALL    NEWCODE
         JMP     SUPMD000

SUPMD031:CJNE    A,#1,SUPMD032
         CALL    GETFIRST
         JMP     SUPMD000

SUPMD032:CJNE    A,#2,SUPMD033
         CALL    GETSECND
         JMP     SUPMD000

SUPMD033:CJNE    A,#3,SUPMD034
         CALL    GETTIM
         JMP     SUPMD000

SUPMD034:CJNE    A,#4,SUPMD035
         CALL    GETDAT
         JMP     SUPMD000

SUPMD035:CJNE    A,#5,SUPMD036
         CALL    TKTNUM
         JMP     SUPMD000

SUPMD036:CJNE    A,#6,SUPMD037
         CALL    GETTRK
         JMP     SUPMD000

SUPMD037:CJNE    A,#7,SUPMD038
         CALL    SETMONY
         JMP     SUPMD000

SUPMD038:CJNE    A,#8,SUPMD039
         CALL    GETREGNUM             ; MOVED HERE FROM CAL MODE
         JMP     SUPMD000

SUPMD039:CJNE    A,#9,SUPMD039A
         CALL    GETPRT
         JMP     SUPMD000

SUPMD039A:CJNE    A,#10,SUPMD040
         CALL    GETCOPYS
         JMP     SUPMD000

SUPMD040:CJNE    A,#11,SUPMD041
         CALL    GETLOGOHT
         JMP     SUPMD000

SUPMD041:CJNE    A,#12,SUPMD042
         CALL    SETSSRST
         JMP     SUPMD000

SUPMD042:CJNE    A,#13,SUPMD043
         CALL    SETEMULATE            ; EMULATION SETTING, 0=OFF, 1=TEMP OFF, 2=SLS/PA21, 3=E141
         JMP     SUPMD000              ; PA21 FOR SLS/SUBURBAN CUSTOMERS, E141 FOR P98 CUSTOMERS

SUPMD043:CJNE    A,#14,SUPMD044
         CALL    SETSWAUTH             ; ALLOW MODIFY SOFTWARE AUTHORIZATION
         JMP     SUPMD000

SUPMD044:CJNE    A,#15,SUPMD045
         CALL    SETQOBFLAG            ; TURN ON OR OFF QOB, DEFAULT OFF
         JMP     SUPMD000

SUPMD045:CJNE    A,#16,SUPMD046
         CALL    GTHSTFIX
         JMP     SUPMD000

SUPMD046:CJNE    A,#17,SUPMD047
         CALL    SETPBTNS
         JMP     SUPMD000

SUPMD047:CJNE    A,#18,SUPMD048
         CALL    GETPSRQD
         JMP     SUPMD000

SUPMD048:CJNE    A,#19,SUPMD049
         MOV     DPTR,#BROKNVLV
         MOVX    A,@DPTR
         JNZ     SUPMD048A

         MOV     DPTR,#MSG150          ; BRKVLV
         CALL    SHOW
         MOV     DPTR,#MSG24           ; "OFF" = CANT CHANGE HOSEPACK (FIXED AT OFF)
         CALL    SHOW
         JMP     SUPMD000

SUPMD048A:
         CALL    SETHOSPK              ; BROKEN VALVE ENABLED, LET THEM MODIFY HOSEPACK SETTING
         JMP     SUPMD000

SUPMD049:CJNE    A,#20,SUPMD050
         MOV     DPTR,#MSG31
         CALL    SHOW
         CALL    NVRINIT
         MOV     DPTR,#MSG105
         CALL    SHOW
         JMP     SUPMD000

SUPMD050:CJNE    A,#21,SUPMD051
         CALL    SETTRNC
         JMP     SUPMD000

SUPMD051:CJNE    A,#22,SUPMD052
         CALL    SET65PRT
         JMP     SUPMD000

SUPMD052:CJNE    A,#23,SUPMD099
         JMP     MAIN


SUPMD099:JMP     SUPMD000


;************************************************
; CHECKS POSITION OF CAL SWITCH AND
;  SETS C IF IN CAL MODE
;  CLEARS C IF NOT IN CAL MODE
; NOTE: SWITCH 'SENSE' IS OPPOSITE IN LT THAN EC
;  EXAMPLE: IN ECOUNT: "JNB  CALSW, ADDRIFINCALMODE"    EC: "JNB CALSW" JUMPS IF IN CAL MODE, JB JUMPS IF *NOT* IN CAL MODE
;  EXAMPLE: IN LT    : "JB   CALSW, ADDRIFINCALMODE"    LT: "JB CALSW" JUMPS IF IN CAL MODE, JNB JUMPS IF *NOT* IN CAL MODE
;************************************************
CHKCALMODE:
         CLR     C
         JNB     CALSW,CHKCALMODY      ; ECOUNT LINE 1/2
         JMP     CHKCALMODN            ; ECOUNT LINE 2/2
;         JB      CALSW,CHKCALMODY      ; LT     LINE 1/2
;         JMP     CHKCALMODN            ; LT     LINE 2/2
CHKCALMODY:
         SETB    C
CHKCALMODN:
         RET


; CALMODE TOGGLES BETWEEN PROVE/DISPLAY NET GROSS/ADJUST FACTORS
; MODE KEY TOGGLES, S/S SELECTS

CALMODE:
         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JNC     CALMODEX

         CALL    OPNV1AUX              ; VALVE OPEN FOR DURATION

         MOV     DPTR,#MSG17           ; CALIBRATE
         CALL    SHOW
         MOV     DPTR,#MSG4            ; MODE
         CALL    SHOW

         CALL    SHWVOPEN

         MOV     DPTR,#CMODEFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; START WITH NET/GROSS SELECTION

CALMODEX:
         CALL    WARNEMCHG
         CALL    CLOSVLVS
         CALL    SHWVCLSD
         CALL    DMODMSG
         JMP     MAIN

SETEMVCHG:
         MOV     DPTR,#EMVARCHGD       ; SET FLAG INDICATING EM CONFIG CHANGED WHILE IN CAL MODE
         MOV     A,#1
         MOVX    @DPTR,A
         RET

WARNEMCHG:
         MOV     DPTR,#EMVARCHGD       ; TEST FLAG INDICATING EM CONFIG CHANGED WHILE IN CAL MODE
         MOVX    A,@DPTR
         JZ      WARNEMCX
DOWRNEMCH:
         MOV     DPTR,#MSG102          ; CYCLE
         CALL    SHOW
         MOV     DPTR,#MSG103          ; POWER
         CALL    SHOW
WARNEMCX:RET


CALMOD1: MOV     DPTR,#CMODEFLG
         MOVX    A,@DPTR               ; CHECK CURRENT SELECTION

         CJNE    A,#0,CALMOD30
         MOV     DPTR,#MSG15           ; PRCOD MESSAGE
         JMP     CALMOD6

CALMOD30:CJNE    A,#1,CALMOD2
         MOV     DPTR,#MSG25           ; NET/GROSS MESSAGE
         JMP     CALMOD6

CALMOD2: CJNE    A,#2,CALMOD75
         MOV     DPTR,#MSG104          ; AUTOCAL MESSAGE
         JMP     CALMOD6

CALMOD75:CJNE    A,#3,CALMOD3
         MOV     DPTR,#MSG16           ; CALFAC CALIBRATION FACTOR MESSAGE
         JMP     CALMOD6

CALMOD3: ;CJNE    A,#4,CALMOD3A
         ;MOV     DPTR,#MSG131          ; MP CAL FACTOR MESSAGE (DISABLED E300)
         ;JMP     CALMOD6
         ;INC     A
         ;MOV     DPTR,#CMODEFLG
         ;MOVX    @DPTR,A

CALMOD3A:CJNE    A,#4,CALMOD4
         MOV     DPTR,#MSG11           ; TEMPF MESSAGE
         JMP     CALMOD6

CALMOD4: CJNE    A,#5,CALMOD5
         MOV     DPTR,#MSG10           ; TEMPC MESSAGE
         JMP     CALMOD6

CALMOD5: CJNE    A,#6,CALMOD21A
         MOV     DPTR,#MSG12           ; TABLE MESSAGE
         JMP     CALMOD6

CALMOD21A:
         CJNE    A,#7,CALMOD21B
         MOV     DPTR,#MSG21           ; DIVISOR MESSAGE
         JMP     CALMOD6

CALMOD21B:
         CJNE    A,#8,CALMOD21C
         MOV     DPTR,#MSG233          ; PLSREV MESSAGE
         JMP     CALMOD6

CALMOD21C:
         CJNE    A,#9,CALMOD21D
         MOV     DPTR,#MSG234          ; CHANNL MESSAGE
         JMP     CALMOD6

CALMOD21D:
         CJNE    A,#10,CALMOD14
         MOV     DPTR,#MSG235          ; FLODIR MESSAGE
         JMP     CALMOD6


CALMOD14:CJNE    A,#11,CALMOD25
         MOV     DPTR,#MSG22           ; COMP TABLE MESSAGE
         JMP     CALMOD6

CALMOD25:CJNE    A,#12,CALMOD48        ; SERIAL MESSAGE
         MOV     DPTR,#MSG59
         JMP     CALMOD6

CALMOD48:CJNE    A,#13,CALMOD54
         MOV     DPTR,#MSG61           ; TIMER MSG
         JMP     CALMOD6

CALMOD54:CJNE    A,#14,CALMOD81
         MOV     DPTR,#MSG73           ; REGNUM MESSAGE
         JMP     CALMOD6

CALMOD81:CJNE    A,#15,CALMOD51
         MOV     DPTR,#MSG111          ; U TYPE MESSAGE
         JMP     CALMOD6

CALMOD51:CJNE    A,#16,CALMOD63
         MOV     DPTR,#MSG56           ; UNITS MESSAGE
         JMP     CALMOD6

CALMOD63:CJNE    A,#17,CALMOD66
         MOV     DPTR,#MSG88           ; PGROSS MESSAGE
         JMP     CALMOD6

CALMOD66:CJNE    A,#18,CALMOD69
         MOV     DPTR,#MSG92           ; DECIMAL MESSAGE
         JMP     CALMOD6

CALMOD69:CJNE    A,#19,CALMOD74
         MOV     DPTR,#MSG96           ; AIRSEN MESSAGE
         JMP     CALMOD6

CALMOD74:CJNE    A,#20,CALMOD76
         MOV     DPTR,#MSG97           ; PROBE MESSAGE
         JMP     CALMOD6

CALMOD76:CJNE    A,#21,CALMOD82
         MOV     DPTR,#MSG150          ; BRKVLV MESSAGE
         JMP     CALMOD6

CALMOD82:CJNE    A,#22,CALMOD57
         MOV     DPTR,#MSG208          ; DEMO MESSAGE
         JMP     CALMOD6

CALMOD57:CJNE    A,#23,CALMOD6
         MOV     DPTR,#MSG48           ; EXIT MESSAGE
         JMP     CALMOD6

CALMOD6: CALL    SPLASH                ; SHOW IT

CALMOD7: CALL    KPRESS
         MOV     A,KEY
         JZ      CALMOD7               ; USE KEY != 0 TO SCROLL

         CJNE    A,#MODEKEY,CALMOD60   ; MODEKEY IN CALIBRATION MENU
         MOV     DPTR,#CMODEFLG
         MOVX    A,@DPTR
         JMP     CALMOD61
CALMOD60:JMP     CALMOD13

CALMOD61:
         MOV     TEMP,#0				;Check if CMODEFLG =24 (end of menu) then set to 0 otherwise increment
         MOV     R2,#24					;Not sure why the complicated code
CALX3:   CJNE    A,TEMP,CALX1
         INC     A
         CJNE    A,#24,CALX2
         MOV     A,#0
         JMP     CALX2
CALX1:   INC     TEMP
         DJNZ    R2,CALX3
CALX2:   JMP     CALMOD15


CALMOD15:MOV     DPTR,#CMODEFLG
         MOVX    @DPTR,A
         JMP     CALMOD1

CALMOD13:CJNE    A,#SSKEY,CALMOD62     ; BACK TO TOP
         MOV     DPTR,#CMODEFLG
         MOVX    A,@DPTR               ; GET CURRENT SELECTION
         JMP     CALMOD46

CALMOD62:CJNE    A,#RIGHTKEY,CALMOD64
         MOV     DPTR,#CMODEFLG
         MOV     A,#23
         MOVX    @DPTR,A
         JMP     CALMOD1               ; QUICK TO EXIT WITH RIGHT ARROW

CALMOD64:CJNE    A,#LEFTKEY,CALMOD64A
         MOV     DPTR,#CMODEFLG
         MOV     A,#14
         MOVX    @DPTR,A
CALMOD67:JMP     CALMOD1               ; QUICK TO REGNUM WITH LEFT ARROW

CALMOD64A:
         CJNE    A,#UPKEY,CALMOD67
         MOV     DPTR,#CMODEFLG
         MOV     A,#7
         MOVX    @DPTR,A
         JMP     CALMOD1               ; QUICK TO MRATIO WITH UP ARROW


CALMOD46:CJNE    A,#0,CALMOD16
         CALL    GETPRCODE
         CALL    SETCMP
         JMP     CALMOD1

CALMOD16:CJNE    A,#1,CALMOD77
         JMP     DISPNG                ; EXITS TO CALMOD1

CALMOD77:CJNE    A,#2,CALMOD17
         MOV     DPTR,#MSG42
         CALL    SHOW
         MOV     DPTR,#MSG106
         CALL    SHOW
         CALL    GETENT
         MOV     DPTR,#MSG31
         CALL    SHOWX
         CALL    AUTOCAL
         JMP     CALMOD1

CALMOD17:CJNE    A,#3,CALMOD86
         CALL    GETFAC
         CALL    SETADJ
         JMP     CALMOD1

CALMOD86:;CJNE    A,#4,CALMOD18 		;Disabled VE300
         ;CALL    SETMPCAL
         ;JMP     CALMOD1

CALMOD18:CJNE    A,#4,CALMOD19
CALMOD31:CALL    TEMPF
         CALL    WAIT500
         CALL    LOADDP
         CALL    KPRESS
         MOV     A,KEY
         JZ      CALMOD31
         CJNE    A,#SSKEY,CALMOD31
         CALL    SETADJ
         JMP     CALMOD1

CALMOD19:CJNE    A,#5,CALMOD20
CALMOD32:CALL    TEMPC
         CALL    WAIT500
         CALL    LOADDP
         CALL    KPRESS
         MOV     A,KEY
         JZ      CALMOD32
         CJNE    A,#SSKEY,CALMOD32
         CALL    SETADJ
         JMP     CALMOD1

CALMOD20:CJNE    A,#6,CALMOD23A
CALMOD33:CALL    SHOWTBL
         CALL    WAIT1000
         CALL    KPRESS
         MOV     A,KEY
         JZ      CALMOD33
         CJNE    A,#SSKEY,CALMOD33
         JMP     CALMOD1

CALMOD23A:CJNE    A,#7,CALMOD23B
         CALL    GETDIVISOR
         CALL    SETADJ
         CALL    SETEMVCHG
         CALL    DOWRNEMCH
         JMP     CALMOD1


CALMOD23B:CJNE    A,#8,CALMOD23C
         CALL    SETPLSREV
         CALL    SETADJ
         CALL    SETEMVCHG
         CALL    DOWRNEMCH
         JMP     CALMOD1

CALMOD23C:CJNE    A,#9,CALMOD23D
         CALL    SETCHANNL
         CALL    SETADJ
         CALL    SETEMVCHG
         CALL    DOWRNEMCH
         JMP     CALMOD1

CALMOD23D:CJNE    A,#10,CALMOD34
         CALL    SETFLODIR
         CALL    SETADJ
         CALL    SETEMVCHG
         CALL    DOWRNEMCH
         JMP     CALMOD1


CALMOD34:CJNE    A,#11,CALMOD35

         CALL    CHKTBLS               ; CHECK FOR LP IN TABLE1 AND ETHANOL IN TABLE9
         JNC     CALMOD34A             ; IF CARRY SET TABLES LOADED

         MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JNZ     CALMOD34A             ; SHOW 'NO COMP' IF IN MASS MODE

         CALL    GETCMP                ; COMP >0 & VALID
         CALL    SETCMP                ; SET COMP FLAG
         CALL    SETCONF
         JMP     CALMOD1

CALMOD34A:
         MOV     DPTR,#MSG62           ; NOCOMP MESSAGE
         CALL    SPLASH
         MOV     COMP,#0
         JMP     CALMOD1

CALMOD35:CJNE    A,#12,CALMOD36
         CALL    GETSER
         JMP     CALMOD1

CALMOD36:CJNE    A,#13,CALMOD43
         CALL    SETTIMER
         JMP     CALMOD1

CALMOD43:CJNE    A,#14,CALMOD79
         CALL    GETREGNUM             ; ALSO IN SETUP MODE, GET IT RIGHT!
         JMP     CALMOD1

CALMOD79:CJNE    A,#15,CALMOD50
         CALL    TUNITS
         JMP     CALMOD1

CALMOD50:CJNE    A,#16,CALMOD65
         CALL    VUNITS
         JMP     CALMOD1

CALMOD65:CJNE    A,#17,CALMOD56
         CALL    SETPRGRS
         JMP     CALMOD1

CALMOD56:CJNE    A,#18,CALMOD71
         CALL    SETDECML
         JMP     CALMOD1

CALMOD71:CJNE    A,#19,CALMOD72
         CALL    SETAIRIX
         JMP     CALMOD1

CALMOD72:CJNE    A,#20,CALMOD83
         CALL    SETPROBE
         JMP     CALMOD1

CALMOD83:CJNE    A,#21,CALMOD84
         CALL    SETBRKNVLV
         JMP     CALMOD1

CALMOD84:CJNE    A,#22,CALMOD68
         CALL    SETDEMO
         JMP     CALMOD1

CALMOD68:CJNE    A,#23,CALMOD59
         JMP     PRV12

CALMOD59:JMP     CALMOD1


SETCONF: MOV     DPTR,#CONFIG
         MOV     A,#1
         MOVX    @DPTR,A
         RET

SETADJ:  MOV     DPTR,#ADJUST
         MOV     A,#1
         MOVX    @DPTR,A
         RET



SPLASH:
         PUSH    DPL
         PUSH    DPH
         CALL    LF0018

         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JC      SPLASH1

         MOV     A,#00H
SPLASH1: MOVX    @DPTR,A
         POP     DPH
         POP     DPL
         CALL    SHOWLCD
         RET

;SHOW:
;         PUSH    DPL
;         PUSH    DPH
;         CALL    LF0000
;         POP     DPH
;         POP     DPL
;         CALL    SHOWLCD
;         RET

SHOW:
         PUSH    DPL
         PUSH    DPH
         CALL    LF0000
         POP     DPH
         POP     DPL
         CALL    SHOWLCD
         RET

SHOWS:
         PUSH    DPL
         PUSH    DPH
         CALL    LF0010
         POP     DPH
         POP     DPL
         CALL    SHOWLCD
         RET

SHOWX:
         PUSH    DPL
         PUSH    DPH
         CALL    LF0000
         POP     DPH
         POP     DPL
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD
         RET

;SHOWLCD:
;         MOV     COMMAND,#LOADLCD
;         CALL    XLOAD
;         CALL    WAIT1000
;         RET

SHOWLCD:
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD
         CALL    WAIT1000
         RET


SHOWVL:  MOV     COMMAND,#LOADLCD
         CALL    VLOAD
         RET


DEL100:
         MOV     DPTR,#SWAUTHFLG
         MOVX    A,@DPTR
         JZ      DEL103                ; CHECK FMS SWAUTH FLAG AND SKIP IF OFF

         MOV     DPTR,#RSTARM
         MOVX    A,@DPTR
         CJNE    A,#0,DEL103           ; ARMED SO CHECK HARDWARE AUTH
         CALL    CLOSVLVS
         JMP     DEL10
DEL103:
         MOV     DPTR,#FLTIME
         MOVX    A,@DPTR
         JZ      DEL1Z                 ; IF FLEET TIME ZERO SKIP

         MOV     DPTR,#FLTIME+1        ; TIMER TRIP OVERRIDE BYTE
         MOVX    A,@DPTR
         JZ      DEL1X2                ; IF FLTIME+1 ZERO, FLOW MUST OCCUR BEFORE TRIPPED

         MOV     DPTR,#TTRIP           ; IF FLTIME+1 NON-ZERO, IF NOT TRIPPED THEN TRIP AND RESET TIMER
         MOVX    A,@DPTR
         JNZ     DEL1X3                ; OTHERWISE JUST CONTINUE AS NORMAL

;         CALL   VOLUMCHK              ; IF NO VOLUME THEN DON'T TRIP, THE NO-FLOW RESET OVERRIDE IS ONLY
;         CJNE   A,#0,DEL1X2           ; ENABLED UNTIL FIRST FLOW

         MOV     DPTR,#TTRIP           ; SET TRIPPED AND LAST TIME AS IF FLOW HAD OCURRED
         MOV     A,#1
         MOVX    @DPTR,A               ; SET FLEET TIMER TRIP

         MOV     DPTR,#FLTIME
         MOVX    A,@DPTR
         MOV     DPTR,#FTLEFT
         MOVX    @DPTR,A               ; LOAD OR RELOAD FLEET TIMEOUT

DEL1X2:  MOV     DPTR,#TTRIP
         MOVX    A,@DPTR
         JZ      DEL1Z                 ; CHECK IF FLEET TIMER TRIPPED

DEL1X3:
         MOV     DPTR,#RTIMECNTR
         MOVX    A,@DPTR
         INC     A
         MOVX    @DPTR,A
         JNZ     DEL1X3A

         CALL    RTIME                 ; GET TIME

DEL1X3A:
         MOV     DPTR,#LASTSEC         ; SEE IF CURRENT SECONDS
         MOVX    A,@DPTR               ; SAME AS LAST
         CJNE    A,DATEIMG+1,DEL1X
         JMP     DEL1Z                 ; SAME DO NOTHING

DEL1X:   MOV     DPTR,#LASTSEC         ; DIFFERENT SO SAVE
         MOV     A,DATEIMG+1
         MOVX    @DPTR,A

;!! WHAT IS THIS?? IT WAS UNCOMMENTED IN E180
;         CPL     RPRVK

         MOV     DPTR,#FTLEFT          ; DECREMENT TIME LEFT
         MOVX    A,@DPTR
         DEC     A

         MOVX    @DPTR,A               ; EITHER WAY STORE IT BACK

         JNZ     DEL1Z                 ; CHECK FOR DONE

         MOV     DPTR,#TTRIP
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR TRIP FLEET TIMER TRIP

         CALL    TESTV1V2              ; CHECK VALVES CLOSED
         JC      DEL1X1                ; IF EITHER VALVE OPEN, SAME AS SSKEY

         JMP     DEL1Z
DEL1X1:  JMP     DEL39


; *********

DEL1Z:
         CALL    SETCMP

         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#2,DEL1Z_SLS0       ; 2=PA21, ELSE CONTINUE

         CALL    INPA                  ; LOOK FOR SLSMODE CHARS DURING OUTER LOOP ONLY
         JNC     TSTHSTFXDOX           ; NO SLSMODE CHAR CONTINUE TESTS, THIS JUMPS TO DEL35..

         CJNE    A,#'+',TSTHSTFXDOX    ; IGNORE CHAR IN SLSMODE IN OUTER LOOP (FOR NOW)
                                       ; SLSMODE, '+' IS EC REMOTE PRINT FOR SLS MODE
         CALL    LF0000
         MOV     DPTR,#MSG231          ; 'REMOTE' = REMOTE PRINT BOX SAID REMOTE PRINT
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD

         CALL    WAIT3
         CALL    WAIT3
         CALL    WAIT3
         CALL    WAIT3

         JMP     DEL37B                ; JUMP TO CODE WHERE <PRINT> ENDS PUMP & PRINT DELIVERY

DEL1Z_SLS0:
         CALL    INPA
         JC      TSTHSTFXDO
         JMP     DEL35                 ; NO CHAR CONTINUE TESTS

TSTHSTFXDO:
         CALL    DOHOSTFIX
         JC      DEL1A                 ; GOT TILDE CHARS OK OR HOST FIX OFF, JUMP TO COMMAND CHAR TESTS
TSTHSTFXDOX:
         JMP     DEL35                 ; HOST FIX ON AND IGNORING CHAR(S) IN OUTER LOOP (AND INNER)
                                       ; OTHER FUNCTIONS JUMP HERE ...

DEL1A:   CJNE    A,#'J',DEL41
         CALL    SENDSTAT
         CALL    SENDVOL
         CALL    CHKSUM
         JMP     DEL35

DEL41:   CJNE    A,#'T',DEL38A
         CALL    OUTA
         CALL    SENDTMP               ; BUILD AND SEND TEMPORARY DELIVERY DATA WHILE DELIVERY IS STILL ACTIVE
         JMP     DEL3                  ; INTERIM 'T' VALUES

DEL38A:  CJNE    A,#'V',DEL106
         CALL    OUTA
         CALL    SENDVER
         JMP     DEL3

DEL106:  CJNE    A,#'y',DEL38B         ; FMS ARM COMMAND ALLOWS SSKEY AND
         CALL    OUTA                  ; CAN END DELIVERY
         MOV     DPTR,#RSTARM
         MOV     A,#0
         MOVX    @DPTR,A
         CALL    SENDPIPE
         JMP     DEL3

DEL38B:  CJNE    A,#'P',DEL38
         CALL    OUTA
         CALL    PRODST
         CALL    SENDPIPE
         JMP     DEL3

DEL38:   CJNE    A,#'E',DEL38D         ; PRESET / HOST MODE COMMAND FOR 0000.0 - 9999.9 (E176 AND OLDER, NEED TO KEEP BACKWARDS COMPATIBLE)
         MOV     B,A                   ; SAVE E
         MOV     DPTR,#PS6FLG
         MOV     A,#0
         MOVX    @DPTR,A               ; ASSUME REGULAR PRESET UNTIL 'A' COMMAND
DEL38X1:
         MOV     A,B                   ; GET E BACK (THEREFORE THE JMP MIGHT NEED TO GO TO ELSEWHERE INSTEAD OF THE CALLS TO PULSFLG1!)
         CALL    OUTA
         MOV     DPTR,#PS6FLG
         MOVX    A,@DPTR
         JZ      DEL38X3
         CALL    LD6PRST
         JMP     DEL38X4
DEL38X3: CALL    LOADPRST              ; LOADS PRCOD,PRESET,TIMER
DEL38X4: MOV     PULSFLG1,#0
         MOV     PULSFLG2,#0
DEL38X2: MOV     DPTR,#SSFLAG          ; SEE IF VALVES OPEN
         MOVX    A,@DPTR
         JZ      DEL38C                ; CLOSED (HIDE DELIVER LEGEND)
         CALL    DLASTV                ; OPEN (SHOW DELIVER LEGEND)
         JMP     DEL3
DEL38C:  CALL    LASTVOL
         JMP     DEL3

DEL38D:  CJNE    A,#'A',DEL40          ; PRESET / HOST MODE COMMAND FOR 00000.0 - 99999.9 (E177 AND NEWER), THIS IS RE-PRESET DURING DELIVERY
         MOV     B,A
         MOV     DPTR,#PS6FLG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     DEL38X1

DEL40:   CJNE    A,#'K',DEL37C
         CALL    OUTA
         MOV     DPTR,#KPUMP
         MOV     A,#1
         MOVX    @DPTR,A
         MOV     DPTR,#TTRIP
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR TRIP FLEET TIMER TRIP
         JMP     DEL39                 ; TREAT LIKE S/S PRESSED

DEL37C:  CJNE    A,#'O',DEL37
         CALL    OUTA
         CALL    GETFLT
         JMP     DEL3

DEL37:   CJNE    A,#'N',NOCMDDELO
         CALL    OUTA

         MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JZ      DEL37A                ; NOT HOST MODE, NEED TO SEND PIPE

         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#3,DEL37B           ; 3=E141 EMULATION, ELSE SKIP

         MOV     A,#'1'
         CALL    OUTA                  ; TELL P98 WERE IN HOST TO EMULATE E141

         JMP     DEL37B                ; HOST MODE, PIPE WILL BE SENT

DEL37A:  MOV     DPTR,#HDPFLG          ; SET FLAG TO DO PIPE SINCE NOT HOST MODE BUT IN HOST CMD
         MOV     A,#1
         MOVX    @DPTR,A

         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#3,DEL37B           ; 3=E141 EMULATION, ELSE SKIP

         MOV     A,#'0'
         CALL    OUTA                  ; TELL P98 WERE NOT IN HOST MODE TO EMULATE E141

DEL37B:
         CALL    TESTV1V2              ; CHECK VALVES CLOSED, C IS SET IF EITHER IS OPEN
         CALL    CLOSVLVS
         JNC     DEL37B2               ; IF VALVES WERE ALREADY CLOSED DON'T NEED TO TELL THEM
         CALL    SHWVCLSD

DEL37B2:
         MOV     DPTR,#SSFLAG
         MOV     A,#0
         MOVX    @DPTR,A               ; INSURE VALVE FLAG IS CLEARED

         JMP     DEL10                 ; HOST SAYS TO EXIT

NOCMDDELO:                             ; NOMORE COMMAND CHARS TO TEST
         CLR     C                     ; IGNORE CHAR IN A
         JMP     DEL35


DEL35:   MOV     DPTR,#ETFLG
         MOVX    A,@DPTR
         JZ      DEL11                 ; NO TIMER- SKIP
         MOV     DPTR,#TORFLG
         MOVX    A,@DPTR
         JNZ     DEL11                 ; ET ON BUT OVERRIDE ON

         MOV     DPTR,#ETFLG           ; 0=OFF, 1=3 MIN AFTER FLOW, 2=3MIN NO FLOW REQ'D, 3=30SEC AFTER FLOW, 4=30 SEC NO FLOW REQ'D
         MOVX    A,@DPTR
         CJNE    A,#1,DEL35A1
         JMP     DEL35B                ; 1=3 MIN AFTER FLOW
DEL35A1: CJNE    A,#2,DEL35A2
         JMP     DEL35C                ; 2=3 MIN NO FLOW REQ'D
DEL35A2: CJNE    A,#3,DEL35C           ; 4=30 SEC NO FLOW REQ'D
         JMP     DEL35B                ; 3=30 SEC AFTER FLOW

DEL35B:                                ; 1,3=FLOW REQUIRED BEFORE TIMEOUT FIRES
         MOV     DPTR,#TEN
         MOVX    A,@DPTR
         JZ      DEL11                 ; JUMP IF NOT ENOUGH UNITS PUMPED YET

DEL35C:                                ; HASN'T TIMED OUT YET, BUMP TIMEOUT LOOP COUNTER
         CALL    BUMPTMT

         MOV     DPTR,#ETFLG           ; 0=OFF, 1=3 MIN AFTER FLOW, 2=3MIN NO FLOW REQ'D, 3=30SEC AFTER FLOW, 4=30 SEC NO FLOW REQ'D
         MOVX    A,@DPTR
         CJNE    A,#1,DEL35D1
         JMP     DEL35E                ; 1=3 MIN AFTER FLOW
DEL35D1: CJNE    A,#2,DEL35D2
         JMP     DEL35E                ; 2=3 MIN NO FLOW REQ'D
DEL35D2: CJNE    A,#3,DEL35F           ; 4=30 SEC NO FLOW REQ'D
         JMP     DEL35F                ; 3=30 SEC AFTER FLOW

DEL35E:                                ; 1,2=3 MINUTES BEFORE TIMEOUT FIRES
; UPDATED FOR E200E0-C
         MOV     DPTR,#TMT+2
         MOVX    A,@DPTR
;         CJNE    A,#01H,DEL11         ; E180REV6 EC:2334=91Eh=180SEC.
;         CJNE    A,#09H,DEL11
         CJNE    A,#020H,DEL11         ; DS2 UPDATE EC:8300=206Ch=180SEC.
         MOV     DPTR,#TMT+1
         MOVX    A,@DPTR
;         CJNE    A,#0B9H,DEL11
;         CJNE    A,#056H,DEL11
         CJNE    A,#06CH,DEL11
         JMP     DEL35G

DEL35F:                                ; 3,4=30 SECONDS BEFORE TIMEOUT FIRES
; UPDATED FOR E200E0-C
         MOV     DPTR,#TMT+2
         MOVX    A,@DPTR
;         CJNE    A,#01H,DEL11          ; E200E0-C EC:389=185h=30SEC.
         CJNE    A,#05H,DEL11          ; DS2 EC:1285=505h=30SEC.

         MOV     DPTR,#TMT+1
         MOVX    A,@DPTR
;         CJNE    A,#04BH,DEL11
;         CJNE    A,#085H,DEL11
         CJNE    A,#005H,DEL11

DEL35G:
         CALL    CLOSVLVS
         CALL    SHWVCLSD

         MOV     DPTR,#SSFLAG
         MOV     A,#0
         MOVX    @DPTR,A               ; INSURE VALVE FLAG IS CLEARED

         MOV     DPTR,#TIMEOUT
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG TIMEOUT OCCURRED

DEL31:
         JMP     DEL10                 ; TIMED OUT - - PRINT TICKET
DEL11:
         MOV     DPTR,#STOPFLG         ; DELIVERY STOPPED FROM PRESET OR S/S
         MOVX    A,@DPTR
         JZ      DEL24
         MOV     A,#0                  ; <<<<
         MOVX    @DPTR,A               ; CLEAR STOPFLG - ONLY NEED ONCE


DEL11A:  MOV     DPTR,#SSFLAG
         MOVX    @DPTR,A               ; TOGGLE SSFLAG

         CALL    SHWVCLSD

         CALL    LASTVOL

         MOV     DPTR,#KPUMP
         MOVX    A,@DPTR
         JZ      DEL24A
         CALL    SENDPIPE              ; PIPE FROM K IN PUMP LOOP
DEL24A:  MOV     A,#0
         MOVX    @DPTR,A               ; RESET FLAG


DEL23B:  JMP     DEL3                  ; (LONG JUMP)

DEL24:   CALL    KPRESS
         JC      DEL23
         JMP     DEL22
DEL23:   MOV     A,KEY
         CJNE    A,#PRINTKEY,DEL18     ; PRINT 2
; 01/15/2014 PPBTNENBL: FLAG FOR PRESET+PRINT BUTTONS ENABLED: 0=BOTH(00), 1=PRINT ONLY(01), 2=PRESET ONLY(10), 3=NEITHER(11)
;E300EA - switched logic so 0 = enabled so defaults work better JC
         MOV     DPTR,#PPBTNENBL       ; CHECK IF PRINT BUTTON ENABLED
         MOVX    A,@DPTR
         ANL     A,#02H                ; AND WITH 2, IF ZERO SET THEN PRINT DISABLED
         JNZ      DEL23B                ; PRINT DISABLED, IGNORE AND JUMP TO END OF KEY-CHECKING ... DEL23B->DEL3
DEL23A:                                ; <LT>  SSKEY GOES HERE
         CALL    CLOSVLVS              ; NEED TO CLOSE VALVES IN CASE BUSY

         MOV     DPTR,#SSFLAG
         MOV     A,#0
         MOVX    @DPTR,A               ; FLAG VALVES CLOSED

         MOV     DPTR,#PRTPRSD
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG PRINT KEY PRESSED
         JMP     DEL10

DEL18:   CJNE    A,#SSKEY,DEL18A       ; S/S KEY TOGGLES VALVES
         JMP     DEL18B
DEL18A:  JMP     DEL21                 ; CJMP OUT OF RANGE
DEL18B:  CALL    TESTV1V2              ; CHECK VALVES CLOSED, C IS SET IF EITHER IS OPEN
         JC      DEL39                 ; IF VALVES ARE CLOSED, NEED TO CHECK AND SEE IF RE-OPEN DISABLED IN HOST MODE

         MOV     DPTR,#HOST            ; 0=PUMP&PRINT, 1=HOST
         MOVX    A,@DPTR
         JZ      DEL39


         MOV     DPTR,#CMDFLAG+7       ; BYTE7(0-9) = HOST OVERRIDE OF SSRST SETTING
         MOVX    A,@DPTR
         CJNE    A,#'0',DEL18C0        ; E179_A: LET SSRST OVERRIDE FROM HOST
         JMP     DEL18C                ; ITS 0 = NOT OVERRIDEN, CHECK SETTING
DEL18C0:
         CJNE    A,#'3',DEL39          ; SSRST HOST OVERRIDE: 0=USE SSRSTFLG, 1=OFF, 2=ON, 3=NSSOHM:NO S/S TO RE-OPEN VALVES IN HOST MODE
         JMP     DEL18D                ; HOST SAYS NO
DEL18C:
         MOV     DPTR,#SSRSTFLG        ; SSRST: 0=ON, 1=OFF, 2=NSSOHM:NO S/S TO OPEN VALVES IN HOST MODE
         MOVX    A,@DPTR
         CJNE    A,#2,DEL39            ; IF IT'S NOT 2 THEN CONTINUE
DEL18D:
         MOV     DPTR,#MSG117          ; SSRST ...  2=OFF, S/S RE-OPEN VALVES NOT ALLOWED IN HST MODE DELIVERY
         CALL    SHOW
         MOV     DPTR,#MSG141          ; NSSOHM
         CALL    SHOW
         CALL    LASTVOL
         JMP     DEL3                  ; BACK TO LOOKING FOR PULSES


DEL39:   MOV     DPTR,#SSFLAG          ; ENTRY FOR 'K' OPEN COMMAND
         MOVX    A,@DPTR
         JZ      DEL19                 ; IF OFF THEN OPEN VALVES

         MOV     DPTR,#SSFLAG          ; SET SSFLAG APPROPRIATELY
         MOV     A,#0
         MOVX    @DPTR,A

         CALL    CLOSVLVS
         CALL    SHWVCLSD

         CALL    LASTVOL

         MOV     DPTR,#KPUMP
         MOVX    A,@DPTR
         JZ      DEL20A
         CALL    SENDPIPE
DEL20A:  MOV     A,#0
         MOVX    @DPTR,A               ; RESET FLAG
         JMP     DEL3

DEL19:   MOV     DPTR,#SSFLAG
         MOV     A,#1                  ; SSFLAG
         MOVX    @DPTR,A

         CALL    SHWVOPEN

         MOV     DPTR,#STOPFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; FLAG DELIVERY ON AGAIN
         CALL    DLASTV

         CALL    OPNV1AUX

         MOV     DPTR,#FIRST
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET FLAG FROM PRESET
         MOV     DPTR,#KPUMP
         MOVX    A,@DPTR
         JZ      DEL19A
         CALL    SENDPIPE
DEL19A:  MOV     A,#0
         MOVX    @DPTR,A               ; RESET FLAG

         CALL    ISDEMO
         JNC     DEMO1

         MOV     DPTR,#DEMOFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; TURN ON DEMO COUNT

         MOV     DPTR,#DEMOCTR1
         MOV     A,#0
         MOVX    @DPTR,A
         MOV     DPTR,#DEMOCTR2
         MOVX    @DPTR,A               ; RESET DEMO COUNTERS

DEMO1:   JMP     DEL3

DEL21:   CJNE    A,#RIGHTKEY,DEL43
;USED FOR RE-PRICING DURING EC DELIVERY
         MOV     DPTR,#MONEYFLG        ; PRICING ENABLED: 0=OFF,1=PRICE CHECK,2=ALWAYS ON
         MOVX    A,@DPTR
         JZ      DEL43

         MOV     DPTR,#CLOSED
         MOV     A,#1                  ; FLAG VALVES CLOSED TO START
         MOVX    @DPTR,A

         CALL    TESTV1V2              ; CHECK VALVES CLOSED
         JNC     DEL44

         CALL    CLOSVLVS
         CALL    SHWVCLSD

         MOV     DPTR,#CLOSED
         MOV     A,#0
         MOVX    @DPTR,A

DEL44:   MOV     DPTR,#MSG78
         CALL    SHOW
         CALL    GETPCPR
         CALL    CONV2BCD
         CALL    GETPCE

         MOV     DPTR,#CLOSED
         MOVX    A,@DPTR
         JNZ     DEL44A

         CALL    SHWVOPEN

         MOV     DPTR,#FIRST
         MOVX    A,@DPTR
         JNZ     DEL44B

         CALL    OPNV1AUX

         CALL    DLASTV
         JMP     DEL44C

DEL44B:  CALL    OPNV2AUX
         CALL    DLASTV
         JMP     DEL44C

DEL44A:  CALL    LASTVOL
DEL44C:  JMP     DEL3

DEL43C:  JMP     DEL3                  ; (LONG JUMP)

DEL43:   CJNE    A,#PRSETKEY,DEL22     ; PRESET KEY 2, DURING DELIVERY
; 01/15/2014 PPBTNENBL: FLAG FOR PRESET+PRINT BUTTONS ENABLED: 0=BOTH(00), 1=PRINT ONLY(01), 2=PRESET ONLY(10), 3=NEITHER(11)
;E300EA - switched logic so 0 = enabled so defaults work better JC
         MOV     DPTR,#PPBTNENBL       ; CHECK IF PRESET BUTTON ENABLED
         MOVX    A,@DPTR
         ANL     A,#01H                ; AND WITH 1, IF ZERO SET THEN PRESET DISABLED
         JNZ      DEL43C                ; PRESET DISABLED, IGNORE AND JUMP TO END OF KEY-CHECKING ... DEL22->DEL3
         CALL    CHKDEC
         CJNE    A,#2,DEL43B
         MOV     DPTR,#MSG63           ; ERR, NO PRESET IF DECIMAL SET TO 2
         CALL    SHOW
         JMP     DEL22

DEL43B:  MOV     DPTR,#PRSSET
         MOVX    A,@DPTR
         JZ      DEL43A
         MOV     DPTR,#MSG3            ; PRESET
         CALL    SHOW
         MOV     DPTR,#MSG120          ; ACTIVE
         CALL    SHOW
         JMP     DEL19

DEL43A:  MOV     DPTR,#SSFLAG
         MOV     A,#0
         MOVX    @DPTR,A               ; FLAG STOPPED LIKE S/S

         CALL    TESTV1V2              ; CHECK VALVES CLOSED  ;TEST 3
         JNC     DEL26

         CALL    CLOSVLVS
         CALL    SHWVCLSD

DEL26:   MOV     DPTR,#TEMPPRST        ; TEMP SAVE CURRENT PRESET
         MOV     A,PRESET
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,PRESET+1
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,PRESET+2            ; SHOULD BE RIGHT ORDER
         MOVX    @DPTR,A

         MOV     DPTR,#REPRFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; FLAG REPERSET TO AVOID FIXED CHARGES

         CALL    GETPRST               ; GET NEW PRESET
         CLR     C
         MOV     DPTR,#TEMPPRST        ; GET OLD PRESET
         MOVX    A,@DPTR
         ADDC    A,PRESET
         DA      A
         MOV     PRESET,A
         INC     DPTR
         MOVX    A,@DPTR
         ADDC    A,PRESET+1
         DA      A
         MOV     PRESET+1,A
         INC     DPTR
         MOVX    A,@DPTR
         ADDC    A,PRESET+2
         DA      A
         MOV     PRESET+2,A            ; NEW PRESET ADDED

         MOV     PULSFLG1,#0           ; AGAIN FOR RE-PRESET
         MOV     PULSFLG2,#0           ; CLEAR PRESET FLAGS
         JMP     DEL19
DEL22:   JMP     DEL3                  ; BACK TO LOOKING FOR PULSES

; DELIVERY HAS ENDED IF HERE.

DEL10:
         MOV     DPTR,#SWAUTHFLG
         MOVX    A,@DPTR
         JZ      DEL10D

         MOV     DPTR,#RSTARM
         MOV     A,#0
         MOVX    @DPTR,A               ; DISARM IF MANUAL S/S OUT OF DELIVERY

DEL10D:  MOV     DPTR,#FLTIME
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR FLEET TIME

         MOV     DPTR,#FLTIME+1
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR FLEET TIME OVERRIDE

         MOV     DPTR,#GETSTAT
         MOV     A,#1
         MOVX    @DPTR,A
         CALL    SENDSTAT
         MOV     A,TEMP
         MOV     DPTR,#STATUS
         MOVX    @DPTR,A               ; SAVE LAST STATUS

         MOV     DPTR,#DECMLADJ
         MOVX    A,@DPTR
         JNZ     DEL10B

         CALL    RTIME                 ; ON POW FAIL THIS WILL BE CALLED TWICE, NEED TO SKIP SECOND CALL
         MOV     DPTR,#FTIME           ; MOVED TO HERE SO ONLY CALLED ONCE, FIXED E178F REV 3
         CALL    SAVTIME               ; SAVE FINISH TIME

         MOV     DPTR,#NVOL
         CALL    ROTVOL
         MOV     DPTR,#NVOL
         CALL    RLVOL
         MOV     DPTR,#GVOL
         CALL    ROTVOL
         MOV     DPTR,#GVOL
         CALL    RLVOL

         MOV     DPTR,#DECMLADJ
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG ROTATION DONE FOR POWFAIL

DEL10B:
         CALL    TESTV1V2              ; CHECK VALVES OPEN/CLOSED
         JNC     DEL25                 ; SKIP CLOSED MESSAGE

         CALL    CLOSVLVS
         CALL    SHWVCLSD

         CALL    GETUNIT
DEL25:   MOV     A,COMP
         JZ      DEL28
         MOV     DPTR,#LGDFLG1
         MOV     A,#41H
         JNB     PSW_F0,U1
         ANL     A,#9FH
         ORL     A,#20H
U1:      MOVX    @DPTR,A
         MOV     DPTR,#LGDFLG2
         MOV     A,#05H
         JNB     PSW_F0,U2
         ANL     A,#0F9H
         ORL     A,#02H
U2:      MOVX    @DPTR,A
         JMP     DEL29

DEL28:   CALL    GETUNIT

         CALL    LGD101

         MOV     DPTR,#LGDFLG2
         MOV     A,#04H
         JNB     PSW_F0,U31
         ANL     A,#0F9H
         ORL     A,#02H
U31:     MOVX    @DPTR,A

DEL29:
         CALL    SHOWVL
         MOV     COMMAND,#DISPVOL      ; SHOW LAST DM VOLUME
         CALL    VLOAD

         MOV     DPTR,#TOTALADJ        ; ONLY NEED TO DO ADD TOTALIZER 1 TIME PER PRINT
         MOVX    A,@DPTR               ; ON POW FAIL THIS WILL BE CALLED TWICE, NEED TO SKIP SECOND CALL
         JNZ     DEL29A0               ; FIXED E178F REV 3

         CALL    COMPQOB               ; SHOULD HAVE MOVED THIS CALL TO UPDATE QOB AS WELL, FIXED 179REV9 12/31/2014
         CALL    ADDTOT                ; CALL 1
         CALL    ADDPTOT

         MOV     DPTR,#TOTALADJ
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG TOTALIZER UPDATED FOR POWFAIL

DEL29A0:
         MOV     PRESET,#0
         MOV     PRESET+1,#0
         MOV     PRESET+2,#0

DEL29A:  MOV     DPTR,#POWFAIL
         MOVX    A,@DPTR
         JZ      DEL36A
         JMP     DEL36B                ; BYPASS HOST IF POWER FAILURE

DEL36A:  MOV     DPTR,#HOST            ; PROBLEM WITH HOST POWERDOWN IS RIGHT HERE
         MOVX    A,@DPTR
         JNZ     DEL36                 ; DETERMINE IF HOST PRINTS

         MOV     DPTR,#EMULATEFLG      ; IF PA21 SLSMODE THEN DO IT
         MOVX    A,@DPTR               ; 2=PA21, ELSE CONTINUE
         CJNE    A,#2,DEL36B           ; DEL36B = DONE WITH ALL SLSMODE STUFF PRE-PRINTING

         MOV     A,#'Z'                ; Z+CRLF = TELL LAPTOP WE ARE DONE PRINTING
         CALL    OUTA
         CALL    CRLF

         MOV     R1,#0
         MOV     R2,#80
         MOV     R3,#17

SLSWAIT1:CALL    INPA                  ; WAIT HERE FOR HOST TO SEND I
         JC      SLSWA1Y               ; GOT CHAR
         DJNZ    R1,SLSWAIT1
         DJNZ    R2,SLSWAIT1
         DJNZ    R3,SLSWAIT1

         JMP     DEL36B                ; NO CHAR = BAIL

SLSWA1Y: CJNE    A,#'I',DEL36B         ; IT'S NOT AN I ASKING FOR DATA = BAIL

         CALL    OUTA
         CALL    CRLF                  ; SEND DELIVERY DATA IN PA21 FORMAT = 82 CHARS
         CALL    SENDIDEL              ; SENDS PIPE+CRLF AT END OF DELIVERY DATA

         MOV     R3,#17
SLSWNXTP:
         MOV     R1,#0
         MOV     R2,#80

SLSWAIT2:CALL    INPA                  ; WAIT HERE FOR HOST TO SEND P
         JC      SLSWA2Y               ; GOT CHAR
         DJNZ    R1,SLSWAIT2
         DJNZ    R2,SLSWAIT2
         DJNZ    R3,SLSWAIT2

         JMP     DEL36B                ; NO CHAR = BAIL

SLSWA2Y: CJNE    A,#'P',DEL36B         ; IT'S NOT A P TRYING TO PRINT LINE = BAIL

         CALL    DOSLSSAVAL            ; ECHO P+CRLF
                                       ; INCREMENT ABYTE BUFFER COUNT
                                       ; WAIT FOR 25 CHARS
                                       ; MOVE DATA TO ABYTES
                                       ; ECHO EACH CHAR
                                       ; ECHO PIPE+CRLF AT END

                                       ; BAIL FROM HERE IF NO NEXT IN TIME
         MOV     R3,#5                 ; 17=10.2 SEC, 9=5.4 SEC, 5=3.2 SEC
         JMP     SLSWNXTP              ; WAIT FOR NEXT P OR BAIL

                                       ; SLSMODE DONE, NOW CHECK PRINTER

DEL36B:  CALL    CHKTKT                ; CALL #9: PRINT TICKET AT END OF DELIVERY
         JNC     DEL36B                ; ONLY WAY OUT IS TO PRINT

         JMP     PRTFINAL              ; CALL #4: PRTFINAL JUMPS TO MAIN
                                       ; THIS IS STANDARD PRINT TO END DELIVERY
                                       ; REQUIRED TO HANDLE SLSMODE HERE !!!!



DEL36:   MOV     DPTR,#TKTPND
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG TICKET PENDING IF HOST MODE
         MOV     DPTR,#ACTIVE
         MOV     A,#0
         MOVX    @DPTR,A               ; FLAG HOST OUT OF DELIVERY MODE
         MOV     DPTR,#RETURN
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG RETURN. DISALLOW S/S TO NEW DEL
         CALL    CON2HOST              ; RECONNECT TO HOST FOR LAZY SOFTWARE

         MOV     DPTR,#TIMEOUT
         MOVX    A,@DPTR
         JNZ     DEL36C                ; DONT SEND PIPE IF TIMER FIRED

         MOV     DPTR,#PRTPRSD
         MOVX    A,@DPTR
         JNZ     DEL36C                ; DONT SEND PIPE IF PRINT KEY PRESSED

         CALL    SELECTHST

DEL36ZZ:
         CALL    SENDPIPE              ; RETURN N CMD IS COMPLETE
                                       ; N VALID IN HOST MODE OR PUMP AND PRINT

DEL36C:  JMP     MAINX

; ***************************************************************
; THIS IS THE START OF A NEW DELIVERY. ALL RESET.

DEL9:
         MOV     DPTR,#ACOST
         MOV     R1,#40
         MOV     A,#'0'
DEL9A:   MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,DEL9A              ; CLEAR ASCII DELIVERY DATA

         MOV     DPTR,#TANKID
         MOV     A,#0
         MOVX    @DPTR,A
         INC     DPTR
         MOVX    @DPTR,A
         INC     DPTR
         MOVX    @DPTR,A              ; CLEAR TANKID
         MOV     DPTR,#STORFLG         ; IT'S FLAG
         MOVX    @DPTR,A

         MOV     DPTR,#BROKNVLV        ; BROKEN VALVE DETECTION SETTING ADDED IN 178, 0=OFF(DO NOT CHECK), 1=ON(CHECK)
         MOVX    A,@DPTR
         CJNE    A,#0,DEL9B            ; IF NOT 0 THEN NEED TO SKIP CLEARING OUT PULSES

         CALL    CLRALEMPLS            ; CLEAR PULSES IN EM = INSURANCE

DEL9B:   CALL    DOEMLOAD

         CALL    SETCMP
         CALL    SELECTHST
         MOV     DPTR,#PLSCTR
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR PULSE COUNTER IF
         CLR     V2ND                  ; INSURANCE

         MOV     COMMAND,#SEGTEST      ; LEGIT DELIVERY
         MOV     DPTR,#MSGBLK          ; LOAD BLANK MESSAGE
         CALL    XLOAD

         CALL    WAIT1000
         CALL    WAIT100                ;1 second plus a little extra time to make sure DM is done

         MOV     COMMAND,#BLNKTST      ; NEED TO POINT DPTR TO VALID ADDRESS OR WILL END UP IN THE WEEDS
         MOV     DPTR,#MSGBLK          ; LOAD BLANK MESSAGE
         CALL    XLOAD

         CALL    WAIT1000
         CALL    WAIT100				   ;1 second plus a little extra time to make sure DM is done


         CALL    SHWVOPEN

DEL99:

         MOV     A,#1

         MOV     DPTR,#DELFLG          ; FLAG DELIVERY STARTED
         MOVX    @DPTR,A


         MOV     A,#0

         MOV     DPTR,#STATUS+1
         MOVX    @DPTR,A               ; CLEAR POWER AND HOST EXIT FLAGS

         MOV     DPTR,#SLSACHUNKS      ; MAKE SURE SLSMODE ABYTECHUNKS = 0
         MOVX    @DPTR,A


         CALL    CLOSVLVS

         MOV     A,COMP                ; ENTRY FOR AUTO RESET
         JZ      DEL12
         MOV     DPTR,#NGFLAG          ; INSURE NET DISPLAYED IF
         MOV     A,#1                  ; COMP IS ON IN DELIVER MODE.
         MOVX    @DPTR,A
         CALL    GETUNIT
         MOV     DPTR,#LGDFLG1
         MOV     A,#41H
         JNB     PSW_F0,U3
         ANL     A,#9FH
         ORL     A,#20H
U3:      MOVX    @DPTR,A
         MOV     DPTR,#LGDFLG2
         MOV     A,#85H
         JNB     PSW_F0,U4
         ANL     A,#0F9H
         ORL     A,#02H
U4:      MOVX    @DPTR,A
         JMP     DEL13
DEL12:   CALL    GETUNIT
         CALL    LGD101
         MOV     DPTR,#LGDFLG2
         MOV     A,#84H
         JNB     PSW_F0,U5
         ANL     A,#0F9H
         ORL     A,#02H
U5:      MOVX    @DPTR,A

DEL13:   CALL    PSLABEL
         CALL    SHOWVL
         MOV     COMMAND,#CLRVOL       ; CLEAR LCD COUNT
         CALL    VLOAD
         MOV     COMMAND,#DISPVOL      ; SHOW ZERO
         CALL    VLOAD

         MOV     DPTR,#ACTIVE
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG HOST DELIVERY MODE
         CALL    CLRBYTES              ; CLEAR PRINT BYTES. ASSUMES 64K SIP
         CALL    CHKTBLS               ; CHECK COMP TABLES INSTALLED - WILL CLR COMP IF NO TBLS
         CALL    GETPCPR               ; << PROVED TO BE A PROBLEM BELOW
         CALL    CONV2BCD              ; INSURANCE
         MOV     DPTR,#ATCFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR ATC FAILURE FLAG
         MOV     DPTR,#ATCCNT
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR FAILURE COUNTS

         MOV     DPTR,#SSFLAG          ; FLAG START
         MOV     A,#1
         MOVX    @DPTR,A

         MOV     DPTR,#GRTOTN          ; SAVE NMT TOTALIZER TO PRE-DELIVERY TOTALIZER
         MOV     R1,#DATEIMG
         MOV     R2,#6
         CALL    EXTMOV
         MOV     DPTR,#GRTOTSTRT
         MOV     R0,#DATEIMG
         MOV     R2,#6
         CALL    CARDMOV

         CALL    CLRLINES

         SETB    RRCLR
         CALL    WAIT50
;         MOV     R7,#5
;         DJNZ    R7,$
         CLR     RRCLR
         CALL    CLROUT                ; CLEAR NET/GROSS ETC
         CALL    CLRDEL
         CALL    GETDWL
         CALL    GETCAL                ; GET BOTH BY PRODUCT CODE
         MOV     PULSFLG1,#0
         MOV     PULSFLG2,#0           ; CLEAR PRESET FLAGS

         CALL    OPNV1AUX              ; FINALLY OPEN VALVE

         CALL    FINHSTRSET            ; NEED TO FINISH HOST COMMS IF CMD STARTED BY HOST

DEL99B:
; LOOK AT LT, THERE IS SOME ADDITIONAL CHECKING OF ATC HERE, DO WE NEED IN HERE???
DEL99D:  MOV     DPTR,#PLSCTR
         MOVX    A,@DPTR
         JZ      DEL99A
         DEC     A
         MOVX    @DPTR,A
         CALL    ADCR                  ; GET A/D MSB:LSB [100 uS]
         CALL    CHKATC                ; SEE IF OUT OF RANGE
         CALL    GETTC                 ; GET COMP CORRECTION FACTOR [1 MS]
         MOV     DPTR,#EMCFLAG
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG DOIT NOT TO TOGGLE EMCOUNT
         CALL    DOIT
         MOV     DPTR,#EMCFLAG
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET FLAG AFTER DOIT
         CALL    WAIT1
         CALL    WAIT1
         JMP     DEL99B

DEL99A:
         CALL    DOEMLOAD

         CALL    CON2HOST
         CALL    SELECTHST

         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#2,DEL99A_SLS0      ; 2=PA21 SLSMODE

         MOV     A,#'X'                ; SLSMODE - TELL HOST TICKET INSERTED ...
         CALL    OUTA
         CALL    CRLF

DEL99A_SLS0:
         MOV     A,#0
         MOV     DPTR,#DECMLADJ
         MOVX    @DPTR,A               ; CLEAR ROTATE FLAG FOR POWFAIL

         MOV     DPTR,#TOTALADJ
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR FLAG TOTALIZER UPDATE FOR POWFAIL

         MOV     DPTR,#TTRIP
         MOVX    @DPTR,A               ; CLEAR TRIP FLEET TIMER TRIP

         MOV     DPTR,#FIRST
         MOVX    @DPTR,A               ;<<

         CALL    ISDEMO
         JNC     DEMO2

         MOV     DPTR,#DEMOFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; SET DEMO FLAG TO COUNT

         MOV     DPTR,#DEMOCTR1
         MOV     A,#0
         MOVX    @DPTR,A
         MOV     DPTR,#DEMOCTR2
         MOVX    @DPTR,A               ; RESET DEMO COUNTERS

DEMO2:
         MOV     LOOPCTR,#0
         MOV     LOOPCTR+1,#0A0H       ; RESET LOOP COUNTER BEFORE INNER LOOP

         JMP     DEL3                  ; GO TO INNER LOOP


; ********************************************************
; LOOK FOR PULSE AND KRPRESS TO CLOSE VALVES.
; RESET LOOP COUNTER EACH PULSE.
; ONLY EXITS AFTER COUNTER TIMES OUT.
; ********************************************************

DEL6:
         MOV     DPTR,#SWAUTHFLG
         MOVX    A,@DPTR
         JZ      DEL6B                 ; CHECK S/W AUTHORIZATION, SKIP NEXT IF OFF

         MOV     DPTR,#RSTARM
         MOVX    A,@DPTR
         CJNE    A,#0,DEL6B
         CALL    CLOSVLVS
         JMP     DEL100

DEL6B:   MOV     DPTR,#EMULATEFLG      ; IF EMULATING PA21 DURING DELIVERY THEN
         MOVX    A,@DPTR               ;  IGNORE ALL CHARS REGARDLESS OF INNER OR OUTER LOOP
         CJNE    A,#2,DEL6B_SLS0       ; 2=PA21 SLSMODE
         CALL    INPA
         JNC     DEL32                 ; NO CHAR, CONTINUE TESTS
         JMP     NOCMDDELI             ; GOT A CHAR IN PA21 SLSMODE, IGNORE IT

DEL6B_SLS0:
         CALL    INPA
         JNC     DEL32

TSTHSTFXDI:
         CALL    DOHOSTFIX
         JC      DEL6C                 ; GOT TILDE CHARS OK OR HOST FIX OFF, JUMP TO COMMAND CHAR TESTS
         JMP     DEL3                  ; HOST FIX ON AND GOT INVALID CHAR(S), SKIP CHARS ... LOOK FOR OTHER INPUTS

DEL6C:   CJNE    A,#'K',DEL42
         CALL    OUTA                  ; 'K' LOOKS LIKE S/S/
         MOV     DPTR,#KPUMP
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG FOR PIPE LATER
         JMP     DEL33

DEL42:   CJNE    A,#'T',DEL104
         CALL    OUTA
         MOV     A,#'0'                ; T DATA NOT SENT TO HOST WHILE PRODUCT IS FLOWING
         CALL    OUTA
         CALL    SENDPIPE
         JMP     DEL3

DEL104:  CJNE    A,#'y',DEL34          ; FMS DISARM COMMAND PREVENTS SSKEY AND
         CALL    OUTA                  ; CAN END DELIVERY
         MOV     DPTR,#RSTARM
         MOV     A,#0
         MOVX    @DPTR,A
         CALL    SENDPIPE
         JMP     DEL3

DEL34:   CJNE    A,#'J',NOCMDDELI
         MOV     TEMP,#10H
         CALL    SENDST1
         CALL    SENDVOL
         CALL    CHKSUM
         JMP     DEL32

DEL32:   CALL    KPRESS
         JNC     DEL3                  ; NO KEY LOOK FOR PULSE
         CJNE    A,#SSKEY,DEL45        ; S/S KEY TOGGLES VALVES

DEL33:
         CALL    CLOSVLVS

         MOV     DPTR,#STOPFLG         ; FLAG STOPPED IN DELIVERY
         MOV     A,#1
         MOVX    @DPTR,A

         CALL    ISDEMO
         JNC     DEMO3

         MOV     DPTR,#DEMOFLG         ; STOP COUNTING
         MOV     A,#0
         MOVX    @DPTR,A

DEMO3:   JMP     DEL3

DEL45:
         CJNE    A,#MODEKEY,DEL3       ; MODEKEY #1 WHILE PRODUCT FLOWING IN REGULAR DELIVERY
         MOV     DPTR,#FRFLAG
         MOVX    A,@DPTR
         JZ      DEL3                  ; CHECK TO SEE IF EM ENABLED FOR FLOW RATE
         CALL    SHOWFLORAT
         JMP     DEL3


NOCMDDELI:                             ; NOMORE COMMAND CHARS TO TEST, IF HOSTFIX ON
         CLR     C                     ; IGNORE CHAR IN A
         JMP     DEL3


DEL3:
         MOV     DPTR,#AIRFLG
         MOVX    A,@DPTR
         JZ      DEL3B                 ; OPTIC NOT HOOKED UP

         CJNE    A,#1,DEL3E            ; 1=GNDWET ( GETS INVERTED)
         JB      LIQUID,DEL3A          ; H = LIQUID PRESENT
         JMP     DEL3F
DEL3E:   JNB     LIQUID,DEL3A          ; NOT 1 THEN 2=GNDDRY

DEL3F:   CLR     V1ST
         CLR     V2ND
         CLR     VAUX

         MOV     DPTR,#STOPFLG
         MOVX    A,@DPTR
         JNZ     DEL3B                 ; DON'T OPEN VALVE IF DEL STOPPED

         MOV     DPTR,#SSFLAG
         MOVX    A,@DPTR               ; VARIOUS CONDITION OF 1ST AND 2ND STAGES
         JZ      DEL3B

		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     AIRSOL,#0x08			; SETB AIRSOL AIRSOL SET = OPTIC NOT WET
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         JMP     DEL3B

DEL3A:
         MOV     DPTR,#STOPFLG
         MOVX    A,@DPTR
         JNZ     DEL3B                 ; DON'T OPEN VALVE IF DEL STOPPED

         MOV     DPTR,#SSFLAG
         MOVX    A,@DPTR               ; VARIOUS CONDITION OF 1ST AND 2ND STAGES
         JZ      DEL3B

		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     AIRSOL,#0xF7			; CLR AIRSOL AIRSOL CLEAR = OPTIC WET
		 MOV     SFRPAGE,#00H          ;Switch SFR page

         MOV     DPTR,#FIRST
         MOVX    A,@DPTR
         JNZ     DEL3C

         CALL    OPNV1AUX
         JMP     DEL3B

DEL3C:
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     AIRSOL,#0xF7			; CLR AIRSOL AIRSOL CLEAR = OPTIC WET
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    OPNV2AUX

DEL3B:
         CALL    ISDEMO
         JNC     DEMO4

         MOV     DPTR,#DEMOFLG
         MOVX    A,@DPTR
         JNZ     DEL4

DEMO4:
         MOV     DPTR,#MPCFLG
         MOVX    A,@DPTR
         JZ      DEL3D
         CALL    CHKTIM

DEL3D:   MOV     SFRPAGE,#0FH          ;Switch SFR page
		 MOV     TEMPBIT, P4           ;Since P4 is not bit addressable, must use TEMPBIT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
		 JB      TEMPBIT.2,DEL4        ; ;P4.2 is EMPULSE
         MOV     A,LOOPCTR+1
         CJNE    A,#0FFH,DEL8          ; COUNTER FULL GO TO TOP
         JMP     DEL100

DEL8:    MOV     A,LOOPCTR             ; STOPPED, SO STIK AROUND
         CJNE    A,0FFH,DEL7
         INC     LOOPCTR
         INC     LOOPCTR+1
         MOV     A,LOOPCTR+1
         CJNE    A,0FFH,DEL8A
         JMP     DEL100                ; ONLY WAY BACK TO TOP IS FULL COUNTER
DEL8A:   JMP     DEL6                  ; OUT OF RANGE ISSUE
DEL7:    INC     LOOPCTR
         JMP     DEL6
DEL4:
         MOV     DPTR,#TTRIP
         MOV     A,#1
         MOVX    @DPTR,A               ; SET FLEET TIMER TRIP

         MOV     DPTR,#FLTIME
         MOVX    A,@DPTR
         MOV     DPTR,#FTLEFT
         MOVX    @DPTR,A               ; LOAD OR RELOAD FLEET TIMEOUT

         CALL    ISDEMO
         JNC     DEMO5

         MOV     DPTR,#DEMOCTR1
         MOVX    A,@DPTR
         INC     A
         MOVX    @DPTR,A
         JZ      DEL4A
DEL4B:   JMP     DEL6

DEL4A:   MOV     DPTR,#DEMOCTR2
         MOVX    A,@DPTR
         INC     A
         MOVX    @DPTR,A
         JNB     V1ST,DEL4C            ; FIRST STAGE OFF SO GO SLOW
         CJNE    A,#2,DEL4B
         JMP     DEL4D
DEL4C:   CJNE    A,#6,DEL4B
DEL4D:   MOV     A,#0
         MOVX    @DPTR,A

DEMO5:   CALL    CLRTMT
         MOV     LOOPCTR,#0
         MOV     LOOPCTR+1,#0A0H       ; RESET LOOP COUNTER EACH PULSE
         CALL    ADCR                  ; GET A/D MSB:LSB [100 uS]
         CALL    CHKATC                ; SEE IF OUT OF RANGE
         CALL    GETTC                 ; GET COMP CORRECTION FACTOR [1 MS]
         CALL    DOIT
         CALL    PSCHECK

         CALL    BUMPFR

         MOV     DPTR,#TEN
         MOVX    A,@DPTR
         JNZ     DEL27
         MOV     A,CGALS+2
         ANL     A,#0FH
         JZ      DEL27
         MOV     DPTR,#TEN
         MOV     A,#1
         MOVX    @DPTR,A               ; SET TEN UNIT FLAG
DEL27:   JMP     DEL6


ISDEMO:  CLR     C
         MOV     DPTR,#DEMO
         MOVX    A,@DPTR
         JZ      ISDEMON
         SETB    C
ISDEMON: RET


CLRALEMPLS:                            ;Clear out any stray pulses in EM
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
		 MOV     TEMPBIT, P4
		 MOV     SFRPAGE,#00H          ;Switch SFR page
		 JNB     TEMPBIT.2,CLRALEMX     ;P4.2 is EMPULSE
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     EMCOUNT,#0x80			; SETB EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    WAIT500U               ;Delay timer
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     EMCOUNT,#0x7F			; CLR EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
		 CALL    WAIT500U              ;Delay timer
         JMP     CLRALEMPLS              ; CLEAR OUT EM PULSES
CLRALEMX:RET

BUMPFR:
         MOV     DPTR,#INTFR
         MOVX    A,@DPTR               ; GET CURRENT PULSE COUNT
         CJNE    A,#0FFH,BUMPF1        ; DON'T ALLOW OVERFLOW
         JMP     BUMPFDN
BUMPF1:  ADD     A,#1                  ; BUMP IT
         MOVX    @DPTR,A               ; PUT IT BACK
BUMPFDN: RET

CHKTIM:
         JNB     TCON_TF0,CHKTIM1           ; CHECK TIMER OVERFLOW
         CLR     TCON_TF0                   ; OVERFLOW HERE - CLEAR FLAG
         CLR     TCON_TR0                   ; STOP TIMER
         MOV     TL0,#0
         MOV     TH0,#20H              ; RESET TIMER

         MOV     DPTR,#TIMCTR
         MOVX    A,@DPTR
         INC     A                     ; BUMP IT
         MOVX    @DPTR,A               ; PUT IT BACK
         CJNE    A,#31H,CHKTIM2        ; CHECK # OF OVERFLOWS - TWEAK TIME HERE
                                       ; SET FOR 500 U/MIN = 83HZ

CHKTIM2: JC      CHKTIM1                ; A IS LESS THAN 0x20 (31 CYCLES) - GET OUT

         MOV     A,#0                  ; CLEAR TIMER COUNTER
         MOVX    @DPTR,A               ; DPTR IS STILL TIMCTR

         MOV     DPTR,#INTFR
         MOVX    A,@DPTR
         MOV     B,A                   ; TEMP SAVE
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR LAST COUNT

         MOV     A,B                   ; GET COUNT BACK
         MOV     DPTR,#FLOWRATE
         MOVX    @DPTR,A               ; STORE CURRENT FLOWRATE

;         MOV     R1,A
;         CALL    OUTHEX                ; SEE CURRENT FLOWRATE

;         MOV     A,#' '
;         CALL    OUTA

;         MOV     A,SWARRAY+2
;         CALL    SENDASC
;         MOV     A,SWARRAY+1
;         CALL    SENDASC
;         CALL    CRLF

;         CALL    TENOUT

         MOV     DPTR,#CPMFLG          ; DON'T COMPUTE CALFAC IF IN MPCAL MODE
         MOVX    A,@DPTR
         JNZ     CHKTIM1
         CALL    COMPFAC

CHKTIM1: SETB    TCON_TR0                   ; START TIMER
         RET

RESTCPT: MOV     DPTR,#MPCPTR          ; NOTE FLOWRATES IN ASCENDING ORDER
         MOV     A,#0                  ; START AT FIRST POINT
         MOVX    @DPTR,A
         RET

BUMPCPT: MOV     DPTR,#MPCPTR          ; OTHERWISE BUMP MPCTR
         MOVX    A,@DPTR
         INC     A
         MOVX    @DPTR,A               ; BUMP CALPT
;         CALL    SENDCP
         RET


; COMPUTES CALFAC FOR CURRENT FLOWRATE. CALFAC +/- DEVIATION

COMPFAC:
         MOV     DPTR,#FLOWRATE
         MOVX    A,@DPTR
         MOV     R7,A                  ; R7 HAS CURRENT FLOWRATE

         CALL    RESTCPT

COMPF3:  CALL    CPTPTR                ; POINT TO FIRST CALPT
         MOVX    A,@DPTR               ; GET CALPT FLOWRATE
         MOV     TEMP,A                ; TEMP=CALPT FLOWRATE

         MOV     A,R7
         CJNE    A,TEMP,COMPF1
COMPF1:  JC      COMPF2                ; FLOWRATE < CALPT FLOW. USE THIS POINT
         CALL    BUMPCPT
         CJNE    A,#9,COMPF3

COMPF2:

         CALL    GETCAL
         MOV     DATEIMG,SWARRAY+1
         MOV     DATEIMG+1,SWARRAY+2

         CALL    CPTPTR                ; POINT TO PRESENT CALPT
         INC     DPTR
         INC     DPTR
         MOVX    A,@DPTR
         MOV     DATEIMG+2,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     DATEIMG+3,A
         CALL    DECDPTR
         CALL    DECDPTR               ; POINT TO SIGN
         MOVX    A,@DPTR
         JZ      COMPSUB

         MOV     R0,#DATEIMG
         MOV     R1,#DATEIMG+2
         MOV     R2,#2
         CALL    SUMUP
         JMP     COMPFDN

COMPSUB:
         MOV     R0,#DATEIMG+2         ; CALPT
         MOV     R2,#2
         CALL    NEGATE
         MOV     R0,#DATEIMG
         MOV     R1,#DATEIMG+2
         MOV     R2,#2
         SETB    C
         CALL    SUMUP

COMPFDN:
         MOV     A,DATEIMG+1
         MOV     SWARRAY+2,A
;         CALL    SENDASC
         MOV     A,DATEIMG
         MOV     SWARRAY+1,A           ; STORE BACK NEW CALFAC
;         CALL    SENDASC
;         CALL    CRLF
         CALL    DOSWFAC

         RET


; SORTS CALPOINT IN ASCENDING FLOWRATE ORDER
SORT:
         MOV     R1,#TEMP              ; START OF RAM
         MOV     R2,#40                ; JUST LOVE REUSING VARIABLES
         MOV     DPTR,#CALPT1
         CALL    EXTMOV

         MOV     DPTR,#CALPT1
         MOV     R2,#40                ; 4 BYTES PER POINT
         MOV     A,#0
SORT6:   MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R2,SORT6              ; CLEAR CALPTRS FOR SORT

         MOV     DPTR,#CALPT1          ; FIRST CAL POINT
SORT5:   MOV     R7,#0FFH              ; LOOP COUNTER 255
         MOV     LSB,#1                ; FLOW RATE UP COUNTER 0-255 TRIAL


; THIS LOOP 10 TIMES THRU UP TO 255 TIMES
SORT4:
         MOV     R0,#TEMP
         MOV     R6,#10                ; NUMBER OF CAL POINTS
SORT3:   MOV     A,@R0                 ; FLOW RATE BYTE
         CJNE    A,LSB,SORT2           ; CHECK IT

         MOV     R2,#4
         CALL    CARDMOV               ; STORE IT ; DPTR NOW AT NEXT CALPT
         JMP     SORT7                 ; R0 POINTS TO NEXT

SORT2:   INC     R0
         INC     R0
         INC     R0
         INC     R0                    ; MOV OVER 4 BYTES TO NEXT FLOW RATE
SORT7:   DJNZ    R6,SORT3              ; CHECK EACH CALPT FOR EQUALITY

         INC     LSB                   ; BUMP TRIAL FLOWRATE
         DJNZ    R7,SORT4              ; INC TO NEXT TRIAL FLOW RATE
         RET                           ; SHOULD BE DONE HERE

; COMPUTES +/- DEVIATION OF EACH CALPT FROM CALFAC
COMPDV:
         CALL    CON2HOST
         CALL    SELECTP1

COMPDV1:
         CALL   RESTCPT

COMPDV5:
         CALL   GETCAL

         MOV    R0,#SWARRAY+1          ; CALFAC LSB
         MOV    R2,#2                  ; 2 BYTES
         CALL   NEGATE                 ; CALFAC=-CALFAC
         MOV    R2,#2
         MOV    R0,#SWARRAY+1
         MOV    R1,#DATEIMG
         CALL   MOVER                  ; SAVE NEGATIVE IN DATEIMG

         CALL   CPTPTR
         INC    DPTR
         INC    DPTR                   ; SHOULD BE CURRENT CALPT
         MOV    R0,#TEMP               ; INTERIM CALPT IN TEMP
         MOVX   A,@DPTR
         MOV    @R0,A
         INC    DPTR
         INC    R0
         MOVX   A,@DPTR
         MOV    @R0,A                  ; PUT CALPT INTO R0

         MOV   A,TEMP
         ORL   A,TEMP+1
         JZ    COMPDV3                 ; DON'T DO IF CALPT=0

         MOV    R0,#TEMP
         MOV    R1,#DATEIMG
         MOV    R2,#2
         CALL   SUMUP                  ; NOW HAVE SUM IN @R0=TEMP
         JNC    COMPDV2

         SETB    C
         MOV     R0,#TEMP
         MOV     A,@R0
         ADDC    A,#0
         DA      A                     ; 10.18.11
         MOV     @R0,A
         INC     R0
         MOV     A,@R0
         ADDC    A,#0
         DA      A                     ; 10.18.11
         MOV     @R0,A                 ; ADD 1 TO RESULT FOR POSITIVE RESULT
;         MOV     R7,#'+'
         MOV     R7,#01H               ; 1 FOR POSITIVE
         CALL    PUTBACK

;         CALL    TENOUT
;         CALL    CRLF
         JMP     COMPDV3               ; DONE FOR POSITIVE

COMPDV2:
         MOV     R2,#2
         MOV     R0,#TEMP
         CALL    NEGATE                ; TEMP NOW NEGATIVE
;         MOV     R7,#'-'
         MOV     R7,#00H               ; 0 FOR NEGATIVE
         CALL    PUTBACK

;         CALL    TENOUT               ; DONE FOR NEGATIVE
;         CALL    CRLF

COMPDV3:
         CALL    BUMPCPT
         CJNE    A,#10,COMPDV4
         RET

COMPDV4: JMP     COMPDV5


; COMPUTES DIFFERENCE BETWEEN CAL POINTS. .25% MAX SPLASHES ERR

PUTBACK:

         CALL    CPTPTR                ; POINT TO CURRENT CALPOINT
         INC     DPTR
         MOV     A,R7
         MOVX    @DPTR,A               ; SAVE SIGN
         INC     DPTR                  ; NOW POINTING TO CALFAC / DEV #
         MOV     R0,#TEMP
         MOV     R2,#2
         CALL    CARDMOV               ; MOVE RESULT IN TEMP TO CALPT
         RET

TENOUT:
         CALL    CON2HOST
         CALL    SELECTP1

         MOV     DPTR,#MPCPTR          ; CALPT#
         MOVX    A,@DPTR
         CALL    SENDASC

         CALL    CPTPTR
         MOVX    A,@DPTR
         CALL    SENDASC               ; FLOW RATE AT CALPTF

         INC     DPTR
         MOVX    A,@DPTR               ; SIGN ASCII
         CALL    SENDASC

         INC     DPTR                  ; POINTS TO CALPT+3
         INC     DPTR

         MOVX    A,@DPTR
         CALL    SENDASC
         CALL    DECDPTR

         MOVX    A,@DPTR
         CALL    SENDASC
         CALL    CRLF

         RET

SETMPCAL:							//TODO - Can this be removed since no MPCAL in E300
         CALL    LF0018
         MOV     DPTR,#MPCFLG
         MOVX    A,@DPTR
         JZ      SETMPCA1
         MOV     DPTR,#MSG23
         JMP     SETMPCA2
SETMPCA1:MOV     DPTR,#MSG24
SETMPCA2:
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD
         CALL    WAIT1000
SETMPCA3:CALL    KPRESS
         JNC     SETMPCA3
         CJNE    A,#UPKEY,SETMPCA4
         MOV     DPTR,#MPCFLG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETMPCAL

SETMPCA4:CJNE    A,#DOWNKEY,SETMPCA5
         MOV     DPTR,#MPCFLG
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETMPCAL
SETMPCA5:CJNE    A,#SSKEY,SETMPCA7
         JMP     SETMPDN
SETMPCA7:CJNE    A,#MODEKEY,SETMPCA8
         CALL    RESTCPT
         JMP     MPSET6
SETMPCA8:CJNE    A,#PRSETKEY,SETMPCA3
         MOV     DPTR,#CPMFLG
         MOVX    A,@DPTR
         JNZ     SETMPCA9

         MOV     DPTR,#CALPT1
         MOV     R2,#40
         MOV     A,#0
SETMPCA6:MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R2,SETMPCA6           ; CLEAR CALPTS WITH PRESET KEY
         CALL    RESTCPT               ; RESETS MPCPTR

         MOV     DPTR,#CPMFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; SET FLAG FOR AUTOCAL RECORDING TO CALPTS

         MOV     DPTR,#MSG72           ; RESET MESSAGE
         CALL    SHOW
         CALL    WAIT1000

         CALL   TENOUT                 ; ### UNCOMMENT TO DEBUG MPC CALCS OVER HOST PORT

         JMP     SETMPDN

SETMPCA9:MOV     DPTR,#CPMFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET AUTOCAL FLAG
         CALL    COMPDV
SORTX:   CALL    SORT
         MOV     DPTR,#MPCPTR
         MOV     A,#0
         MOVX    @DPTR,A

SORTX2:
;         CALL    TENOUT
         MOV     DPTR,#MPCPTR
         MOVX    A,@DPTR
         INC     A                     ; BUMP MPCPTR AND CHECK FOR ROLLOVER
         CJNE    A,#10,SORTX1
         JMP     SORTX3
SORTX1:  MOVX    @DPTR,A
         JMP     SORTX2


SORTX3:  MOV     DPTR,#MSG105          ; DONE MESSAGE
         CALL    SHOW

; LOTS GOING TO HAPPEN HERE

SETMPDN: RET


MPSET:
MPSET1:  CALL    KPRESS
         JNC     MPSET1
         CJNE    A,#SSKEY,MPSET2
         JMP     SETMPCAL
MPSET2:  CJNE    A,#MODEKEY,MPSET1
MPSET7:
;         CALL   SENDCP
         MOV     DPTR,#MPCPTR
         MOVX    A,@DPTR
         INC     A
         CJNE    A,#10,MPSET6
         MOV     A,#0

MPSET6:
         MOVX    @DPTR,A               ; SAVE MPCPTR
         MOV     DPTR,#MSG132          ; CAL PT MESSAGE
         CALL    SHOW
         MOV     DPTR,#MPCPTR
         MOVX    A,@DPTR
         MOV     VOLUME,A
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H
         CALL    LF0010
         MOV     DGTPOS,#0
         MOV     DATEIMG,#0FH
         CALL    DISPSET               ; SHOW CAL POINT

         CALL    WAIT1000

MPSET8:  CALL    CPTPTR                 ; POINT TO CAL POINT
         INC     DPTR
         MOVX    A,@DPTR
         MOV     VOLUME+2,A             ; SIGN
         INC     DPTR
         MOVX    A,@DPTR
         MOV     VOLUME,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     VOLUME+1,A
         CALL    SHOWFAC1               ; SHOW FACTOR

         CALL    WAIT500                ; TIME TO GET OFF PRESETKEY

         MOV     DGTPOS,#3             ; START WITH DIGIT3 BLINK
         MOV     DATEIMG+1,#2          ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#7FH          ; 4 DIGITS WORTH
MPSET3:  CALL    KPRESS
         JNC     MPSET4
         MOV     A,KEY
         CJNE    A,#SSKEY,MPSET4
         JMP     MPSET5
MPSET4:  CJNE    A,#PRSETKEY,MPSET9
         MOV     A,VOLUME+2            ; GET SIGN
         JZ      MPSET10
         MOV     VOLUME+2,#0
         JMP     MPSET11
MPSET10: MOV     VOLUME+2,#1           ; IF ZERO MAKE 1
         JMP     MPSET11
MPSET9:  CALL    ENTRY
         JMP     MPSET3

MPSET11: CALL    CPTPTR
         INC     DPTR
         MOV     A,VOLUME+2
         MOVX    @DPTR,A
         JMP     MPSET8

MPSET5:
         CALL    CPTPTR
         INC     DPTR
         MOV     A,VOLUME+2
         MOVX    @DPTR,A               ; SAVE SIGN
         INC     DPTR
         MOV     A,VOLUME
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,VOLUME+1
         MOVX    @DPTR,A

         JMP     MPSET1


; ***************************************************************************************************
; CALIBRATION MODE
PROVE:
         CALL    SETCMP
         CALL    OPNV1AUX              ; EC: OPENS VALVES HERE SO THEY ARE ALWAYS OPEN DURING CALIBRATION MODE

         MOV     DPTR,#ATCFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR ATC FLAG

         MOV     DPTR,#MSG17           ; CALIBRATE
         CALL    SHOW
         MOV     DPTR,#MSG4
         CALL    SHOW

         CALL    SHWVOPEN
PRV12:
         CALL    SETCMP

         MOV     DPTR,#SSFLAG
         MOV     A,#0
         MOVX    @DPTR,A               ; S/S STOPPED

         CALL    CLASTVOL
         CALL    WAIT1000

PRV2:
         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JC      PRV13

         CALL    CLRALEMPLS            ; IN CASE ANY STRAY PULSES IN EM CLEAR THEM BEFORE RETURNING TO DELIVER MODE

         CALL    CLOSVLVS              ; EC/LT: CLOSE VALVES WHEN CALIBRATE SWITCH CLOSED
         CALL    SHWVCLSD

         JMP     MAIN

PRV13:   CALL    KPRESS
         JC      PRV5

         MOV     DPTR,#SSFLAG
         MOVX    A,@DPTR
         JZ      PRV2                 ; IF NOT IN DELIVERY THEN JUST LOOK FOR KPRESS

         JMP     PRV3
PRV5:
         MOV     DPTR,#SSFLAG
         MOVX    A,@DPTR
         JNZ     PRV1
         MOV     A,KEY                 ; RESTORE KEYPRESS
         CJNE    A,#MODEKEY,PRV1       ; S/S KEY

         MOV     DPTR,#CMODEFLG        ; GO TO DISPLAY MODE
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     CALMOD1

PRV1:    MOV     A,KEY                 ; RESTORE KEY
         CJNE    A,#SSKEY,PRV11
         JMP     PRV10
PRV11:   JMP     PRV3
PRV10:   MOV     DPTR,#SSFLAG
         MOVX    A,@DPTR
         JZ      PRV9

         MOV     DPTR,#NVOL
         CALL    ROTVOL
         MOV     DPTR,#NVOL
         CALL    RLVOL
         MOV     DPTR,#GVOL
         CALL    ROTVOL
         MOV     DPTR,#GVOL
         CALL    RLVOL

         JMP     PRV12

PRV9:
         MOV     DPTR,#SSFLAG
         MOV     A,#1
         MOVX    @DPTR,A
         MOV     COMMAND,#SEGTEST
		 MOV     DPTR,#MSGBLK			; LOAD BLANK MESSAGE
         CALL    XLOAD
         CALL    WAIT1000
         CALL    WAIT100                ;1 second plus a little extra time to make sure DM is done
         MOV     COMMAND,#BLNKTST
         MOV     DPTR,#MSGBLK			; LOAD BLANK MESSAGE
         CALL    XLOAD
         CALL    WAIT1000
         CALL	 WAIT100

         SETB    RRCLR
         CALL    WAIT50
;         MOV     R7,#5
;         DJNZ    R7,$
         CLR     RRCLR

         CALL    OPNV1V2AUX            ; OPEN VALVES IF MP CLOSED

         CALL    SETCMP
         MOV     A,COMP
         JZ      PRV14
         MOV     DPTR,#NGFLAG
         MOV     A,#1
         MOVX    @DPTR,A               ; INSURE NET SHOWS IF COMP ON
         CALL    GETUNIT
         MOV     DPTR,#LGDFLG1
         MOV     A,#41H
         JNB     PSW_F0,U6
         ANL     A,#9FH
         ORL     A,#20H
U6:      MOVX    @DPTR,A
         MOV     DPTR,#LGDFLG2
         MOV     A,#8DH
         JNB     PSW_F0,U32
         ANL     A,#0F9H
         ORL     A,#02H
U32:     MOVX    @DPTR,A
         JMP     PRV15
PRV14:   CALL    GETUNIT
         CALL    LGD101
         MOV     DPTR,#LGDFLG2
         MOV     A,#8CH
         JNB     PSW_F0,U7
         ANL     A,#0F9H
         ORL     A,#02H
U7:      MOVX    @DPTR,A

PRV15:
         CALL    SHOWVL
         MOV     COMMAND,#CLRVOL       ; CLEAR LCD COUNT
         CALL    VLOAD
         MOV     COMMAND,#DISPVOL      ; SHOW ZERO
         CALL    VLOAD
         CALL    CLROUT                ; CLEAR NET/GROSS ETC
         CALL    CLRDEL
         CALL    GETCAL
         CALL    GETDWL                ; GET BOTH BY PROD CODE
         CALL    CLRRP                 ; CLEAR RAW PULSE FOR AUTO CAL

         MOV     DPTR,#ATCFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR ATC FLAG

         CALL    CLRALEMPLS            ; IN CASE ANY STRAY PULSES IN EM CLEAR THEM BEFORE STARTING CAL MODE DELIVERY

         CALL    DOEMLOAD

         MOV     LOOPCTR,#0            ; INIT FIRST TIME IN INNER LOOP
         MOV     LOOPCTR+1,#0A0H       ; RESET LOOP COUNTER EACH PULSE

         JMP     PRV3                  ; GET OUT

PRV6:    CALL    KPRESS
         JNC     PRV3                  ; NO KEY LOOK FOR PULSE
         CJNE    A,#SSKEY,PRV3A        ; S/S KEY KILLS VALVES

PRV3A:
         CJNE    A,#MODEKEY,PRV3       ; MODEKEY #2 WHILE PRODUCT FLOWING IN CAL MODE FOR FLOW RATE
         MOV     DPTR,#FRFLAG
         MOVX    A,@DPTR
         JZ      PRV3
         CALL    SHOWFLORAT

         MOV     DPTR,#FLOWRATE
         MOVX    A,@DPTR
         MOV     DPTR,#FINALFR
         MOVX    @DPTR,A               ; STORE LAST FLOW RATE

         JMP     PRV3

PRV3:
         MOV     DPTR,#MPCFLG
         MOVX    A,@DPTR
         JZ      PRV3D
         CALL    CHKTIM

PRV3D:   MOV     SFRPAGE,#0FH          ;Switch SFR page
		 MOV     TEMPBIT, P4           ;Because P4 is not bit addressable, must use TEMPBIT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         JB      TEMPBIT.2,PRV4       ;P4.2 is EMPULSE
         MOV     A,LOOPCTR+1
         CJNE    A,#0FFH,PRV8
         JMP     PRV2
PRV8:    MOV     A,LOOPCTR             ; STOPPED, SO STIK AROUND
         CJNE    A,0FFH,PRV7
         INC     LOOPCTR
         INC     LOOPCTR+1
         MOV     A,LOOPCTR+1
         CJNE    A,0FFH,PRV6
PRV16:   JMP     PRV2                  ; ONLY WAY BACK TO TOP IS FULL COUNTER
PRV7:    INC     LOOPCTR
         JMP     PRV6

PRV4:
         MOV     LOOPCTR,#0
         MOV     LOOPCTR+1,#0A0H       ; RESET LOOP COUNTER EACH PULSE
         CALL    ADCR                  ; GET A/D MSB:LSB [100 uS]
         CALL    CHKATC
         MOV     DPTR,#ATCFLG
         MOVX    A,@DPTR               ; CHECK FOR PROBE FAILURE
         JZ      PRV17
         JMP     PRV18
PRV17:   CALL    GETTC                 ; GET COMP COMP CORRECTION FACTOR [1 MS]
         CALL    ADDPLS
         CALL    DOIT
         CALL    BUMPFR
         JMP     PRV6

PRV18:   MOV     DPTR,#MSG97
         CALL    SHOW
         MOV     DPTR,#MSG63
         CALL    SHOW
         CALL    KPRESS
         JNC     PRV18

         MOV     R0,#0
PRV19:   MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     EMCOUNT,#0x80			; SETB EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    WAIT1
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     EMCOUNT,#0x7F			; CLR EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    WAIT1
         DJNZ    R0,PRV19
         JMP     PROVE

; ***************************************************************************************
; DO SOME HANDSHAKING FOR DECIMAL SETTING TO SHOW FLOW RATE IN DM IN WHOLE UNITS
; CLOCK DMDATA:
; LT 'DECIMAL' IS DIFFERENT FROM ECOUNT
;  'DECIMAL'=0 = DP0 = 1 EXTRA CYCLE = DM NEEDS TO ADJUST DISPLAYED COUNT*10
;  'DECIMAL'=1 = DP1 = 0 CYCLES = DP1 (DEFAULT, THAT WAY BACKWARDS COMPATIBLE WITH DM11--
;  'DECIMAL'=2 = DP2 = 2 EXTRA CYCLES = DM NEEDS TO ADJUST DISPLAYED COUNT*0.1
; AS OF 180, ECOUNT ONLY HAS DP1 DP0
;  'DECIMAL'=0 = DP1 = 0TIMES = DP1 (DEFAULT, THAT WAY BACKWARDS COMPATIBLE WITH DM11--
;  'DECIMAL'=1 = DP0 = DM NEEDS TO ADJUST COUNT*10
SHOWFLORAT:
         SETB    DMDATA                ; CYCLE #1: SIGNAL TO SHOW FLOW RATE
         CALL    WAIT1
         CLR     DMDATA

         MOV     DPTR,#DECIMAL
         MOVX    A,@DPTR
         CJNE    A,#1,SHOWFLORX

                                       ; 1=CLOCK DMDATA 1 TIME FOR 10X
         CALL    WAIT3

         SETB    DMDATA
         CALL    WAIT3
         CLR     DMDATA

SHOWFLORX:
         CALL    WAIT1000                ; DM WILL SHOW FR FOR ONE SECOND
         CALL    WAIT250				; TIME FOR DM TO FINISH WITH FLOW RATE

         RET


;*****************************************************
; CHECK CGALS AGAINST PRESET, S1SHOFF AND S2DWELL
PSCHECK:

         MOV    DPTR,#PSMSD       ; PRESET MOST SIG DIGIT
         MOVX   A,@DPTR
         MOV    TEMP,CGALS+4      ; GET MSD OF CGALS
         ANL    TEMP,#0FH         ; STRIP OFF MSN
         CJNE   A,TEMP,PSC1       ; DO NOTHING - RETURN
;         ORL     AIRSOL,#0x08			; SETB AIRSOL
         JMP    PSC2
PSC1:    RET

PSC2:    MOV    DPTR,#PRSSET     ; PRESET SET FLAG
         MOVX   A,@DPTR          ; IF <> 0 CHECK FOR PRESET VALUE
         JNZ    PULSIN5
         JMP    PULSIN8C         ; EXIT

PULSIN5:
         MOV    R1,#DATEIMG+1    ; DATEIMG := PRESET
         MOV    R0,#PRESET       ;  XX XX.XX AND EXPANDED
         MOV    R2,#3            ;    TO FOUR BYTES
         CALL   MOVER
         MOV    DATEIMG,#0       ; XX XX.XX 00
         CALL   CLRRES           ; RES:= 00000500
         MOV    RES+1,#0
         MOV    RES+2,S1SHOFF    ; RES:= 00XX0000
         MOV    R0,#RES
         MOV    R1,#CGALS
         MOV    R2,#4            ; ADD CGALS TO S1 IN RES
         SETB   C                ; CARRY IN = 1 TO OFFSET
         CALL   SUMUP            ; RES = CGALS + S1 + 0.0001
         MOV    R2,#4            ;  XX XX.XX XX
         MOV    R0,#RES
         CALL   NEGATE           ; RES:= -(C+S1)
         MOV    R0,#DATEIMG      ; DATEIMG := PRESET - RES
         MOV    R1,#RES
         MOV    R2,#4
         SETB   C                ; CARRY IN FOR SUBTRACTION
         CALL   SUMUP
         JC     PULSIN6
PULSIN6A:
         MOV    A,PULSFLG1       ; IF == 0 FIX V1ST AND V2ND
         JNZ    PULSIN6

         CLR    V1ST             ; CLOSE VALVES FIRST 1/28/92

         CALL   OPNV2AUX         ; THIS IS FOR PROPANE DIFF VALVE

         MOV    PULSFLG1,#1
         MOV    DPTR,#FIRST
         MOV    A,#1
         MOVX   @DPTR,A

PULSIN6:
         MOV    R1,#DATEIMG+1    ; DATEIMG := PRESET
         MOV    R0,#PRESET       ;  XX XX.XX AND EXPANDED
         MOV    R2,#3            ;    TO FOUR BYTES
         CALL   MOVER
         MOV    DATEIMG,#0       ; XX XX.XX 00
         MOV    RES,#0           ; 22 2.2 00 00 >> 1 NIBBLE
         MOV    RES+1,#0         ; 02 22.20 00
         MOV    RES+2,S2DWELL    ; TO ALIGN DECIMAL POINT
         MOV    RES+3,S2DWELL+1
         MOV    A,#0
         MOV    R2,#4            ; POINT TO HIGH END
         MOV    R1,#RES+3        ;   AND MOVE 4 BYTES
         CALL   SHIFTR1          ; USED PART OF SHIFTR FOR SHIFTING RES
         MOV    R0,#RES
         MOV    R1,#CGALS
         MOV    R2,#4            ; ADD CGALS TO S2 IN RES
         SETB   C                ; CARRY IN = 1 TO OFFSET
         CALL   SUMUP            ; RES = CGALS + S2 + 0.0001
         MOV    R2,#4            ;  XX XX.XX XX
         MOV    R0,#RES
         CALL   NEGATE           ; RES := -(CGALS+S2)
         MOV    R0,#DATEIMG      ; DATEIMG := PRESET - (CGALS+S2)
         MOV    R1,#RES
         MOV    R2,#4
         SETB   C                ; CARRY IN FOR SUBTRACTION
         CALL   SUMUP
         JC     PULSIN8C
PULSIN8B:
         MOV    A,PULSFLG2       ; PRESENCE TESTING FLAG
         JNZ    PULSIN8C         ; SKIP IF EVER HERE BEFORE

         CALL   CLOSVLVS         ; COOL WAY TO USE FIRST STAGE FOR .1'S

         MOV    PULSFLG2,#1
         MOV    DPTR,#STOPFLG
         MOV    A,#1
         MOVX   @DPTR,A          ; FLAG DELIVERY STOPPED

         MOV    DPTR,#PRSSET
         MOV    A,#0             ; PULSFLGS GET CLOBBERED
         MOVX   @DPTR,A          ; DON'T COME BACK HERE

PUX:     MOV    A,#0
         MOV    DPTR,#SSFLAG
         MOVX   @DPTR,A          ; FLAG VALVES CLOSED
         MOV    DPTR,#DEMOFLG
         MOVX   @DPTR,A

PULSIN8C:RET


;************************************************
; DOES LOGICAL SHIFT RIGHT OF RESULT AREA ONE NIBBLE
;************************************************
SHIFR:   CLR     A                     ; SHIFT RESULT AREA DOWN
         MOV     R2,#3                 ; IE. 00 00 00 00 0X.XX XX XY
         MOV     R1,#PRESET+2          ; >>  00 00 00 00 00 X.X XX XX
SHIFR1:  XCH     A,@R1
         SWAP    A
         XCHD    A,@R1
         DEC     R1
         DJNZ    R2,SHIFR1
         RET

; LOADDP SETS THE DIGITAL POT FOR COMPENSATOR PROBE ADJUST
LOADDP:
         CALL    KPRESS
         MOV     A,KEY
         JZ      LOADDP2
         MOV     DPTR,#DPOTSET         ; POINT TO DPOT SETTING
         MOV     A,KEY
LOADDP8: CJNE    A,#UPKEY,LOADDP3
         MOVX    A,@DPTR
         CJNE    A,#255,LOADDP6        ; CHECK NOT TO OVERFLOW
         JMP     LOADDP2
LOADDP6: INC     A
         JMP     LOADDP5
LOADDP3: CJNE    A,#DOWNKEY,LOADDP2
         MOVX    A,@DPTR
         CJNE    A,#0,LOADDP7          ; CHECK NOT TO UNDERFLOW
         JMP     LOADDP2
LOADDP7: DEC     A
LOADDP5: MOVX    @DPTR,A
         JMP     LOADDP4
LOADDP2: MOV     DPTR,#DPOTSET
         MOVX    A,@DPTR               ; GET CURRENT DPOT BYTE

LOADDP4: MOV     R0,#8                 ; 8 BITS TO ROTATE OUT
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     KBCLK,#0xFE          ; CLR KBCLK CLOCK STARTS LOW
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CLR     KBSHLD                ; ACTUALLY DPOT DATA
         CALL    SELECTDP
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     KBCLK,#0x01		   ; SETB KBCLK
         NOP
         ANL     KBCLK,#0xFE			; CLR KBCLK
         ORL     KBCLK,#0x01			; SETB KBCLK
         NOP
         ANL     KBCLK,#0xFE			; CLR KBCLK 2 LOW BITS OUT FIRST
		 MOV     SFRPAGE,#00H          ;Switch SFR page
LOADDP1: RLC     A
         MOV     KBSHLD,C              ; PUT CARRY TO PORT
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     KBCLK,#0x01			; SETB KBCLK
         ANL     KBCLK,#0xFE			; CLR KBCLK CLOCK IT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         DJNZ    R0,LOADDP1
         CALL    SELECTLD
LOADDPN: RET

LOADPOT: MOV     DPTR,#DPOTSET
         MOVX    A,@DPTR               ; GET CURRENT DPOT BYTE
         MOV     R0,#8                 ; 8 BITS TO ROTATE OUT
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     KBCLK,#0xFE			; CLR KBCLK CLOCK STARTS LOW
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CLR     KBSHLD                ; ACTUALLY DPOT DATA
         CALL    SELECTDP              ; <<<
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     KBCLK,#0x01			; SETB KBCLK
         NOP
         ANL     KBCLK,#0xFE			; CLR KBCLK
         ORL     KBCLK,#0x01			; SETB KBCLK
         NOP
         ANL     KBCLK,#0xFE			; CLR KBCLK 2 LOW BITS OUT FIRST
		 MOV     SFRPAGE,#00H          ;Switch SFR page
LOADPO1: RLC     A
         MOV     KBSHLD,C              ; PUT CARRY TO PORT
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     KBCLK,#0x01			; SETB KBCLK
         ANL     KBCLK,#0xFE			; CLR KBCLK CLOCK IT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         DJNZ    R0,LOADPO1
         CALL    SELECT92
         RET

SHOWTBL: CALL    ADCR
         CALL    GETTC
         MOV     A,BCDLT
         CALL    MAKEASC
         MOV     DPTR,#DIGIT6
         MOV     TEMP,A
         MOV     A,B
         MOVX    @DPTR,A
         MOV     A,TEMP
         MOV     DPTR,#DIGIT5
         MOVX    @DPTR,A
         MOV     A,BCDHT
         CALL    MAKEASC
         MOV     DPTR,#DIGIT4
         MOV     TEMP,A
         MOV     A,B
         MOVX    @DPTR,A
         MOV     A,TEMP
         MOV     DPTR,#DIGIT3
         MOVX    @DPTR,A

         MOV     A,MSB                 ; GET MSB
         CJNE    A,#HIGH BREAK,SHOWT1
         MOV     A,LSB                 ; GET LSB
         CJNE    A,#LOW BREAK,SHOWT2
         SJMP    SHOWT4                ; M:L == H:L IF HERE
SHOWT2:  JC      SHOWT4                ; C==0 IF L > L
         SJMP    SHOWT3                ; SKIP CORR IF L < L
SHOWT1:  JNC     SHOWT3                ; C==1 IF M < H
SHOWT4:  MOV     DPTR,#DIGIT2
         MOV     A,#'1'
         MOVX    @DPTR,A
         JMP     SHOWT5
SHOWT3:  MOV     DPTR,#DIGIT2
         MOV     A,#SPACE
         MOVX    @DPTR,A
SHOWT5:  MOV     DPTR,#DIGIT1
         MOV     A,#SPACE
         MOVX    @DPTR,A
         MOV     A,COMP
         JNZ     SHOWT6                ; ADD 1.XXXX IF COMP OFF
         MOV     DPTR,#DIGIT2
         MOV     A,#'1'
         MOVX    @DPTR,A
SHOWT6:
         CALL    LGD108
         CALL    SHOWVL
         RET

; TEMPF WILL CALCULATE TEMPERATURE IN DEGREES F AND DISPLAY
;   BOTH ON PC AND ON LEDS
TEMPF:   MOV     DPTR,#DECFLG
         MOV     A,#1                  ; FLAG KEEP DECIMAL ON
         MOVX    @DPTR,A

         CALL    ADCR
         MOV     TVALH,#HIGH DTF      ; PUT DELTA T IN RWM
         MOV     TVALL,#LOW DTF
         MOV     TNEGH,#HIGH MINF     ;  PUT 10S COMP OF MIN TEMP IN RWM
         MOV     TNEGL,#LOW MINF
         CALL    BCDTMP                ; CONVERT TO BCD TEMPERATURE
         CALL    TMPOUT                ; TO PC

         MOV     DPTR,#DECFLG
         MOV     A,#0
         MOVX    @DPTR,A
         RET

; TEMPC WILL CALCULATE TEMPERATURE IN DEGREES C AND DISPLAY
;   BOTH ON PC AND ON LEDS
TEMPC:   MOV     DPTR,#DECFLG
         MOV     A,#1
         MOVX    @DPTR,A

         CALL    ADCR
         MOV     TVALH,#HIGH DTC
         MOV     TVALL,#LOW DTC
         MOV     TNEGH,#HIGH MINC
         MOV     TNEGL,#LOW MINC
         CALL    BCDTMP
         CALL    TMPOUT

         MOV     DPTR,#DECFLG
         MOV     A,#0
         MOVX    @DPTR,A
         RET


; BCDTMP CONVERT BCD TO TEMP+40
;  PLAN IS TO READ ADC ==> BCD
;  RES +1  +2  +3
;   48 45  71  01
BCDTMP:  CALL    BINBCD                ; BCDM:BCDL
         MOV     MP+1,BCDM             ; PUT IN PLACE
         MOV     MP,BCDL               ; LITTLE ENDIAN
         MOV     ML,TVALL              ; DELTA T LOW
         MOV     ML+1,TVALH            ;  HIGH
         CALL    CLRRES                ; CLEAR OUT RESULT AREA
         CALL    MLPLY                 ; 1023*.1676 = 01 71.5X XX
         RET                           ;  XX BECAUSE ITS ROUNDED

; TMPOUT CONVERTS RES TO TEMP AND PUTS TO SCREEN
;   MUST CONVERT TO NEGATIVE VALUES BY USE OF
;    10'S COMPLEMENT ARITHMETIC
TMPOUT:  MOV     A,RES+1               ; GET LOWEST MEANINGFUL DIGIT
         ANL     A,#0F0H
         CLR     C                     ; 9'S COMP + 1
         ADD     A,TNEGL               ; IE. $40 + $10
         DA      A                     ; DECIMAL ADJUST
         MOV     RES,A                 ; SAVE IN RES TEMPORARILY
         MOV     A,RES+2               ; NEXT DIGIT UP
         ADDC    A,TNEGH
         DA      A
         MOV     RES+1,A               ; SAVE IN RES AGAIN
         MOV     A,RES+3               ; GET NEXT DIGIT
         ADDC    A,#99H
         DA      A
         MOV     RES+2,A               ; ANSWER IN RES..+2
         JC      TMPOK                 ; IF CARRY THEN OK AS IS
TMPNOK:  CLR     C                     ; C=BORROW
         MOV     A,#90H                ; NUMBER IS NEGATIVE...
         SUBB    A,RES                 ;  99 99 90 - NUM
         MOV     RES,A                 ; BACK IN THERE
         MOV     A,#99H
         SUBB    A,RES+1
         MOV     RES+1,A
         MOV     A,#99H
         SUBB    A,RES+2
         MOV     RES+2,A
         CLR     C                     ; INITIALIZE CY
         MOV     A,#10H                ;  MUST ADD +1 ($10)
         ADDC    A,RES
         DA      A
         MOV     RES,A
         CLR     A
         ADDC    A,RES+1
         DA      A
         MOV     RES+1,A
         MOV     A,RES+2
         ADDC    A,#0
         DA      A
         MOV     RES+2,A
         MOV     RES+3,#'-'            ; SHOW NEG FLAG
TMPOK:
         MOV     A,RES
         CALL    MAKEASC
         MOV     DPTR,#DIGIT6
         MOVX    @DPTR,A
         MOV     A,RES+1
         CALL    MAKEASC
         MOV     DPTR,#DIGIT5
         MOV     TEMP,A
         MOV     A,B
         MOVX    @DPTR,A
         MOV     A,TEMP
         MOV     DPTR,#DIGIT4
         MOVX    @DPTR,A
         MOV     A,RES+2
         CALL    MAKEASC
         MOV     A,B                   ; WILL TEST FOR 0 OR MINUS
         CJNE    A,#'1',TMP1
         MOV     DPTR,#DIGIT3
         MOV     TEMP,A
         MOV     A,B
         MOVX    @DPTR,A
         MOV     A,TEMP
         JMP     TMP2
TMP1:    MOV     A,RES+3
         CJNE    A,#'-',TMP3
         MOV     DPTR,#DIGIT3
         MOV     A,#'-'
         MOVX    @DPTR,A
         JMP     TMP2
TMP3:    MOV     DPTR,#DIGIT3
         MOV     A,#SPACE
         MOVX    @DPTR,A
TMP2:    MOV     DPTR,#DPOTSET
         MOVX    A,@DPTR
         CALL    MAKEASC
;         MOV     A,#' '
;         MOV     B,A                   ; MASK DPOT SETTING = THIS WOULD HIDE THE DPOT SETTING ON THE SCREEN WHEN VIEWING TEMPS, SHWO IT IN DEL MODE AND CAL MODE BOTH FOR NOW
         MOV     DPTR,#DIGIT1
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT2
         MOV     TEMP,A
         MOV     A,B
         MOVX    @DPTR,A
         MOV     A,TEMP

         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JNC     TMP4
         CALL    LF0118
         JMP     TMP5
TMP4:    CALL    LF0100
TMP5:    CALL    SHOWVL
         RET

;**************************************
; CLRRES PUTS 00 00 05 00 IN RESULT AREA
;  SO MULT WILL ROUND TO XX XX.X- --
CLRRES:  MOV     RES,#0                ; DON'T LAUGH
         MOV     RES+1,#5              ;  ITS THE FASTEST
         MOV     RES+2,#0              ;   WAY KNOWN (ROUNDS TOO)
         MOV     RES+3,#0              ;    TO MAN
         RET

CLRRSLT: MOV     R7,#8
         MOV     R0,#RESULT
         MOV     A,#0
CLRR1:   MOV     @R0,A
         INC     R0
         DJNZ    R7,CLRR1
         RET

CLRRP:   MOV     DPTR,#RAWPLS
         MOV     R1,#3
         MOV     A,#0
CLRRP1:  MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,CLRRP1
         RET

;**************************************
; BINBCD CONVERTS MSB,LSB WHICH IS HEX AND *2 INTO THEIR
;        BCD EQUIVALENT BCDM, BCDL
BINBCD:  MOV     R3,MSB                ; GET OPERANDS INTO
         MOV     R4,LSB                ;  R3:R4
         MOV     R7,#10                ; TEN BITS WORTH
         MOV     R5,#0                 ; FINAL ANSWER IN
         MOV     R6,#0                 ;  R5:R6
BINBCD1: XCH     A,R4                  ; SAVE A AND GET R4
         RLC     A                     ; GET MSB OF OF LSB IN CY
         XCH     A,R4                  ; PUT IT BACK
         XCH     A,R3                  ; GET MSB AND
         RLC     A                     ; PUT CY IN LSB AND
         XCH     A,R3                  ;  RETURN.  CY=MSBIT
         MOV     A,R3
         CLR     C
         JNB     ACC.3,BINBCD2         ; IF R3.3 == 0 SKIP
         SETB    C                     ; SET CARRY
BINBCD2: MOV     A,R6                  ; GET R6: BCDL
         ADDC    A,R6                  ; ADD 1
         DA      A                     ; ADJUST IT
         MOV     R6,A                  ; PUT IT BACK
         MOV     A,R5                  ; GET BCDH  .. HIGH BYTE
         ADDC    A,R5                  ; RIPPLE CARRY
         DA      A                     ; ADJUST
         MOV     R5,A
         DJNZ    R7,BINBCD1            ; ALL TEN TIMES ?
         MOV     BCDM,R5               ; SAVE RESULTS
         MOV     BCDL,R6               ;
         RET                           ; EXIT FROM BINBCD

;******************************************************
; MAKEASC CONVERTS ACC HEX VALUE INTO ASCII
;  PLACES RESULT IN ACCA:ACCB  IE $31 --> A=$33 B=$31
MAKEASC: PUSH    DPH
         PUSH    DPL                   ; SAVE DPTR
         PUSH    ACC
         MOV     DPTR,#HEXTABL         ; POINT TO TABLE
         ANL     A,#0FH                ; MASK OFF LSN
         MOVC    A,@A+DPTR             ; GET ASCII EQ
         MOV     B,A
         POP     ACC
         SWAP    A
         ANL     A,#0FH
         MOVC    A,@A+DPTR
         POP     DPL
         POP     DPH                   ; RESTORE DPTR
         RET

SENDASC:    CALL    MAKEASC
            CALL    OUTA
            MOV     A,B
            CALL    OUTA
            MOV     A,#' '
            CALL    OUTA
            RET


;; *****************************************************
;; OUTPUT HEX VALUE IN R1 IN ASCII IE 0X2F == '2F'
;OUTHEX:
;           MOV     A,R1             ; GET MSN
;           SWAP    A                ;  TO ASCII
;           ANL     A,#0FH           ;   ISOLATE IT AND
;           CALL    PUTA             ; CONVERT TO ASCII
;           MOV     A,R1             ; GET LSN
;           ANL     A,#0FH           ; ISOLATE LSB
;           CALL    PUTA             ;
;           RET

;; *****************************************************
;; OUTPUT LSB OF ACC IN ASCII FROM TABLE HEXTABL
;PUTA:      MOV     DPTR,#HEXTABL    ; POINT TO TABLE
;           MOVC    A,@A+DPTR        ; GET THE ASCII CHAR
;           CALL    OUTA             ; THE REAL THING
;           RET




DOIT:    CLR     C                     ; CECGALS += CFCTR
         MOV     R2,#6                 ; FIVE BYTES
         MOV     CFCTR+5,#0            ; SLOPPY FIX FOR EXTENDED VOLUME
         MOV     R0,#CFCTR             ; POINTER FROM
         MOV     R1,#CECGALS           ;  PTR TO
DOIT1:   MOV     A,@R0                 ; GET BYTE

         ADDC    A,@R1                 ;  ADD ECGALS BYTE
         DA      A                     ; ADJUST IT
         MOV     @R1,A                 ; PUT IT IN ECGALS
         INC     R0                    ; UP PTRS
         INC     R1                    ;
         DJNZ    R2,DOIT1              ; TILL DONE ALL FIVE BYTES

         MOV     R2,#6
         MOV     SWFCTR+5,#0
         MOV     R0,#SWFCTR
         MOV     R1,#UECGALS
         CLR     C
DOIT2:   MOV     A,@R0
         ADDC    A,@R1
         DA      A
         MOV     @R1,A
         INC     R0
         INC     R1
         DJNZ    R2,DOIT2

DOIT3:   MOV     DPTR,#NGFLAG
         MOVX    A,@DPTR
         JNZ     DOIT4
         MOV     R0,#CECGALS           ; IF NOT COMP THEN DO COMP FIRST
         CALL    DOIT5
         MOV     DPTR,#NVOL
         CALL    SAVEVOL
         MOV     R0,#UECGALS
         CALL    DOIT5
         MOV     DPTR,#GVOL
         CALL    SAVEVOL
         JMP     GPULSCHK
DOIT4:
         MOV     R0,#UECGALS           ; IF COMP THEN DO UNCOMP FIRST
         CALL    DOIT5
         MOV     DPTR,#GVOL
         CALL    SAVEVOL
         MOV     R0,#CECGALS
         CALL    DOIT5
         MOV     DPTR,#NVOL
         CALL    SAVEVOL
         JMP     GPULSCHK

; ROUND EXTENDED CORRECTED GALLONS INTO NEW CGALS
;  ECGALS OF THE FORM AT BEGINNING  XX XX.XX XX XX
;   AND WE ROUND ON THE WAY THRU +5        5
;  NCGALS OF THE FORM AT END XX XX.XX 00
;  45 CYCLES
;    POSSIBLE CHANGE:  MAKE CGALS AND NCGALS THREE BYTES LONG
;                      OR ADD TWO POSITIONS.  XX XX XX.XX
DOIT5:   MOV     R2,#6
         MOV     R1,#ECGALS
DOIT6:   MOV     A,@R0
         MOV     @R1,A
         INC     R1
         INC     R0
         DJNZ    R2,DOIT6              ; MOVE CORRECT FACTOR INTO ECGALS
REC2NC:  CLR     C                     ; GONNA USE ADDC
         MOV     R2,#4                 ; FOUR BYTES
         MOV     A,#5H                 ; ROUNDING DIGIT
         MOV     R0,#ECGALS+2
         MOV     R1,#NCGALS+1          ; PTRS
REC2NC1: ADDC    A,@R0                 ; ADD
         DA      A                     ;  ADJUST
         MOV     @R1,A                 ; SAVE IN NCGALS
         CLR     A                     ; CLEAR A FOR NEXT GO
         INC     R0
         INC     R1
         DJNZ    R2,REC2NC1            ; BACK AROUND AGAIN
         MOV     NCGALS,#0             ; PUT ZERO IN LSB
         ANL     NCGALS+1,#0F0H        ; INSURANCE FOR FORM XX XX.X0 00
         RET

; *****************************************************
; CHECK FOR GALLONS ROLLOVER.  IF NOT EQUAL THEN PULSE TOTALIZER
; R1:R2 = CGALS XX.X0 ;
; WHILE (CGALS != NCGALS){ PULSELED; PULSE8K; IF(X==TENTH) PULSETOTAL;CGALS++}
;
;  MODIFIED  BACK TO GALLONS   5/20/93
;
GPULSCHK:
         MOV     DPTR,#EMCFLAG
         MOVX    A,@DPTR
         JNZ     GPUL1
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     EMCOUNT,#0x80			; SETB EMCOUNT added 5/19/06   sez
		 MOV     SFRPAGE,#00H          ;Switch SFR page
GPUL1:
         MOV     R1,CGALS+2            ; TEMP SAVE FOR LOOPING FROM BOTTOM
         MOV     R2,CGALS+1
GPULSCH1:
         MOV     A,R1                  ; NCGALS = FG HI.J0 00  ACCA = CD'
         CJNE    A,NCGALS+2,GPULSCH2   ; CD' ?= HI
         MOV     A,R2                  ; ACCA = E0'
         CJNE    A,NCGALS+1,GPULSCH2   ; IF != GOTO 2:
         SJMP    PULSCH3               ; IF == THEN YOU'RE DONE
GPULSCH2:
         SETB    DMCLK
         SETB    RRCNT
		 CALL    WAIT20U				; ALLOW 20 us FOR DM
         CLR     DMCLK
         CLR     RRCNT                 ; DO REMOTE READOUT TOO
		 CALL    WAIT20U				;DM NEEDS AT LEAST 60us, SO 100us TO BE SAFE
		 CALL    WAIT20U				;DM NEEDS AT LEAST 60us, SO 100us TO BE SAFE

         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#2,GPULSCH2_SLS0    ; 2=PA21 SLSMODE

         MOV     A,#'*'
         CALL    OUTA                  ; SLSMODE - PROB BAD IDEA HERE, 1 CHAR PER PULSE

GPULSCH2_SLS0:
         MOV     A,R2                  ; R1:R2 += .1
         ADD     A,#10H
         DA      A                     ; AND BE SURE TO DAA
         MOV     R2,A
         MOV     A,R1
         ADDC    A,#0                  ; PROPAGATE CARRY
         DA      A
         MOV     R1,A
         CALL	 WAIT3					;Wait 3ms for DM. Tried less time and missed pulses
;         MOV     R7,#20
;         DJNZ    R7,$                 ; SPREAD PULSES 2.5 MS
         SJMP    GPULSCH1              ; LOOP TIL YOU DROP
; MOVE NCGALS TO CGALS    8 CYCLES
PULSCH3: MOV     CGALS,NCGALS          ; REALLY ! CAN'T BEAT IT
         MOV     CGALS+1,NCGALS+1      ; QUICK AND SHORT TOO
         MOV     CGALS+2,NCGALS+2
         MOV     CGALS+3,NCGALS+3
         MOV     CGALS+4,NCGALS+4
//	Need a delay here for EM. Original code was "25uS. TWICE QUAD DETECTION TIME ;was #10  sez."
//	With no delay the eCount would over register pulses for temperatures above 60 deg F
		 CALL	 WAIT20U				;ADDED PAUSE BACK IN TESTING EXTRA PULSES IN TEMP COMPENSATION
		 CALL	 WAIT5U;
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     EMCOUNT,#0x7F			; CLR EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         RET

SAVEVOL:
         MOV   A,NCGALS+1
         MOVX  @DPTR,A
         INC   DPTR
         MOV   A,NCGALS+2
         MOVX  @DPTR,A
         INC   DPTR
         MOV   A,NCGALS+3
         MOVX  @DPTR,A
         INC   DPTR
         MOV   A,NCGALS+4
         MOVX  @DPTR,A
         RET

; TC := (MEM<TTABLE + MSB,LSB + 2*X>    )
;       (MEM<TTABLE + MSB,LSB + 2*X + 1>)
;    VALUE IS IN PACKED BCD  AND  * 0.1 FOR 10 PULSES/LITRE
; MSB:LSB ALTERED FROM THIS POINT FORWARD.
GETTC:
         CALL    CMPPTR           ; GET TABLE# / PRCOD RETURNED AS DPTR TO VALUE
         MOVX    A,@DPTR		  ; RETRIEVE COMP TABLE VALUE 0-9, 0 = NO COMP
		 ;JZ		 TABLE10		  ; IF =0 THEN NO COMP SO NEED TO LOAD 0.000

TABLE1:  CJNE    A,#1,TABLE2
         MOV     A,#LOW PROPANE  ; GET POINTER TO TABLEHEX
         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
         MOV     DPL,A            ;   RESERVED WORD DPL
         MOV     A,#HIGH PROPANE ;; NOW FOR THE OTHER
         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
         MOV     DPH,A            ;     AND PTR COMPLETE NOW
         JMP     TABLE11
TABLE2:  CJNE    A,#2,TABLE3
         MOV     A,#LOW FUELOIL  ; GET POINTER TO TABLEHEX
         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
         MOV     DPL,A            ;   RESERVED WORD DPL
         MOV     A,#HIGH FUELOIL ;; NOW FOR THE OTHER
         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
         MOV     DPH,A            ;     AND PTR COMPLETE NOW
         JMP     TABLE11
TABLE3:  CJNE    A,#3,TABLE4
         MOV     A,#LOW GASOLINE ; GET POINTER TO TABLEHEX
         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
         MOV     DPL,A            ;   RESERVED WORD DPL
         MOV     A,#HIGH GASOLINE;; NOW FOR THE OTHER
         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
         MOV     DPH,A            ;     AND PTR COMPLETE NOW
         JMP     TABLE11
TABLE4:  CJNE    A,#4,TABLE5
         MOV     A,#LOW LUBEOIL ; GET POINTER TO TABLEHEX
         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
         MOV     DPL,A            ;   RESERVED WORD DPL
         MOV     A,#HIGH LUBEOIL ;; NOW FOR THE OTHER
         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
         MOV     DPH,A            ;     AND PTR COMPLETE NOW
         JMP     TABLE11
TABLE5:  CJNE    A,#5,TABLE6
         MOV     A,#LOW METHANOL ; GET POINTER TO TABLEHEX
         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
         MOV     DPL,A            ;   RESERVED WORD DPL
         MOV     A,#HIGH METHANOL;; NOW FOR THE OTHER
         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
         MOV     DPH,A            ;     AND PTR COMPLETE NOW
         JMP     TABLE11
TABLE6:  CJNE    A,#6,TABLE7
         MOV     A,#LOW AVGAS    ; GET POINTER TO TABLEHEX
         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
         MOV     DPL,A            ;   RESERVED WORD DPL
         MOV     A,#HIGH AVGAS   ;; NOW FOR THE OTHER
         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
         MOV     DPH,A            ;     AND PTR COMPLETE NOW
         JMP     TABLE11
TABLE7:  CJNE    A,#7,TABLE8
         MOV     A,#LOW JETA     ; GET POINTER TO TABLEHEX
         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
         MOV     DPL,A            ;   RESERVED WORD DPL
         MOV     A,#HIGH JETA    ;; NOW FOR THE OTHER
         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
         MOV     DPH,A            ;     AND PTR COMPLETE NOW
         JMP     TABLE11
TABLE8:  CJNE    A,#8,TABLE9
         MOV     A,#LOW JETB     ; GET POINTER TO TABLEHEX
         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
         MOV     DPL,A            ;   RESERVED WORD DPL
         MOV     A,#HIGH JETB    ;; NOW FOR THE OTHER
         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
         MOV     DPH,A            ;     AND PTR COMPLETE NOW
         JMP     TABLE11
TABLE9:  CJNE    A,#9,TABLE10
         MOV     A,#LOW ETHANOL  ; GET POINTER TO TABLEHEX
         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
         MOV     DPL,A            ;   RESERVED WORD DPL
         MOV     A,#HIGH ETHANOL ;  NOW FOR THE OTHER
         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
         MOV     DPH,A            ;     AND PTR COMPLETE NOW
         JMP	 TABLE11
TABLE10: MOV	 DPTR, #NOCMP	  ;NO COMPENSATION SO USE 1.000

; ONLY 9 TABLES, TOO MANY PROGRAMS USE 1 DIGIT, 1-9 (MATRIX, ETC)
;         CJNE    A,#10,TABLE11
;         MOV     A,#LOW AVGAS    ; GET POINTER TO TABLEHEX
;         ADD     A,LSB            ;  AND OFFSET INTO DPTR LOW
;         MOV     DPL,A            ;   RESERVED WORD DPL
;         MOV     A,#HIGH AVGAS   ;  NOW FOR THE OTHER
;         ADDC    A,MSB            ;   SHOE TO DROP WITH CY
;         MOV     DPH,A            ;     AND PTR COMPLETE NOW

TABLE11:
;;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
;         MOVX    A,@DPTR
;;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
;         MOV     MP+1,A                ; REVERSES ORDER TOO...
;         INC     DPTR                  ; GET NEXT BYTE...
;;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
;         MOVX    A,@DPTR
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
;         MOV     MP,A                  ;   IE. BCDL


         CALL    VCF                    ; GET 2 BYTES FROM @DPTR IN NVRAM.
         MOV     MP+1,TEMP              ; REVERSES ORDER HERE TOO
         MOV     MP,TEMP+1              ; TEMP+1 IS ALSO "COMMAND". JUST DOUBLE DUTY

         MOV     A,COMP
         JNZ     GOON                  ; COMP ON, GO ON
         MOV     MP+1,#00H             ; OTHERWISE ZERO OUT
         MOV     MP,#00H
         MOV     A,#0
         JMP     GOON                  ; DISABLED - MAKE 1.0000
GOON:    MOV     BCDHT,MP+1            ;  BCDH TEMP
         MOV     BCDLT,MP              ;  BCDL TEMP

         MOV     ML+1,SWARRAY+2        ; AND ONLY USE 4X4 MULTIPLY
         MOV     ML,SWARRAY+1          ;  .XX XX
         MOV     RES+3,#0              ; INITIALIZE RES
         MOV     RES+2,#0              ;  TO  .00 00 50 00
         MOV     RES+1,#50H            ; ROUNDING WHILE HERE
         MOV     RES,#0                ; TEMPCORR IN RES
         CALL    MLPLY                 ; ONLY FRACTIONAL PART FIRST
         MOV     RES,RES+1             ; MOVE ONE BYTE RIGHT
         MOV     RES+1,RES+2
         MOV     RES+2,RES+3
         MOV     RES+3,#0              ; RES IS NOW 00.XX XX 00
         MOV     A,COMP
         JNZ     GETTC7
         JMP     GETTC4


GETTC7:  MOV     A,MSB                 ; GET MSB
         CJNE    A,#HIGH BREAK,GETTC1
         MOV     A,LSB                 ; GET LSB
         CJNE    A,#LOW BREAK,GETTC2
         SJMP    GETTC4                ; M:L == H:L IF HERE
GETTC2:  JC      GETTC4                ; C==0 IF L > L
         SJMP    GETTC3                ; SKIP CORR IF L < L
GETTC1:  JNC     GETTC3                ; C==1 IF M < H
GETTC4:  MOV     A,RES+1
         ADD     A,SWARRAY+1           ; IF BELOW BREAK THEN ADD IN
         DA      A                     ;  SWITCHES SINCE IT BECOMES 1.CCCC
         MOV     RES+1,A
         MOV     A,RES+2
         ADDC    A,SWARRAY+2
         DA      A
         MOV     RES+2,A
         MOV     A,RES+3
         ADDC    A,#0
         DA      A
         MOV     RES+3,A
         MOV     A,SWARRAY
         ANL     A,#0FH
         JZ      GETTC3
         MOV     A,RES+3
         ADD     A,#1
         DA      A
         MOV     RES+3,A               ; ACCOUNT FOR BOTH BEING 1.XXXX
GETTC3:  MOV     A,SWARRAY
         ANL     A,#0FH
         JZ      GETTC5                ; IE.  IF IN ZERO POSITION USE AS 0.SSSS
         MOV     A,RES+1
         ADD     A,BCDLT               ; ELSE SINCE SWITCHES ARE 1.SSSS ADD IN
         DA      A                     ;  BCD HIGH AND LOW TEMPERATURE FROM TABLE
         MOV     RES+1,A
         MOV     A,RES+2
         ADDC    A,BCDHT
         DA      A
         MOV     RES+2,A
         MOV     A,RES+3
         ADDC    A,#0
         DA      A
         MOV     RES+3,A
GETTC5:  CALL    RRF
         MOV     A,RES+3
         ANL     A,#0FH
         SWAP    A
         ORL     RES+2,A
GETTC6:  ANL     RES,#0F0H
GETTC8:  MOV     CFCTR,RES             ; 0.1 PULSES/LITRE
         MOV     CFCTR+1,RES+1         ; INITIAL LOAD OF CFCTR
         MOV     CFCTR+2,RES+2         ; CFCTR IS OF THE FORM NOW
         MOV     CFCTR+3,#0            ;   00 00.0X XX XX
         MOV     CFCTR+4,#0
         RET

VCF:
		MOV		 R1,#TEMP
		CLR		 A
		MOVC	 A,@A+DPTR
		MOV		 @R1,A
		CLR		 A
		INC		 A
		MOVC	 A,@A+DPTR
		INC		 R1
		MOV		 @R1,A
        RET


SAND:                          ; SEND WAS TAKEN
         MOV     R2,#8         ; BITS PER BYTE - R1 HAS # OF BYTES  72 TOTAL
SAND3:
         MOV     A,DPH
SAND2:   RRC     A
         ;MOV     DATAOUT,C     ; PUT BIT ON PORT <<<<<<<<
         //Because P4 is not bit addressable, we must use this to send data out
		 MOV     TEMPBIT,#0     ; LOAD TEMPBIT with 0's
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
		 ANL     P4,#239        ; Clear P4.4
		 MOV     SFRPAGE,#00H          ;Switch SFR page
		 MOV     TEMPBIT.4,C    ; LOAD CARRY BIT INTO 5th BIT
		 PUSH    ACC
		 MOV     A,TEMPBIT
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
		 ORL     P4,A           ; WRITE 5th BIT BACK TO P4.4 which is DATAOUT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
		 POP     ACC
         JNB     CLK,$         ; WAIT FOR CLOCK TO GO HIGH
         JB      CLK,$         ; WAIT TO GO LOW

         DJNZ    R2,SAND2      ; DO 8 TIMES
         MOV     R2,#8         ; RELOAD BIT COUNTER

         MOV     A,DPL
         DJNZ    R1,SAND2      ; DO R1 TIMES
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     DATAOUT,#0xEF			; CLR DATAOUT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         RET


; ADC DOES READ OF ADC WITH 2* RESULT IN MSB:LSB
; LTC1092  10-BIT READ.
ADCR:
         CALL    SELECTLD              ; NOT SURE ABOUT "SETB  NCS"
         CALL    SELECT92
         CLR     ADCLK
         MOV     DPTR,#PROBE
         MOVX    A,@DPTR
         JZ      ADCR3
         MOV     R4,#4
         JMP     ADCR1                 ; WASTE BITS BYT PROBE/ADC TYPE
ADCR3:   MOV     R4,#6                 ; LOOP CTR
ADCR1:   MOV     C,ADDO                ; TWO WASTE BITS
         RLC     A
         SETB    ADCLK                 ; PULSE ADC CLOCK BIT
         CALL	 WAIT2U				   ; WAIT FOR TIMING OF HC138 MIN.14us AND MAX 20us
         CLR     ADCLK                 ; HI/LOW
         DJNZ    R4,ADCR1              ;
         ANL     A,#3                  ; ISOLATE THREE MSB
         MOV     MSB,A                 ;
         MOV     R4,#8                 ; LOOP CTR
ADCR2:   MOV     C,ADDO                ; TWO WASTE BITS
         RLC     A                     ;
         SETB    ADCLK                 ; PULSE ADC CLOCK BIT
         CALL	 WAIT2U				   ; WAIT FOR TIMING OF HC138 MIN.14us AND MAX 20us
         CLR     ADCLK                 ; HI/LOW
         DJNZ    R4,ADCR2              ;
         MOV     LSB,A                 ;
         RLC	 A
         ANL     A,#0FEH
         MOV     LSB,A
         MOV     A,MSB
         RLC	 A
         MOV     MSB,A
         CALL    SELECTLD
         RET                           ; EXIT FROM ADC


CHKATC:
         MOV     A,COMP
         JZ      CHKATC3               ; DON'T TEST IF NO COMP

         MOV     R0,LSB                ; START CHECK FOR 1'S & 0'S
         MOV     R1,MSB
         MOV     A,R0
         ORL     A,R1
         CJNE    A,#0,CHKATC1          ; KILL VALVES--ALL ZEROES
         JMP     CHKATC2

CHKATC1: MOV     A,MSB
         CJNE    A,#7,CHKATC3          ; CHECK MSB FOR ALL HIGH
         MOV     A,LSB
         CJNE    A,#0FEH,CHKATC3       ; CHECK LSB ALL HIGH
CHKATC2: MOV     DPTR,#ATCCNT
         MOVX    A,@DPTR
         CJNE    A,#10,CHKATC4         ; MUST BE BAD XX TIMES
         CALL    CLOSVLVS
         MOV     DPTR,#ATCFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG ATC FAILED
         JMP     CHKATC3
CHKATC4: ADD     A,#1
         MOVX    @DPTR,A               ; BUMP IT ONE
CHKATC3: RET


DOSWFAC: CALL    CLRRES
         MOV     RES+2,SWARRAY+2
         MOV     RES+1,SWARRAY+1
         MOV     RES,#0
         CALL    RRF                   ; RES ?? 0S SS S0

DOSWFAC2:MOV     SWFCTR+4,#0
         MOV     SWFCTR+3,#0
         MOV     SWFCTR+2,RES+2
         MOV     SWFCTR+1,RES+1
         MOV     SWFCTR,RES
         MOV     A,SWARRAY             ; GET ADDED SWITCH POSITION
         ANL     A,#0FH
         XRL     A,#1
         JNZ     DOSWFAC1
DOSWFAC3:ORL     SWFCTR+2,#10H
DOSWFAC1:RET

; ***********************************************
; GETPRST GETS PRESET VALUE FROM KEYBOARD
; ***********************************************
GETPRST: MOV     DPTR,#LASTPRST
         CALL    GETVOL

         MOV     DPTR,#PSMSD
         MOVX    A,@DPTR
         SWAP    A                     ; GET MSD AND MOV TO MSN
         ORL     VOLUME+2,A            ; RESTORE MSN

         CALL    GETUNIT
         CALL    LGD101
         MOV     DPTR,#LGDFLG2
         MOV     A,#44H
         JNB     PSW_F0,U28
         ANL     A,#0F9H
         ORL     A,#02H
U28:     MOVX    @DPTR,A
         CALL    DISPSET               ; SHOW IT
         CALL    BCD2HEX               ; CONVERTS PACKED BCD IN VOLUME
         SETB    PSW_F0
         CALL    ALLSIX
PRESET1: CALL    KPRESS
         JNC     PRESET2
         MOV     A,KEY
         JMP     PRESET3
         CJNE    A,#PRSETKEY,PRESET3   ; PRESET KEY 3, WHILE GETTING NEW PRESET (NO TESTING FOR PRESET ENABLED)
         CPL     PSW_F0                    ; TOGGLE SETTING ON/OFF
PRESET2:
         CALL    ENTRY
         JMP     PRESET1
PRESET3: CJNE    A,#SSKEY,PRESET7
         JMP     PRESET6
PRESET7:
         CJNE    A,#MODEKEY,PRESET1
         JMP     GETAMT

PRESET6: MOV     DPTR,#LASTPRST
         CALL    PUTVOL

         MOV     A,VOLUME+2            ; MSB
         SWAP    A                     ; SWAP NIBBLES
         ANL     A,#0FH                ; MASK MSN
         MOV     DPTR,#PSMSD
         MOVX    @DPTR,A               ; PUT AWAY.

;         MOV     PRESET,#50H
;         MOV     PRESET+1,#34H
;         MOV     PRESET+2,#12H         ; MOVE TO THE REAL THING

; PROTOTYPE
PRESET4:
         MOV     R0,#VOLUME+2          ;  A    VOL+2  VOL+1  VOL+0
         MOV     A,@R0                 ;  01    01     23     45
         SWAP    A                     ;  10    01     23     45
         MOV     @R0,A                 ;  10    10     23     45
         DEC     R0
         MOV     A,@R0                 ;  23    10     23     45
         SWAP    A                     ;  32    10     23     45
         INC     R0
         XCHD    A,@R0                 ;  30    12     23     45
         DEC     R0
         MOV     @R0,A                 ;  30    12     30     45
         DEC     R0
         MOV     A,@R0                 ;  45    12     30     45
         SWAP    A                     ;  54    12     30     45
         INC     R0
         XCHD    A,@R0                 ;  50    12     34     45
         DEC     R0
         MOV     @R0,A                 ;  50    12     34     50
                                       ; MAN! WHAT A TRICK

         ANL     VOLUME,#0F0H
         MOV     PRESET,VOLUME
         MOV     PRESET+1,VOLUME+1
         MOV     PRESET+2,VOLUME+2

BATPSET:
         MOV     A,PRESET
         ORL     A,PRESET+1
         ORL     A,PRESET+2            ; IF A!=0 THEN PRESETVOL !=0
         MOV     DPTR,#PRSSET
         JNZ     BATPSETY              ; PRESETVOL !=0
         MOV     DPTR,#PSMSD
         MOVX    A,@DPTR
         JNZ     BATPSETY              ; MSD PRESETVOL !=0
         MOV     A,#0
         JMP     BATPSETX              ; BOTH MSD AND PRESETVOL = 0
BATPSETY:MOV     A,#1
BATPSETX:MOV     DPTR,#PRSSET
         MOVX    @DPTR,A

         MOV     DPTR,#CONVFLG
         MOVX    A,@DPTR
         JNZ     PRESET5

         MOV     DPTR,#PSRQD
         MOVX    A,@DPTR
         JNZ     SKIPMSG

SKIPMSG: CALL    LASTVOL
         CALL    WAIT1000

PRESET5:
         MOV     DPTR,#CONVFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET FLAG
         MOV     DPTR,#FIRST
         MOV     A,#0
         MOVX    @DPTR,A
         RET

GETDRV:
         MOV     DPTR,#DRIVER
         CALL    GETVOL
         CALL    LF0000                ; NO ICONS
         MOV     DGTPOS,#3             ; START WITH DIGIT3 BLINK
         MOV     DATEIMG+1,#3          ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#3FH          ; 4 DIGITS WORTH
GETDRV1: CALL    KPRESS
         JNC     GETDRV2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETDRV2
         JMP     GETDRV3
GETDRV2: CALL    ENTRY
         JMP     GETDRV1
GETDRV3: MOV     DPTR,#DRIVER
         CALL    PUTVOL
         RET

GETTRK:
         MOV     DPTR,#TRUCK
         CALL    GETVOL
         CALL    LF0010                ; SETUP ICON ONLY
         MOV     DGTPOS,#3             ; START WITH DIGIT3 BLINK
         MOV     DATEIMG+1,#3          ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#3FH          ; 4 DIGITS WORTH
GETTRK1: CALL    KPRESS
         JNC     GETTRK2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETTRK2
         JMP     GETTRK3
GETTRK2: CALL    ENTRY
         JMP     GETTRK1
GETTRK3: MOV     DPTR,#TRUCK
         CALL    PUTVOL
         RET

GETTIM:
         CALL    RTIME
         MOV     VOLUME,DATEIMG+2      ; MINUTES
         MOV     VOLUME+1,DATEIMG+3    ; HOURS
         MOV     VOLUME+2,#00H
         CALL    LF0210
         MOV     DGTPOS,#3             ; START WITH DIGIT3 BLINK
         MOV     DATEIMG+1,#3          ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#3FH          ; 4 DIGITS WORTH
GETTIM1: CALL    KPRESS
         JNC     GETTIM2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETTIM2
         JMP     GETTIM3
GETTIM2: CALL    ENTRY
         JMP     GETTIM1
GETTIM3: MOV     DATEIMG+2,VOLUME
         MOV     DATEIMG+3,VOLUME+1
         MOV     DATEIMG+4,#0          ; START CLOCK
         CALL    WTIME
         RET

GETDAT:  CALL    RTIME
         MOV     VOLUME,DATEIMG+7      ; YEAR
         MOV     VOLUME+1,DATEIMG+5    ; DAY
         MOV     VOLUME+2,DATEIMG+6    ; MONTH
         CALL    LF0A10
         CALL    ALLSIX
GETDAT1: CALL    KPRESS
         JNC     GETDAT2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETDAT2
         JMP     GETDAT3
GETDAT2: CALL    ENTRY
         JMP     GETDAT1
GETDAT3: MOV     DATEIMG+7,VOLUME
         MOV     DATEIMG+5,VOLUME+1
         MOV     DATEIMG+6,VOLUME+2
         MOV     DATEIMG+4,#0          ; START CLOCK HERE 2
         CALL    WTIME
         RET

GHTIME:
         CALL    INTIME
         MOV     DATEIMG+7,A
         CALL    INTIME
         MOV     DATEIMG+6,A
         CALL    INTIME
         MOV     DATEIMG+5,A
         CALL    INTIME
         MOV     DATEIMG+3,A
         CALL    INTIME
         MOV     DATEIMG+2,A

         MOV     DATEIMG+4,#0

         CALL    WTIME
SEND0PIP:MOV     A,#'0'
         CALL    OUTA                  ; DATA GOOD BYTE FORCED 0
         CALL    SENDPIPE
         RET


INTIME:  CALL    INPA
         JNC     INTIME
         MOV     TEMP,A                ; TEMP SAVE ACC
INTIME1: CALL    INPA
         JNC     INTIME1
         MOV     B,A
         MOV     A,TEMP
         CALL    ASCHEX                ; MAKE 2 ASCII DIGITS INTO BCD IN ACC
         RET

SHTIME:  CALL    RTIME                 ; TIME DATE IN DATEIMG
         MOV     A,DATEIMG+7
         CALL    OUTTIME
         MOV     A,DATEIMG+6
         CALL    OUTTIME
         MOV     A,DATEIMG+5
         CALL    OUTTIME
         MOV     A,DATEIMG+3
         CALL    OUTTIME
         MOV     A,DATEIMG+2
         CALL    OUTTIME
         CALL    SENDPIPE
         RET


OUTTIME: CALL    HEXASC                ; MSB IN A - LSB IN B
         CALL    OUTA                  ; MSB IN ACC
         MOV     A,B
         CALL    OUTA
         RET

GETFIRST:
         MOV     VOLUME,S1SHOFF
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H

         CALL    LF0010
         CALL    DGT00FHD
         CALL    TWODIGAT5

GET1ST1: CALL    KPRESS
         JNC     GET1ST2
         MOV     A,KEY
         CJNE    A,#SSKEY,GET1ST2
         JMP     GET1ST3
GET1ST2: CALL    ENTRY
         JMP     GET1ST1
GET1ST3:
         MOV     S1SHOFF,VOLUME
         MOV     DPTR,#S1LAST
         MOV     A,S1SHOFF
         MOVX    @DPTR,A
         CALL    WAIT50
         RET                           ; S/S KEY GETS OUT

GETSECND:
         CALL    GETDWL
         MOV     VOLUME,S2DWELL
         MOV     VOLUME+1,S2DWELL+1
         MOV     VOLUME+2,#00H
         CALL    LF0110
         MOV     DGTPOS,#0
         MOV     DATEIMG,#3FH
         CALL    DISPSET

GET2ND:
         MOV     DGTPOS,#3             ; STARTING DIGIT POSITION
         MOV     DATEIMG+1,#3
         MOV     DATEIMG,#3FH          ; BLANKED DIGITS [00001111]

GET2ND1: CALL    KPRESS
         JNC     GET2ND2
         MOV     A,KEY
         CJNE    A,#SSKEY,GET2ND2
         JMP     GET2ND3
GET2ND2: CALL    ENTRY
         JMP     GET2ND1
GET2ND3:
         MOV     S2DWELL,VOLUME        ; SLOPPY FIX FOR PRESET OVERRUN
         MOV     DPTR,#S2LAST
         MOV     A,S2DWELL
         MOVX    @DPTR,A
         MOV     S2DWELL+1,VOLUME+1
         MOV     DPTR,#S2LAST+1
         MOV     A,S2DWELL+1
         MOVX    @DPTR,A
         CALL    PUTDWL
         CALL    WAIT50
         RET                           ; S/S KEY GETS OUT

GETCODE:
         CALL    SHOWENTCODE
         CALL    LF0010
         CALL    ALLSIX
GETCOD1: CALL    KPRESS
         JNC     GETCOD2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETCOD2
         JMP     GETCOD3
GETCOD2: CALL    ENTRY
         JMP     GETCOD1
GETCOD3: MOV     DPTR,#SETUPCOD
         MOVX    A,@DPTR
         CJNE    A,VOLUME,GETCOD4
         MOV     DPTR,#SETUPCOD+1
         MOVX    A,@DPTR
         CJNE    A,VOLUME+1,GETCOD4
         MOV     DPTR,#SETUPCOD+2
         MOVX    A,@DPTR
         CJNE    A,VOLUME+2,GETCOD4
         MOV     DPTR,#MSG44
         CALL    SHOW                  ; GOOD
         SETB    C
         JMP     GETCOD5
GETCOD4: MOV     DPTR,#MSG45           ; NO GOOD
         CALL    SHOW
         CLR     C
GETCOD5: RET

NEWCODE:
         MOV     DPTR,#SETUPCOD
         CALL    GETVOL
         CALL    LF0000
         CALL    ALLSIX
NEWCOD1: CALL    KPRESS
         JNC     NEWCOD2
         MOV     A,KEY
         CJNE    A,#SSKEY,NEWCOD2
         JMP     NEWCOD3
NEWCOD2: CALL    ENTRY
         JMP     NEWCOD1
NEWCOD3: MOV     DPTR,#SETUPCOD
         CALL    PUTVOL
         RET

SHOWENTCODE:
         MOV     DPTR,#MSG42           ; ENTER
         CALL    SHOW
         MOV     DPTR,#MSG43           ; CODE
         CALL    SHOW
         MOV     VOLUME,#00H
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H         ; CODE ALWAYS SHOWS ZERO
         RET




GETAMT:
         MOV     DPTR,#PREAMT
         MOV     A,#00H
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,#00H
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,#00H
         MOVX    @DPTR,A               ; TEST ENTRY

         MOV     DPTR,#MSG77
         CALL    SHOW

         MOV     DPTR,#PREAMT
         CALL    GETVOL

         CALL    LF0240
         CALL    ALLSIX

GETAMT1: CALL    KPRESS
         JNC     GETAMT2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETAMT2
         JMP     GETAMT3
GETAMT2: CALL    ENTRY
         JMP     GETAMT1
GETAMT3:
         MOV     DPTR,#PREAMT
         CALL    PUTVOL

         MOV     DPTR,#PRINTFLG
         MOV     A,#0
         MOVX    @DPTR,A

         CALL    SAVEVAR               ; SAVE ALL REUSED VARIABLES

         CALL    COMPPRES

         CALL    RESTVAR

         MOV     DPTR,#PRINTFLG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     GETPRST

GETSER:
         MOV     DPTR,#SERNUM
         CALL    GETVOL
         CALL    LF0018
         CALL    ALLSIX
GETSER1: CALL    KPRESS
         JNC     GETSER2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETSER2
         JMP     GETSER3
GETSER2: CALL    ENTRY
         JMP     GETSER1
GETSER3: MOV     DPTR,#SERNUM
         CALL    PUTVOL
         RET

GETPCE:
         MOV     DPTR,#PCEBCD
         CALL    GETVOL
         CALL    LF0800
         MOV     DGTPOS,#2             ; START WITH DIGIT1 BLINK
         MOV     DATEIMG+1,#2          ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#7FH          ; ALL 6 DIGITS
GETPCE1: CALL    KPRESS
         JNC     GETPCE2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETPCE2
         JMP     GETPCE3
GETPCE2: CALL    ENTRY
         JMP     GETPCE1
GETPCE3: MOV     DPTR,#PCEBCD
         CALL    PUTVOL

GETPCE4: MOV     R2,#3                 ; USED TO INITIALLY LOAD ZERO PRICE
         MOV     DPTR,#PCEBCD          ; WITH 6501PF SELECTION
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#3
         MOV     R0,#DATEIMG+2
         MOV     DPTR,#PRICE
         CALL    MOVDECASC
         RET

GETADJ:
         MOV     VOLUME,#0
         MOV     VOLUME+1,#0
         MOV     VOLUME+2,#0
         CALL    LF0800
         MOV     DGTPOS,#2             ; START WITH DIGIT1 BLINK
         MOV     DATEIMG+1,#2          ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#7FH          ; ALL 6 DIGITS
GETADJ1: CALL    KPRESS
         JNC     GETADJ2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETADJ2
         JMP     GETADJ3
GETADJ2: CALL    ENTRY
         JMP     GETADJ1
GETADJ3: MOV     DPTR,#ADJAMT
         MOV     A,VOLUME
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,VOLUME+1
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,VOLUME+2
         ANL     A,#0FH                ; MASK MSD
         MOVX    @DPTR,A               ; STORE BACK NEW CODE.

GETADJ8: MOV     DPTR,#MSG115          ; SHOW 'ADD'
         CALL    SHOW
         MOV     DPTR,#SIGNFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; ASSUME ADD AND SET FLAG

GETADJ4: CALL    KPRESS
         JNC     GETADJ4
         MOV     A,KEY
         CJNE    A,#SSKEY,GETADJ6
         JMP     GETADJ5
GETADJ6: CJNE    A,#UPKEY,GETADJ7
         JMP     GETADJ8
GETADJ7: CJNE    A,#DOWNKEY,GETADJ4
         MOV     DPTR,#MSG116
         CALL    SHOW
         MOV     DPTR,#SIGNFLG
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     GETADJ4

GETADJ5:
         MOV     BCDM,PRCOD            ; SAVE CURRENT PRODUCT CODE
         MOV     PRCOD,#1              ; START WITH FIRST (ZERO INDEXED)

GETADJ14:CALL    GETPCPR
         CALL    CONV2BCD

         MOV     DPTR,#PCEBCD
         MOV     R0,#3
GETADJ12:MOVX    A,@DPTR
         JNZ     GETADJ11
         INC     DPTR
         DJNZ    R0,GETADJ12
         JMP     GETADJ13              ; PRICE ZERO, SKIP TO NEXT PRCOD

GETADJ11:MOV     DPTR,#SIGNFLG         ; 0=- 1=+
         MOVX    A,@DPTR
         JZ      GETADJ9

         MOV     R2,#3
         MOV     DPTR,#ADJAMT          ; MOV VOLUME VARIABLE TO DATEIMG
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         CLR     C
         MOV     R2,#3                 ; 3 BYTES - 6 DIGITS
         MOV     DPTR,#PCEBCD
         MOV     R0,#DATEIMG
         CALL    SUMUPPER
         JMP     GETADJ10

GETADJ9:
         MOV     R2,#3
         MOV     DPTR,#ADJAMT
         MOV     R1,#DATEIMG
         CALL    EXTMOV                ; DATEIMG=ADJAMT
         MOV     R0,#DATEIMG
         MOV     R2,#3
         CALL    NEGATE                ; DATEIMG= -ADJAMT
         MOV     R2,#3
         MOV     DPTR,#PCEBCD
         MOV     R1,#ECGALS            ; ECGALS=PCEBCD
         CALL    EXTMOV
         MOV     R2,#3
         SETB    C                     ; PCEBCD-ADJAMT
         MOV     R0,#ECGALS
         MOV     R1,#DATEIMG
         CALL    SUMUP

         MOV     R0,#ECGALS
         MOV     R2,#3
         MOV     DPTR,#PCEBCD
         CALL    CARDMOV

GETADJ10:MOV     R2,#3
         MOV     DPTR,#PCEBCD
         MOV     R1,#DATEIMG
         CALL    EXTMOV                ; CONVERT BACK TO ASCII
         MOV     R2,#3
         MOV     R0,#DATEIMG+2
         MOV     DPTR,#PRICE
         CALL    MOVDECASC

         CALL    PUTPCPR               ; PUT BACK INTO MATRIX

GETADJ13:INC     PRCOD
         MOV     A,PRCOD
         CJNE    A,#100,GETADJ14
         MOV     PRCOD,BCDM            ; RESTORE PRODUCT CODE
         CALL    GETPCPR
         CALL    CONV2BCD
         RET

GETVOL:  MOVX    A,@DPTR
         MOV     VOLUME,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     VOLUME+1,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     VOLUME+2,A
         RET

PUTVOL:  MOV     A,VOLUME
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,VOLUME+1
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,VOLUME+2
         MOVX    @DPTR,A               ; STORE BACK NEW CODE.
         RET


GETQOB:
         CALL    QOBPTR                ; GET DPTR FOR PRCOD
         CALL    GETVOL
         CALL    LF0110
         CALL    ALLSIX
GETQOB1: CALL    KPRESS
         JNC     GETQOB2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETQOB2
         JMP     GETQOB3
GETQOB2: CALL    ENTRY
         JMP     GETQOB1
GETQOB3: CALL    QOBPTR
         CALL    PUTVOL
         RET

GETENT:
         MOV     DPTR,#ENTVOL          ; GET ENTERED VOLUME
         CALL    GETVOL

GETENTX: CALL    LF0110
         CALL    ALLSIX
GETENT1: CALL    KPRESS
         JNC     GETENT2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETENT2
         JMP     GETENT3
GETENT2: CALL    ENTRY
         JMP     GETENT1
GETENT3: MOV     DPTR,#ENTVOL
         CALL    PUTVOL
         RET

GETTAX1:
         MOV     DPTR,#TAX1BCD
         CALL    GETVOL
         CALL    LF0800
         MOV     DPTR,#TAX1T
         MOVX    A,@DPTR
         CJNE    A,#'$',GETTAX14
         MOV     DGTPOS,#2             ; START WITH DIGIT1 BLINK
         MOV     DATEIMG+1,#2          ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#7FH          ; ALL 6 DIGITS
         JMP     GETTAX11              ; 0X.XXXX FOR $
GETTAX14:CALL    ALLSIX                ; XX.XXXX FOR %
GETTAX11:CALL    KPRESS
         JNC     GETTAX12
         MOV     A,KEY
         CJNE    A,#SSKEY,GETTAX12
         JMP     GETTAX13
GETTAX12:CALL    ENTRY
         JMP     GETTAX11
GETTAX13:MOV     DPTR,#TAX1BCD
         MOV     A,VOLUME
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,VOLUME+1
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,VOLUME+2
         MOVX    @DPTR,A               ; STORE BACK NEW CODE.
         MOV     DPTR,#TAX1T
         MOVX    A,@DPTR
         CJNE    A,#'$',GETTAX15
         MOV     DPTR,#TAX1BCD+2
         MOVX    A,@DPTR
         ANL     A,#0FH
         MOVX    @DPTR,A               ; ZERO OUT MSD IF $

GETTAX15:MOV     R2,#3
         MOV     DPTR,#TAX1BCD
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#3
         MOV     R0,#DATEIMG+2
         MOV     DPTR,#TAX1            ; SERIAL NUMBER TO CARD IMAGE
         CALL    MOVDECASC
         RET

GETTAX2: MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JZ      GETTAX2A
         MOV     DPTR,#MSG121
         CALL    SHOW
         RET                           ; DON'T ALLOW TAX2 IF 6501

GETTAX2A:MOV     DPTR,#TAX2BCD
         CALL    GETVOL
         CALL    LF0800
         MOV     DPTR,#TAX2T
         MOVX    A,@DPTR
         CJNE    A,#'$',GETTAX24
         MOV     DGTPOS,#2            ; START WITH DIGIT1 BLINK
         MOV     DATEIMG+1,#2         ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#7FH         ; ALL 6 DIGITS
         JMP     GETTAX21             ; 0X.XXXX FOR $
GETTAX24:
         CALL    ALLSIX               ; XX.XXXX FOR %
GETTAX21:CALL    KPRESS
         JNC     GETTAX22
         MOV     A,KEY
         CJNE    A,#SSKEY,GETTAX22
         JMP     GETTAX23
GETTAX22:CALL    ENTRY
         JMP     GETTAX21
GETTAX23:MOV     DPTR,#TAX2BCD
         MOV     A,VOLUME
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,VOLUME+1
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,VOLUME+2
         MOVX    @DPTR,A               ; STORE BACK NEW CODE.
         MOV     DPTR,#TAX2T
         MOVX    A,@DPTR
         CJNE    A,#'$',GETTAX25
         MOV     DPTR,#TAX2BCD+2
         MOVX    A,@DPTR
         ANL     A,#0FH
         MOVX    @DPTR,A               ; ZERO OUT MSD IF $

GETTAX25:MOV     R2,#3
         MOV     DPTR,#TAX2BCD
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#3
         MOV     R0,#DATEIMG+2
         MOV     DPTR,#TAX2            ; SERIAL NUMBER TO CARD IMAGE
         CALL    MOVDECASC
         RET

GETDSC:
         MOV     DPTR,#DSCBCD
         CALL    GETVOL
         CALL    LF0800
         MOV     DPTR,#DISCAKEY
         MOVX    A,@DPTR
         CJNE    A,#'$',GETDSC4
         MOV     DGTPOS,#2             ; START WITH DIGIT1 BLINK
         MOV     DATEIMG+1,#2          ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#7FH          ; ALL 6 DIGITS
         JMP     GETDSC1               ; 0X.XXXX FOR $
GETDSC4:
         CALL    ALLSIX                ; XX.XXXX FOR %
GETDSC1: CALL    KPRESS
         JNC     GETDSC2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETDSC2
         JMP     GETDSC3
GETDSC2: CALL    ENTRY
         JMP     GETDSC1
GETDSC3: MOV     DPTR,#DSCBCD
         CALL    PUTVOL

         MOV     DPTR,#DISCAKEY
         MOVX    A,@DPTR
         CJNE    A,#'$',GETDSC5
         MOV     DPTR,#DSCBCD+2
         MOVX    A,@DPTR
         ANL     A,#0FH
         MOVX    @DPTR,A               ; ZERO OUT MSD IF $

GETDSC5: MOV     R2,#3
         MOV     DPTR,#DSCBCD
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#3
         MOV     R0,#DATEIMG+2
         MOV     DPTR,#DISCRATE        ; SERIAL NUMBER TO CARD IMAGE
         CALL    MOVDECASC
         RET

SETTYP1:
         CALL    LF0000
         MOV     DPTR,#TAX1T
         MOVX    A,@DPTR
         CJNE    A,#'$',SETTYP11
         MOV     DPTR,#MSG86
         JMP     SETTYP12
SETTYP11:MOV     DPTR,#MSG87
SETTYP12:CALL    SHOWLCD
SETTYP13:CALL    KPRESS
         JNC     SETTYP13
         CJNE    A,#UPKEY,SETTYP14
         MOV     DPTR,#TAX1T
         MOV     A,#'$'
         MOVX    @DPTR,A
         JMP     SETTYP1
SETTYP14:CJNE    A,#DOWNKEY,SETTYP15
         MOV     DPTR,#TAX1T
         MOV     A,#'%'
         MOVX    @DPTR,A
         JMP     SETTYP1
SETTYP15:CJNE    A,#SSKEY,SETTYP13
         RET


SETTYP2:
         CALL    LF0000
         MOV     DPTR,#TAX2T
         MOVX    A,@DPTR
         CJNE    A,#'$',SETTYP21
         MOV     DPTR,#MSG86
         JMP     SETTYP22
SETTYP21:MOV     DPTR,#MSG87
SETTYP22:CALL    SHOWLCD
SETTYP23:CALL    KPRESS
         JNC     SETTYP23
         CJNE    A,#UPKEY,SETTYP24
         MOV     DPTR,#TAX2T
         MOV     A,#'$'
         MOVX    @DPTR,A
         JMP     SETTYP2
SETTYP24:CJNE    A,#DOWNKEY,SETTYP25
         MOV     DPTR,#TAX2T
         MOV     A,#'%'
         MOVX    @DPTR,A
         JMP     SETTYP2
SETTYP25:CJNE    A,#SSKEY,SETTYP23
         RET

SETDSC2:
         CALL    LF0000
         MOV     DPTR,#DISCAKEY
         MOVX    A,@DPTR
         CJNE    A,#'$',SETDSC21
         MOV     DPTR,#MSG86
         JMP     SETDSC22
SETDSC21:MOV     DPTR,#MSG87
SETDSC22:CALL    SHOWLCD
SETDSC23:CALL    KPRESS
         JNC     SETDSC23
         CJNE    A,#UPKEY,SETDSC24
         MOV     DPTR,#DISCAKEY
         MOV     A,#'$'
         MOVX    @DPTR,A
         JMP     SETDSC2
SETDSC24:CJNE    A,#DOWNKEY,SETDSC25
         MOV     DPTR,#DISCAKEY
         MOV     A,#'%'
         MOVX    @DPTR,A
         JMP     SETDSC2
SETDSC25:CJNE    A,#SSKEY,SETDSC23
         RET


GETDAYS:
         MOV     DPTR,#DAYSBIN
         MOVX    A,@DPTR
         MOV     VOLUME,A
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H
         CALL    LF0000
         CALL    DGT00FHD

GETDAY:
         CALL    TWODIGAT5
GETDAY1: CALL    KPRESS
         JNC     GETDAY2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETDAY2
         JMP     GETDAY3
GETDAY2: CALL    ENTRY
         JMP     GETDAY1
GETDAY3:
         MOV     DPTR,#DAYSBIN
         MOV     A,VOLUME
         MOVX    @DPTR,A
         CALL    MAKEASC
         MOV     DPTR,#DISCDAY
         MOVX    @DPTR,A
         MOV     A,B
         MOV     DPTR,#DISCDAY+1
         MOVX    @DPTR,A
         RET


TKTNUM:
         MOV     DPTR,#TKTNO
         CALL    GETVOL
         CALL    LF0010
         CALL    ALLSIX
TKTNUM1: CALL    KPRESS
         JNC     TKTNUM2
         MOV     A,KEY
         CJNE    A,#SSKEY,TKTNUM2
         JMP     TKTNUM3
TKTNUM2: CALL    ENTRY
         JMP     TKTNUM1
TKTNUM3: MOV     DPTR,#TKTNO
         CALL    PUTVOL
         RET

TNKNUM:                            ; TANK ID NUMBER
         MOV     DPTR,#TANKID
         CALL    GETVOL
         CALL    LF0010
         CALL    ALLSIX
TNKNUM1: CALL    KPRESS
         JNC     TNKNUM2
         MOV     A,KEY
         CJNE    A,#SSKEY,TNKNUM2
         JMP     TNKNUM3
TNKNUM2: CALL    ENTRY
         JMP     TNKNUM1
TNKNUM3: MOV     DPTR,#TANKID
         CALL    PUTVOL

         MOV     DPTR,#TANKID
         MOVX    A,@DPTR
         MOV     TEMP,A
         INC     DPTR
         MOVX    A,@DPTR
         ORL     TEMP,A
         INC     DPTR
         MOVX    A,@DPTR
         ORL     TEMP,A
         MOV     A,TEMP
         JZ      TNKNUM4
         MOV     DPTR,#STORFLG
         MOV     A,#1
         MOVX    @DPTR,A          ; FLAG NON-ZERO TANK ID

         CALL    TDATOUT
         CALL    LASTVOL

TNKNUM4: RET


CALNUM:
         MOV     DPTR,#CALNO
         CALL    GETVOL
         CALL    LF0010
         CALL    ALLSIX
CALNUM1: CALL    KPRESS
         JNC     CALNUM2
         MOV     A,KEY
         CJNE    A,#SSKEY,CALNUM2
         JMP     CALNUM3
CALNUM2: CALL    ENTRY
         JMP     CALNUM1
CALNUM3: MOV     DPTR,#CALNO
         CALL    PUTVOL
         RET

SHOWFAC:
         CALL    GETCAL
         MOV     VOLUME,SWARRAY+1
         MOV     VOLUME+1,SWARRAY+2
         MOV     VOLUME+2,SWARRAY
SHOWFAC1:CALL    LF0818
         MOV     DGTPOS,#0
         MOV     DATEIMG,#7FH
         CALL    DISPSET
         RET

GETFAC:  CALL    SHOWFAC
         MOV     DGTPOS,#2             ; STARTING DIGIT POSITION
         MOV     DATEIMG+1,#2          ; SAVES DGTPOS FOR LEFT BLANKING
         MOV     DATEIMG,#7FH          ; BLANKED DIGITS [00111111]

GETFAC1: CALL    KPRESS
         JNC     GETFAC2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETFAC2
         JMP     GETFAC3
GETFAC2: CALL    ENTRY
         JMP     GETFAC1
GETFAC3: MOV     A,VOLUME+2
         ANL     A,#0FH
         CJNE    A,#2,GETFAC4
GETFAC4: JC      GETFAC5
         MOV     A,VOLUME+2
         MOV     A,#01H
         MOV     VOLUME+2,A            ; IF MSD>1 THEM MAKE 1
GETFAC5: MOV     SWARRAY+1,VOLUME
         MOV     SWARRAY+2,VOLUME+1
         MOV     SWARRAY,VOLUME+2
         CALL    PUTCAL

         MOV     DPTR,#SWAR
         MOV     A,SWARRAY
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,SWARRAY+1
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,SWARRAY+2
         MOVX    @DPTR,A
         CALL    DOSWFAC
         CALL    WAIT50
         RET                         ; S/S KEY GETS OUT

SHOWTOT:
         MOV     DPTR,#GRTOTN
         MOV     A,KEY
         CJNE    A,#LEFTKEY,SHOWTOT8
         INC     DPTR
         INC     DPTR
         INC     DPTR

SHOWTOT8:MOVX    A,@DPTR
         CALL    GETVOL
         MOV     DGTPOS,#0
         MOV     DATEIMG,#0FFH
         MOV     A,COMP                ; LEGENDS FOR COMP/NON
         JZ      SHOWTOT4
         CALL    GETUNIT
         MOV     DPTR,#LGDFLG1
         MOV     A,KEY
         CJNE    A,#LEFTKEY,SHOWT10
         MOV     A,#40H
         JMP     SHOWT11
SHOWT10: MOV     A,#41H
SHOWT11: JNB     PSW_F0,U8
         ANL     A,#9FH
         ORL     A,#20H
U8:      MOVX    @DPTR,A
         MOV     DPTR,#LGDFLG2
         MOV     A,#05H
         JNB     PSW_F0,U9
         ANL     A,#0F9H
         ORL     A,#02H
U9:      MOVX    @DPTR,A
         JMP     SHOWTOT5

SHOWTOT4:CALL    GETUNIT
         MOV     DPTR,#LGDFLG1
         MOV     A,KEY
         CJNE    A,#LEFTKEY,SHOWT12
         MOV     A,#00H
         JMP     U10
SHOWT12: MOV     A,#01H
U10:     MOVX    @DPTR,A
         MOV     DPTR,#LGDFLG2
         MOV     A,#04H
         JNB     PSW_F0,U11
         ANL     A,#0F9H
         ORL     A,#02H
U11:     MOVX    @DPTR,A
SHOWTOT5:CALL    DISPSET
         CALL    WAIT1000                ; TIME FOR FINGER OFF KEY

SHOWTOT1:CALL    KPRESS
         JNC     SHOWTOT1
         CJNE    A,#SSKEY,SHOWTOT6
         JMP     SHOWTOT7              ; GO TO TOP WITH KEY
SHOWTOT6:JMP     SHOWTOT
SHOWTOT7:RET

; GET PRODUCT CODE FROM KEYBOARD
GETPRCODE:
         MOV     TEMP,PRCOD
         CALL    BIN2BCD               ; PRCOD IS BINARY
         MOV     VOLUME,TEMP
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H
         CALL    LF0020
         CALL    DGT00FHD

         CALL    TWODIGAT5
GETPRCOD1:
         CALL    KPRESS
         JNC     GETPRCOD2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETPRCOD2
         JMP     GETPRCOD3
GETPRCOD2:
         CALL    ENTRY
         JMP     GETPRCOD1
GETPRCOD3:
         MOV     TEMP,VOLUME           ; TURN BACK INTO BINARY
         CALL    BCD2BIN
         MOV     PRCOD,TEMP
         JNZ     GETPRCOD5
         MOV     PRCOD,#01H
         JMP     GETPRCODE
GETPRCOD5:

;         JNB     CALSW,GETPRCOD4       ; EC: "JNB CALSW" JUMPS IF IN CAL MODE, JB JUMPS IF *NOT* IN CAL MODE
         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JC      GETPRCOD4

         CALL    GETCAL
         MOV     A,#0
         ORL     A,SWARRAY
         ORL     A,SWARRAY+1
         ORL     A,SWARRAY+2
         JNZ     GETPRCOD4
         MOV     DPTR,#MSG45
         CALL    SHOW
         MOV     PRCOD,#01H            ; SAY NOGOOD AND REST TO 1
         JMP     GETPRCODE
GETPRCOD4:
         MOV     DPTR,#LASTPRC
         MOV     A,PRCOD
         MOVX    @DPTR,A
         CALL    WAIT50
         CALL    GETPCPR               ; LOAD CURRENT PRICING BY PRCOD
         CALL    CONV2BCD              ; UPDATE KEYBOARD BCD
         RET                           ; S/S KEY GETS OUT

GETLOGOHT:
         MOV     DPTR,#LOGOHT
         MOVX    A,@DPTR
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     VOLUME,TEMP
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H
         CALL    LF0018
         CALL    DGT00FHD
         CALL    TWODIGAT5

GETLOG1: CALL    KPRESS
         JNC     GETLOG2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETLOG2
         JMP     GETLOG3
GETLOG2: CALL    ENTRY
         JMP     GETLOG1
GETLOG3: MOV     A,VOLUME
         JNZ     GETLOG4               ; IF ZERO MAKE 1
         MOV     VOLUME,#1
GETLOG4: MOV     TEMP,VOLUME           ;  BACK TO BINARY
         CALL    BCD2BIN
         MOV     DPTR,#LOGOHT
         MOV     A,TEMP
         MOVX    @DPTR,A
         RET                           ; S/S KEY GETS OUT


GETDIVISOR:
         MOV     DPTR,#DIVISOR
         MOVX    A,@DPTR
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     VOLUME,TEMP
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H
         CALL    LF0018
         CALL    DGT00FHD
         CALL    TWODIGAT5

GETDIV1: CALL    KPRESS
         JNC     GETDIV2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETDIV2
         JMP     GETDIV3
GETDIV2: CALL    ENTRY
         JMP     GETDIV1
GETDIV3: MOV     A,VOLUME
         JNZ     GETDIV4               ; IF ZERO MAKE 1
         MOV     VOLUME,#1
GETDIV4: MOV     TEMP,VOLUME           ;  BACK TO BINARY
         CALL    BCD2BIN
         MOV     DPTR,#DIVISOR
         MOV     A,TEMP
         MOVX    @DPTR,A
         CALL    WAIT50
         RET                           ; S/S KEY GETS OUT

GETCOPYS:
         MOV     DPTR,#COPIES
         MOVX    A,@DPTR
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     VOLUME,TEMP
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H
         CALL    LF0010
         CALL    DGT00FHD

         CALL    TWODIGAT5
GETCOPY1:CALL    KPRESS
         JNC     GETCOPY2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETCOPY2
         JMP     GETCOPY3
GETCOPY2:CALL    ENTRY
         JMP     GETCOPY1
GETCOPY3:MOV     A,VOLUME
         JNZ     GETCOPY4              ; IF ZERO MAKE 1
         MOV     VOLUME,#1
GETCOPY4:MOV     TEMP,VOLUME           ;  BACK TO BINARY
         CALL    BCD2BIN
         MOV     DPTR,#COPIES
         MOV     A,TEMP
         MOVX    @DPTR,A
         MOV     DPTR,#CPYCPY
         MOVX    @DPTR,A               ; MAKE COPY OF COPIES
         CALL    WAIT50
         RET                           ; S/S KEY GETS OUT


GETREGNUM:
         MOV     DPTR,#REGNUM
         MOVX    A,@DPTR
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     VOLUME,TEMP
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H
         CALL    LF0018
         CALL    DGT00FHD
         CALL    TWODIGAT5
GETREGNM1:
         CALL    KPRESS
         JNC     GETREGNM2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETREGNM2
         JMP     GETREGNM3
GETREGNM2:
         CALL    ENTRY
         JMP     GETREGNM1
GETREGNM3:
         MOV     TEMP,VOLUME           ;  BACK TO BINARY
         CALL    BCD2BIN
         MOV     DPTR,#REGNUM
         MOV     A,TEMP
         MOVX    @DPTR,A
         CALL    WAIT50
         RET                           ; S/S KEY GETS OUT


GETCMP:  CALL    CMPPTR
         MOVX    A,@DPTR
         MOV     TEMP,A
         CALL    BIN2BCD               ; CMPOD IS BINARY
         MOV     VOLUME,TEMP
         MOV     VOLUME+1,#00H
         MOV     VOLUME+2,#00H
         CALL    LF0018
         CALL    DGT00FHD

         CALL    TWODIGAT5
GETCMP1: CALL    KPRESS
         JNC     GETCMP2
         MOV     A,KEY
         CJNE    A,#SSKEY,GETCMP2
         JMP     GETCMP3
GETCMP2: CALL    ENTRY
         JMP     GETCMP1
GETCMP3: MOV     TEMP,VOLUME           ; TURN BACK INTO BINARY
         CALL    BCD2BIN
         CALL    CMPPTR
         MOV     A,TEMP
         MOVX    @DPTR,A
         CALL    WAIT50

         RET                           ; S/S KEY GETS OUT


SETCMP:
         MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JNZ     SETCMP2

         CALL    CMPPTR
         MOVX    A,@DPTR
         JNZ     SETCMP1
SETCMP2: MOV     COMP,#0               ; IF CMPTBL = 0 THEN KILL COMP FLAG
         JMP     SETCMPN
SETCMP1: MOV     COMP,#1               ; SET COMP FLAG
SETCMPN: RET




SETBRKNVLV:
         MOV     DPTR,#BROKNVLV
         MOVX    A,@DPTR
         JNZ     SETBRKN1
         MOV     DPTR,#MSG24           ; 0=OFF
         JMP     SETBRKN2
SETBRKN1:MOV     DPTR,#MSG23           ; 1=ON
SETBRKN2:CALL    SHOWLCD
SETBRKN3:CALL    KPRESS
         JNC     SETBRKN3
         CJNE    A,#UPKEY,SETBRKN4
         MOV     A,#1
         MOV     DPTR,#BROKNVLV
         MOVX    @DPTR,A
         JMP     SETBRKNVLV
SETBRKN4:CJNE    A,#DOWNKEY,SETBRKN5
         MOV     A,#0
         MOV     DPTR,#BROKNVLV
         MOVX    @DPTR,A
         MOV     DPTR,#HOSEPKFLG       ; IF BROKEN VALVE IS DISABLED THEN ALSO DISABLE HOSEPACK
         MOVX    @DPTR,A
         JMP     SETBRKNVLV
SETBRKN5:CJNE    A,#SSKEY,SETBRKN3
         RET


SETDEMO: MOV     DPTR,#DEMO
         MOVX    A,@DPTR
         JNZ     SETDEMO1
         MOV     DPTR,#MSG24           ; 0=OFF
         JMP     SETDEMO2
SETDEMO1:MOV     DPTR,#MSG23           ; 1=ON
SETDEMO2:CALL    SHOWLCD
SETDEMO3:CALL    KPRESS
         JNC     SETDEMO3
         CJNE    A,#UPKEY,SETDEMO4
         MOV     A,#1
         MOV     DPTR,#DEMO
         MOVX    @DPTR,A
         JMP     SETDEMO
SETDEMO4:CJNE    A,#DOWNKEY,SETDEMO5
         MOV     A,#0
         MOV     DPTR,#DEMO
         MOVX    @DPTR,A
         JMP     SETDEMO
SETDEMO5:CJNE    A,#SSKEY,SETDEMO3
         RET


GETPSRQD:MOV     DPTR,#PSRQD
         MOVX    A,@DPTR
         JNZ     GETPSRQ1
         MOV     DPTR,#MSG24
         JMP     GETPSRQ2
GETPSRQ1:MOV     DPTR,#MSG23
GETPSRQ2:CALL    SHOWLCD
GETPSRQ3:CALL    KPRESS
         JNC     GETPSRQ3
         CJNE    A,#UPKEY,GETPSRQ4
         MOV     DPTR,#PSRQD
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     GETPSRQD
GETPSRQ4:CJNE    A,#DOWNKEY,GETPSRQ5
         MOV     DPTR,#PSRQD
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     GETPSRQD
GETPSRQ5:CJNE    A,#SSKEY,GETPSRQ3
         RET


; ****************************************************
; GETPTR ALLOWS SELECTION OF PRINTER WITH UP/DOWN KEYS
; ****************************************************
GETPRT:
         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JZ      GETPRT12
         MOV     DPTR,#MSG121
         CALL    SHOW
         JMP     GETPRTN               ; DON'T ALLOW SELECTION IF 6501
GETPRT12:MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#0,GETPRT1
         MOV     DPTR,#MSG9            ; MIDCOM
         JMP     GETPRT5
GETPRT1: CJNE    A,#1,GETPRT2
         MOV     DPTR,#MSG65           ; TM-295
         JMP     GETPRT5
GETPRT2: CJNE    A,#2,GETPRT3
         MOV     DPTR,#MSG66           ; THERML
         JMP     GETPRT5
GETPRT3: CJNE    A,#3,GETPRT4
         MOV     DPTR,#MSG67           ; NOPRTR
         JMP     GETPRT5
GETPRT4: CJNE    A,#4,GETPRT10         ; 4 = ZEBRA RW-420 V1 = NO BMFF
         MOV     DPTR,#MSG113          ; ZEBRA
         JMP     GETPRT5
GETPRT10:CJNE    A,#5,GETPRT11
         MOV     DPTR,#MSG118          ; BLASTR
         JMP     GETPRT5
GETPRT11:CJNE    A,#6,GETPRT13
         MOV     DPTR,#MSG122          ; TM-220
         JMP     GETPRT5
GETPRT13:CJNE    A,#7,GETPRT15
         MOV     DPTR,#MSG128          ; FP-460
         JMP     GETPRT5
GETPRT15:CJNE    A,#8,GETPRT16
         MOV     DPTR,#MSG130          ; CTS310
         JMP     GETPRT5
GETPRT16:CJNE    A,#9,GETPRT17
         MOV     DPTR,#MSG133          ; CTS651
         JMP     GETPRT5
GETPRT17:CJNE    A,#10,GETPRT18
         MOV     DPTR,#MSG136          ; TM-88
         JMP     GETPRT5
GETPRT18:CJNE    A,#11,GETPRT19        ; 11 = BLASTB = BLASTER + BLACK MARK
         MOV     DPTR,#MSG146          ; BLASTB
         JMP     GETPRT5
GETPRT19:CJNE    A,#12,GETPRT20        ; 12 = PRINTEK RT43 BLUETOOTH
         MOV     DPTR,#MSG147          ; RT43BT
         JMP     GETPRT5
GETPRT20:CJNE    A,#13,GETPRT21        ; 13 = DATAMAX M4TE
         MOV     DPTR,#MSG148          ; DMM4TE
         JMP     GETPRT5
GETPRT21:CJNE    A,#14,GETPRT22        ; 14 = ZEBRA RW-420 V2 = DO BMFF
         MOV     DPTR,#MSG149          ; RW420B
         JMP     GETPRT5
GETPRT22:CJNE    A,#15,GETPRT23        ; 15 = CUSTOM Q3 = DO LOGO + BMFF
         MOV     DPTR,#MSG157          ; Q3
         JMP     GETPRT5
GETPRT23:CJNE    A,#16,GETPRT9         ; 16 = STAR TSP700II = DO LOGO + BMFF
         MOV     DPTR,#MSG158          ; TSP700
         JMP     GETPRT5
GETPRT9: MOV     DPTR,#PRTFLG
         MOV     A,#3
         MOVX    @DPTR,A
         JMP     GETPRT                ; FORCE NO PRINTER IF OUT OF RANGE
GETPRT5: CALL    SHOWS
GETPRT6: CALL    KPRESS
         MOV     A,KEY
         JZ      GETPRT6

         CJNE    A,#SSKEY,GETPRTA
         JMP     GETPRTN

GETPRTA: CJNE    A,#DOWNKEY,GETPRTB    ; DOWN KEY = NEXT PRINTER
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         INC     A
         CJNE    A,#17,GETPRTZ
         MOV     A,#0
         JMP     GETPRTZ

GETPRTB: CJNE    A,#UPKEY,GETPRT6      ; UP KEY = PREVIOUS PRINTER
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         DEC     A
         CJNE    A,#255,GETPRTZ
         MOV     A,#16
GETPRTZ: MOVX    @DPTR,A
         JMP     GETPRT

GETPRTN: RET


; ****************************************************
; SETTIMER SPLASHES TIMER SCREEN AND ALLOWS FOR
; TOGGLE WITH UP/DOWN KEYS
; UPDATED FOR L414 REV2, SETTINGS ARE:
;   OFF(0)DEFAULT, 3MINFLOW(1), 3MINNOFLOW(2), 30SECFLOW(3), 30SECNOFLOW(4)
; ****************************************************
SETTIMER:CALL    LF0018
         MOV     DPTR,#ETFLG
         MOVX    A,@DPTR
         CJNE    A,#0,SETTIMRA
         MOV     DPTR,#MSG24           ; 0: 24=OFF
         JMP     SETTIMR2
SETTIMRA:CJNE    A,#1,SETTIMRB
         MOV     DPTR,#MSG222          ; 1: 3MN FL
         JMP     SETTIMR2
SETTIMRB:CJNE    A,#2,SETTIMRC
         MOV     DPTR,#MSG223          ; 2: 3MN NF
         JMP     SETTIMR2
SETTIMRC:CJNE    A,#3,SETTIMRD
         MOV     DPTR,#MSG224          ; 3: 3OS FL
         JMP     SETTIMR2
SETTIMRD:MOV     DPTR,#MSG225          ; 4: 30S NF
SETTIMR2:CALL    SHOWLCD
SETTIMR3:CALL    KPRESS
         MOV     A,KEY
         JZ      SETTIMR3
SETTIMR4:CJNE    A,#DOWNKEY,SETTIMR6
         MOV     DPTR,#ETFLG
         MOVX    A,@DPTR
         INC     A
         CJNE    A,#5,SETTIMR5
         MOV     A,#0
SETTIMR5:MOVX    @DPTR,A
         JMP     SETTIMER
SETTIMR6:CJNE    A,#SSKEY,SETTIMR3
         RET


; PULSES PER REVOLUTION IN NEW EM FOR E180+
SETPLSREV:
         CALL    LF0018
         MOV     DPTR,#EMPLSREV
         MOVX    A,@DPTR
         CJNE    A,#0,SETPLSRA
         MOV     DPTR,#MSG239          ; 0: 256
         JMP     SETPLSR2
SETPLSRA:MOV     DPTR,#MSG238          ; 1: 100
SETPLSR2:CALL    SHOWLCD
SETPLSR3:CALL    KPRESS
         MOV     A,KEY
         JZ      SETPLSR3
SETPLSR4:CJNE    A,#DOWNKEY,SETPLSR4B
         MOV     DPTR,#EMPLSREV
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETPLSREV
SETPLSR4B:CJNE   A,#UPKEY,SETPLSR6
         MOV     DPTR,#EMPLSREV
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETPLSREV
SETPLSR6:CJNE    A,#SSKEY,SETPLSR3
         RET

; SINGLE OR DUAL CHANNEL IN NEW EM FOR E180+
SETCHANNL:
         CALL    LF0018
         MOV     DPTR,#EMCHANNL
         MOVX    A,@DPTR
         CJNE    A,#0,SETCHANA
         MOV     DPTR,#MSG240          ; 0: DUAL
         JMP     SETCHAN2
SETCHANA:MOV     DPTR,#MSG241          ; 1: SINGLE
SETCHAN2:CALL    SHOWLCD
SETCHAN3:CALL    KPRESS
         MOV     A,KEY
         JZ      SETCHAN3
SETCHAN4:CJNE    A,#DOWNKEY,SETCHAN4B
         MOV     DPTR,#EMCHANNL
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETCHANNL
SETCHAN4B:CJNE   A,#UPKEY,SETCHAN6
         MOV     DPTR,#EMCHANNL
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETCHANNL
SETCHAN6:CJNE    A,#SSKEY,SETCHAN3
         RET


; FLOW DIRECTION IN NEW EM FOR E180+
SETFLODIR:
         CALL    LF0018
         MOV     DPTR,#EMFLODIR
         MOVX    A,@DPTR
         CJNE    A,#0,SETFLODA
         MOV     DPTR,#MSG236          ; 0: CW
         JMP     SETFLOD2
SETFLODA:MOV     DPTR,#MSG237          ; 1: CCW
SETFLOD2:CALL    SHOWLCD
SETFLOD3:CALL    KPRESS
         MOV     A,KEY
         JZ      SETFLOD3
SETFLOD4:CJNE    A,#DOWNKEY,SETFLOD4B
         MOV     DPTR,#EMFLODIR
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETFLODIR
SETFLOD4B:CJNE   A,#UPKEY,SETFLOD6
         MOV     DPTR,#EMFLODIR
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETFLODIR
SETFLOD6:CJNE    A,#SSKEY,SETFLOD3
         RET

SETQOBFLAG:
         CALL    LF0010
         MOV     DPTR,#QOBMODE
         MOVX    A,@DPTR
         JNZ     SETQOBF1
         MOV     DPTR,#MSG24           ; 24 = OFF
         JMP     SETQOBF2
SETQOBF1:MOV     DPTR,#MSG23           ; 23 = ON/ENABLED
SETQOBF2:CALL    SHOWLCD
SETQOBF3:CALL    KPRESS
         JNC     SETQOBF3
         CJNE    A,#UPKEY,SETQOBF4
         MOV     DPTR,#QOBMODE
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETQOBFLAG
SETQOBF4:CJNE    A,#DOWNKEY,SETQOBF5
         MOV     DPTR,#QOBMODE
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETQOBFLAG
SETQOBF5:CJNE    A,#SSKEY,SETQOBF3
         RET



SETHOSPK:CALL    LF0010
         MOV     DPTR,#HOSEPKFLG
         MOVX    A,@DPTR
         JNZ     SETHSPK1
         MOV     DPTR,#MSG24           ; 24 = OFF
         JMP     SETHSPK2
SETHSPK1:MOV     DPTR,#MSG23           ; 23 = ON/ENABLED
SETHSPK2:CALL    SHOWLCD
SETHSPK3:CALL    KPRESS
         JNC     SETHSPK3
         CJNE    A,#UPKEY,SETHSPK4
         MOV     DPTR,#HOSEPKFLG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETHOSPK
SETHSPK4:CJNE    A,#DOWNKEY,SETHSPK5
         MOV     DPTR,#HOSEPKFLG
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETHOSPK
SETHSPK5:CJNE    A,#SSKEY,SETHSPK3
         RET

SETTRNC: CALL    LF0010
         MOV     DPTR,#TRUNC
         MOVX    A,@DPTR
         JNZ     SETTRNC1
         MOV     DPTR,#MSG24           ; 24 = OFF
         JMP     SETTRNC2
SETTRNC1:MOV     DPTR,#MSG23           ; 23 = ON/ENABLED
SETTRNC2:CALL    SHOWLCD
SETTRNC3:CALL    KPRESS
         JNC     SETTRNC3
         CJNE    A,#UPKEY,SETTRNC4
         MOV     DPTR,#TRUNC
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETTRNC
SETTRNC4:CJNE    A,#DOWNKEY,SETTRNC5
         MOV     DPTR,#TRUNC
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETTRNC
SETTRNC5:CJNE    A,#SSKEY,SETTRNC3
         RET


; ****************************************************
; GTHSTFIX SPLASHES HOSTFIX SCREEN AND ALLOWS FOR OFF/MATRIX/ALL
; SCROLL WITH DOWN KEY
; ****************************************************
GTHSTFIX:CALL    LF0010
         MOV     DPTR,#HOSTFIX
         MOVX    A,@DPTR
         CJNE    A,#0,GTHSTFI1
         MOV     DPTR,#MSG24           ; OFF
         JMP     GTHSTFI5
GTHSTFI1:CJNE    A,#1,GTHSTFI2
         MOV     DPTR,#MSG135          ; MATRIX
         JMP     GTHSTFI5
GTHSTFI2:CJNE    A,#2,GTHSTFI9
         MOV     DPTR,#MSG137          ; ALL
         JMP     GTHSTFI5
GTHSTFI9:MOV     DPTR,#HOSTFIX
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     GTHSTFIX              ; FORCE HOSTFIX OFF IF OUT OF RANGE
GTHSTFI5:CALL    SHOWS
GTHSTFI6:CALL    KPRESS
         MOV     A,KEY
         JZ      GTHSTFI6
         CJNE    A,#SSKEY,GTHSTFI7
         JMP     GTHSTFIN
GTHSTFI7:CJNE    A,#DOWNKEY,GTHSTFI6   ; NEITHER KEY GO BACK FOR MORE
         MOV     DPTR,#HOSTFIX
         MOVX    A,@DPTR
         INC     A
         CJNE    A,#3,GTHSTFI8
         MOV     A,#0
GTHSTFI8:MOVX    @DPTR,A
         JMP     GTHSTFIX
GTHSTFIN:RET


; ****************************************************
; SETDECML SPLASHES DECIMAL SCREEN AND ALLOWS FOR EDITING
; ****************************************************
SETDECML:CALL    LF0018
         MOV     DPTR,#DECIMAL
         MOVX    A,@DPTR
         JZ      SETDCML1
         MOV     DPTR,#MSG24
         JMP     SETDCML2
SETDCML1:MOV     DPTR,#MSG23
SETDCML2:
         CALL    SHOWLCD
SETDCML3:CALL    KPRESS
         JNC     SETDCML3
         CJNE    A,#UPKEY,SETDCML4
         MOV     DPTR,#DECIMAL
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETDECML
SETDCML4:CJNE    A,#DOWNKEY,SETDCML5
         MOV     DPTR,#DECIMAL
         MOV     A,#1
         MOVX    @DPTR,A
         MOV     DPTR,#MONEYFLG        ; PRICING ENABLED: 0=OFF,1=PRICE CHECK,2=ALWAYS ON
         MOV     A,#0
         MOVX    @DPTR,A               ; IF DECIMAL OFF THEN CURRENCY OFF
         JMP     SETDECML
SETDCML5:CJNE    A,#SSKEY,SETDCML3
SETDCMN: RET


; ****************************************************************
; SETSSRST SPLASHES SSRST SCREEN AND ALLOWS FOR OFF/NSSOHM/ON
; SCROLL WITH DOWN KEY
; NSSOHM = NO START/STOP TO OPEN VALVES DURING HOST MODE DELIVERY
; ****************************************************************
SETSSRST:
         CALL    LF0010

         MOV     DPTR,#CMDFLAG+7       ; BYTE7(0-9) = HOST OVERRIDE SSRST SETTING
         MOVX    A,@DPTR
         CJNE    A,#'0',SETSSRSZ       ; E179_A: SEE IF SSRST OVERRIDE FROM HOST
         JMP     SETSSRS0              ; NOT OVERRIDEN, PROCEED
SETSSRSZ:MOV     DPTR,#MSG93           ; "HOST" = TELL THEM THE HOST HAS OVERRIDDEN
         CALL    SHOW
         JMP     SETSSRSX              ; THEY CAN PRINT A CAL TKT IF THEY NEED TO KNOW THE SETTING

SETSSRS0:MOV     DPTR,#SSRSTFLG
         MOVX    A,@DPTR
SETSSRSY:CJNE    A,#0,SETSSRS1
         MOV     DPTR,#MSG23           ; ON
         JMP     SETSSRS4
SETSSRS1:CJNE    A,#1,SETSSRS2
         MOV     DPTR,#MSG24           ; OFF
         JMP     SETSSRS4
SETSSRS2:CJNE    A,#2,SETSSRS3
         MOV     DPTR,#MSG141          ; NSSOHM
         JMP     SETSSRS4
SETSSRS3:MOV     DPTR,#SSRSTFLG        ; SET TO ON = DEFAULT
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETSSRST
SETSSRS4:CALL    SHOWS
SETSSRS5:CALL    KPRESS                ; THIS IS A HOLD-METHOD FOR CALLING KPRESS
         MOV     A,KEY
         JZ      SETSSRS5
         CJNE    A,#DOWNKEY,SETSSRS9
SETSSRS5A:
         MOV     DPTR,#SSRSTFLG        ; <DOWN> = SET SSRST TO NEXT VALUE & UPDATE DISPLAY
         MOVX    A,@DPTR
         CJNE    A,#2,SETSSRS7         ; IF RSTFLAG = 2 THEN SET TO 0, ELSE INC IT
         MOV     A,#0                  ; THEN UPDATE DISPLAY
         JMP     SETSSRS8
SETSSRS7:INC     A
SETSSRS8:MOVX    @DPTR,A
         JMP     SETSSRST
SETSSRS9:CJNE    A,#SSKEY,SETSSRS5
SETSSRSX:RET                           ; <S/S> = SAVE & EXIT


; ****************************************************************
; SETPBTNS ENABLES/OR DISABLES THE PRESET & PRINT BUTTONS
; ADDED 01/15/2014 L413_REV0, E179E0_REV4
; SCROLL WITH DOWN KEY
; 0=00=BOTH ENABLED
; 1=01=PRINT ONLY ENABLED
; 2=10=PRESET ONLY ENABLED
; 3=11=NEITHER ENABLED (DEFAULT)
; ****************************************************************
SETPBTNS:CALL    LF0010
         MOV     DPTR,#PPBTNENBL
         MOVX    A,@DPTR
         CJNE    A,#0,SETPBTN1
         MOV     DPTR,#MSG215          ; BOTH (BOTHON)
         JMP     SETPBTN4
SETPBTN1:CJNE    A,#1,SETPBTN2
         MOV     DPTR,#MSG218          ; PRINT ONLY (NOPRST)
         JMP     SETPBTN4
SETPBTN2:CJNE    A,#2,SETPBTNQ
         MOV     DPTR,#MSG217          ; PRESET ONLY (NOPRNT)
         JMP     SETPBTN4
SETPBTNQ:CJNE    A,#3,SETPBTN3
         MOV     DPTR,#MSG216          ; NEITHER (BTHOFF)
         JMP     SETPBTN4
SETPBTN3:MOV     DPTR,#PPBTNENBL       ; SET TO BOTH ENABLED (3) = DEFAULT
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETPBTNS
SETPBTN4:CALL    SHOWS
SETPBTN5:CALL    KPRESS                ; THIS IS A HOLD-METHOD FOR CALLING KPRESS
         MOV     A,KEY
         JZ      SETPBTN5
         CJNE    A,#DOWNKEY,SETPBTN9
         MOV     DPTR,#PPBTNENBL       ; <DOWN> = SET TO NEXT VALUE & UPDATE DISPLAY
         MOVX    A,@DPTR
         CJNE    A,#3,SETPBTN7         ; IF LAST VALUE THEN SET TO 0, ELSE INCREMENT
         MOV     A,#0                  ; THEN UPDATE DISPLAY
         JMP     SETPBTN8
SETPBTN7:INC     A
SETPBTN8:MOVX    @DPTR,A
         JMP     SETPBTNS
SETPBTN9:CJNE    A,#SSKEY,SETPBTN5
         RET                           ; <S/S> = SAVE & EXIT


SET65PRT:CALL    LF0010
         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JNZ     SET65PR1
         MOV     DPTR,#MSG24
         JMP     SET65PR2
SET65PR1:MOV     DPTR,#MSG23
SET65PR2:CALL    SHOWLCD
SET65PR3:CALL    KPRESS
         JNC     SET65PR3
         CJNE    A,#UPKEY,SET65PR4
         MOV     DPTR,#PR65FLG
         MOV     A,#1
         MOVX    @DPTR,A
         MOV     DPTR,#PRTFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; FORCE MIDCOM PRINTER
         MOV     DPTR,#MONEYFLG        ; PRICING ENABLED: 0=OFF,1=PRICE CHECK,2=ALWAYS ON
         MOV     A,#1
         MOVX    @DPTR,A               ; FORCE CURRENCY ON (DO PRICE CHECK)
         CALL    GETPCE4               ; FORCE PRICE LOAD
         JMP     SET65PRT
SET65PR4:CJNE    A,#DOWNKEY,SET65PR5
         MOV     DPTR,#PR65FLG
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SET65PRT
SET65PR5:CJNE    A,#SSKEY,SET65PR3
         RET

; CODE IN LT THAT NEEDS TO BE HERE BUT COMMENTED TO FORCE BEYOND COMPARE TO ALIGN WHEN COMPARING EC<-->LT
;SETHWAUTH: - THIS IS HARDWARE AUTHORIZATION
;         MOV    DPTR,#LGDFLG1
;         MOV    A,#00H
;         MOVX   @DPTR,A
;         MOV    DPTR,#LGDFLG2
;         MOV    A,#10H
;         MOVX   @DPTR,A
;
;         MOV    DPTR,#HWAUTHFLG
;         MOVX   A,@DPTR
;         JNZ    SETAUTH1
;         MOV    DPTR,#MSG206
;         JMP    SETAUTH2
;SETAUTH1:MOV    DPTR,#MSG205
;SETAUTH2:
;         MOV    COMMAND,#LOADLCD
;         CALL   XLOAD
;         CALL   WAIT1000
;SETAUTH3:CALL   KPRESS
;         JNC    SETAUTH3
;         CJNE   A,#UPKEY,SETAUTH4
;         MOV    DPTR,#HWAUTHFLG
;         MOV    A,#1
;         MOVX   @DPTR,A
;         JMP    SETHWAUTH
;SETAUTH4:CJNE   A,#DOWNKEY,SETAUTH5
;         MOV    DPTR,#HWAUTHFLG
;         MOV    A,#0
;         MOVX   @DPTR,A
;         JMP    SETHWAUTH
;SETAUTH5:CJNE   A,#SSKEY,SETAUTH3
;         RET


SETSWAUTH:
         CALL    LF0010
         MOV     DPTR,#SWAUTHFLG      ; SW AUTHORIZATION
         MOVX    A,@DPTR
         JZ      SETAUTX1
         MOV     DPTR,#MSG23           ; ON
         JMP     SETAUTX2
SETAUTX1:MOV     DPTR,#MSG24           ; OFF
SETAUTX2:CALL    SHOWLCD
SETAUTX3:CALL    KPRESS
         JNC     SETAUTX3
         CJNE    A,#UPKEY,SETAUTX4
         MOV     DPTR,#SWAUTHFLG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETSWAUTH
SETAUTX4:CJNE    A,#DOWNKEY,SETAUTX5
         MOV     DPTR,#SWAUTHFLG
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETSWAUTH
SETAUTX5:CJNE    A,#SSKEY,SETAUTX3
         RET



; ****************************************************************
; SETEMULATE ENABLES/OR DISABLES THE EMULATION COMMANDS TO ALLOW
;  CUSTOMERS WITH SOFTWARE THAT CANNOT OR WILL NOT BE CHANGED TO
;  USE MAINLINE ECOUNT VERSIONS
; I HAVE AN IDEA OF HOW TO SUPPORT DUAL, WE JUST MAKE REG1 THE 'PRIMARY' AND IT
; DOES ALL OF THE COMMS, EXCEPT REG2 DOES PRINT IT'S OWN TICKET
;
; ADDED 04/01/2015
; 0: DEFAULT, EMULAITON OFF
; 1: EMULATION TEMPORARILY OFF UNTIL REBOOT, ON REBOOT REVERTS TO PREVIOUS VALUE
; 2: PA21 SLS EMULATION
; 3: E141 ECOUNT EMULATION
;
; ****************************************************************
SETEMULATE:
         CALL    LF0010
         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#0,SETEMUM1
         MOV     DPTR,#MSG24           ; 0=OFF
         JMP     SETEMUMW
SETEMUM1:CJNE    A,#1,SETEMUM2
         MOV     DPTR,#MSG230          ; 1=TEMP OFF, ON REBOOT -->LAST
         JMP     SETEMUMW              ; ALLOWS 'STANDARD' FUNCTIONS UNTIL NEXT REBOOT
SETEMUM2:CJNE    A,#2,SETEMUM3
         MOV     DPTR,#MSG227          ; 2=PA21 EMULATION FOR SUBURBAN SLS CUSTOMERS
         JMP     SETEMUMW
SETEMUM3:CJNE    A,#3,SETEMUMV
         MOV     DPTR,#MSG232          ; 2=E141E EMULAITON FOR P98 EC CUSTOMERS
         JMP     SETEMUMW

SETEMUMV:MOV     DPTR,#EMULATEFLG      ; IF INVALID, SET TO OFF
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETEMULATE

SETEMUMW:CALL    SHOWS
SETEMUMX:CALL    KPRESS                ; THIS IS A HOLD-METHOD FOR CALLING KPRESS
         MOV     A,KEY
         JZ      SETEMUMX
         CJNE    A,#DOWNKEY,SETEMUMZ2
         MOV     DPTR,#EMULATEFLG      ; <DOWN> = SET TO NEXT VALUE & UPDATE DISPLAY
         MOVX    A,@DPTR
         CJNE    A,#3,SETEMUMY         ; IF LAST VALUE THEN SET TO 0, ELSE INCREMENT
         MOV     A,#0                  ; THEN UPDATE DISPLAY
         JMP     SETEMUMZ1
SETEMUMY:INC     A
SETEMUMZ1:
         MOVX    @DPTR,A
         JMP     SETEMULATE
SETEMUMZ2:
         CJNE    A,#SSKEY,SETEMUMX
         MOV     DPTR,#EMULATEFLG      ; NEED TO MAKE SURE THAT LAST IS VALID IF THIS <> 1
         MOVX    A,@DPTR
         CJNE    A,#1,SETEMUMZ9        ; IF IT'S NOT A 1 THEN UPDATE LAST
SETEMUMZ3:                             ; IF IT IS A 1 THEN DON'T CHANGE LAST
         RET                           ; <S/S> = SAVE & EXIT
SETEMUMZ9:
         MOV     DPTR,#LASTEMUFLG
         MOVX    @DPTR,A
         JMP     SETEMUMZ3



SETPROBE:
         CALL    LF0018
         MOV     DPTR,#PROBE
         MOVX    A,@DPTR
         JZ      SETPROB1
         MOV     DPTR,#MSG99           ; ANALOG
         JMP     SETPROB2
SETPROB1:MOV     DPTR,#MSG98           ; RTD
SETPROB2:CALL    SHOWLCD
SETPROB3:CALL    KPRESS
         JNC     SETPROB3
         CJNE    A,#UPKEY,SETPROB4
         MOV     DPTR,#PROBE
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETPROBE
SETPROB4:CJNE    A,#DOWNKEY,SETPROB5
         MOV     DPTR,#PROBE
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETPROBE
SETPROB5:CJNE    A,#SSKEY,SETPROB3
         RET

; CODE IN LT THAT NEEDS TO BE HERE BUT COMMENTED TO FORCE BEYOND COMPARE TO ALIGN WHEN COMPARING EC<-->LT
;SETMLOCK:CALL   LF0010
;         MOV    DPTR,#MLKFLG
;         MOVX   A,@DPTR
;         JZ     SETMLOC1
;         MOV    DPTR,#MSG23
;         JMP    SETMLOC2
;SETMLOC1:MOV    DPTR,#MSG24
;SETMLOC2:CALL   SHOWLCD
;SETMLOC3:CALL   KPRESS
;         JNC    SETMLOC3
;         CJNE   A,#UPKEY,SETMLOC4
;         MOV    DPTR,#MLKFLG
;         MOV    A,#1
;         MOVX   @DPTR,A
;         JMP    SETMLOCK
;SETMLOC4:CJNE   A,#DOWNKEY,SETMLOC5
;         MOV    DPTR,#MLKFLG
;         MOV    A,#0
;         MOVX   @DPTR,A
;         JMP    SETMLOCK
;SETMLOC5:CJNE   A,#SSKEY,SETMLOC3
;         RET


SETAIRIX:CALL    LF0010
         MOV     DPTR,#AIRFLG
         MOVX    A,@DPTR
         CJNE    A,#0,SETAIRI1
         MOV     DPTR,#MSG24           ; OFF
         JMP     SETAIRI5
SETAIRI1:CJNE    A,#1,SETAIRI2
         MOV     DPTR,#MSG96A          ; GNDWET
         JMP     SETAIRI5
SETAIRI2:CJNE    A,#2,SETAIRI9
         MOV     DPTR,#MSG96B          ; GNDDRY
         JMP     SETAIRI5
SETAIRI9:MOV     DPTR,#AIRFLG
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETAIRIX              ; FORCE AIRFLG OFF IF OUT OF RANGE
SETAIRI5:CALL    SHOWS
SETAIRI6:CALL    KPRESS
         MOV     A,KEY
         JZ      SETAIRI6
         CJNE    A,#SSKEY,SETAIRI7
         JMP     SETAIRIN
SETAIRI7:CJNE    A,#DOWNKEY,SETAIRI6   ; NEITHER KEY GO BACK FOR MORE
         MOV     DPTR,#AIRFLG
         MOVX    A,@DPTR
         INC     A
         CJNE    A,#3,SETAIRI8
         MOV     A,#0
SETAIRI8:MOVX    @DPTR,A
         JMP     SETAIRIX
SETAIRIN:RET


SETTXSUB:
         CALL   LF0000
         MOV     DPTR,#TAXSUB          ; 0=OFF, 1=ON
         MOVX    A,@DPTR
         JZ      SETTXSU1
         MOV     DPTR,#MSG23           ; ON
         JMP     SETTXSU2
SETTXSU1:MOV     DPTR,#MSG24           ; OFF
SETTXSU2:CALL    SHOWLCD
SETTXSU3:CALL    KPRESS
         JNC     SETTXSU3
         CJNE    A,#UPKEY,SETTXSU4
         MOV     DPTR,#TAXSUB
         MOV     A,#1                  ; <UP> = ON
         MOVX    @DPTR,A
         JMP     SETTXSUB
SETTXSU4:CJNE    A,#DOWNKEY,SETTXSU5
         MOV     DPTR,#TAXSUB
         MOV     A,#0                  ; <DOWN> = OFF
         MOVX    @DPTR,A
         JMP     SETTXSUB
SETTXSU5:CJNE    A,#SSKEY,SETTXSU3
         RET                           ; <S/S> = SAVE & EXIT


; ****************************************************************
; SETPRGRS SPLASHES PGROSS SCREEN AND ALLOWS FOR OFF/ON/NOTEMP
; SCROLL WITH DOWN KEY - SETTING FOR PRTGRS
; NOTEMP = DO NOT PRINT FINAL TEMP WHEN PGROSS ON, ADDED E177
; TEMP   = DO NOT PRINT GROSS, ADDED E179_A
; ****************************************************************
SETPRGRS:MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JZ      SETPRGR0              ; UTYPE: 0=VOLUME, 1=MASS
         MOV     DPTR,#MSG62           ; MASS, SHOW "NOCOMP" AS PGROSS MUST BE OFF
         CALL    SPLASH
         MOV     DPTR,#GRSFLG          ; GRSFLG: 0=OFF, 1=ON, 2=NOTEMP (PRINT ALL EXCEPT TEMP), 3=TEMP (ALL EXCEPT GROSS)
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETPRGRA              ; JUMP TO EXIT
SETPRGR0:CALL    LF0018
         MOV     DPTR,#GRSFLG
         MOVX    A,@DPTR
         CJNE    A,#0,SETPRGR1
         MOV     DPTR,#MSG24           ; OFF
         JMP     SETPRGR4
SETPRGR1:CJNE    A,#1,SETPRGR2
         MOV     DPTR,#MSG23           ; ON
         JMP     SETPRGR4
SETPRGR2:CJNE    A,#2,SETPRGR2A
         MOV     DPTR,#MSG144          ; NOTEMP
         JMP     SETPRGR4
SETPRGR2A:CJNE   A,#3,SETPRGR3
         MOV     DPTR,#MSG154          ; TEMP
         JMP     SETPRGR4
SETPRGR3:MOV     DPTR,#GRSFLG          ; OUT OF RANGE, SET TO OFF = DEFAULT
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETPRGRS
SETPRGR4:CALL    SHOWS
SETPRGR5:CALL    KPRESS
         MOV     A,KEY
         JZ      SETPRGR5
SETPRGR6:CJNE    A,#DOWNKEY,SETPRGR9   ; <DOWN> = CYCLE THROUGH VALUES
         MOV     DPTR,#GRSFLG
         MOVX    A,@DPTR
         CJNE    A,#3,SETPRGR7
         MOV     A,#0
         JMP     SETPRGR8
SETPRGR7:INC     A
SETPRGR8:MOVX    @DPTR,A
         JMP     SETPRGRS
SETPRGR9:CJNE    A,#SSKEY,SETPRGR5     ; <S/S> = EXIT
SETPRGRA:RET


SETTOVR: CALL    LF0000
         MOV     DPTR,#TORFLG
         MOVX    A,@DPTR
         JNZ     SETTOVR1
         MOV     DPTR,#MSG24
         JMP     SETTOVR2
SETTOVR1:MOV     DPTR,#MSG23
SETTOVR2:CALL    SHOWLCD
SETTOVR3:CALL    KPRESS
         JNC     SETTOVR3
         CJNE    A,#UPKEY,SETTOVR4
         MOV     DPTR,#TORFLG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     SETTOVR
SETTOVR4:CJNE    A,#DOWNKEY,SETTOVR5
         MOV     DPTR,#TORFLG
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     SETTOVR
SETTOVR5:CJNE    A,#SSKEY,SETTOVR3
         RET


SETMONY: MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JZ      SETMONY0
         MOV     DPTR,#MSG121          ; 6501PF = NO PRICING
         CALL    SHOW
         RET
SETMONY0:CALL    LF0010
         CALL    CHKDEC
         JZ      SETMONY1
         MOV     DPTR,#MONEYFLG        ; PRICING ENABLED: 0=OFF,1=PRICE CHECK,2=ALWAYS ON
         MOV     A,#0                  ; (DONE)
         MOVX    @DPTR,A               ; CAN'T HAVE MONEY IF DECIMAL OFF
SETMONY1:MOV     DPTR,#MONEYFLG        ; (DONE)
         MOVX    A,@DPTR
         CJNE    A,#0,SETMON1A         ; PRICING ENABLED: 0=OFF,1=PRICE CHECK,2=ALWAYS ON
         MOV     DPTR,#MSG24           ; OFF
         JMP     SETMONY2
SETMON1A:CJNE    A,#1,SETMON1B
         MOV     DPTR,#MSG139          ; PRICE CHECK
         JMP     SETMONY2
SETMON1B:MOV     DPTR,#MSG137          ; ALL
         JMP     SETMONY2
SETMONY2:CALL    SHOWS
SETMONY3:CALL    KPRESS
         MOV     A,KEY
         JZ      SETMONY3
         CJNE    A,#DOWNKEY,SETMONY6
         MOV     DPTR,#MONEYFLG        ; <DOWN> = SET MONEYFLAG TO NEXT VALUE
         MOVX    A,@DPTR               ; IF MONEYFLAG = 2 THEN SET TO 0 AND DISPLAY, ELSE INC MONEYFLAG
         CJNE    A,#2,SETMONY4
         MOV     A,#0
         JMP     SETMONY5
SETMONY4:INC     A
SETMONY5:MOV     DPTR,#MONEYFLG        ; PRICING ENABLED: 0=OFF,1=PRICE CHECK,2=ALWAYS ON
         MOVX    @DPTR,A               ; SAVE NEW VALUE OF MONEYFLAG AND JUMP TO UPDATE DISPLAY
         JMP     SETMONY
SETMONY6:CJNE    A,#SSKEY,SETMONY3
         RET                           ; <S/S> = SAVE & EXIT


; DISPNG TOGGLES NGFLAG BETWEEN NET/GROSS. S/S EXITS BACK TO CALMODE
DISPNG:
         MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JZ      DISPNG15
         MOV     DPTR,#MSG62           ; NOCOMP MESSAGE
         CALL    SPLASH
         JMP     DISPNG16

DISPNG15:MOV     DPTR,#NGFLAG
         MOVX    A,@DPTR
         JZ      DISPNG1
         MOV     DPTR,#MSG26
         JMP     DISPNG2
DISPNG1: MOV     DPTR,#MSG27
DISPNG2: CALL    SPLASH
         JMP     DISPNG6               ; SHOW NET OR GROSS VOLUME
DISPNG3: CALL    KPRESS
         JNC     DISPNG3
         CJNE    A,#UPKEY,DISPNG4
         MOV     DPTR,#NGFLAG
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     DISPNG
DISPNG4: CJNE    A,#DOWNKEY,DISPNG5
         MOV     DPTR,#NGFLAG
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     DISPNG
DISPNG5: CJNE    A,#SSKEY,DISPNG3
DISPNG16:MOV     DPTR,#CMODEFLG        ; NOT SURE ABOUT THIS<<<
         MOV     A,#1
         MOVX    @DPTR,A

         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JC      DISPNG8

         JMP     MAIN                  ; EXIT DEPENDS ON CALMODE/DELMODE
DISPNG8: JMP     CALMOD1

DISPNG6:
         MOV     DPTR,#NGFLAG
         MOVX    A,@DPTR
         JZ      DISPNG7
         MOV     DPTR,#NVOL
         CALL    GETVOL
         MOV     DGTPOS,#0
         MOV     DATEIMG,#0FFH
         CALL    GETUNIT
         MOV     DPTR,#LGDFLG1
         MOV     A,COMP
         JZ      DISPNG11
         MOV     A,#41H
         JNB     PSW_F0,U12
         ANL     A,#9FH
         ORL     A,#20H
U12:     JMP     DISPNG12

DISPNG11:MOV     A,#01H                ; NO VRC MESSAGE IF COMP OFF
DISPNG12:
         MOVX    @DPTR,A
         MOV     DPTR,#LGDFLG2
         MOV     A,COMP
         JZ      DISPNG13
         MOV     A,#1DH

         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JC      DISPNG9

         MOV     A,#15H                ; COMP ON/CAL OFF
         JMP     DISPNG9
DISPNG13:MOV     A,#14H

         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JNC     DISPNG9

         MOV     A,#1CH
DISPNG9: JNB     PSW_F0,U13
         ANL     A,#0F9H
         ORL     A,#02H
U13:     MOVX    @DPTR,A
         CALL    DISPSET
         JMP     DISPNG3
DISPNG7:
         MOV     DPTR,#GVOL
         CALL    GETVOL
         MOV     DGTPOS,#0
         MOV     DATEIMG,#0FFH
         CALL    GETUNIT
         CALL    LGD101
         MOV     DPTR,#LGDFLG2
         MOV     A,#1CH

         CALL    CHKCALMODE            ; EC/LT: C SET IF IN CAL MODE
         JC      DISPNG10

         MOV     A,#14H
DISPNG10:JNB     PSW_F0,DISPNG14
         ANL     A,#0F9H
         ORL     A,#02H
DISPNG14:MOVX    @DPTR,A
         CALL    DISPSET
         JMP     DISPNG3


VUNITS:  MOV     DPTR,#UNITS
         MOVX    A,@DPTR
         JZ      VUNITS1               ; 0=GALLONS, 1=LITRES
         MOV     DPTR,#MSG75           ; LITRES
         CALL    SHDP6                 ; WILL CALL MSG75A
         JMP     VUNITS2
VUNITS1: MOV     DPTR,#MSG74
         CALL    SHDP6                 ; WILL CALL MSG74A
VUNITS2: CALL    SPLASH
VUNITS3: CALL    KPRESS
         JNC     VUNITS3
         CJNE    A,#UPKEY,VUNITS4
         MOV     DPTR,#UNITS
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     VUNITS
VUNITS4: CJNE    A,#DOWNKEY,VUNITS5
         MOV     DPTR,#UNITS
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     VUNITS
VUNITS5: CJNE    A,#SSKEY,VUNITS3
VUNITS8: RET


TUNITS:  MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JZ      TUNITS1               ; 0=VOLUME, 1=MASS
         MOV     DPTR,#MSG109          ; MASS
         JMP     TUNITS2
TUNITS1: MOV     DPTR,#MSG110          ; VOLUME
TUNITS2: CALL    SPLASH
TUNITS3: CALL    KPRESS
         JNC     TUNITS3
         CJNE    A,#UPKEY,TUNITS4
         MOV     DPTR,#UTYPE
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     TUNITS
TUNITS4: CJNE    A,#DOWNKEY,TUNITS5
         MOV     DPTR,#UTYPE
         MOV     A,#0
         MOVX    @DPTR,A
         JMP     TUNITS
TUNITS5: CJNE    A,#SSKEY,TUNITS3
TUNITS8: MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JZ      TUNITS9
         MOV     DPTR,#GRSFLG          ; GRSFLG: 0=OFF, 1=ON, 2=NOTEMP (PRINT ALL EXCEPT TEMP), 3=TEMP(TEMP ONLY)
         MOV     A,#0
         MOVX    @DPTR,A               ; IF MASS ON THEN PGROSS OFF
TUNITS9: RET


ROTVOL:  MOVX    A,@DPTR
         MOV     RES,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     RES+1,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     RES+2,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     RES+3,A
         CALL    RRF
         RET

RLVOL:   MOV     A,RES
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,RES+1
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,RES+2
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,RES+3
         MOVX    @DPTR,A
         RET

ENTRY:   CALL    BCD2HEX
ENTRY1:  CALL    KPRESS
         MOV     A,KEY
         CJNE    A,#LEFTKEY,ENTRY2
         MOV     A,DGTPOS
         CJNE    A,DATEIMG+1,ENTRY4    ; CHECK NOT TO UNDERFLOW
         JMP     ENTRY3
ENTRY4:  DEC     DGTPOS                ; MOV LEFT ONE CYCLE AT A TIME
         JMP     ENTRY3
ENTRY2:
         MOV     A,KEY
         CJNE    A,#RIGHTKEY,ENTRY6
         MOV     A,DGTPOS
         CJNE    A,#6,ENTRY5           ; CHECK NOT TO OVERFLOW
         JMP     ENTRY3
ENTRY5:  INC     DGTPOS                ; MOV RIGHT ONE CYCLE AT A TIME
         JMP     ENTRY3
ENTRY6:
         MOV     A,KEY
         CJNE    A,#UPKEY,ENTRY7
         MOV     A,DGTPOS
         CLR     C
         SUBB    A,#1                  ; ZERO INDEX DIGIT POSITION
         ADD     A,#DGT1HEX            ; ADD OFFSET
         MOV     R0,A                  ; POINTER WITH OFFSET
         MOV     A,@R0
         CJNE    A,#9,ENTRY8
         JMP     ENTRY3
ENTRY8:  INC     @R0
         JMP     ENTRY3

ENTRY7:
         MOV     A,KEY
         CJNE    A,#DOWNKEY,ENTRY3
         MOV     A,DGTPOS
         CLR     C
         SUBB    A,#1                  ; ZERO INDEX DIGIT POSITION
         ADD     A,#DGT1HEX            ; ADD OFFSET
         MOV     R0,A                  ; POINTER WITH OFFSET
         MOV     A,@R0
         CJNE    A,#0,ENTRY9
         JMP     ENTRY3
ENTRY9:  DEC     @R0
         JMP     ENTRY3

ENTRY3:
         CALL    HEX2BCD               ; PUTS DIGIT HEX INTO VOLUME
         CALL    DISPSET               ; DISPLAY CURRENT INFO
         CALL    WAIT100               ; WAIT ABOUT 100MS
         MOV     TEMP,DGTPOS           ; SAVE POSITION
         MOV     DGTPOS,#0             ; TURN ON DIGIT IN DISPSET. NON-ZERO BLANKS
         CALL    DISPSET               ; PUTS VOLUME INTO DIGIT ASCII AND SHOWS
         CALL    WAIT250               ; 250ms wait
         MOV     DGTPOS,TEMP           ; RESTORE DIGIT POSITION
         CALL    BCD2HEX               ; PUT VOLUME INTO DIGIT HEX
         RET

XLOAD:
         CALL    MOVMSG
VLOAD:   SETB    DMMODE                ; SET TO SEND DATA TO LCD
		 CALL    WAIT20U               ; WAIT FOR DM TO COMPLETE MAIN LOOP AT LEAST ONCE
         CALL    CHKDEC
         JZ      XLOAD5
         MOV     DPTR,#DECFLG
         MOVX    A,@DPTR
         JNZ     XLOAD5                ; KEEP DECIMAL IF IN TEMP

         MOV     DPTR,#LGDFLG1
         MOVX    A,@DPTR
         ANL     A,#0FEH               ; KILL DECIMAL
         MOVX    @DPTR,A

XLOAD5:
         MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JZ      XLOAD6
         MOV     DPTR,#LGDFLG2
         MOVX    A,@DPTR
         ANL     A,#0F9H
         MOVX    @DPTR,A

XLOAD6:  MOV     DPTR,#DECFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET TEMP DECIMAL FLAG

         MOV     DPTR,#LGDFLG1         ; FIRST BYTE OF INCOMING DATA
         MOV     R3,#8                 ; NUMBER OF INCOMING BYTES MINUS COMMAND
XLOAD2:  MOV     R4,#8                 ; 8 BITS PER BYTE
         MOVX    A,@DPTR               ; GET BYTE
XLOAD1:  RRC     A                     ; BIT TO CARRY
         MOV     DMDATA,C              ; OUT TO PORT
         CALL    SHIFT                 ; CLOCK HIGH/LOW 20 uS
         DJNZ    R4,XLOAD1             ; 8 BITS PER BYTE
         INC     DPTR                  ; POINT TO NEXT BYTE
         DJNZ    R3,XLOAD2             ; DO FOR 9 BYTES
         MOV     R0,#COMMAND
         MOV     R4,#8                 ; LAST 8 BITS FOR COMMAND
         MOV     A,@R0
XLOAD4:  RRC     A
         MOV     DMDATA,C
         CALL    SHIFT
         DJNZ    R4,XLOAD4
         CLR     DMMODE                ; END TRANSMISSION
         CALL    WAIT3               ; ALLOW DMICRO TO FINISH UP
         RET


DOEMLOAD:MOV     SFRPAGE,#0FH          ;Switch SFR page
		 ANL     EMLOAD,#0xBF			; CLR EMLOAD
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    WAIT3
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     EMLOAD,#0x40			; SETB EMLOAD
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         RET


; ****************************************
; SETDIV SENDS PULSE DIVISOR TO EMICRO # IN R0
; ****************************************
SENDDIV:
;        MOV     DPTR,#DIVISOR
         MOVX    A,@DPTR
         MOV     R0,A                  ; GO GET DIVISOR
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     EMLOAD,#0x40			; SETB EMLOAD
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    WAIT20U                ;Let EM see LOAD
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     EMCOUNT,#0x7F			; CLR EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    WAIT3
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     EMLOAD,#0xBF			; CLR EMLOAD
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    WAIT20U
SENDDI1: MOV     SFRPAGE,#0FH          ;Switch SFR page
		 ORL     EMCOUNT,#0x80			; SETB EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    WAIT3
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     EMCOUNT,#0x7F			; CLR EMCOUNT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL    WAIT3
         DJNZ    R0,SENDDI1
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ORL     EMLOAD,#0x40			; SETB EMLOAD
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         RET

SHIFT:   SETB    DMCLK
		 CALL    WAIT20U                ;20us delay
         ;MOV     R5,#60
         ;DJNZ    R5,$
         CLR     DMCLK
		 CALL    WAIT20U                ;20us delay
         ;MOV     R5,#60
         ;DJNZ    R5,$
         RET

BLINK:   SETB    V1ST
         CALL    WAIT500
         CLR     V1ST



//Need to move each character into DIGIT1-6
MOVMSG:  MOV     R1,#6
         MOV     R0,#DATEIMG
MOVMSG1:
		 CLR     A
		 MOVC    A,@A+DPTR
         MOV     @R0,A
         INC     DPTR
         INC     R0
         DJNZ    R1,MOVMSG1            ; GOT DPRT INTO DATEIMG
         MOV     R1,#6
         MOV     DPTR,#DIGIT1
         MOV     R0,#DATEIMG
MOVMSG2: MOV     A,@R0
         MOVX    @DPTR,A
         INC     R0
         INC     DPTR
         DJNZ    R1,MOVMSG2
         RET


; ******************************************************
; KPRESS CHECKS FOR KEYPRESS AND RETURNS C=1 IF SO AND
; KEY# IN 'KEY'. RETURNS KEY=0 IF NO KEY. RETURNS C=0
; IF KEY # THE SAME AS LAST SEEN.
; ******************************************************
KPRESS:  CLR     C
         CALL    KBSHFT
         CPL     A                     ; NEGATIVE LOGIC SO INVERT
         JZ      KP2                   ; NOTHING PRESSED SO EXIT
		 ;CALL   WAIT500
         CJNE    A,KEY,KP3             ; IF SAME AS LAST KEY EXIT
         CLR     C                     ; WITH "NO KEY" RESPONSE
         JMP     KPDN
KP3:     MOV     R7,#1                 ; DEBOUNCE LOOKING AT KEY
KP1:     CALL    KBSHFT
         CPL     A
         JZ      KP2
         DJNZ    R7,KP1
         MOV     KEY,A                 ; OK IF HERE. STORE KEY#
         SETB    C                     ; INDICATE GOT ONE.
         JMP     KPDN
KP2:     MOV     KEY,#0                ; 0=NO KEY
KPDN:    RET

KBSHFT:  CLR     KBSHLD
		 CALL 	 WAIT5U
         SETB    KBSHLD                ; LOAD KEYBOARD TO SHIFT REGISTER
         CALL 	 WAIT5U
         MOV     R2,#8
KBS1:    ;MOV     C,KBDATA              ; GET BIT
//Because P4 is not bit addressable, we have to move data into temp biut addressable location
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
		 MOV     TEMPBIT,P4
		 MOV     C,TEMPBIT.1           ; P4.1 is KBDATA
         RRC     A                     ; ROTATE INTO ACC
         ORL     KBCLK,#0x01			; SETB KBCLK CLOCK TO NEXT BIT
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         CALL 	 WAIT5U
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     KBCLK,#0xFE			; CLR KBCLK
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         ;CALL 	 WAIT20U
         DJNZ    R2,KBS1               ; GET ALL 8
         RET


; *************************************************
; DISPSET CONVERTS THE DIGITHEX TO ASCII.
; SHOWS ALL DIGITS IF DGTPOS IS ZERO.
; BLANKS CURRENT DIGIT WITH DGTPOS NON-ZERO
; *************************************************

DISPSET: MOV     A,DGTPOS
         JZ      DISP6
         CJNE    A,#6,DISP6
         MOV     DPTR,#DIGIT6
         MOV     A,#SPACE
         MOVX    @DPTR,A
         JMP     DISP5
DISP6:   MOV     A,VOLUME
         ANL     A,#0FH
         MOV     DPTR,#NUMTABLE
         MOVC    A,@A+DPTR
         MOV     DPTR,#DIGIT6
         MOVX    @DPTR,A

         MOV     A,DGTPOS
         JZ      DISP5
         CJNE    A,#5,DISP5
         MOV     DPTR,#DIGIT5
         MOV     A,#SPACE
         MOVX    @DPTR,A
         JMP     DISP4
DISP5:   MOV     A,VOLUME
         SWAP    A
         ANL     A,#0FH
         MOV     DPTR,#NUMTABLE
         MOVC    A,@A+DPTR
         MOV     DPTR,#DIGIT5
         MOVX    @DPTR,A

         MOV     A,DGTPOS
         JZ      DISP4
         CJNE    A,#4,DISP4
         MOV     DPTR,#DIGIT4
         MOV     A,#SPACE
         MOVX    @DPTR,A
         JMP     DISP3
DISP4:   MOV     A,VOLUME+1
         ANL     A,#0FH
         MOV     DPTR,#NUMTABLE
         MOVC    A,@A+DPTR
         MOV     DPTR,#DIGIT4
         MOVX    @DPTR,A
         MOV     A,DGTPOS

         JZ      DISP3
         CJNE    A,#3,DISP3
         MOV     DPTR,#DIGIT3
         MOV     A,#SPACE
         MOVX    @DPTR,A
         JMP     DISP2
DISP3:   MOV     A,VOLUME+1
         SWAP    A
         ANL     A,#0FH
         MOV     DPTR,#NUMTABLE
         MOVC    A,@A+DPTR
         MOV     DPTR,#DIGIT3
         MOVX    @DPTR,A

         MOV     A,DGTPOS
         JZ      DISP2
         CJNE    A,#2,DISP2
         MOV     DPTR,#DIGIT2
         MOV     A,#SPACE
         MOVX    @DPTR,A
         JMP     DISP1
DISP2:   MOV     A,VOLUME+2
         ANL     A,#0FH
         MOV     DPTR,#NUMTABLE
         MOVC    A,@A+DPTR
         MOV     DPTR,#DIGIT2
         MOVX    @DPTR,A
         MOV     A,DGTPOS
         JZ      DISP1
         CJNE    A,#1,DISP1
         MOV     DPTR,#DIGIT1
         MOV     A,#SPACE
         MOVX    @DPTR,A
         JMP     DISP0
DISP1:   MOV     A,VOLUME+2
         SWAP    A
         ANL     A,#0FH
         MOV     DPTR,#NUMTABLE
         MOVC    A,@A+DPTR
         MOV     DPTR,#DIGIT1
         MOVX    @DPTR,A
DISP0:
         MOV     R3,#6
         MOV     DPTR,#DIGIT1
         MOV     A,DATEIMG
         MOV     B,A
DISP7:   MOV     A,B
         RLC     A
         MOV     B,A                   ; HAVE TO SAVE ACC FROM @DPTR
         JC      DISP9
         MOV     A,#SPACE
         MOVX    @DPTR,A
DISP9:   INC     DPTR
         DJNZ    R3,DISP7
         CALL    SHOWVL
         RET



; **********************************************
; BCD2HEX SPREADS OUT THE PACKED BDC IN VOLUME TO
; ONE HEX CHARACTER PER DIGIT.
; **********************************************
BCD2HEX: MOV     A,VOLUME
         ANL     A,#0FH
         MOV     DGT6HEX,A
         MOV     A,VOLUME
         SWAP    A
         ANL     A,#0FH
         MOV     DGT5HEX,A

         MOV     A,VOLUME+1
         ANL     A,#0FH
         MOV     DGT4HEX,A
         MOV     A,VOLUME+1
         SWAP    A
         ANL     A,#0FH
         MOV     DGT3HEX,A

         MOV     A,VOLUME+2
         ANL     A,#0FH
         MOV     DGT2HEX,A
         MOV     A,VOLUME+2
         SWAP    A
         ANL     A,#0FH
         MOV     DGT1HEX,A
         RET

; ************************************************
; HEX2BCD TAKES THE SINGLE HEX DIGITS AND PACKS
;  THEM BACK INTO BCD.
; ************************************************
HEX2BCD: MOV     A,#0
         ORL     A,DGT5HEX
         SWAP    A
         ORL     A,DGT6HEX
         MOV     VOLUME,A
         MOV     A,#0
         ORL     A,DGT3HEX
         SWAP    A
         ORL     A,DGT4HEX
         MOV     VOLUME+1,A
         MOV     A,#0
         ORL     A,DGT1HEX
         SWAP    A
         ORL     A,DGT2HEX
         MOV     VOLUME+2,A
         RET


; *****************************************
; BCD2BIN. REAL CRUDE BUT SIMPLE
; TAKES BCD IN TEMP AND RETURNS IN TEMP
; *****************************************
BCD2BIN: CLR     C
         MOV     A,#0
         MOV     R0,#0
BCD2B2:  CJNE    A,TEMP,BCD2B1
         JMP     BCD2BDN
BCD2B1:  ADD     A,#1
         DA      A
         INC     R0
         JMP     BCD2B2
BCD2BDN: MOV     TEMP,R0
         RET

; *********************************************
; BIN2BCD. ALSO CRUDE BUT EFFECTIVE.
; TAKES BINARY IN TEMP AND RETURNS BCD IN TEMP
; *********************************************
BIN2BCD: CLR     C
         MOV     R0,TEMP
         MOV     A,R0
         JZ      BIN2BCN               ; ZERO ON FIRST PASS GET OUT
         MOV     A,#0
BIN2BC1: ADDC    A,#1
         DA      A
         DJNZ    R0,BIN2BC1
BIN2BCN: MOV     TEMP,A
         RET


;******************************************
; DOES BCD SUM
; (R0) += (R1) ; R2 TIMES    CY PRECONDITIONED
;******************************************
SUMUP:   MOV     A,@R0                 ; GET FIRST VARIABLE
         ADDC    A,@R1
         DA      A                     ; DECIMAL ADJUST
         MOV     @R0,A
         INC     R0
         INC     R1
         DJNZ    R2,SUMUP
         RET

CHKDEC:
         MOV     DPTR,#DECIMAL
         MOVX    A,@DPTR
         RET                           ; RETURNS DECIMAL VALUE IN ACC


; CLROUT CLEARS ALL VOLUME VARIABLES
CLROUT:  MOV     R2,#28                ; NCGALS-ECGALS+4  ; COUNTER FOR RWM ABOVE CGALS
         MOV     R0,#ECGALS            ; POINT TO CGALS FIRST
CLR1:    MOV     @R0,#0
         INC     R0
         DJNZ    R2,CLR1
         RET                           ; EXIT FROM CLROUT

CLRDEL:  MOV     DPTR,#GVOL
         MOV     A,#0
         MOV     R1,#8
CLRDEL1: MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,CLRDEL1
         RET

CLRCGALS:MOV     R0,#CGALS
         MOV     A,#0
         MOV     R1,#5
CLRCG1:  MOV     @R0,A
         INC     R0
         DJNZ    R1,CLRCG1
         RET

CLRLINE: MOV     R1,#25
         MOV     R0,#ECGALS
         MOV     A,#' '
CLRLIN1: MOV     @R0,A
         INC     R0
         DJNZ    R1,CLRLIN1
         RET

PRT65:   CALL    CLRLINES
         CALL    SAYPRNT
         CALL    SELECTP2
         CALL    PRTFTIM
         CALL    CLRLINE
         MOV     ECGALS+22,#'0'
         MOV     ECGALS+23,#'.'
         MOV     ECGALS+24,#'0'
         CALL    PRTLINE

         MOV     A,#' '
         MOV     R1,#18
PRT65A:  CALL    OUTA
         DJNZ    R1,PRT65A             ; 18 SPACES

         MOV     DPTR,#NVOL+2
         CALL    ASCOUT
         CALL    DASCOUT
         CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC
         CALL    OUTA
         CALL    CHKDEC
         JNZ     PRT65B
         MOV     A,#'.'
         CALL    OUTA
PRT65B:  MOV     A,B
         CALL    OUTA
         CALL    PLINEEND

         CALL    CLRLINE
         MOV     DPTR,#TKTNO+2
         MOV     R0,#ECGALS+19
         CALL    ASCBLD
         CALL    DASCBLD
         CALL    DASCBLD
         CALL    PRTLINE
         CALL    CONVERT
         CALL    DOPRINT

; 12/18/2013 - THE FOLLOWING CODE ISN'T ENOUGH, THERE ARE A LOT OF AFTER-DELIVERY VARIABLES
; THAT AREN'T BEING SET, NEED TO JUMP TO NORMAL END OF TKT CODE
;         MOV     DPTR,#DELFLG
;         MOV     A,#0
;         MOVX    @DPTR,A               ; RESET DELIVERY FLAG WITH NO PRINTER
;         CALL    DISCON
;         CALL    UNLATCH
;
;         JMP     MAINX
;

          JMP     PRTFX1               ; E179E0 REV3, TESTING THIS INSTEAD OF CODE ABOVE

PRTFINAL:
         MOV     DPTR,#DUPFLG
         MOVX    A,@DPTR
         JZ      PRTFC                 ; NOT DUPLICATE JUMP AROUND THE NEXT
         MOV     DPTR,#HOST
         MOVX    A,@DPTR               ; GET HOST BYTE
         MOV     DPTR,#CURRHOST
         MOVX    @DPTR,A               ; SAVE CURRENT HOST IF DUPLICATE

         MOV     DPTR,#STATUS
         MOVX    A,@DPTR               ; GET LAST HOST STATUS
         ANL     A,#80H                ; MASK HOST BIT

         MOV     DPTR,#HOST
         JZ      PRTFB
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     PRTFC
PRTFB:   MOV     A,#0
         MOVX    @DPTR,A

PRTFC:
         MOV     DPTR,#PR65FLG			;6501 PRINT FORMAT
         MOVX    A,@DPTR
         JZ      PRTFA
         JMP     PRT65
PRTFA:   CALL    CLRLINES
         CALL    SAYPRNT

         MOV     DPTR,#PRTFLG		   ; WHICH PRINTER IS SELECTED
         MOVX    A,@DPTR
         CJNE    A,#3,PRTFIN5          ; NO PRINTER - NEEDS TESTING
         JMP     PRTFX

PRTFIN5: CALL    SELECTP2
         MOV     DPTR,#XCOPIES		   ; NUMBER OF COPIES FROM HOST
         MOVX    A,@DPTR
         JNZ     PRTFI20			   ; HOST COPY > 0 TRUMPS HEAD COPY #

         MOV     DPTR,#COPIES		   ; NUMBER OF COPIES FROM HEAD
         MOVX    A,@DPTR
         JNZ	 PRTFI20			   ; IF 0 (NEW DIPSTICK) THEN IT NEEDS TO BE SET TO 1
         MOV	 A, #1
         MOVX	 @DPTR,A
PRTFI20:
         MOV     DPTR,#CPYDCTR
         MOVX    @DPTR,A               ; DOWN COUNTER
         MOV     DPTR,#CPYUCTR
         MOV     A,#1
         MOVX    @DPTR,A               ; UP COUNTER STARTS AT 1
         MOV     DPTR,#XCOPIES
         MOV     A,#0
         MOVX    @DPTR,A

;TOP OF METER TICKET HERE ***********************************************************

PRTFINTOP:
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#7,PRTFINTPA
         CALL    PR460LOGO             ; PRINT LOGO IF FUJITSU FP-460
         JMP     PRTFINTPZ
PRTFINTPA:
         CJNE    A,#8,PRTFINTPB
         CALL    PRTM88LOGO            ; PRINT LOGO IF CITIZEN CTS-310
         JMP     PRTFINTPZ
PRTFINTPB:
         CJNE    A,#9,PRTFINTPC
         CALL    PR6X1LOGO             ; PRINT LOGO IF CITIZEN CTS-601/CTS-651
         JMP     PRTFINTPZ
PRTFINTPC:
         CJNE    A,#10,PRTFINTPD
         CALL    PRTM88LOGO            ; PRINT LOGO IF EPSON TM-88
         JMP     PRTFINTPZ
PRTFINTPD:
         CJNE    A,#13,PRTFINTPE
         CALL    PFEED                 ; NEED ONE BLANK LINE AT TOP OF TICKET FOR DATAMAX M4TE
         JMP     PRTFINTPZ

; SINCE THE LOGO FILE DIMENSIONS CAN VARY AND WE HAVE TO KNOW THE HEIGHT IN ORDER TO SPECIFY
;  THE 'LABEL' DIMENSIONS, AND WE ARE UING HARD-CODED COMMANDS TO TELL THE BLASTER TO PRINT THE LOGO,
;  WE ARE NOT GOING TO PRINT LOGOS IN THE BLASTER AT THIS TIME
;PRTFINTPD:
;         CJNE    A,#5,PRTFINTPZ
;         CALL    PRBLSTLOGO            ; PRINT LOGO ON BLASTER
;         JMP     PRTFINTPZ

PRTFINTPE:
         CJNE    A,#15,PRTFINTPF       ; JUST LIKE BLASTER, THE LOGO IS VARYING HEIGHT :(
         CALL    PRQ3LOGO              ; PRINT LOGO IF CUSTOM Q3
         JMP     PRTFINTPZ

PRTFINTPF:
         CJNE    A,#16,PRTFINTPZ       ; THE LOGO PRINT CMD WORKS WITHOUT HEIGHT

         ; ALL 3 LINES REQUIRED FOPR TSP700II LOGO
         CALL    CLRLEFTMRGN           ; STAR TSP700II - NEED TO MAKE SURE LEFT MARGIN IS 0
         CALL    PRTM88LOGO            ; PRINT LOGO IF STAR TSP700II = SAME AS TM88/CTS310
         CALL    CHKSETUPQ3            ; NEED TO RESET LEFT MARGIN

         JMP     PRTFINTPZ             ; BUT NEED TO ADJUST LEFT MARGIN 1ST

PRTFINTPZ:
         CALL    LODHSTLNFLG           ; IF PASS-THROUGH-PRINT (NON-ZERO), NEED TO 'RET'
         JZ      PRTFINTPY
         RET                           ; NEED TO 'RET' SINCE WAS 'CALL'ED HERE

PRTFINTPY:
         MOV     DPTR,#DUPFLG
         MOVX    A,@DPTR
         JZ      PRTFIN6
         MOV     DPTR,#PRMSG50
         CALL    PRTCMSG

         CALL    PFEED

PRTFIN6: MOV     DPTR,#POWFAIL
         MOVX    A,@DPTR
         JZ      PRHDRBBYTS
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET PRINT POWER FLAG
         MOV     DPTR,#PRMSG54
         CALL    PRTCMSG

         CALL    PFEED


; TOP OF TICKET
; A) PA21 SLSMODE      = PRINT HEADER, PRINT B-BYTES
; B) HOST              = SKIP HEADER,  PRINT B-BYTES
; C) NON-HOST, NON-SLS = PRINT HEADER, SKIP B-BYTES

PRHDRBBYTS:
         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#2,PRHDRBBYT1       ; 2=PA21 SLSMODE, ELSE CONTINUE TESTING

                                       ; SLSMODE PRINT HEADER
         CALL    PRINTH                ; PRINT HEADER
         JMP     PRHDRBBYT2

PRHDRBBYT1:
         MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JNZ     PRHDRBBYT2            ; SKIP PRINT OF HEADER IF HOST

         CALL    PRINTH                ; PRINT HEADER
         JMP     PRHDRBBYT2

PRHDRBBYT2:
         MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JZ      PRHDRBBYT3            ; IF HOST IS OFF (0) THEN CONTINUE TESTS

                                       ; HOST = PRINT B-BYTES
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#BBYTES          ; POINT TO START
         CALL    PRTBYTES
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         JMP     FINTKTTOP

PRHDRBBYT3:
         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#2,FINTKTTOP        ; 2=PA21 SLSMODE, ELSE SKIP B-BYTES

;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#BBYTES          ; POINT TO START
         CALL    PRTBYTES
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         JMP     FINTKTTOP

LNGJMP2SKP:
         JMP     SKIPHA                ; JNB OUT OF RANGE BELOW

FINTKTTOP:
         MOV     DPTR,#HDRTYPFL        ; HEADER TYPE FLAG, 0=DELTICKET, 1=SHIFTTICKET, 2=CALTICKET
         MOV     A,#0
         MOVX    @DPTR,A
         CALL    DOPRTHDR
         CALL    PRTFTIM
         CALL    PRTVOL                ; CALL 1

; TEMP STAMP
         CALL    CKPGRSPTMP
         JNB     PSW_F0,LNGJMP2SKP         ; F0=SET IF GRSFLG=1 (ON) OR GRSFLG=3 (TEMP ONLY) TO PRINT TEMP

         MOV     DPTR,#PRTFLG          ; IF THE PRINTEK MOBILE RT43 BLUETOOTH IS BEING USED THE DEGREE SYMBOL IS WRONG, FIX IT
         MOVX    A,@DPTR
         CJNE    A,#12,FINTKTT2



; FOR PRINTEK MOBILE RT43 TO PRINT DEGREE SYMBOLS
         CALL    TEMPF
         MOV     DPTR,#DIGIT3
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55A+9
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT4
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55A+10
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT5
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55A+11
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT6
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55A+13
         MOVX    @DPTR,A

         CALL    TEMPC
         MOV     DPTR,#DIGIT3
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55A+17
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT4
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55A+18
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT5
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55A+19
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT6
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55A+21
         MOVX    @DPTR,A
         JMP     FINTKTT9

FINTKTT2:
; FOR ALL EXCEPT PRINTEK MOBILE
         CALL    TEMPF
         MOV     DPTR,#DIGIT3
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55+9
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT4
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55+10
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT5
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55+11
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT6
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55+13
         MOVX    @DPTR,A

         CALL    TEMPC
         MOV     DPTR,#DIGIT3
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55+17
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT4
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55+18
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT5
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55+19
         MOVX    @DPTR,A
         MOV     DPTR,#DIGIT6
         MOVX    A,@DPTR
         MOV     DPTR,#PRMSG55+21
         MOVX    @DPTR,A

FINTKTT9:

         CALL    SAYPRNT               ; ACTUALLY NEED TO RE-SHOW PRINT

         CALL    SETBLDG2FLG           ; SET THE BLASTER DEGREE SYMBOL FLAG, IT WILL BE CLEARED IN BLAST
         MOV     DPTR,#PRMSG55
         CALL    PRTMSG
         CALL    CLRBLDGFLG            ; CLEAR THE BLASTER DEGREE SYMBOL FLAG, IT WILL BE CLEARED IN BLAST THIS IS INSURANCE

SKIPHA:
         MOV     DPTR,#DELFLG
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET DELIVERY FLAG

; BOTTOM OF TICKET
; A) SLSMODE           = PRINT A-BYTES, PRINT FOOTER
; B) HOST              = PRINT A-BYTES, SKIP FOOTER
; C) NON-HOST, NON-SLS = SKIP A-BYTES,  PRINT FOOTER

PRFTRABYTS:
         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#2,PRFTRABYT1       ; 2=PA21 SLSMODE, ELSE CONTINUE TESTING

;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#ABYTES
         CALL    PRTBYTES
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, RESET MCON TO LOCAL MEMORY

         JMP     PRFTRABYT2

PRFTRABYT1:
         MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JZ      PRFTRABYT2            ; SKIP PRINT OF A-BYTES IF NOT HOST

                                       ; HOST MODE HAS A-BYTES, PRINT THEM
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#ABYTES
         CALL    PRTBYTES
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, RESET MCON TO LOCAL MEMORY
         JMP     PRFTRABYT2

PRFTRABYT2:
         MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JNZ     FINISHTKT             ; IF HOST IS ON (NOT 0) THEN SKIP FOOTER, ELSE PRINT FOOTER
         CALL    PRINTF                ; PRINT FOOTER (SLSMODE OR NON-HOST)


FINISHTKT:                             ; IN THIS FIRST BLOCK WE CHECK THE IMPACT PRINTER(S), THEY
                                       ;  MIGHT NEED EXTRA LINEFEEDS TO SEND THE PRINTED TEXT PAST THE TEARBAR
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#6,FINISTK1         ; TM-220 = NEEDS EXTRA LF'S
         CALL    MFEEDWSKIP            ; PRIOR TO REV 8 WE DID 6 LFS BELOW, CHANGED IT TO 7 HERE ONLY TO BE CONSISTENT
         JMP     FINISTKZ              ; TM-220 = IMPACT = NO MORE COPIES TO PRINT

                                       ; IN THIS SECOND BLOCK WE CHECK THE THERMALS, THEY
                                       ;  PRINT MULTIPLE COPIES, PRINT COPY# (OR NOT), AND DO BMFF (OR NOT)
FINISTK1:CJNE    A,#2,FINISTK2
         CALL    PRTCOPYWSKP           ; THERMAL
         CALL    MFEEDWSKIP            ; IF LAST COPY AND IF HOST PARM 3 = '1' THEN SKIP TRAILING LF'S/BMFF'S
         JMP     SKIPA1

FINISTK2:CJNE    A,#4,FINISTK2A        ; ZEBRA RW-420 V1 = NO BMFF
         CALL    PRTCOPYWSKP           ; SAME AS THERMAL
         CALL    MFED3WSKP             ; PRIOR TO REV 8 WE DID 3 LFS HERE AND 3 LFS BELOW (?), CHANGED IT TO 3 HERE ONLY TO BE CONSISTENT
         JMP     SKIPA1

FINISTK2A:CJNE   A,#14,FINISTK3        ; ZEBRA RW-420 V2 = DO BMFF
         CALL    PRTCOPYWSKP           ; SAME AS THERMAL
         CALL    CKSKPFNCMD            ; PRINT TRAILING LINES IF CMDFLG+3='0' OR CPYDCTR!=1, SKIP IF CMDFLG+3='1' AND CPYDCTR=1, C WILL BE SET IF SHOULD PRINT
         JNC     FINISTK2B
         CALL    PR295FF               ; RW-420 BMFF USES SAME FF CMD AS TM-295
FINISTK2B:JMP     SKIPA1

FINISTK3:CJNE    A,#5,FINISTK3B        ; BLASTER V1 = 3 LINEFEEDS
         ;CALL    PRTCOPYWSKP          ; ORIGINALLY REMOVED CODE PRINTING COPY# PER BILL AT TOUCHSTAR 11/5/2012
                                       ; ADDED PARAMETER[4] CHECK IN E178F TO ALLOW HOST SOFTWARE TO CONTROL IT PER DELIVERY, FOR CONSISTENCY
                                       ; LEFT IN CODE TO PRINT BLANK LINE BELOW IT (???)
         CALL    CKSKPFNCMD            ; PRINT TRAILING LINES IF CMDFLG+3='0' OR CPYDCTR!=1, SKIP IF CMDFLG+3='1' AND CPYDCTR=1, C WILL BE SET IF SHOULD PRINT
         JNC     FINISTK3A
         CALL    DO3BLNKLNS            ; PRINT 3 BLANK LINES FOR BLASTER V1
FINISTK3A:JMP     SKIPA1

FINISTK3B:CJNE    A,#11,FINISTK4       ; BLASTER V2 = BLASTER V1 + BLACK MARK ENABLED
         CALL    PRTCOPYWSKP           ; ORIGINALLY REMOVED CODE PRINTING COPY# PER BILL AT TOUCHSTAR 11/5/2012
                                       ; ADDED PARAMETER[4] CHECK IN E178F TO ALLOW HOST SOFTWARE TO CONTROL IT PER DELVEIRY,
                                       ;  AS WELL AS TO HAVE PUMP & PRINT TICKETS PRINT IT, TOUCHSTAR WILL HAVE TO UPDATE THEIR CODE
                                       ; LEFT IN CODE TO PRINT BLANK LINE BELOW IT (???)
         CALL    CKSKPFNCMD            ; PRINT TRAILING LINES IF CMDFLG+3='0' OR CPYDCTR!=1, SKIP IF CMDFLG+3='1' AND CPYDCTR=1, C WILL BE SET IF SHOULD PRINT
         JNC     FINISTK3C
         CALL    PRBLSTBMFF            ; BLASTER BLACK MARK FF
FINISTK3C:JMP     SKIPA1

FINISTK4:CJNE    A,#7,FINISTK5         ; FUJITSU FP-460
         CALL    PRTCOPYWSKP
         CALL    CKSKPFNCMD            ; PRINT TRAILING LINES/BMFF IF CMDFLG+3='0' OR CPYDCTR!=1, SKIP IF CMDFLG+3='1' AND CPYDCTR=1, C WILL BE SET IF SHOULD PRINT
         JNC     SKIPA1
         CALL    PR460BMF              ; FUJITSU FP-460 BLACK MARK FF, DOES 1 SEC WAIT
         JMP     SKIPA1                ; AUTOMATICALLY DOES A CUT

FINISTK5:CJNE    A,#8,FINISTK6         ; CITIZEN CTS-310
         CALL    PRTCOPYWSKP           ; SAME AS THERMAL
         CALL    MFEEDWSKIP
         JMP     SKIPA1

FINISTK6:CJNE    A,#9,FINISTK7         ; CITIZEN CT-S601/651
         CALL    PRTCOPYWSKP           ; SAME AS THERMAL
         CALL    MFEEDWSKIP
         JMP     SKIPA1

FINISTK7:CJNE    A,#10,FINISTK8        ; EPSON TM-88
         CALL    PRTCOPYWSKP           ; SAME AS THERMAL
         CALL    MFEEDWSKIP
         JMP     SKIPA1

FINISTK8:CJNE    A,#12,FINISTK9        ; PRINTEK RT43BT
         CALL    PRTCOPYWSKP           ; SAME AS THERMAL
         CALL    MFEEDWSKIP
         JMP     SKIPA1

FINISTK9:CJNE    A,#13,FINISTK9A       ; DATAMAX M4TE
         CALL    PRTCOPYWSKP           ; SAME AS THERMAL
;         CALL    MFEEDWSKIP ; 7 IS TOO MANY AT LARGER FONT ..
; TRYING 3 SAME AS ZEBRA ... 3 IS TOO MANY, TRYING 2
         CALL    CKSKPFNCMD            ; PRINT TRAILING LINES/BMFF IF CMDFLG+3='0' OR CPYDCTR!=1, SKIP IF CMDFLG+3='1' AND CPYDCTR=1, C WILL BE SET IF SHOULD PRINT
         JNC     SKIPA1
         CALL    PFEED                 ; NO CUT (TEAR ONLY) AFTER 2 LF'S
         CALL    PFEED                 ; NO CUT (TEAR ONLY) AFTER 2 LF'S
         JMP     SKIPA1

FINISTK9A:
         CJNE    A,#15,FINISTK9B       ; CUSTOM Q3
         CALL    PRTCOPYWSKP
         CALL    CKSKPFNCMD            ; PRINT TRAILING LINES/BMFF IF CMDFLG+3='0' OR CPYDCTR!=1, SKIP IF CMDFLG+3='1' AND CPYDCTR=1, C WILL BE SET IF SHOULD PRINT
         JNC     SKIPA1
         CALL    PRQ3BMFF              ; CUSTOM Q3 BLACK MARK FF, DOES 1 SEC WAIT
         JMP     SKIPA1

FINISTK9B:
         CJNE    A,#16,FINISTKZ        ; STAR TSP700II
         CALL    PRTCOPYWSKP
         CALL    CKSKPFNCMD            ; PRINT TRAILING LINES/BMFF IF CMDFLG+3='0' OR CPYDCTR!=1, SKIP IF CMDFLG+3='1' AND CPYDCTR=1, C WILL BE SET IF SHOULD PRINT
         JNC     SKIPA1
         CALL    PR295FF               ; STAR TSP700II BLACK MARK FF SAME AS TM-295, DOES 2 SEC WAIT
         JMP     SKIPA1


; ALL THE REST OF THE PRINTERS ARE IMPACT AND DON'T NEED EXTRA LF'S/ETC = NO MORE COPIES TO PRINT
FINISTKZ:JMP     PRTFINZ

SKIPA1:
         CALL    LODHSTLNFLG           ; IF PASS-THROUGH-PRINT (NON-ZERO), NEED TO SKIP THE COPY COUNTER CODE
         CJNE    A,#0,PRTFIN4          ;  AND JUST PRINT AS IF THIS WAS THE LAST COPY

         MOV     DPTR,#CPYUCTR
         MOVX    A,@DPTR
         INC     A
         MOVX    @DPTR,A

         MOV     DPTR,#CPYDCTR
         MOVX    A,@DPTR
         DEC     A                     ; BUMP DOWN ONE
         MOVX    @DPTR,A               ; PUT IT BACK
         CJNE    A,#0,PRTFIN3
         JMP     PRTFIN4

PRTFIN3:
         MOV     DPTR,#CMDFLAG+1       ; BYTE1(0-9) = SKIP BETWEEN-COPY CUTS IF '1'
         MOVX    A,@DPTR
         CJNE    A,#'1',PRTF1          ; SKIP BETWEEN-COPY CUTS IN HOST MODE IF PARAMETER[1] FROM HOST = '1'
         JMP     PRTFINTOP
PRTF1:
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#2,PRTF1A2          ; THERMAL
         CALL    THRMPRTCUT
         JMP     PRTF1C1

PRTF1A2: CJNE    A,#8,PRTF1A3          ; CT-S310
         CALL    THRMPRTCUT
         JMP     PRTF1C1

PRTF1A3: CJNE    A,#9,PRTF1A4          ; CT-S601/651
         CALL    THRMPRTCUT
         JMP     PRTF1C1

PRTF1A4: CJNE    A,#10,PRTF1A5         ; TM-88
         CALL    THRMPRTCUT
         JMP     PRTF1C1

PRTF1A5: CJNE    A,#15,PRTF1A6         ; Q3
         CALL    THRMPRTCUT
         JMP     PRTF1C1

PRTF1A6: CJNE    A,#16,PRTF1C2         ; TSP700II
         CALL    THRMPRTCUT
         JMP     PRTF1C1

THRMPRTCUT:
         MOV     A,#1BH                ; THERMAL(S)
         CALL    OUTA
         MOV     A,#6DH
         CALL    OUTA                  ; PARTIAL CUT
         RET

PRTF1C1: CALL    WAIT1000
PRTF1C2: JMP     PRTFINTOP

PRTFIN4:
         MOV     DPTR,#CMDFLAG+2       ; BYTE2(0-9) = SKIP FINAL COPY CUT IF '1'
         MOVX    A,@DPTR
         CJNE    A,#'1',PRTF2          ; SKIP FINAL-COPY CUT IN HOST MODE IF PARAMETER[+2] FROM HOST = '1'
         JMP     PRTFINZ               ; GO TO PRINTING FINISHED
PRTF2:
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#2,PRTF2A2          ; THERMAL
         CALL    THRMPRTCUT
         JMP     PRTFINZ               ; GO TO PRINTING FINISHED

PRTF2A2: CJNE    A,#8,PRTF2A3          ; CT-S310
         CALL    THRMPRTCUT
         JMP     PRTFINZ               ; GO TO PRINTING FINISHED

PRTF2A3: CJNE    A,#9,PRTF2A4          ; CT-S601/651
         CALL    THRMPRTCUT
         JMP     PRTFINZ               ; GO TO PRINTING FINISHED

PRTF2A4: CJNE    A,#10,PRTF2A5         ; TM-88
         CALL    THRMPRTCUT
         JMP     PRTFINZ               ; GO TO PRINTING FINISHED

PRTF2A5: CJNE    A,#15,PRTF2A6         ; Q3
         CALL    THRMPRTCUT
         JMP     PRTFINZ               ; GO TO PRINTING FINISHED

PRTF2A6: CJNE    A,#16,PRTFINZ         ; TSP700II
         CALL    THRMPRTCUT
         JMP     PRTFINZ               ; GO TO PRINTING FINISHED

PRTFINZ: CALL    WAIT4000               ; ALL PRINTERS END UP HERE WAITING FOR FEW SECS TO FINISH PRINTING

PRTFX:
         CALL    DISCON                ; CALL #3: END OF PRINTING DEL TICKET(S)
         CALL    UNLATCH

         CALL    LODHSTLNFLG           ; IF PASS-THROUGH-PRINT (NON-ZERO), NEED TO 'RET'
         JZ      PRTFX1
         RET                           ; NEED TO 'RET' SINCE WAS 'CALL'ED HERE BY HOST

PRTFX1:
         MOV     A,#0                  ; IF YOU ADD/CHANGE VARS HERE UPDATE BOTH INIT6B + PRTFX1

         MOV     DPTR,#DELFLG          ; RESET DELIVERY FLAG WITH NO PRINTER
         MOVX    @DPTR,A

         MOV     DPTR,#POWFAIL         ; RESET POWFAIL TO DELIVERY PRINTED
         MOVX    @DPTR,A



         MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JZ      PRTFN9A

         CALL    CON2HOST              ; REMAKE CONNECTION TO HOST AFTER PRINT
         CALL    SELECTHST             ; IF IN HOST MODE 5/7/08
         CALL    SENDPIPE              ; SIGNAL PRINTING DONE
         JMP     PRTFIN9

PRTFN9A: MOV     DPTR,#HDPFLG          ; FLAG TO DO PIPE IF NOT HOST MODE BUT IN HOST CMD
         MOVX    A,@DPTR
         JZ      PRTFIN9
         CALL    CON2HOST              ; REMAKE CONNECTION TO HOST AFTER PRINT
         CALL    SELECTHST             ; IF IN HOST MODE 5/7/08
         CALL    SENDPIPE              ; SIGNAL PRINTING DONE

PRTFIN9:
         CALL    DISCON                ; DISCONNECT
         CALL    UNLATCH

         MOV     DPTR,#DUPFLG
         MOVX    A,@DPTR
         JZ      PRTFD
         MOV     DPTR,#CURRHOST
         MOVX    A,@DPTR
         MOV     DPTR,#HOST
         MOVX    @DPTR,A               ; RESTORE CURRENT HOST BYTE

         JMP     PRTFE

PRTFD:
         MOV     A,#0
         MOV     DPTR,#PRSSET
         MOVX    @DPTR,A
PRTFD1:
         MOV     A,#0
         MOV     DPTR,#SSFLAG
         MOVX    @DPTR,A

         MOV     DPTR,#HOST
         MOVX    @DPTR,A

PRTFE:

         CALL    CLRCGALS

         MOV     DPTR,#CPYCPY
         MOVX    A,@DPTR
         MOV     DPTR,#COPIES
         MOVX    @DPTR,A               ; RESTORE COPIES IF HOST CLOBBERED.


; ALL OF THE VALUES BELOW HERE NEED TO GET SET TO 0 BEFORE THE JMP TO MAINX
         MOV     A,#0

         MOV     DPTR,#DUPFLG
         MOVX    @DPTR,A               ; CLEAR DUPLICATE FLAG

         MOV     DPTR,#HDPFLG
         MOVX    @DPTR,A               ; CLEAR HOST MODE DUPLICATE FLAG

         MOV     DPTR,#SNDDUP
         MOVX    @DPTR,A               ; CLEAR SEND DUPLICATE TO HOST FLAG

         MOV     DPTR,#ACTIVE          ; CLEAR STATUS FLAGS
         MOVX    @DPTR,A
         MOV     DPTR,#PRTPRSD
         MOVX    @DPTR,A
         MOV     DPTR,#TIMEOUT
         MOVX    @DPTR,A
         MOV     DPTR,#TKTPND
         MOVX    @DPTR,A
         MOV     DPTR,#RETURN
         MOVX    @DPTR,A
         MOV     DPTR,#ESC
         MOVX    @DPTR,A

         MOV     DPTR,#TORFLG
         MOVX    @DPTR,A               ; CLEAR TIMER OVERIDE FLAG
                                       ; TRYING THIS HERE INSTEAD OF IN PRINTVOL

         MOV     DPTR,#KILLPIPE
         MOVX    @DPTR,A               ; RESET KILLPIPE

         MOV     DPTR,#DECMLADJ        ; CLEAR ROTATE FLAG
         MOVX    @DPTR,A

         MOV     DPTR,#TOTALADJ
         MOVX    @DPTR,A               ; CLEAR FLAG TOTALIZER UPDATE FOR POWFAIL

         CALL    CLRCMNDFLG            ; CLEAR ALL HOST 'i' COMMAND FLAGS

; E200 ONLY, SENDS DELIVERY DATA TO NVRAM
         CALL    TDATOUT

         JMP     MAINX
; *************************************************************

PRTHOST:
         MOV     DPTR,#CMDFLAG         ; BYTE0(0-9) = HOST SUPRESSING PRINTING OF NEXT METER TICKET
         MOVX    A,@DPTR
         CJNE    A,#'1',PRTH7
         MOV     A,#'4'                ; TELL HOST PRINTING SUPRESSED BY i COMMAND BYTE1
         CALL    OUTA
         JMP     PRTFX

PRTH7:   MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JZ      PRTHSTER1             ; BAIL IF NOT HOST MODE
         MOV     DPTR,#TKTPND
         MOVX    A,@DPTR
         JNZ     PRTH7A
         JMP     PRTHSTER4             ; BAIL IF NOT TICKET PENDING
PRTH7A:
         MOV     R0,#0                 ; NOTE: THE CODE THAT LOOKS FOR 'X'
         MOV     R1,#0                 ;  WILL NEVER BE CALLED IN PUMP & PRINT
PRTH3:   CALL    INPA
         JC      PRTH5
         DJNZ    R0,PRTH3
         DJNZ    R1,PRTH3
         JMP     PRTHSTER2             ; BAIL IF NO ARGUMENT IN 60ms
PRTH5:   CJNE    A,#1BH,PRTH1
         MOV     DPTR,#ESC
         MOV     A,#1
         MOVX    @DPTR,A               ; ESC = CANCEL OUTA HERE
         JMP     PRTH4
PRTH1:
         ANL     A,#0FH                ; CHANGE ASCII TO BCD
         MOV     DPTR,#XCOPIES
         MOVX    @DPTR,A

PRTH6:   CALL    CHKTKT                ; CALL #10: PRINT HOST DELVERY TICKET
         JC      PRTH2
         JMP     PRTHSTER3             ; BAIL IF NO TICKET
PRTH2:
         CALL    CON2HOST
         CALL    SELECTHST

         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#3,PRTH2ZY          ; 3=E141 EMULATION, ELSE SKIP
         JMP     PRTH2ZZ               ; 'X' NEEDS TO SKIP SENDING RETURN VALUE WHEN EMULATING E141
PRTH2ZY:
         MOV     A,#'1'                ; RETURN 1= GOING TO PRINT
         CALL    OUTA
PRTH2ZZ:
         JMP     PRTFINAL              ; CALL #5: PRTFINAL JUMPS TO MAIN
                                       ; I THINK THIS IS X CMD FROM HOST
                                       ; DON'T WORRY ABOUT SLSMODE - NEED TO CHECK !!!!

PRTHSTER1:
         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#3,PRTHSTER1ZY      ; 3=E141 EMULATION, ELSE SKIP
         JMP     PRTHSTER1ZZ           ; 'X' NEEDS TO SKIP SENDING RETURN VALUE WHEN EMULATING E141
PRTHSTER1ZY:
         MOV     A,#'2'                ; RETURN 2= IF NOT HOST MODE
         CALL    OUTA
PRTHSTER1ZZ:
         CALL    SENDPIPE
         JMP     PRTH4

PRTHSTER2:
         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#3,PRTHSTER2ZY      ; 3=E141 EMULATION, ELSE SKIP
         JMP     PRTHSTER2ZZ           ; 'X' NEEDS TO SKIP SENDING RETURN VALUE WHEN EMULATING E141
PRTHSTER2ZY:
         MOV     A,#'3'                ; RETURN 3= IF NO ARGUMENT IN 60ms
         CALL    OUTA
PRTHSTER2ZZ:
         CALL    SENDPIPE
         JMP     PRTH4

PRTHSTER3:
         CALL    CON2HOST
         CALL    SELECTHST

         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#3,PRTHSTER3ZY      ; 3=E141 EMULATION, ELSE SKIP
         JMP     PRTHSTER3ZZ           ; 'X' NEEDS TO SKIP SENDING RETURN VALUE WHEN EMULATING E141
PRTHSTER3ZY:
         MOV     A,#'0'                ; RETURN 0= PRINTER ERR OR NO PAPER
         CALL    OUTA
PRTHSTER3ZZ:
         CALL    SENDPIPE
         JMP     PRTH4

PRTHSTER4:
         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#3,PRTHSTER4ZY      ; 3=E141 EMULATION, ELSE SKIP
         JMP     PRTHSTER4ZZ           ; 'X' NEEDS TO SKIP SENDING RETURN VALUE WHEN EMULATING E141
PRTHSTER4ZY:
         MOV     A,#'4'                ; RETURN 4= NO TICKET PENDING
         CALL    OUTA
PRTHSTER4ZZ:
         CALL    SENDPIPE
         JMP     PRTH4

PRTH4:   JMP     MAINX


MFEEDWSKIP:
         MOV     R7,A
         CALL    CKSKPFNCMD            ; PRINT TRAILING LINES IF CMDFLG+3='0' OR CPYDCTR>1, SKIP IF CMDFLG+3='1' AND CPYDCTR=0, C WILL BE SET IF SHOULD PRINT
         JNC     MFEEDX
         MOV     A,R7
MFEED:   MOV     R7,#7
MFEED1:  CALL    PFEED
         DJNZ    R7,MFEED1
MFEEDX:  RET

MFED3WSKP:
         MOV     R7,A                  ; ADDED THIS TO DO ONLY 3 BLANK LINES, ZEBRA AT END OF TICKET
         CALL    CKSKPFNCMD            ; PRINT TRAILING LINES IF CMDFLG+3='0' OR CPYDCTR>1, SKIP IF CMDFLG+3='1' AND CPYDCTR=0, C WILL BE SET IF SHOULD PRINT
         JNC     MFEEDX
         MOV     A,R7
         MOV     R7,#3
         JMP     MFEED1

PRTCAL:  CALL    CLRLINES
         CALL    SAYPRNT
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#3,PRTCAL7
         JMP     PRTCAL8
PRTCAL7:
         CALL    CHKTKT                ; CALL #11: PRINT CAL TICKET
         JC      PRTCAL1

PRTCAL8: MOV     DPTR,#CALLED
         MOVX    A,@DPTR
         JZ      PRTCAL8A
         MOV     A,#0
         MOVX    @DPTR,A               ; IF CALLED BY 'd' THEN CLEAR AND
         JMP     M47X                  ; RETURN TO COMMAND ROUTINE

PRTCAL8A:
         ;CALL    DISCON               ; CALL #4: MAKE SURE NOT CONNECTED TO PRINTER AFTER CHK IN CALRPT, = NOT NEEDED

         MOV     DPTR,#EMULATEFLG      ; AFTER DISCON/UNLATCH NEED TO SEE IF SLSMODE TO RECON TO HOST
         MOVX    A,@DPTR
         CJNE    A,#2,PRTCAL8AXX       ; 2=PA21 SLSMODE

         CALL    CON2HOST              ; REMAKE CONNECTION TO HOST IN SLS MODE AFTER PRINT
         CALL    SELECTHST

PRTCAL8AXX:
         JMP     MAIN

PRTCAL1:
         ;CALL    DISCON                ; CALL #5: MAKE SURE NOT CONNECTED TO PRINTER AFTER CHK IN CALRPT, = NOT NEEDED
         MOV     DPTR,#ADJUST
         MOVX    A,@DPTR
         JZ      PRTCAL4
         CALL    BUMPCAL
PRTCAL4: MOV     DPTR,#ADJUST
         MOV     A,#0
         MOVX    @DPTR,A               ; CLEAR ADJUST FLAG
         MOV     DPTR,#CONFIG
         MOVX    A,@DPTR
         JZ      PRTCAL5
         CALL    BUMPCON
PRTCAL5: MOV     DPTR,#CONFIG
         MOV     A,#0
         MOVX    @DPTR,A

CALRPT:
         MOV     DPTR,#LASTPCOD
         MOV     A,PRCOD
         MOVX    @DPTR,A

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#13,CALRP00
         CALL    PFEED                 ; NEED ONE BLANK LINE AT TOP OF TICKET FOR DATAMAX M4TE
CALRP00:
         MOV     DPTR,#PRMSG13         ; CALIBRATION REPORT
         CALL    PRTCMSG

         CALL    PFEED

         MOV     DPTR,#HDRTYPFL        ; HEADER TYPE FLAG, 0=DELTICKET, 1=SHIFTTICKET, 2=CALTICKET
         MOV     A,#2
         MOVX    @DPTR,A
         CALL    DOPRTHDR

         CALL    PRTTOTFONLY           ; PURPOSEFULLY ONLY PRINTING FINAL ON CAL

         CALL    PFEED

         MOV     DPTR,#PRMSG23         ; METEROLOGICAL VERNO
         CALL    PRTCMSG
         MOV     DPTR,#PRMSG22         ; USER SOFTWARE VERSION
         CALL    PRTCMSG

         CALL    PRTSER                ; ECOUNT SERIAL NUMBER
         MOV     DPTR,#SERPRT
         MOV     A,#1
         MOVX    @DPTR,A
         CALL    PRTVOL                ; CALL 2
         MOV     DPTR,#SERPRT          ; FLAG PRTVOL FOR ONLY SALE#
         MOV     A,#0
         MOVX    @DPTR,A

         JMP     PRINTPRTR
PRINTPRTRDX:
         CALL    PRTDIV                ; METER RATIO






         CALL    BLAST

         MOV     DPTR,#MSG123          ; TMPSET
         CALL    OUTMSG
         MOV     DPTR,#DPOTSET
         MOVX    A,@DPTR
         CALL    MAKEASC
         CALL    OUTA
         MOV     A,B
         CALL    OUTA
         MOV     A,#' '
         CALL    OUTA

         MOV     DPTR,#MSG233          ; PLSREV
         CALL    OUTMSG
         MOV     DPTR,#EMPLSREV
         MOVX    A,@DPTR
         JNZ     PLSR00A
         MOV     DPTR,#MSG239          ; 0=256
         JMP     PLSR00B
PLSR00A: MOV     DPTR,#MSG238          ; 1=100
PLSR00B: CALL    OUTMSG

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG50           ; STAGE 1
         CALL    OUTMSG
         MOV     A,S1SHOFF
         CALL    MAKEASC
         CALL    OUTA
         MOV     A,B
         CALL    OUTA
         MOV     A,#' '
         CALL    OUTA

         MOV     DPTR,#MSG234          ; CHANNL
         CALL    OUTMSG
         MOV     DPTR,#EMCHANNL
         MOVX    A,@DPTR
         JNZ     CHAN00A
         MOV     DPTR,#MSG240          ; 0=DUAL
         JMP     CHAN00B
CHAN00A: MOV     DPTR,#MSG241          ; 1=SINGLE
CHAN00B: CALL    OUTMSG

         CALL    PLINEEND


;CALR7:

; EC: ECOUNT SUPPORTS ONLY SOFTWARE AUTHORIZATION AS OF E180
         CALL    BLAST

         MOV     DPTR,#MSG213           ; SWAUTHFLG = SOFTWARE AUTHORIZATION
         CALL    OUTMSG
         MOV     DPTR,#SWAUTHFLG
         CALL    OUTASC

         MOV     DPTR,#MSG235          ; FLODIR
         CALL    OUTMSG
         MOV     DPTR,#EMFLODIR
         MOVX    A,@DPTR
         JNZ     FLOD00A
         MOV     DPTR,#MSG236          ; 0=CW
         JMP     FLOD00B
FLOD00A: MOV     DPTR,#MSG237          ; 1=CCW
FLOD00B: CALL    OUTMSG

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG121        ; 6501PF
         CALL    OUTMSG
         MOV     DPTR,#PR65FLG
         CALL    OUTASC

         MOV     DPTR,#MSG90
         CALL    OUTMSG
         MOV     DPTR,#MONEYFLG   ; PRICING ENABLED: 0=OFF,1=PRICE CHECK,2=ALWAYS ON
         MOVX    A,@DPTR
         CJNE    A,#0,CALR7A
         MOV     DPTR,#MSG24      ; OFF
         JMP     CALR7C
CALR7A:  CJNE    A,#1,CALR7B
         MOV     DPTR,#MSG139     ; PRICE CHECK
         JMP     CALR7C
CALR7B:  MOV     DPTR,#MSG137     ; ALL
CALR7C:  CALL    OUTMSG

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG71
         CALL    OUTMSG
         MOV     DPTR,#COPIES          ; #COPIES FOR THERMAL ROLL PRINTERS
         CALL    OUTASC

         MOV     DPTR,#MSG101
         CALL    OUTMSG
         MOV     DPTR,#HOSEPKFLG       ; HOSE PACK (1=ON/0=OFF) >> EC ONLY
         CALL    OUTASC

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG94
         CALL    OUTMSG
         MOV     DPTR,#PSRQD
         CALL    OUTASC

         MOV     DPTR,#MSG117
         CALL    OUTMSG
         MOV     DPTR,#SSRSTFLG        ; S/S RESET ENABLED: 0=OFF,1=ON,2=NSSOHM:NO S/S TO OPEN VALVES DURING HOST MODE DELIVERY
         MOVX    A,@DPTR
         CJNE    A,#0,CALR7D
         MOV     DPTR,#MSG23           ; ON
         JMP     CALR7F
CALR7D:  CJNE    A,#1,CALR7E
         MOV     DPTR,#MSG24           ; OFF
         JMP     CALR7F
CALR7E:  MOV     DPTR,#MSG141          ; NSSOHM
CALR7F:  CALL    OUTMSG
         MOV     DPTR,#CMDFLAG+7       ; BYTE7(0-9) = HOST OVERRIDE SSRST SETTING
         MOVX    A,@DPTR
         CJNE    A,#'0',CALR7F01       ; E179_A: SEE IF SSRST OVERRIDE FROM HOST
         JMP     CALR7F1               ; ITS 0 = NOT OVERRIDEN, IGNORE FOR REPORT
CALR7F01:CALL    OUTA

CALR7F1: CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG73
         CALL    OUTMSG
         MOV     DPTR,#REGNUM
         MOVX    A,@DPTR
         CALL    BIN2BCD
         CALL    OUTASC

         MOV     DPTR,#MSG61           ; TIMER SETTING (CAL MODE)
         CALL    OUTMSG
         MOV     DPTR,#ETFLG
         MOVX    A,@DPTR
         CJNE    A,#0,CAL7K1
         MOV     DPTR,#MSG24           ; 0: 24=OFF
         JMP     CAL7J
CAL7K1:  CJNE    A,#1,CAL7K2
         MOV     DPTR,#MSG222          ; 1: 222=3MN FL
         JMP     CAL7J
CAL7K2:  CJNE    A,#2,CAL7K3
         MOV     DPTR,#MSG223          ; 2: 223=3MN NF
         JMP     CAL7J
CAL7K3:  CJNE    A,#3,CAL7K4
         MOV     DPTR,#MSG224          ; 3: 224=30S FL
         JMP     CAL7J
CAL7K4:  MOV     DPTR,#MSG225          ; 4: 225=30S NF
CAL7J:   CALL    OUTMSG

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG96
         CALL    OUTMSG
         MOV     DPTR,#AIRFLG
         CALL    OUTASC

         MOV     DPTR,#MSG88           ; PGROSS
         CALL    OUTMSG
         MOV     DPTR,#GRSFLG          ; GRSFLG: 0=OFF, 1=ON, 2=NOTEMP (PRINT ALL EXCEPT TEMP), 3=TEMP(TEMP ONLY)
         MOVX    A,@DPTR
         CJNE    A,#0,CALR7G
         MOV     DPTR,#MSG24           ; OFF
         JMP     CALR7I
CALR7G:  CJNE    A,#1,CALR7G1
         MOV     DPTR,#MSG23           ; ON
         JMP     CALR7I
CALR7G1: CJNE    A,#2,CALR7H
         MOV     DPTR,#MSG144          ; NOTEMP
         JMP     CALR7I
CALR7H:  MOV     DPTR,#MSG154          ; TEMP
CALR7I:  CALL    OUTMSG

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG92
         CALL    OUTMSG
         MOV     DPTR,#DECIMAL
         MOVX    A,@DPTR
         JZ      DECML1
         MOV     A,#0
         JMP     DECML2
DECML1:  MOV     A,#1
DECML2:  CALL    OUTASC1

         MOV     DPTR,#MSG208          ; DEMO
         CALL    OUTMSG
         MOV     DPTR,#DEMO
         MOVX    A,@DPTR
         JNZ     CALR00A
         MOV     DPTR,#MSG24           ; OFF
         JMP     CALR00B
CALR00A: MOV     DPTR,#MSG23           ; ON
CALR00B: CALL    OUTMSG

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG97
         CALL    OUTMSG
         MOV     DPTR,#PROBE
         CALL    OUTASC

         MOV     DPTR,#MSG134          ; HOSTFIX
         CALL    OUTMSG
         MOV     DPTR,#HOSTFIX
         MOVX    A,@DPTR
         JNZ     CALR0A
         MOV     DPTR,#MSG24           ; OFF
         JMP     CALR0C
CALR0A:  CJNE    A,#01,CALR0B
         MOV     DPTR,#MSG135          ; MATRIX
         JMP     CALR0C
CALR0B:  MOV     DPTR,#MSG137          ; ALL
CALR0C:  CALL    OUTMSG

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG150          ; BRKVLV ADDED IN 178, LT411_REV3
         CALL    OUTMSG
         MOV     DPTR,#BROKNVLV
         CALL    OUTASC

; PPBTNENBL: FLAG FOR PRESET+PRINT BUTTONS ENABLED: 0=BOTH(00), 1=PRINT ONLY(01), 2=PRESET ONLY(10), 3=NEITHER(11)
;E300EA - switched logic so 0 = enabled so defaults work better JC
         MOV     DPTR,#MSG214          ; 'KEYSON'
         CALL    OUTMSG
         MOV     DPTR,#PPBTNENBL
         MOVX    A,@DPTR
         CJNE    A,#0,CALRX99A
         MOV     DPTR,#MSG215          ; BOTH ENABLED, DEFAULT (BOTHON)
         JMP     CALRX99Z
CALRX99A:CJNE    A,#1,CALRX99B
         MOV     DPTR,#MSG218          ; PRINT ONLY (NOPRST)
         JMP     CALRX99Z
CALRX99B:CJNE    A,#2,CALRX99C
         MOV     DPTR,#MSG217          ; PRESET ONLY (NOPRNT)
         JMP     CALRX99Z
CALRX99C:MOV     DPTR,#MSG216          ; NEITHER (BTHOFF)
CALRX99Z:CALL    OUTMSG

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG111
         CALL    OUTMSG
         MOV     DPTR,#UTYPE
         CALL    OUTASC

         MOV     DPTR,#MSG226          ; EMULATION
         CALL    OUTMSG
         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         JNZ     CALR70A
         MOV     DPTR,#MSG24           ; OFF
         JMP     CALR70Z
CALR70A: CJNE    A,#1,CALR70B
         MOV     DPTR,#MSG230          ; TMPOFF
         JMP     CALR70Z
CALR70B: CJNE    A,#2,CALR70C
         MOV     DPTR,#MSG227          ; PA21
         JMP     CALR70Z
CALR70C: CJNE    A,#3,CALR70D
         MOV     DPTR,#MSG232          ; E141E
         JMP     CALR70Z
CALR70D: MOV     DPTR,#MSG45           ; NOGOOD = UNKNOWN
CALR70Z: CALL    OUTMSG

         CALL    PLINEEND



         CALL    BLAST

         MOV     DPTR,#MSG56
         CALL    OUTMSG
         MOV     DPTR,#UNITS
         CALL    OUTASC

         MOV     DPTR,#MSG160          ; TRNCAT
         CALL    OUTMSG
;         MOV     DPTR,#DEMO
         MOV     DPTR,#TRUNC
         MOVX    A,@DPTR
         JNZ     CALRX0A
         MOV     DPTR,#MSG24           ; OFF
         JMP     CALRX0B
CALRX0A: MOV     DPTR,#MSG23           ; ON
CALRX0B: CALL    OUTMSG

         CALL    PLINEEND

         MOV     DPTR,#MSG229          ; QOB MD
         CALL    OUTMSG
;         MOV     DPTR,#DEMO
         MOV     DPTR,#QOBMODE
         MOVX    A,@DPTR
         JNZ     CALRZ0A
         MOV     DPTR,#MSG24           ; OFF
         JMP     CALRZ0B
CALRZ0A: MOV     DPTR,#MSG23           ; ON
CALRZ0B: CALL    OUTMSG

         CALL    PLINEEND



         CALL    PFEED


         CALL    PRTCNO                ; CAL/ADJUST AUDIT NUMBER

         CALL    PRTCON                ; CONFIGURATION AUDIT NUMBER



         CALL    PFEED


PRTCOD:
         MOV     DPTR,#PRTCTR
         MOV     A,#4                  ; START WITH NEW TICKET
         MOVX    @DPTR,A               ; CLEAR PRINT COUNTER

         MOV     PRCOD,#1              ; ONLY 4 PER TICKET

PRTCOD2:
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#0,PRTCOD8
         JMP     PRTCOD8A
PRTCOD8: CJNE    A,#1,PRTCOD4

PRTCOD8A:MOV     DPTR,#PRTCTR
         MOVX    A,@DPTR
         CJNE    A,#4,PRTCOD4
PRTCOD6:
         CALL    LODHCMDFLG
         JNZ     PRTCOD4

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#0,CHKT14A

CHKTK14X:
         MOV     DPTR,#MSG129          ; MIDCOM IMPACT - FORCE TICKET REMOVAL
         CALL    SHOW
         MOV     DPTR,#MSG40
         CALL    SHOW
         CALL    CHKTKT                ; CALL #12: NEED TICKET REMOVED
         JC      CHKTK14X              ; STAY IN LOOP WHILE TICKET DETECTED
         CALL    WAIT2000

CHKTK14Y:
         CALL    CHKTKT                ; CALL #13: NEED NEXT TICKET
         JNC     CHKTK14Y              ; STAY IN LOOP WHILE TICKET NOT DETECTED
         CALL    SAYPRNT
         CALL    WAIT4000
         JMP     CHKT14C

CHKT14A:
         CALL    PR295FF               ; EPSON TM-295 FORM FEED
                                       ;  IN ORDER TO FORCE NEW SHEET
CHKT14B: CALL    CHKTKT                ; CALL #14: WAIT FOR TICKET
         JNC     CHKT14B               ; STAY IN LOOP UNTIL TICKET DETECTED

CHKT14C: CALL    SAYPRNT
         CALL    SELECTP2

         MOV     DPTR,#PRTCTR
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET COUNTER

PRTCOD4: CALL    GETCAL
         MOV     A,#0
         ORL     A,SWARRAY
         ORL     A,SWARRAY+1
         ORL     A,SWARRAY+2
         JNZ     PRTCOD1

PRTCOD3: INC     PRCOD
         MOV     A,PRCOD
         CJNE    A,#100,PRTCOD2
         JMP     PRTCAL3               ; GET OUT TOTALLY

PRTCOD1: CALL    PRTPRC                ; "PRODUCT # XX" ACTUAL PRCOD
         CALL    PCMPTR                ; GET PRODUCT MESSAGE POINTER
         CALL    PRTMSG
         CALL    PRTFCTR
         CALL    PRTDWL

         CALL    SETCMP                ; GET CURRENT COMP STATUS
         MOV     A,COMP
         JZ      PRTCAL2
         MOV     DPTR,#PRMSG18
         JMP     PRTCAL9
PRTCAL2: MOV     DPTR,#PRMSG19
PRTCAL9: CALL    PRTCMSG
         MOV     A,COMP
         JZ      PRTCOD7               ; SKIP COMP MESSAGES IF OFF
         MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         CJNE    A,#0,PRTCOD7
         CALL    PRTTBL                ; PRINT COMP TABLE NUMBER
         MOV     DPTR,#UNITS           ; CHECK LITRES OR GALLONS
         MOVX    A,@DPTR               ; ONLY PRINT TABLE INFO IF LITRES
         JZ      PRTCOD7               ; THIS IS TO PREVENT ASTM54 / TABLE 6 COMPLAINTS
                                       ;  BETTER TO PRINT NOTHING THAN PRINT 'WRONG' IF GALLONS
         CALL    PCTPTR                ; GET TABLE INFO - LINE 1
         CALL    PRTCMSG                ; PRINT TABLE INFO - LINE 1
         CALL    PCTPTR                ; GET TABLE INFO - LINE 1
         MOV     R1,#25
PRTCAL9A:INC     DPTR                  ; MOVE OVER 25 CHARS FOR LINE 2
         DJNZ    R1,PRTCAL9A
         CALL    PRTCMSG                ; PRINT TABLE INFO - LINE 2

PRTCOD7:
         CALL    PFEED

         MOV     DPTR,#PRTCTR
         MOVX    A,@DPTR
         INC     A                     ; BUMP PRINT COUNTER
         MOVX    @DPTR,A
         JMP     PRTCOD3




PRINTPRTR:
; E179A: MOVED WHERE PRINTER PRINTS TO ALIGN BETTER
         CALL    BLAST
         MOV     DPTR,#PRMSG57         ; PRINTER MESSAGE
         MOV     R1,#7
CALR1:   CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,CALR1
         MOV     A,#' '                ; EXTRA SPACES TO LINE UP BETTER
         MOV     R1,#12
CALR1A0: CALL    OUTA
         DJNZ    R1,CALR1A0

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#0,CALR8
         MOV     DPTR,#MSG9
         JMP     PRTRDN
CALR8:   CJNE    A,#1,CALR2
         MOV     DPTR,#MSG65
         JMP     PRTRDN
CALR2:   CJNE    A,#2,CALR3
         MOV     DPTR,#MSG66
         JMP     PRTRDN
CALR3:   CJNE    A,#3,CALR4
         MOV     DPTR,#MSG67
         JMP     PRTRDN
CALR4:   CJNE    A,#4,CALR5
         MOV     DPTR,#MSG113          ; 4 = ZEBRA RW-420 V1 = NO BMFF
         JMP     PRTRDN
CALR5:   CJNE    A,#5,CALR6
         MOV     DPTR,#MSG118
         JMP     PRTRDN
CALR6:   CJNE    A,#6,CALR9
         MOV     DPTR,#MSG122
         JMP     PRTRDN
CALR9:   CJNE    A,#7,CALR11
         MOV     DPTR,#MSG128
         JMP     PRTRDN
CALR11:  CJNE    A,#8,CALR12
         MOV     DPTR,#MSG130
         JMP     PRTRDN
CALR12:  CJNE    A,#9,CALR13
         MOV     DPTR,#MSG133
         JMP     PRTRDN
CALR13:  CJNE    A,#10,CALR14
         MOV     DPTR,#MSG136
         JMP     PRTRDN
CALR14:  CJNE    A,#11,CALR15
         MOV     DPTR,#MSG146          ; 11 = BLASTER + BLACK MARK ENABLED
         JMP     PRTRDN
CALR15:  CJNE    A,#12,CALR16
         MOV     DPTR,#MSG147          ; 12 = PRINTEK RT43BT
         JMP     PRTRDN
CALR16:  CJNE    A,#13,CALR17
         MOV     DPTR,#MSG148          ; 13 = DATAMAX M4TE
         JMP     PRTRDN
CALR17:  CJNE    A,#14,CALR18
         MOV     DPTR,#MSG149          ; 14 = ZEBRA RW-420 V2 = DO BMFF
         JMP     PRTRDN
CALR18:  CJNE    A,#15,CALR19
         MOV     DPTR,#MSG157          ; 15 = CUSTOM Q3 = DO BMFF+CUT
         JMP     PRTRDN
CALR19:  CJNE    A,#16,CALRDNB
         MOV     DPTR,#MSG158          ; 16 = STAR TSP700II = DO LOGO+BMFF
         JMP     PRTRDN

PRTRDN:  MOV     R1,#6
CALRDN:  CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,CALRDN
CALRDNB: CALL    PLINEEND
         JMP     PRINTPRTRDX





; *******************************************

PRTCAL3:
// MPC removed E300 so skipp all MPC printing and jump to PRTDEV5

;PRTDEV:
;         MOV     DPTR,#MPCFLG
;         MOVX    A,@DPTR
;         CJNE    A,#0,PRTDEV6
;         MOV     DPTR,#PRMSG101
;         CALL    PRTCMSG
;         CALL    CLRLINE
;         JMP     PRTDEV5            ; MPC IS OFF. SAY SO AND GET OUT.

;PRTDEV6: MOV     DPTR,#PRMSG100     ; MPC BE ON MAN
;         CALL    PRTCMSG
;         CALL    CLRLINE

// MULTI-POINT CALIBRATION PRINT FORMAT # FR:####[FR IN HEX] DEV:0.#### (DEVIATION)
;         MOV     DPTR,#CALPT1
;         MOV     TEMP,#'0'
;         CALL    PRTDEV3A			 ;PRINT # FR:####
;         MOV     DPTR,#CALPT1
;		 CALL	 PRTDEV3B         	 ;PRINT [##] (FLOWRATE IN HEX)
;         MOV     DPTR,#CALPT1+1
;         CALL    PRTDEV3C			 ;PRINT DEV:+/-
;         MOV     DPTR,#CALPT1+3
;         CALL    PRTDEV4			 ;PRINT 0.#### (DEVIATION)

;         MOV     DPTR,#CALPT2
;         MOV     TEMP,#'1'
;         CALL    PRTDEV3A
;         MOV     DPTR,#CALPT2
;		 CALL	 PRTDEV3B
;         MOV     DPTR,#CALPT2+1
;         CALL    PRTDEV3C
;         MOV     DPTR,#CALPT2+3
;         CALL    PRTDEV4

;         MOV     DPTR,#CALPT3
;         MOV     TEMP,#'2'
;         CALL    PRTDEV3A
;         MOV     DPTR,#CALPT3
;		 CALL	 PRTDEV3B
;         MOV     DPTR,#CALPT3+1
;         CALL    PRTDEV3C
;         MOV     DPTR,#CALPT3+3
;         CALL    PRTDEV4

;         MOV     DPTR,#CALPT4
;         MOV     TEMP,#'3'
;         CALL    PRTDEV3A
;         MOV     DPTR,#CALPT4
;		 CALL	 PRTDEV3B
;         MOV     DPTR,#CALPT4+1
;         CALL    PRTDEV3C
;         MOV     DPTR,#CALPT4+3
;         CALL    PRTDEV4

;         MOV     DPTR,#CALPT5
;         MOV     TEMP,#'4'
;         CALL    PRTDEV3A
;         MOV     DPTR,#CALPT5
;		 CALL	 PRTDEV3B
;         MOV     DPTR,#CALPT5+1
;         CALL    PRTDEV3C
;         MOV     DPTR,#CALPT5+3
;         CALL    PRTDEV4

;         MOV     DPTR,#CALPT6
;         MOV     TEMP,#'5'
;         CALL    PRTDEV3A
;         MOV     DPTR,#CALPT6
;		 CALL	 PRTDEV3B
;         MOV     DPTR,#CALPT6+1
;         CALL    PRTDEV3C
;         MOV     DPTR,#CALPT6+3
;         CALL    PRTDEV4

;         MOV     DPTR,#CALPT7
;         MOV     TEMP,#'6'
;         CALL    PRTDEV3A
;         MOV     DPTR,#CALPT7
;		 CALL	 PRTDEV3B
;         MOV     DPTR,#CALPT7+1
;         CALL    PRTDEV3C
;         MOV     DPTR,#CALPT7+3
;         CALL    PRTDEV4

;         MOV     DPTR,#CALPT8
;         MOV     TEMP,#'7'
;         CALL    PRTDEV3A
;         MOV     DPTR,#CALPT8
;		 CALL	 PRTDEV3B
;         MOV     DPTR,#CALPT8+1
;         CALL    PRTDEV3C
;         MOV     DPTR,#CALPT8+3
;         CALL    PRTDEV4

;         MOV     DPTR,#CALPT9
;         MOV     TEMP,#'8'
;         CALL    PRTDEV3A
;         MOV     DPTR,#CALPT9
;		 CALL	 PRTDEV3B
;         MOV     DPTR,#CALPT9+1
;         CALL    PRTDEV3C
;         MOV     DPTR,#CALPT9+3
;         CALL    PRTDEV4

;         MOV     DPTR,#CALPT10
;         MOV     TEMP,#'9'
;         CALL    PRTDEV3A
;         MOV     DPTR,#CALPT10
;		 CALL	 PRTDEV3B
;         MOV     DPTR,#CALPT10+1
;         CALL    PRTDEV3C
;         MOV     DPTR,#CALPT10+3
;         CALL    PRTDEV4

;         JMP     PRTDEV5


;PRTDEV4: CALL    ASCBLD
;         CALL    DASCBLD
;         CALL    PRTLINE
;         RET

;PRTDEV3A: MOV     R0,#ECGALS
;         MOV     @R0,TEMP
;         INC     R0
;         MOV     @R0,#' '              ; SPACE
;         INC     R0
;		 MOV     @R0,#'F'              ; FR:
;         INC	 R0
;		 MOV     @R0,#'R'              ;
;         INC	 R0
;		 MOV     @R0,#':'              ;
;         INC	 R0
;		 CALL	 ASCBLD					;Display flow rate in ascii
;		 RET

;PRTDEV3B:
;		 MOV     @R0,#'['              ; [
;         INC	 R0
;         MOVX    A,@DPTR               ; FLOW RATE, 00-FF
;         CALL    MAKEASC
;         MOV     @R0,A
;         INC     R0
;         MOV     A,B
;         MOV     @R0,A
;         INC     R0
;		 MOV     @R0,#']'              ; ]
;         INC	 R0
;         MOV     @R0,#' '              ; SPACE
;         INC     R0
;         RET


;PRTDEV3C:
;		 MOV     @R0,#'D'              ; DEV:
;         INC	 R0
;		 MOV     @R0,#'E'              ;
;         INC	 R0
;		 MOV     @R0,#'V'              ;
;         INC	 R0
;		 MOV     @R0,#':'              ;
;         INC	 R0
;         MOVX    A,@DPTR
;         CJNE    A,#0,PRTDEV1          ; +/-
;         MOV     @R0,#'-'
;         JMP     PRTDEV2
;PRTDEV1: MOV     @R0,#'+'
;PRTDEV2: INC     R0
;         MOV     @R0,#'0'              ; 0
;         INC     R0
;         MOV     @R0,#'.'              ; .
;         INC     R0
;         RET



PRTDEV5: MOV     DPTR,#PRMSG52         ; END REPORT MSG
         CALL    PRTCMSG

         CALL    LODHCMDFLG
         JNZ     PRTCAL10              ; NO CUT COMMAND FOR ISHSTCMD

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR

         CJNE    A,#0,PRTCAL6A         ; 0 = MIDCOM IMPACT
         JMP     PRTCAL6

PRTCAL6A:CJNE    A,#1,PRTCAL6B         ; 1 = EPSON TM-295
         CALL    PR295FF
         JMP     PRTCAL6

PRTCAL6B:CJNE    A,#4,PRTCAL6C
         CALL    DO3BLNKLNS            ; PRINT 3 BLANK LINES FOR ZEBRA RW-420 V1 = NO BMFF
         JMP     PRTCAL6

PRTCAL6C:CJNE    A,#5,PRTCAL6D
         CALL    DO3BLNKLNS            ; PRINT 3 BLANK LINES FOR BLASTER V1
         JMP     PRTCAL6

PRTCAL6D:CJNE    A,#7,PRTCAL6E
         CALL    PR460BMF              ; FUJITSU FP-460 BLACK MARK FF, DOES 1 SEC WAIT
         JMP     PRTCAL6

PRTCAL6E:CJNE    A,#11,PRTCAL6F
         CALL    PRBLSTBMFF            ; BLASTER V2 BLACK MARK FF
         JMP     PRTCAL6

PRTCAL6F:CJNE    A,#12,PRTCAL6G
         CALL    MFEED                 ; DO MFEED BUT NO CUT FOR PRINTEK RT43BT
         JMP     PRTCAL6

PRTCAL6G:CJNE    A,#13,PRTCAL6H
         CALL    PFEED                 ; PRINT 2 BLANK LINES FOR DATAMAX M4TE
         CALL    PFEED
         JMP     PRTCAL6

PRTCAL6H:CJNE    A,#14,PRTCAL6I
         CALL    PR295FF               ; 14 = RW-420 BMFF USES SAME FF CMD AS TM-295
         JMP     PRTCAL6               ; SKIP LFEEDS AND CUT

PRTCAL6I:CJNE    A,#15,PRTCAL6J
         CALL    PRQ3BMFF              ; 15 = CUSTOM Q3 BMFF
         JMP     PRTCAL6Z1             ; NEED CUT, ALREADY FF

PRTCAL6J:CJNE    A,#16,PRTCAL6Z
         CALL    PR295FF               ; 16 = STAR TSP700II BMFF
         JMP     PRTCAL6Z1             ; NEED CUT, ALREADY FF

PRTCAL6Z:                              ; ALL THE PRINTERS THAT NEED TO CALL MFEED WITH A CUT END UP HERE, EVERYTHING ELSE CAUGHT ABOVE
         CALL    MFEED                 ; 2=THERMAL, 6=TM-220, 8=CTS310, 9=CTS651, 10=TM88, 15=CUSTOM Q3, 16=STAR TSP700II
PRTCAL6Z1:
         CALL    THRMPRTCUT            ; Q3 AND TSP700/FVP10 NEED A CUT

PRTCAL6: CALL    DISCON                ; CALL #6: END OF PRINT CAL

         MOV     DPTR,#EMULATEFLG      ; AFTER DISCON/UNLATCH NEED TO SEE IF SLSMODE TO RECON TO HOST
         MOVX    A,@DPTR
         CJNE    A,#2,PRTCAL6XX        ; 2=PA21 SLSMODE

         CALL    CON2HOST              ; REMAKE CONNECTION TO HOST IN SLS MODE AFTER PRINT
         CALL    SELECTHST

PRTCAL6XX:
PRTCAL10:
         MOV     DPTR,#LASTPCOD
         MOVX    A,@DPTR
         MOV     PRCOD,A

         MOV     R0,#CGALS
         MOV     R1,#5
         MOV     A,#0
PRTCALX: MOV     @R0,A
         INC     R0
         DJNZ    R1,PRTCALX            ; NEED CLEARED FOR HOST

         CALL    LODHCMDFLG
         JZ      PRTCALY
         JMP     MAIN61X               ; RETURN TO FINISH CALRPT COMMAND

PRTCALY: MOV     DPTR,#CALLED
         MOVX    A,@DPTR
         JZ      PRCALN
         MOV     A,#0
         MOVX    @DPTR,A               ; IF CALLED BY 'd' THEN CLEAR AND
         JMP     M47X                  ; RETURN TO COMMAND ROUTINE
PRCALN:  JMP     MAIN


OUTMSG:  MOV     R1,#6
OUTMSG1: CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,OUTMSG1
         MOV     A,#' '
         CALL    OUTA
         RET

OUTASC:  MOVX    A,@DPTR
OUTASC1: ORL     A,#30H
         CALL    OUTA
         MOV     A,#' '
         CALL    OUTA
         CALL    OUTA
         RET


PRTDIV:
         MOV     DPTR,#PRMSG16         ; DIVISOR
         CALL    MOVMES
         MOV     DPTR,#DIVISOR
         MOVX    A,@DPTR
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     A,TEMP
         CALL    MAKEASC
         MOV     R0,#ECGALS+23         ; DON'T GET FOOLED
         MOV     @R0,A                 ; MAKESASC MESSES WITH R0
         MOV     A,B
         INC     R0
         MOV     @R0,A
         CALL    PRTLINE
         RET

; PRINTS "PRODUCT # XX"
PRTPRC:
         MOV     DPTR,#PRMSG30         ; PRODUCT CODE
         CALL    MOVMES
         MOV     A,PRCOD
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     A,TEMP
         CALL    MAKEASC
         MOV     R0,#ECGALS+10         ; DON'T GET FOOLED
         MOV     @R0,A                 ; MAKESASC MESSES WITH R0
         MOV     A,B
         INC     R0
         MOV     @R0,A
         CALL    PRTLINE
         RET

; PRINTS COMP TABLE #
PRTTBL:
         MOV     DPTR,#PRMSG31         ; COMP TABLE #
         CALL    MOVMES
         CALL    CMPPTR                ; POINT TO COMP#
         MOVX    A,@DPTR               ; GETIT
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     A,TEMP
         CALL    MAKEASC
         MOV     R0,#ECGALS+23         ; DON'T GET FOOLED
         MOV     @R0,A                 ; MAKESASC MESSES WITH R0
         MOV     A,B
         INC     R0
         MOV     @R0,A
         CALL    PRTLINE
         RET

; ************************************************************************
; PRTCOPYWSKP PRINT THE COPY NUMBER ON THE BOTTOM OF TICKETS FOR PRINTERS
;  THAT PRINT MULTIPLE COPIES
; SKIPS PRINTING IF DOING A HOST PASS-THROUGH PRINT 'FINISH'
; ALSO SKIPS PRINTING IF HOST-MODE PRINT PARAMETER[+4]='1'
PRTCOPYWSKP:
         CALL    LODHSTLNFLG           ; IF PASS-THROUGH-PRINT (NON-ZERO), SKIP
         CJNE    A,#0,PRTCPX

         MOV     DPTR,#CMDFLAG+4       ; BYTE4(0-9) = SKIP COPY# IF HOST PARAMETER[+4] IS NOT '0'
         MOVX    A,@DPTR
         CJNE    A,#'0',PRTCPX

         MOV     DPTR,#PRMSG29         ; OTHERWISE, PRINT COPY #
         CALL    MOVMES
         MOV     DPTR,#CPYUCTR
         MOVX    A,@DPTR
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     A,TEMP
         CALL    MAKEASC
         MOV     R0,#ECGALS+7          ; DON'T GET FOOLED
         MOV     @R0,A                 ; MAKESASC MESSES WITH R0
         MOV     A,B
         INC     R0
         MOV     @R0,A
         CALL    PRTLINE
PRTCPX:  RET

PRTSER:  MOV     DPTR,#PRMSG12         ; SERIAL NUMBER
         CALL    MOVMES
         MOV     DPTR,#SERNUM+2
         MOV     R0,#ECGALS+19
         CALL    ASCBLD
         CALL    DASCBLD
         CALL    DASCBLD
         CALL    PRTLINE
         RET

PRTFCTR: CALL    GETCAL                ; SWARRAY GETS CLOBBERED IN PRICING
         MOV     DPTR,#PRMSG17         ; MUST RESTORE.
         CALL    MOVMES
         MOV     R1,#SWARRAY           ; SWARRAY IS NOT IN LOGICAL ORDER
         MOV     R0,#ECGALS+19
         MOV     A,@R1
         CALL    MAKEASC
         MOV     A,B
         MOV     @R0,A
         INC     R0
         MOV     @R0,#'.'
         INC     R0
         MOV     R1,#SWARRAY+2
         MOV     A,@R1
         CALL    MAKEASC
         MOV     @R0,A
         INC     R0
         MOV     A,B
         MOV     @R0,A
         INC     R0
         MOV     R1,#SWARRAY+1
         MOV     A,@R1
         CALL    MAKEASC
         MOV     @R0,A
         INC     R0
         MOV     A,B
         MOV     @R0,A
         CALL    PRTLINE
         RET

PRTDWL:  CALL    GETDWL                ; SWARRAY GETS CLOBBERED IN PRICING
         MOV     DPTR,#PRMSG17A        ; MUST RESTORE.
         CALL    MOVMES
         MOV     R1,#S2DWELL+1
         MOV     R0,#ECGALS+20
         MOV     A,@R1
         CALL    MAKEASC
         MOV     @R0,A
         INC     R0
         MOV     A,B
         MOV     @R0,A
         INC     R0
         MOV     R1,#S2DWELL
         MOV     A,@R1
         CALL    MAKEASC
         MOV     @R0,A
         INC     R0
         MOV     @R0,#'.'
         INC     R0
         MOV     A,B
         MOV     @R0,A
         CALL    PRTLINE
         RET

PRTCNO:  MOV     DPTR,#PRMSG25         ; ADJUST NUMBER
         CALL    MOVMES
         MOV     DPTR,#CALNO+2
         MOV     R0,#ECGALS+19
         CALL    ASCBLD
         CALL    DASCBLD
         CALL    DASCBLD
         CALL    PRTLINE
         RET

PRTCON:  MOV     DPTR,#PRMSG24         ; CONFIG NUMBER
         CALL    MOVMES
         MOV     DPTR,#CONNO+2
         MOV     R0,#ECGALS+19
         CALL    ASCBLD
         CALL    DASCBLD
         CALL    DASCBLD
         CALL    PRTLINE
         RET


; ********************************************************
; USED TO DETERMINE IF THE HOST HAS SUPRESSED THE FINAL
;  TICKET'S LINEFEEDS/BLACKMARKFF
; ********************************************************
CKSKPFNCMD:                            ; SET C ON IF CMDFLG+3 = '0' = DO FINAL LINEFEEDS/BMFF
         CLR     C                     ; IF C IS ON WHEN RETURNED THEN DO THE FINAL LINEFEEDS OR BMFF
         MOV     DPTR,#CPYDCTR         ; IF CPYDCTR==1 THEN WE JUST PRINTED TEXT FOR LAST COPY, NEED TO CHECK AND SEE IF NEED TO SKIP TRAILING LINES
         MOVX    A,@DPTR
         CJNE    A,#1,CKSKPFNCMY       ; DO PRINT IF NOT LAST COPY
         MOV     DPTR,#CMDFLAG+3       ; BYTE3(0-9) = DO PRINT TRAILING LINES IF '0', SKIP IF NOT '0'
         MOVX    A,@DPTR
         CJNE    A,#'0',CKSKPFNCMN     ; DO PRINT TRAILING LINES IF '0', SKIP TRAILING LINES IF NOT '0'
CKSKPFNCMY:
         SETB    C
CKSKPFNCMN:
         RET


; ********************************************************
; PR295FF ASSUMES OUTA WILL ALREADY SEND TO CORECT COM PORT
; THIS IS ALSO USED FOR THE ZEBRA BMFF COMMAND FOR RW-420 V2
; ********************************************************
PR295FF: MOV     A,#0CH                ; FF
         CALL    OUTA
         CALL    WAIT2000
         RET


; ********************************************************
; PR460BMF ASSUMES FUJITSU FP-460 USED TO EJECT TO BLACK MARK
; CALL BMFF FOR FP-460
; ********************************************************
PR460BMF:MOV     A,#1DH                ; FUJITSU FP-460 FEED TO BLACK MARK SENSOR
         CALL    OUTA                  ;  AND CUT
         MOV     A,#56H
         CALL    OUTA
         MOV     A,#42H
         CALL    OUTA
         MOV     A,#0
         CALL    OUTA
         CALL    WAIT1000
         RET


;; ********************************************************
;; PRS4000BMF ASSUMES CITIZEN CT-S4000 USED TO EJECT TO BLACK MARK
;; WILL VERIFY PRINTER TYPE, THEN CALL FF FOR CT-S4000
;; ********************************************************
;PRS4000BMF:MOV     DPTR,#PRTFLG
;         MOVX    A,@DPTR
;         CJNE    A,#11,PRS4000BMQ
;         MOV     A,#1DH                ; CITIZEN CT-S4000 FEED TO BLACK MARK SENSOR
;         CALL    OUTA                  ;  AND CUT
;         MOV     A,#0CH
;         CALL    OUTA
;PRS4000BMQ:RET


; *****************************************************************
; PRQ3BMFF ASSUMES CUSTOM.IT Q3 PRINTER USED TO EJECT TO BLACK MARK
; CALL BMFF FOR Q3 AND THEN CALL CUT
; *****************************************************************
PRQ3BMFF:MOV     A,#01DH               ; CUSTOM Q3 FEED TO BLACK MARK SENSOR
         CALL    OUTA
         MOV     A,#0F8H
         CALL    OUTA
         CALL    WAIT1000
         RET


;; ********************************************************
;; PRBLSTLOGO ASSUMES BLASTER V2 USED TO PRINT LOGO
;; WILL VERIFY PRINTER TYPE, THEN CALL PRINTLOGO FOR BLASTER
;; ********************************************************
;PRBLSTLOGO:
;         MOV     DPTR,#PRTFLG
;         MOVX    A,@DPTR
;         CJNE    A,#11,PRBLSTLOGQ      ; 11 = BLAST2 V2
;
;         MOV     DPTR,#BMSG81          ; 81 DEFINES LABEL TO PRINT LOGO
;         MOV     R0,#58
;PRBLSTLOG1:
;         MOVX    A,@DPTR
;         CALL    OUTA
;         INC     DPTR
;         DJNZ    R0,PRBLSTLOG1
;
;         CALL    WAIT1000                ; WAIT ONESEC WHILE WORKING
;
;PRBLSTLOGQ:
;         RET


; ********************************************************
; PRBLSTBMFF ASSUMES BLASTER V2 USED TO EJECT TO BLACK MARK
; WILL VERIFY PRINTER TYPE, THEN CALL FF FOR BLASTER
; ********************************************************
PRBLSTBMFF:
         MOV     DPTR,#BMSG91          ; 91 = BMFF
         MOV     R0,#25
PRBLSTBMF1:
         CLR 	 A
         MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R0,PRBLSTBMF1
         CALL    WAIT1000                ; WAIT ONESEC WHILE WORKING
         RET

; ********************************************************
; PR460LOGO ASSUMES FUJITSU FP-460 USED TO PRINT PRE-STORED LOGO #1 (OF 5)
; WILL VERIFY PRINTER TYPE, THEN CALL PRINT CMD (FUNCTION 69)
; ********************************************************
PR460LOGO:
         MOV     A,#1DH                ; FUJITSU FP-460 PRINT PRE-STORED LOGO #1
         CALL    OUTA                  ; NOTE: NO CUT AND NO FF'S ARE SENT!
         MOV     A,#28H
         CALL    OUTA
         MOV     A,#4CH
         CALL    OUTA
         MOV     A,#6
         CALL    OUTA
         MOV     A,#0
         CALL    OUTA
         MOV     A,#30H
         CALL    OUTA
         MOV     A,#45H
         CALL    OUTA
         MOV     A,#20H
         CALL    OUTA
         MOV     A,#20H
         CALL    OUTA
         MOV     A,#1
         CALL    OUTA
         MOV     A,#1
         CALL    OUTA
         CALL    WAIT1000                ; WAIT ONESEC WHILE PRINTING
PR460LGQ:RET


; ***************************************************************
; PRT6X1LOGO ASSUMES CITIZEN CTS-601/CTS-651
; USED TO PRINT PRE-STORED LOGO #00
; WILL VERIFY PRINTER TYPE, THEN CALL PRINT CMD
; ***************************************************************
PR6X1LOGO:
         ;1D 38 4C   6 0 0 0   30 45 30 30   1 1 D A

         MOV     A,#1DH                ; CITIZEN CTS-601/CTS-651 PRINT PRE-STORED LOGO #00
         CALL    OUTA                  ; NOTE: NO EXTRA CUT NOR FF'S ARE SENT!
         MOV     A,#38H
         CALL    OUTA
         MOV     A,#4CH
         CALL    OUTA

         MOV     A,#6
         CALL    OUTA
         MOV     A,#0
         CALL    OUTA
         MOV     A,#0
         CALL    OUTA
         MOV     A,#0
         CALL    OUTA

         MOV     A,#30H
         CALL    OUTA
         MOV     A,#45H
         CALL    OUTA
         MOV     A,#30H
         CALL    OUTA
         MOV     A,#30H
         CALL    OUTA

         MOV     A,#1
         CALL    OUTA
         MOV     A,#1
         CALL    OUTA

         CALL    CRLF
         CALL    WAIT1000                ; WAIT ONESEC WHILE PRINTING
         RET


; ********************************************************
; PRTM88LOGO ASSUMES EPSON TM-88n OR CITIZEN CTS-310
; USED TO PRINT PRE-STORED LOGO #1
; WILL VERIFY PRINTER TYPE, THEN CALL DEPRECATED PRINT CMD INSTEAD OF FUNCTION 69
; ********************************************************
PRTM88LOGO:
         MOV     A,#1CH                ; EPSON TM-88n OR CITIZEN CTS-310 PRINT PRE-STORED LOGO #1
         CALL    OUTA                  ; NOTE: NO CUT AND NO FF'S ARE SENT!
         MOV     A,#70H
         CALL    OUTA
         MOV     A,#1
         CALL    OUTA
         MOV     A,#0
         CALL    OUTA
         CALL    WAIT1000                ; WAIT ONESEC WHILE PRINTING
         RET


; ***********************************************************
; PRQ3LOGO ASSUMES CUSTOM Q3 USED TO PRINT PRE-STORED LOGO #1
; EVENTUALLY NEED PARAMETER FOR HEIGHT IN 10XLINES
; ***********************************************************
PRQ3LOGO:
         MOV     A,#01BH               ; CUSTOM Q3 LOGO #1, START AND # LINES
         CALL    OUTA                  ; NOTE: NO CUT AND NO FF'S ARE SENT!
         MOV     A,#0FAH
         CALL    OUTA
         MOV     A,#01H
         CALL    OUTA
         MOV     A,#00H
         CALL    OUTA
         MOV     A,#01H
         CALL    OUTA
         MOV     A,#00H
         CALL    OUTA
         MOV     A,#0C8H
         CALL    OUTA

         CALL    WAIT1000                ; WAIT ONESEC WHILE PRINTING
PRQ3LOGQ:RET


; ************************************************************
; DO3BLNKLNS WILL PRINT 3 BLANK LINES AT END OF TICKET
; ***********************************************************
DO3BLNKLNS:
         MOV     R1,#3
DO3BLNKLN1:
;         CALL    BLAST
;         MOV     A,#' '
;         CALL    OUTA
;         CALL    BLASTN
         CALL    PFEED
         DJNZ    R1,DO3BLNKLN1
         RET


; ********************************************************
; CHKTKT CHECKS FOR PRINTER PRESENT AND TICKET IN
;  RETURNS:
;    C=1 TICKET IN, C=0 TICKET OUT,
;    ERRFLG: 1 = NO RESPONSE, FOR RETURNING ERR TO HOST
;    F0: USED TO SET F0=0 FOR ERR, NOT USED AS OF E178
; SPLASHES MESSAGES
; ********************************************************
CHKTKT:
CHK2:    MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#3,CHK1
         SETB    C
         JMP     CHKTIKOK              ; PRINTER=NONE: SET FLAGS OK AND GET OUT

CHK1:    CALL    SELECTP2
         CALL    WAIT3
         MOV     A,#0
         MOV     DPTR,#TKTFLG
         MOVX    @DPTR,A
         MOV     DPTR,#ERRFLG
         MOVX    @DPTR,A               ; CLEAR OUT FLAGS FOR CHKPRTR
         CLR     C                     ; NO ERR - NO TICKET/PAPER

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#1,CHKTK6           ; LOOK FOR PRINTER TYPE
         JMP     CHKTK295              ; EPSON TM-295 IMPACT SLIP
CHKTK6:  CJNE    A,#2,CHKTK7
         JMP     CKTHERM               ; THERMAL GENERIC
CHKTK7:  CJNE    A,#0,CHKTK7A
         JMP     CHKMCP                ; MIDCOM IMPACT
CHKTK7A: CJNE    A,#4,CHKTK7B          ; ZEBRA RW-420 V1 = DOES NOT DO BMFF AT END OF TICKETS
         JMP     CHKZBRA
CHKTK7B: CJNE    A,#5,CHKTK7C          ; BLASTER BY COGNITIVE (WITH TOUCHSTAR CUSTOM FIRMWARE REQUIRED)
         JMP     CHKBLSTR
CHKTK7C: CJNE    A,#6,CHKTK7D          ; EPSON TM-220 IMPACT ROLL
         JMP     CHKTM220
CHKTK7D: CJNE    A,#7,CHKTK7E          ; FUJITSU FP-460 THERMAL BLACK MARK AND LOGO
         JMP     CKTHERM
CHKTK7E: CJNE    A,#8,CHKTK7F          ; CITIZEN CTS-310 THERMAL LOGO
         JMP     CKTHERM
CHKTK7F: CJNE    A,#9,CHKTK7G          ; CITIZEN CTS-601/CTS-651 THERMAL LOGO
         JMP     CKTHERM
CHKTK7G: CJNE    A,#10,CHKTK7H         ; EPSON TM-88 THERMAL LOGO
         JMP     CKTHERM
CHKTK7H: CJNE    A,#11,CHKTK7I         ; BLASTER V2 = V1 + BLAKC MARK BUT CHK IS SAME
         JMP     CHKBLSTR
CHKTK7I: CJNE    A,#12,CHKTK7J         ; PRINTEK MOBILE RT43 BLUETOOTH
         JMP     CKRT43BT
CHKTK7J: CJNE    A,#13,CHKTK7K         ; DATAMAX M4TE
         JMP     CKDM4TE
CHKTK7K: CJNE    A,#14,CHKTK7L         ; ZEBRA RW-420 V2 = DOES BMFF AT AND OF TKT
         JMP     CHKZBRA
CHKTK7L: CJNE    A,#15,CHKTK7M         ; CUSTOM Q3 = DOES LOGO AND THEN BMFF AT AND OF TKT
         JMP     CKTHERM
CHKTK7M: CJNE    A,#16,CHKTK7Z         ; STAR TSP700II = DOES LOGO AND THEN BMFF AT AND OF TKT
         JMP     CKTHERM
CHKTK7Z: JMP     CHKTKDN

CHKTK295:
         CALL    CHKSLPSTS
         DJNZ    R1,CHKTKERR           ; IF R1 IS DECREMENTED AND STILL ISN'T 0 THEN WE KNOW THERE WAS NO RESPONSE = ERR OUT
CHKTK295A:
         ANL     A,#60H                ; MASK ALL BUT 60 = 20+40 = BOF+TOF DON'T SENSE PAPER
         CJNE    A,#00H,CHKTK295N      ; REAL-TIME SLIP STATUS: 0 IS OK, ELSE BIT 5 AND BIT 6 ON = SENSORS DON'T SENSE PAPER
         JMP     CHKTK295B             ; BOTH SENSORS SEE TICKET, CONTINUE

CHKTK295N:
         CJNE    A,#40H,CHKSAYINST     ; IF NOT 40 (AND WAS NOT 60 TO GET HERE) THEN NO TICKET, TELL USER TO INSERT
         MOV     A,#1BH                ; IF 40 ONLY THEN TOF ONLY = AT END OF TICKET, RELEASE IT AND CONTINUE
         CALL    OUTA
         MOV     A,#71H
         CALL    OUTA                  ; RELEASE TICKET
         CALL    WAIT500
         JMP     CHKTK295

CHKTKERR:                              ; NO RESPONSE, SHOW PRINT ERR
         MOV     DPTR,#MSG2            ; THIS IS CALLED FROM ALL OVER SO CANNOT CHANGE
         CALL    SHOW
         MOV     DPTR,#MSG63
         CALL    SHOW                  ; "PRINT ERR"
         MOV     DPTR,#ERRFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG ERR
         JMP     CHKTKDN

CHKTK295B:                             ; EPSON TM-295 SLIP PRINTER
         CALL    CHKSETRSCS            ; RIGHT-SIDE CHAR SPACING TO 03

         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#21H
         CALL    OUTA
         MOV     A,#00H
         CALL    OUTA                  ; MODE 5X7

         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#33H
         CALL    OUTA
         MOV     A,#10
         CALL    OUTA                  ; LINES SPACING 1/6

CHKTIKOK:SETB    C
         MOV     DPTR,#TKTFLG
         MOV     A,#1                  ; RETURN SAYING TICKET IN
         MOVX    @DPTR,A
         JMP     CHKTKDN

CHKSAYINST:
         MOV     DPTR,#MSG39
         CALL    SHOW
         MOV     DPTR,#MSG40
         CALL    SHOW                  ; "INSERT TICKET"

CHKTKNO: CLR     C                     ; NO TICKET
         MOV     DPTR,#TKTFLG
         MOV     A,#0
         MOVX    @DPTR,A

CHKTKDN: MOV     PSW_OV,C                  ; THIS IS THE ONLY WAY OUT
         CALL    DISCON                ; CALL #7: END OF CHKTKT
         MOV     C,PSW_OV
         RET

CKRT43BT:                              ; PRINTEK MOBILE RT43 BLUETOOTH

         MOV     A,#1BH                ; "1B 1B 02   1B 7B 53 54 3F 7D  1B 1B 01"
         CALL    OUTA
         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#02H
         CALL    OUTA                  ; SET ONEIL EMULATION, ONLY WAY TO GET PAPER STATUS

CKRT43BTWC:
         CALL    CHKONELSTS            ; SEND ONEIL EMULATION CMD TO REQUEST PRINTER STATUS

         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#01H
         CALL    OUTA                  ; SET PRINTEK EMULATION, ONLY WAY TO SET MARGINS

CKRT43BTYZ:                            ; BOTH THE PRINTEK RT43BT AND THE DATAMAX M4TE USE THE ONIEL EMULATION STATUS REQUEST COMMAND, SAME RESULTS

         MOV     R0,#15
CKRT43BTW1:
         MOV     R1,#0
         MOV     R2,#0
CKRT43BTW2:
         CALL    INPA
         JC      CKRT43BTW3            ; GOT CHAR
         DJNZ    R1,CKRT43BTW2
         DJNZ    R2,CKRT43BTW2
         JMP     CHKTKERR              ; NO RESPONSE, SHOW PRINT ERR IF NO ARGUMENT IN 60ms
CKRT43BTW3:
         DJNZ    R0,CKRT43BTW1
         CJNE    A,#'D',CKRT43OPN      ; BYTE 15 IS 'D' IS LEVER DOWN MEANS LATCH CLOSED, IF NOT SHOW "PRINTR OPEN"

         MOV     R0,#4
CKRT43BTW5:
         MOV     R1,#0
         MOV     R2,#0
CKRT43BTW6:
         CALL    INPA
         JC      CKRT43BTW7            ; GOT CHAR
         DJNZ    R1,CKRT43BTW6
         DJNZ    R2,CKRT43BTW6
         JMP     CHKTKERR              ; NO RESPONSE, SHOW PRINT ERR IF NO ARGUMENT IN 60ms
CKRT43BTW7:
         DJNZ    R0,CKRT43BTW5
         CJNE    A,#'P',CHKPAPROUT     ; BYTE 19 IS 'P' IS PAPER PRESENT, IF NOT SHOW "PAPER OUT"

         MOV     DPTR,#PRTFLG          ; BOTH RT43BT AND DM4TE ARE GOOD
         MOVX    A,@DPTR
         CJNE    A,#12,CKRT43OK        ; 12 = PRINTEK MOBILE RT43 BLUETOOTH = NEED TO SETUP FONT

         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#4BH
         CALL    OUTA
         MOV     A,#01H
         CALL    OUTA                  ; SET FONT PITCH, 01 = 10.2 cpi Sans Serif
                                       ; THERE WILL BE A CHR4 SENT BACK, WE CAN IGNORE IT

         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#48H
         CALL    OUTA
         MOV     A,#0AH
         CALL    OUTA
         MOV     A,#00H
         CALL    OUTA                  ; SET MARGINS, LEFT = 10MM + RIGHT = 0MM
                                       ; THERE WILL BE A CHR4 SENT BACK, WE CAN IGNORE IT

CKRT43OK:JMP     CHKTIKOK              ; RT43BT PRINTER STATUS OK, SETTINGS SENT, CONTINUE

CKRT43OPN:
         MOV     DPTR,#MSG64
         CALL    SHOW
         MOV     DPTR,#MSG29
         CALL    SHOW                  ; "PRINTR OPEN"
         JMP     CHKTKNO

CKTHERM:                               ; ALL THERMALS START HERE
         CALL    CHKROLSTS
         DJNZ    R1,CHKTHE4B           ; IF R1 IS DECREMENTED AND STILL ISN'T 0 THEN WE KNOW THERE WAS NO RESPONSE = ERR OUT
CHKTHER1:ANL     A,#40H                ; MASK ALL BUT OUT BIT
         JNZ     CHKPAPROUT            ; CHECK PAPER PRESENT: 40H = BIT 6 = IF ON ROLL PAPER NOT FOUND
         JMP     CHKTHER4              ; ZERO FOR STATUS AND-ED WITH 40 MEANS ROLL PAPER OK

CHKPAPROUT:
         MOV     DPTR,#MSG68
         CALL    SHOW
         MOV     DPTR,#MSG69
         CALL    SHOW                  ; "PAPER OUT"
         JMP     CHKTKNO

CHKTHER4:
         CALL    CHKTHMOFST
         DJNZ    R1,CHKTHE4B           ; IF R1 IS DECREMENTED AND STILL ISN'T 0 THEN WE KNOW THERE WAS NO RESPONSE = ERR OUT
CHKTHE4A:CJNE    A,#12H,CHKTHE4B       ; TEST COVER STATUS, IF NOT 12H THEN ERR
         JMP     CHKTHR11              ; BITS 1 AND 4 FIXED ON = OK, CONTINUE
CHKTHE4B:JMP     CHKTKERR              ; NO RESPONSE, SHOW PRINT ERR

CHKTHR11:
         CALL    CHKSETRSCS            ; REGULAR THERMALS, SET RIGHT-SIDE CHAR SPACING TO 03
         MOV     DPTR,#PRTFLG          ; ALL THERMALS END UP HERE IF OK
         MOVX    A,@DPTR
         CJNE    A,#15,CHKTHR11A       ; ALL THERMALS GO TO 11->11Z
         CALL    CHKSETUPQ3            ; SET Q3-SPECIFIC SETTINGS, THEN FINISH LIKE THERMAL
         JMP     CHKTIKOK              ; FOR NOW JUST LEFT MARGIN = 70 (BOTH Q3 AND TSP700II)

CHKTHR11A:
         CJNE    A,#16,CHKTHR11Z       ; ALL THERMALS GO TO 11-11Z
         CALL    CHKSETUPQ3            ; SET TSP700II SPECIFIC SETTINGS, THEN FINISH LIKE THERMAL
         JMP     CHKTIKOK              ; FOR NOW JUST LEFT MARGIN = 70 (BOTH Q3 AND TSP700II)

CHKTHR11Z:
         JMP     CHKTIKOK

CHKMCP:  CALL    CHKMCPSTS             ; MIDCOM IMPACT PRINTER
         DJNZ    R1,CHKTKERZ           ; IF R1 IS DECREMENTED AND STILL ISN'T 0 THEN WE KNOW THERE WAS NO RESPONSE = ERR OUT
CHKMCP2: CJNE    A,#'Y',CHKMCPNO
         JMP     CHKTIKOK              ; MIDCOM PRINTER HAS TICKET
CHKMCPNO:JMP     CHKSAYINST            ; SHOW MSG TO INSERT TICKET

;NEXT LINE IS JUST A CLOSE JUMP FOR SHORT-JUMP STATEMENTS
CHKTKERZ:JMP     CHKTKERR              ; NO RESPONSE, SHOW PRINT ERR

CHKZBRA:                               ; ZEBRA RW-420 THERMAL ROLL PRINTER, FOR MOBLTEC CUSTOMERS
         CALL    CHKZEBRSTS
         DJNZ    R1,CHKTKERZ           ; IF R1 IS DECREMENTED AND STILL ISN'T 0 THEN WE KNOW THERE WAS NO RESPONSE = ERR OUT
         ANL     A,#01H                ; MASK UPPER BITS
         CJNE    A,#0,CHKPAPROUT       ; BIT1 NOT 1 MEANS ZEBRA STATUS OK, ELSE SHOW PAPER OUT

;SEND SETUP COMMANDS TO ZEBRA PRINTER
         MOV     R1,#12
         MOV     DPTR,#TMSG3
         CALL    MX1                   ; SET TO JOURNAL MODE ( NO FORM)

         CALL    WAIT50                ; ZQ520 WAS NOT ABLE TO PROCESS COMMANDS WITHOUT A PAUSE
         MOV     R1,#17
         MOV     DPTR,#TMSG1           ; SETUP FOR TEXT
         CALL    MX1

         CALL    WAIT50                ; ZQ520 WAS NOT ABLE TO PROCESS COMMANDS WITHOUT A PAUSE
         MOV     R1,#19
         MOV     DPTR,#TMSG8           ; SETUP FOR TEXT
         CALL    MX1

         CALL    WAIT50                ; ZQ520 WAS NOT ABLE TO PROCESS COMMANDS WITHOUT A PAUSE
         MOV     R1,#15
         MOV     DPTR,#TMSG2

         CALL    MX1

         JMP     CHKTIKOK

CHKBLSTR:                              ; COGNITIVE ADVANTAGE_LX OR ADVANTAGE_DLX = TOUCHSTAR "BLASTER", V1 & V2
         CALL    CHKBLSTSTS
         DJNZ    R1,CHKTKERZ           ; IF R1 IS DECREMENTED AND STILL ISN'T 0 THEN WE KNOW THERE WAS NO RESPONSE = ERR OUT
         CJNE    A,#'R',CHKTKERZ       ; NO R, NO GOOD
         JMP     CHKTIKOK              ; R IN STATUS MEANS BLASTER STATUS OK

CHKTM220:                              ; EPSOM TM-220 IMPACT ROLL PRINTER
         CALL    CHKROLSTS             ; CHECK REAL-TIME ROLL STATUS FOR EPSON TM-220
         DJNZ    R1,CHKTM22N           ; IF R1 IS DECREMENTED AND STILL ISN'T 0 THEN WE KNOW THERE WAS NO RESPONSE = ERR OUT
CHKTM22A:ANL     A,#48H                ; IF BIT 3 OR BIT 6 ARE ON THEN PAPER ERR
         JZ      CHKTM22Y
CHKTM22N:JMP     CHKTKERR              ; PAPER ERR, SHOW PRINT ERR

CHKTM22Y:
         CALL    CHKSETRSCS            ; SET RIGHT-SIDE CHAR SPACING TO 03

         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#21H
         CALL    OUTA
         MOV     A,#01H
         CALL    OUTA                  ; CHAR FONT B 7X9

         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#32H
         CALL    OUTA                  ; SET DEFAULT LINE SPACING

         JMP     CHKTIKOK

CKDM4TE:                               ; CHECK DATAMAX M4TE

         CALL    CHKONELSTS            ; SEND ONEIL EMULATION CMD TO REQUEST PRINTER STATUS
         JMP     CKRT43BTYZ            ; USE RT43BT STATUS PARSER TO DETERMINE RESPONSE


; *******************
; ROUTINES FOR CHKTKT
; *******************

; ********************************************************
; REQUEST PRINTER STATUS USING ONIEL EMULATION STATUS REQUEST COMMAND
;  PRINTEK RT43BT AND DATAMAX M4TE
; ********************************************************
CHKONELSTS:
         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#7BH
         CALL    OUTA
         MOV     A,#53H
         CALL    OUTA
         MOV     A,#54H
         CALL    OUTA
         MOV     A,#3FH
         CALL    OUTA
         MOV     A,#7DH
         CALL    OUTA                  ; REQUEST PRINTER STATUS

         RET


; ********************************************************
; REQUEST MIDCOM IMPACT PRINTER STATUS
;  MIDCOM
; ********************************************************
CHKMCPSTS:
         MOV     A,#14H                ; CTRL T
         CALL    OUTA

         MOV     R2,#0                 ; INIT R2, 0 RESULTS IN ABOUT A 300 MS WAIT TO ALLOW WIRELSS MODEMS TIME
         CALL    CHKWAITCHR
         RET

; ********************************************************
; REQUEST ZEBRA PRINTER STATUS
;  ZEBRA RW-420
; ********************************************************
CHKZEBRSTS:
         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#'h'
         CALL    OUTA
         CALL    WAIT50                 ; CHANGED FROM CRLF SO IT DIDN'T ADVANCE PAPER AT BEGINNING OF DELIVER
;         CALL    CRLF                  ; ZEBRA PRINTER NEEDS CRLF TO PROCESS COMMAND

         MOV     R2,#0                 ; INIT R2, 0 RESULTS IN ABOUT A 300 MS WAIT TO ALLOW WIRELSS MODEMS TIME
         CALL    CHKWAITCHR
         RET

; ********************************************************
; REQUEST COGNTIVE BLASTER PRINTER STATUS
;  COGNITIVE ADVANTAGE_LX
;  COGNITIVE ADVANTAGE_DLX
;  TOUCHSTAR BLASTER
; ********************************************************
CHKBLSTSTS:
         MOV     A,#'!'
         CALL    OUTA
         MOV     A,#'Q'
         CALL    OUTA
         MOV     A,#'S'
         CALL    OUTA
         CALL    CRLF                  ; BLASTER STATUS REQUEST NEEDS CRLF IN TOUCHSTAR FIRMWARE

         MOV     R2,#0                 ; INIT R2, 0 RESULTS IN ABOUT A 300 MS WAIT TO ALLOW WIRELSS MODEMS TIME
         CALL    CHKWAITCHR
         RET

; ********************************************************
; REQUEST REAL-TIME ROLL PRINTER STATUS
;  TM-220
;  THERMALS
; ********************************************************
CHKROLSTS:
         MOV     A,#10H
         CALL    OUTA
         MOV     A,#04H                ; REAL-TIME STATUS REQUEST
         CALL    OUTA
         MOV     A,#04H
         CALL    OUTA                  ; REQUEST ROLL PAPER STATUS

         MOV     R2,#0                 ; INIT R2, 0 RESULTS IN ABOUT A 300 MS WAIT TO ALLOW WIRELSS MODEMS TIME
         CALL    CHKWAITCHR
         RET

; ********************************************************
; REQUEST REAL-TIME SLIP PRINTER STATUS:
;  TM-295
; ********************************************************
CHKSLPSTS:
         MOV     A,#10H
         CALL    OUTA
         MOV     A,#04H                ; REAL-TIME STATUS REQUEST
         CALL    OUTA
         MOV     A,#05H
         CALL    OUTA                  ; REQUEST ROLL PAPER STATUS

         MOV     R2,#0                 ; INIT R2, 0 RESULTS IN ABOUT A 300 MS WAIT TO ALLOW WIRELSS MODEMS TIME
         CALL    CHKWAITCHR
         RET

; ********************************************************
; REQUEST REAL-TIME OFFLINE PRINTER STATUS:
;  THERMALS
; ********************************************************
CHKTHMOFST:
         MOV     A,#10H
         CALL    OUTA
         MOV     A,#04H                ; REAL-TIME OFFLINE STATUS REQUEST
         CALL    OUTA
         MOV     A,#02H
         CALL    OUTA                  ; REQUEST COVER STATUS

         MOV     R2,#0                 ; INIT R2, 0 RESULTS IN ABOUT A 300 MS WAIT TO ALLOW WIRELSS MODEMS TIME
         CALL    CHKWAITCHR
         RET

; ********************************************************
; SET RIGHT-SIDE CHARACTER SPACING:
;  TM-220
;  THERMALS
;  TM-295
; ********************************************************
CHKSETRSCS:
         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#20H
         CALL    OUTA                  ; ADDS A LITTLE SPACE BETWEEN CHARS
         MOV     A,#03H                ; REGULAR THERMAL=3, Q3=8
         CALL    OUTA
         RET


; ********************************************************
; SET Q3 LEFT MARGIN TO 70
;  ALSO USED FOR STAR TSP700II
;  CUSTOM Q3 IS DIFFERENT THAN THE OTHERS
; ********************************************************
CHKSETUPQ3:
         MOV     A,#1DH
         CALL    OUTA
         MOV     A,#4CH
         CALL    OUTA                  ; LEFT MARGIN, DEFAULT = 0
         MOV     A,#046H                ; Q3=70=46H
         CALL    OUTA                  ; MOVE RIGHT TO CENTER 25 CHARS ON LINE
         MOV     A,#00H
         CALL    OUTA

         RET

CLRLEFTMRGN:                           ; REQUIRED FOR STAR TSP700II
         MOV     A,#1DH
         CALL    OUTA
         MOV     A,#4CH
         CALL    OUTA                  ; NEED TO CLEAR LEFT MARGIN, DEFAULT = 0
         MOV     A,#00H                ; REQUIRTED FOR SOME THERMALS BECAUSE THEY SHIFT
         CALL    OUTA                  ; THE LOGO BASED ON THE MARGIN
         MOV     A,#00H                ; NEED TO SET TO 0 BEFORE PRINT LOGO
         CALL    OUTA

         RET


; *********************************************************************************
; WAIT FOR CHARACTER, CALLER MUST SET R1 PRIOR TO CALL TO DELAY
; R2 = #0 = 256 MS DELAY BEFORE RETURN PRINT ERR (DEFAULT, FOR WIRELESS 232 MODEMS)
; *********************************************************************************
CHKWAITCHR:                            ; BEFORE CALLING CALLER MUST SET R2 TO ENABLE DELAY
                                       ; R2 SET BY CALLER, R2 = MILLISECONDS
         MOV     R1,#2                 ; DELAY AND LOOK FOR CHARACTER
CHKWAITCH1:                            ;
         CALL    WAIT500U              ;500 microsecond delay
         CALL    INPA
         JC      CHKWAITCHY
         DJNZ    R2,CHKWAITCH1
         DJNZ    R1,CHKWAITCH1
CHKWAITCHN:
         MOV     R1,#2                 ; PUT 2 IN R1 SO STILL NOT 0 AFTER DECREMENT IN CALLER = DIDN'T GET CHAR IN HERE
         JMP     CHKWAITCHX            ; NO RESPONSE, SHOW PRINT ERR
CHKWAITCHY:
         MOV     R1,#1                 ; PUT 1 IN R1 SO IS 0 AFTER DECREMENT = CALLER KNOWS THAT DID GET CHAR IN HERE
CHKWAITCHX:
         RET





; **************************************************************************
; CHKPRTR USES CHKTKT TO DETERMINE IF PRINTER READY BASED ON HOST QUERY, AND
;  THEN IS ALSO USED FOR PASS-THROUGH PRINT COMMANDS
; RETURNS TO HOST:
;  0 = OK
;  1 = PAPER ERR
;  2 = OTHER ERR
;  3 = NO PRINTER
; *********************************************************************************
CHKPRTR: MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#3,CHKPRT0
         MOV     A,#'3'
         JMP     CHKPR2                ; CHECK FOR NO PRINTER
CHKPRT0: CALL    CHKTKT                ; CALL #15: HOST REQUESTS PRINTER STATUS
         CALL    LODHSTLNFLG           ; IF PASS-THROUGH-PRINT (NON-ZERO), NEED TO SEND LINE IF STATUS OK
         JZ      CHKPR0Z               ; IF HOSTLNFLG=0 THEN GOTO NORMAL PROCESSING
                                       ; ELSE CHECK FOR ERRS BEFORE PROCESSING PASS-THROUGH PRINTING COMMANDS
CHKPR0A: MOV     DPTR,#ERRFLG          ; ERR FLAG: 0=OK,1=ERR
         MOVX    A,@DPTR
         JZ      CHKPR0B               ; CONTINUE TESTING FOR ERR
         JMP     CHKPR0Z               ; RETURN ERR
CHKPR0B: MOV     DPTR,#TKTFLG          ; TKT FLAG: 1=OK,0=ERR (REVERSE LOGIC ...)
         MOVX    A,@DPTR
         JZ      CHKPR0Z               ; RETURN ERR
CHKPR0C: CALL    SELECTP2
         CALL    LODHSTLNFLG
         CJNE    A,#1,CHKPR0D          ; 1 = START PRINTING
         CALL    PRTFINTOP
         JMP     CHKPR0Y
CHKPR0D: CJNE    A,#2,CHKPR0E          ; 2 = PRINTLINE
         MOV     DPTR,#HOSTLNPRT
         CALL    PRTMSG
         JMP     CHKPR0Y
CHKPR0E: CJNE    A,#3,CHKPR0Y          ; 3 = FINISH PRINTING
         CALL    FINISHTKT
         JMP     CHKPR0Y
CHKPR0Y: CALL    DISCON                ; CALL #8: END OF CHKPRTR
         CALL    UNLATCH
CHKPR0Z: CALL    CON2HOST              ; REMAKE CONNECTION TO HOST AFTER PRINT
         CALL    SELECTHST

         MOV     DPTR,#EMULATEFLG
         MOVX    A,@DPTR
         CJNE    A,#2,CHKPR0Z1         ; 2=PA21 SLSMODE
         JMP     CHKPR2_SLS0X          ; DONT SEND RETURN IN SLSMODE
CHKPR0Z1:
         MOV     DPTR,#ERRFLG
         MOVX    A,@DPTR
         JZ      CHKPR1
         MOV     A,#'2'
         JMP     CHKPR2                ; OTHER ERR
CHKPR1:  MOV     DPTR,#TKTFLG
         MOVX    A,@DPTR
         JZ      CHKPR4
         MOV     A,#'1'                ; PRINTER READY
         JMP     CHKPR2
CHKPR4:  MOV     A,#'0'                ; OUT OF PAPER
CHKPR2:  CALL    OUTA                  ; NOPE - PA21 SLSMODE
         CALL    SENDPIPE
CHKPR2_SLS0X:
         RET


DOPRTHDR:CALL    SELECTP2              ; SELECT PRINTER PORT
PRTHDR1A:MOV     DPTR,#HDRTYPFL        ; HEADER TYPE FLAG, 0=DELTICKET, 1=SHIFTTICKET, 2=CALTICKET
         MOVX    A,@DPTR
         CJNE    A,#0,PRTHDR1B         ; DEL TICKET HAS SETTING TO SKIP PRINTING OF TRUCK/DRIVER LINE
         MOV     DPTR,#TRKFLG          ; TRKFLAG = 1 = SKIP TRUCK/DRIVER LINE ON DELTICKET
         MOVX    A,@DPTR
         JNZ     SKPPRTHDR1
PRTHDR1B:MOV     DPTR,#PRMSG7          ; TRUCK AND DRIVER
         CALL    MOVMES
         MOV     DPTR,#TRUCK+1
         MOVX    A,@DPTR
         CALL    MAKEASC
         MOV     ECGALS+7,A
         MOV     A,B
         MOV     ECGALS+8,A
         CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC
         MOV     ECGALS+9,A
         MOV     A,B
         MOV     ECGALS+10,A

         MOV     DPTR,#DRIVER+1
         MOVX    A,@DPTR
         CALL    MAKEASC
         MOV     ECGALS+21,A
         MOV     A,B
         MOV     ECGALS+22,A
         CALL    DECDPTR
         MOVX    A,@DPTR
         CALL    MAKEASC
         MOV     ECGALS+23,A
         MOV     A,B
         MOV     ECGALS+24,A
         CALL    PRTLINE

SKPPRTHDR1:
         MOV     DPTR,#HDRTYPFL        ; HEADER TYPE FLAG, 0=DELTICKET, 1=SHIFTTICKET, 2=CALTICKET
         MOVX    A,@DPTR
         CJNE    A,#0,PRTHDR1C         ; EACH TICKET PRINTS A DIFFERENT DATE/TIME AND LABEL
                                       ;0=DELTICKET
         MOV     DPTR,#PRMSG6          ; TIME AND DATE
         CALL    MOVMES
         MOV     DPTR,#STTIME
         CALL    RETTIME               ; RETREIVE START DATETIME
         JMP     PRTHDR2
PRTHDR1C:CJNE    A,#1,PRTHDR1D         ; EACH TICKET PRINTS A DIFFERENT DATE/TIME AND LABEL
                                       ; 1=SHIFTTICKET
         MOV     DPTR,#PRMSG6          ; TIME AND DATE
         CALL    MOVMES
         MOV     DPTR,#LSTSHFT
         CALL    RETTIME               ; RETREIVE START DATETIME
         JMP     PRTHDR2
PRTHDR1D:                              ;2=CALTICKET
         MOV     DPTR,#PRMSG6A         ; TIME AND DATE
         CALL    MOVMES
         CALL    RTIME
PRTHDR2: MOV     A,DATEIMG+6
         CALL    MAKEASC
         MOV     ECGALS+5,A
         MOV     A,B
         MOV     ECGALS+6,A
         MOV     ECGALS+7,#'/'
         MOV     A,DATEIMG+5
         CALL    MAKEASC
         MOV     ECGALS+8,A
         MOV     A,B
         MOV     ECGALS+9,A
         MOV     ECGALS+10,#'/'
         MOV     A,DATEIMG+7
         CALL    MAKEASC
         MOV     ECGALS+11,A
         MOV     A,B
         MOV     ECGALS+12,A

         MOV     A,DATEIMG+3
         CALL    MAKEASC
         MOV     ECGALS+20,A
         MOV     A,B
         MOV     ECGALS+21,A
         MOV     A,#':'
         MOV     ECGALS+22,A
         MOV     A,DATEIMG+2
         CALL    MAKEASC
         MOV     ECGALS+23,A
         MOV     A,B
         MOV     ECGALS+24,A
         CALL    PRTLINE
SKPPRTHDR2:
         RET


PRTFTIM: MOV     DPTR,#TIMFLG
         MOVX    A,@DPTR
         JNZ     SKPPRTHDR2
         MOV     DPTR,#PRMSG6A         ; TIME AND DATE
         CALL    MOVMES
         MOV     DPTR,#FTIME
         CALL    RETTIME
         JMP     PRTHDR2



//Delay timers
//20 microsecond delay
//2 microsecond delay
WAIT2U:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#9
			MOV 	R6, #1
			MOV 	R5, #1
			MOV 	R4, #1
			JMP WAITLOOP

//5 microsecond delay
WAIT5U:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#36
			MOV 	R6, #1
			MOV 	R5, #1
			MOV 	R4, #1
			JMP WAITLOOP//500 microsecond delay

//20 microsecond delay
WAIT20U:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#160
			MOV 	R6, #1
			MOV 	R5, #1
			MOV 	R4, #1
			JMP WAITLOOP

//100 microsecond delay
WAIT100U:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#40
			MOV 	R6, #4
			MOV 	R5, #1
			MOV 	R4, #1
			JMP WAITLOOP

//500 microsecond delay
WAIT500U:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#240
			MOV 	R6, #16
			MOV 	R5, #1
			MOV 	R4, #1
			JMP WAITLOOP
//1 millisecond delay
WAIT1:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#175
			MOV 	R6, #32
			MOV 	R5, #1
			MOV 	R4, #1
			JMP WAITLOOP
//3 millisecond delay
WAIT3:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#200
			MOV 	R6, #96
			MOV 	R5, #1
			MOV 	R4, #1
			JMP WAITLOOP
//50 millisecond delay
WAIT50:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#115
			MOV 	R6, #60
			MOV 	R5, #7
			MOV 	R4, #1
			JMP WAITLOOP
//100 millisecond delay
WAIT100:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#0
			MOV 	R6, #130
			MOV 	R5, #13
			MOV 	R4, #1
			JMP WAITLOOP
//250 millisecond delay
WAIT250:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#175
			MOV 	R6, #41
			MOV 	R5, #32
			MOV 	R4, #1
			JMP WAITLOOP
//500 millisecond delay
WAIT500:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#0
			MOV 	R6, #83
			MOV 	R5, #63
			MOV 	R4, #1
			JMP WAITLOOP
//1 second delay
WAIT1000:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#0
			MOV 	R6, #0
			MOV 	R5, #125
			MOV 	R4, #1
			JMP WAITLOOP
//2 second delay
WAIT2000:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#0
			MOV 	R6, #0
			MOV 	R5, #249
			MOV 	R4, #1
			JMP WAITLOOP
//3 second delay
WAIT3000:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#0
			MOV 	R6, #0
			MOV 	R5, #120
			MOV 	R4, #2
			JMP WAITLOOP
//4 second delay
WAIT4000:  	SETB   	PSW_RS0     	;Switch to bank1 of RAM for delay loop
			MOV    	R7,#0
			MOV 	R6, #0
			MOV 	R5, #242
			MOV 	R4, #2
			JMP WAITLOOP

//Variable delay loop
WAITLOOP: 	DJNZ    R7,WAITLOOP
		  	DJNZ	R6, WAITLOOP
		  	DJNZ	R5, WAITLOOP
		  	DJNZ	R4, WAITLOOP
         	CLR	 	PSW_RS0 ; Switch back to bank 0 of RAM
         	RET


SAYPRNT:
         CALL    LF0000
         MOV     DPTR,#MSG2            ; SAY "PRINT"
         MOV     COMMAND,#LOADLCD
         CALL    XLOAD
         RET

; ************************************************************************************************
; PFEED = USED TO PRINT A BLANK LINE
; PLINEEND = USED TO FINISH PRINTING TEXT ALREADT SENT
; BOTH DO NECESSARY BLASTER CALLS SINCE EACH LINE OF TEXT FOR A BLASTER IS A SEPERATE 'LABEL'
;  AND NEEDS ADDITIONAL TEXT BEFORE AND AFTER WHAT IS BEING PRINTED
;*************************************************************************************************
PFEED:
         CALL    BLAST
PLINEEND:
         CALL    BLASTN
         CALL    SELECTP2
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#5,PFEED7
         JMP     PFEED2                ; SPECIAL STUFF FOR BLASTER V1 = NOTHING
PFEED7:  CJNE    A,#11,PFEED6
         JMP     PFEED2                ; SPECIAL STUFF FOR BLASTER V2 = NOTHING
PFEED6:  CJNE    A,#4,PFEED6A
         CALL    CRLF                  ; ZEBRA RW-420 V1 = NEEDS CRLF TO ADVANCE 1 LINE
         JMP     PFEED2
PFEED6A: CJNE    A,#14,PFEED5
         CALL    CRLF                  ; ZEBRA RW-420 V2 = NEEDS CRLF TO ADVANCE 1 LINE
         JMP     PFEED2
PFEED5:  CALL    SENDLF                ; THE REST OF THE PRINTERS JUST NEED LINEFEED TO ADVANCE ONE LINE
         CALL    LODHCMDFLG            ; WE NEED SOME DELAYS IF WE'RE PRINTING TO IMPACT PRINTERS BUT NO DELAY IF JUST SENDING TO HOST
         JNZ     PFEED2
         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#1,PFEED3
         CALL    WAIT500               ; 500 MS DELAY PER LINE IF EPSON TM-295 = KEEP BUFFER FROM FILLING UP
         JMP     PFEED2
PFEED3:  CJNE    A,#0,PFEED2
PFEED4:  CALL    WAIT2000                ; 2000 MS DELAY PER LINE IF MIDCOM = WAIT UNTIL PRINTER READY FOR NEXT LINE
                                       ; WE MAY NOT NEED 2 SECONDS, 1 SEC MIGHT BE ENOUGH = NOPE! DEFINITELY NEED 2 SECONDS!
PFEED2:  MOV     DPTR,#LINES           ; INCREMENT LINE COUNTER FOR HOST THAT CARES
         CLR     C
         MOVX    A,@DPTR
         ADDC    A,#1
         DA      A
         MOVX    @DPTR,A
         RET


;************************************************
MOVMES:  MOV     R2,#25
         MOV     R1,#ECGALS            ; TEMP USE FOR PRINTOUT
MOVMES1: CLR     A
		 MOVC    A,@A+DPTR
         MOV     @R1,A
         INC     R1
         INC     DPTR
         DJNZ    R2,MOVMES1
         RET

PRTLINE: PUSH    DPH
         PUSH    DPL
         CALL    SELECTP2
         CALL    BLAST
         POP     DPL
         POP     DPH
         MOV     R0,#ECGALS            ; PRINT LINE STORAGE
         MOV     R1,#25                ; THE WHOLE LINE
PRTLIN1: MOV     A,@R0
         CALL    OUTA
         INC     R0
         DJNZ    R1,PRTLIN1
         CALL    PLINEEND
         RET


PRTMSG:  PUSH    DPH
         PUSH    DPL
         CALL    SELECTP2
         CALL    BLAST
         POP     DPL
         POP     DPH
         MOV     R1,#25
PRTMSG1: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRTMSG1
         CALL    PLINEEND
         RET

PRTCMSG: PUSH    DPH					;USED FOR PRINTING MESSAGES STORED IN FLASH. ADDED 11/6/20 JC
         PUSH    DPL
         CALL    SELECTP2
         CALL    BLAST
         POP     DPL
         POP     DPH
         MOV     R1,#25
PRTCMSG1:CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRTCMSG1
         CALL    PLINEEND
         RET

PRTBYTES:MOV     R1,#25
         MOVX    A,@DPTR
         JZ      PRTB1

PRTB2A:  PUSH    DPL
         PUSH    DPH
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY - SEE BELOW ABOUT WHY THESE TWO ARE CALLED IN REVERSE ORDER
         CALL    BLAST                 ; BEFORE FIRST CHAR ON EACH LINE NEED BLASTER PRE-CHARS
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - NOTE THESE TWO MCON CALLS ARE BACKWARDS SINCE WE WANT TO GO FROM FAR TO LOCAL, THEN BACK
         POP     DPH
         POP     DPL

PRTB2B:  MOVX    A,@DPTR
         JZ      PRTB2C
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,PRTB2B

         PUSH    DPL
         PUSH    DPH
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY - SEE BELOW ABOUT WHY THESE TWO ARE CALLED IN REVERSE ORDER
         CALL    PLINEEND              ; AFTER LAST CHAR ON EACH LINE NEED BLASTER POST-CHARS
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - NOTE THESE TWO MCON CALLS ARE BACKWARDS SINCE WE WANT TO GO FROM FAR TO LOCAL, THEN BACK
         POP     DPH
         POP     DPL
         JMP     PRTBYTES              ; THIS ACTUALLY PRINTS THE BYTES, THE GROUP BELOW SKIPS THEM AS INDICTAED BY BYTE1=0 FOR LINE

PRTB2C:  PUSH    DPL
         PUSH    DPH
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY - SEE BELOW ABOUT WHY THESE TWO ARE CALLED IN REVERSE ORDER
         CALL    PLINEEND              ; AFTER LAST CHAR ON EACH LINE NEED BLASTER POST-CHARS
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - NOTE THESE TWO MCON CALLS ARE BACKWARDS SINCE WE WANT TO GO FROM FAR TO LOCAL, THEN BACK
         POP     DPH
         POP     DPL
PRTB1:   RET


LASTVOL:
         MOV     A,COMP
         JZ      LASTVOL1
         CALL    GETUNIT
         MOV     DPTR,#LGDFLG1
         MOV     A,#41H
         JNB     PSW_F0,U14
         ANL     A,#9FH
         ORL     A,#20H
U14:     MOVX    @DPTR,A

         MOV     DPTR,#LGDFLG2
         MOV     A,#05H
         JNB     PSW_F0,U15
         ANL     A,#0F9H
         ORL     A,#02H
U15:     MOVX    @DPTR,A
         JMP     LASTVOL2

LASTVOL1:CALL    GETUNIT
         CALL    LGD101
         MOV     DPTR,#LGDFLG2
         MOV     A,#04H
         JNB     PSW_F0,U16
         ANL     A,#0F9H
         ORL     A,#02H
U16:     MOVX    @DPTR,A

LASTVOL2:CALL    PSLABEL
         CALL    SHOWVL
         MOV     COMMAND,#DISPVOL      ; SHOW LAST DM VOLUME
         CALL    VLOAD
         MOV     PULSFLG1,#0           ; AGAIN FOR RE-PRESET
         MOV     PULSFLG2,#0           ; CLEAR PRESET FLAGS
         RET

DLASTV:
         MOV     A,COMP
         JZ      DLASTV1
         CALL    GETUNIT
         MOV     DPTR,#LGDFLG1
         MOV     A,#41H
         JNB     PSW_F0,U17
         ANL     A,#9FH
         ORL     A,#20H
U17:     MOVX    @DPTR,A
         MOV     DPTR,#LGDFLG2
         MOV     A,#85H
         JNB     PSW_F0,U18
         ANL     A,#0F9H
         ORL     A,#02H
U18:     MOVX    @DPTR,A
         JMP     DLASTV2
DLASTV1: CALL    LGD101
         MOV     DPTR,#LGDFLG2
         MOV     A,#84H
         JNB     PSW_F0,U19
         ANL     A,#0F9H
         ORL     A,#02H
U19:     MOVX    @DPTR,A
DLASTV2: CALL    PSLABEL
         CALL    SHOWVL
         MOV     COMMAND,#DISPVOL      ; SHOW LAST DM VOLUME
         CALL    VLOAD
         MOV     PULSFLG1,#0           ; AGAIN FOR RE-PRESET
         MOV     PULSFLG2,#0           ; CLEAR PRESET FLAGS
         RET


CLASTVOL:
         MOV     A,COMP
         JZ      CLAST1
         CALL    GETUNIT
         MOV     DPTR,#LGDFLG1
         MOV     A,#41H
         JNB     PSW_F0,U20
         ANL     A,#9FH
         ORL     A,#20H
U20:     MOVX    @DPTR,A
         MOV     DPTR,#LGDFLG2
         MOV     A,#0DH
         JNB     PSW_F0,U21
         ANL     A,#0F9H
         ORL     A,#02H
U21:     MOVX    @DPTR,A
         JMP     CLAST2

CLAST1:  CALL    GETUNIT
         CALL    LGD101
         MOV     DPTR,#LGDFLG2
         MOV     A,#0CH
         JNB     PSW_F0,U22
         ANL     A,#0F9H
         ORL     A,#02H
U22:     MOVX    @DPTR,A

CLAST2:  CALL    SHOWVL
         MOV     COMMAND,#DISPVOL      ; SHOW LAST DM VOLUME
         CALL    VLOAD
         RET

ADDTOT:                                ; 172E MOD FOR 12 DIGIT GRAND TOTAL
         MOV     R2,#4
         MOV     DPTR,#NVOL            ; MOV VOLUME VARIABLE TO DATEIMG
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         CLR     C
         MOV     R2,#4                 ; 4 BYTES - 8 DIGITS
         MOV     DPTR,#TNVOL           ; ADD UP ALL THE TOTALS
         MOV     R0,#DATEIMG
         CALL    SUMUPPER

         MOV     R2,#4
         MOV     DPTR,#NVOL            ; MOV VOLUME VARIABLE TO DATEIMG
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     DATEIMG+4,#0          ; NEED FOR 6 DIGIT ADD
         MOV     DATEIMG+5,#0
         CLR     C
         MOV     R2,#6                 ; 6 BYTES - 12 DIGITS
         MOV     DPTR,#GRTOTN          ; ADD UP ALL THE TOTALS
         MOV     R0,#DATEIMG
         CALL    SUMUPPER

         MOV     R2,#4
         MOV     DPTR,#GVOL            ; MOV VOLUME VARIABLE TO DATEIMG
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         CLR     C
         MOV     R2,#4                 ; 4 BYTES - 8 DIGITS
         MOV     DPTR,#TGVOL           ; ADD UP ALL THE TOTALS
         MOV     R0,#DATEIMG
         CALL    SUMUPPER

         MOV     R2,#4
         MOV     DPTR,#GVOL            ; MOV VOLUME VARIABLE TO DATEIMG
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     DATEIMG+4,#0          ; NEED FOR 6 DIGIT ADD
         MOV     DATEIMG+5,#0
         CLR     C
         MOV     R2,#6                 ; 6 BYTES - 12 DIGITS
         MOV     DPTR,#GRTOTG          ; ADD UP ALL THE TOTALS
         MOV     R0,#DATEIMG
         CALL    SUMUPPER
         RET


; NOW DO IT AGAIN FOR PRODUCT TOTALS
; HAVE TO GET THE PROPER POINTER

ADDPTOT: MOV     R2,#4
         MOV     DPTR,#NVOL            ; MOV VOLUME VARIABLE TO DATEIMG
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     DPTR,#PCNVOL          ; PRCOD NET VOLUME TOTAL
         CALL    PCVPTR
         CLR     C
         MOV     R2,#4                 ; 4 BYTES - 8 DIGITS
         MOV     R0,#DATEIMG
         CALL    SUMUPPER

         MOV     R2,#4
         MOV     DPTR,#GVOL            ; MOV VOLUME VARIABLE TO DATEIMG
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     DPTR,#PCGVOL
         CALL    PCVPTR                ; POINT TO PRODUCT TOTAL
         CLR     C
         MOV     R2,#4                 ; 4 BYTES - 8 DIGITS
         MOV     R0,#DATEIMG
         CALL    SUMUPPER
         RET

CLRSHFT: MOV     R0,#200
         MOV     R1,#4
         MOV     A,#0
         MOV     DPTR,#PCNVOL          ; CLEAR SHIFT TOTALS
CLRSH1:  MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,CLRSH1
         MOV     R0,#200
         DJNZ    R1,CLRSH1
         RET

ADDPLS:
         CLR     C
         MOV     DPTR,#RAWPLS
         MOVX    A,@DPTR
         ADDC    A,#1
         DA      A
         MOVX    @DPTR,A
         INC     DPTR
         MOVX    A,@DPTR
         ADDC    A,#0
         DA      A
         MOVX    @DPTR,A
         INC     DPTR
         MOVX    A,@DPTR
         ADDC    A,#0
         DA      A
         MOVX    @DPTR,A
         RET

ADDFAC1: CLR     C
         MOV     A,MLPR
         ADDC    A,#1
         DA      A
         MOV     MLPR,A
         MOV     A,MLPR+1
         ADDC    A,#0
         DA      A
         MOV     MLPR+1,A
         MOV     A,MLPR+2
         ADDC    A,#0
         DA      A
         MOV     MLPR+2,A
         RET

EQTEST:
         MOV     DATEIMG,RESULT
         MOV     DATEIMG+1,RESULT+1
         MOV     DATEIMG+2,RESULT+2

         MOV     R0,#DATEIMG
         MOV     R2,#3
         CALL    NEGATE                ; DATEIMG = -RESULT

         MOV     R2,#3
         MOV     DPTR,#ENTVOL
         MOV     R1,#DATEIMG+3         ; DATEIMG <= EVOL
         CALL    EXTMOV

         MOV     R2,#3
         SETB    C                     ; EVOL - RESULT
         MOV     R0,#DATEIMG+3
         MOV     R1,#DATEIMG
         CALL    SUMUP                 ; CY==0 IF RESULT > EVOL
         RET

AUTOCAL:
        CALL    SELECTHST             ; ### UNCOMMENT TO DEBUG MPC CALCS OVER HOST PORT

         MOV     MLPR,#0
         MOV     MLPR+1,#70H           ; START CLOSER TO REALITY
         MOV     MLPR+2,#0             ;  CLEAR OUT TEST FACTOR

         MOV     R2,#3
         MOV     DPTR,#RAWPLS
         MOV     R1,#MCND              ; MCND = RAWPLS
         CALL    EXTMOV
         MOV     MCND+3,#0

         MOV     A,#0
         ORL     A,MCND
         ORL     A,MCND+1
         ORL     A,MCND+2
         JNZ     AUTOCAL1
         MOV     DPTR,#MSG63           ; ERR MSG - NO RAW PULSES
         CALL    SHOW
         JMP     AUTODN

AUTOCAL1:

         CALL    ADDFAC1            ; ADD ONE TO MLPR (CALFAC)
                                    ; SUCCSESSIVE ADDITIONS
                                    ; AND MULTIPLICATIONS TIMES RAW PULSES
         CALL    CLRRSLT
         CALL    XLPLY

         CALL    SHIFTR
         CALL    SHIFTR
         CALL    SHIFTR
         CLR     C
         MOV     A,RESULT
         ADDC    A,#05H
         DA      A
         MOV     RESULT,A
         MOV     A,RESULT+1
         ADDC    A,#0
         DA      A
         MOV     RESULT+1,A
         MOV     A,RESULT+2
         ADDC    A,#0
         DA      A
         MOV     RESULT+2,A
         MOV     A,RESULT+3
         ADDC    A,#0
         DA      A
         MOV     RESULT+3,A
         CALL    SHIFTR

         CALL    EQTEST                ; SEE IF RESULT > ENTVOL
         JC      AUTOCAL1              ; KEEP ADDING ONE AT A TIME
                                       ; DONE WITH CALC HERE

         MOV     R7,#8
         MOV     R0,#RESULT+7
AUTOCAL2:MOV     A,@R0
;         CALL    OUTA
         DEC     R0
         DJNZ    R7,AUTOCAL2           ; OUTS RESULT ONLY

         MOV     DATEIMG,#4
         MOV     DATEIMG+1,#0
         MOV     DATEIMG+2,#0

         MOV     R0,#DATEIMG
         MOV     R2,#3
         CALL    NEGATE                ; DATEIMG = -RESULT

         MOV     R2,#3
         SETB    C                     ; EVOL - RESULT
         MOV     R0,#MLPR
         MOV     R1,#DATEIMG
         CALL    SUMUP                 ; CY==0 IF RESULT > EVOL

         MOV     SWARRAY,MLPR+2        ; ACTUAL CALFAC INTO SWARRAY
         MOV     SWARRAY+1,MLPR
         MOV     SWARRAY+2,MLPR+1      ; GOOFY BUT WORKS

         MOV     DPTR,#MPCFLG
         MOVX    A,@DPTR
         JZ      AUTOCAL3
         MOV     DPTR,#CPMFLG
         MOVX    A,@DPTR
         JZ      AUTOCAL3              ; EITHER FLAG OFF DO REGULAR AUTOCAL

; HERE STARTS MP AUTOCAL LOADING

         MOV     DPTR,#FINALFR
         MOVX    A,@DPTR
         PUSH    ACC                   ; CPTPTR WILL CLOBBER

         CALL    CPTPTR
         POP     ACC
         MOVX    @DPTR,A               ; STORE FLOWRATE

         INC     DPTR
         INC     DPTR
         MOV     A,SWARRAY+1
         MOVX    @DPTR,A               ; LSB IN CALPT+2
         INC     DPTR
         MOV     A,SWARRAY+2
         MOVX    @DPTR,A               ; MSB IN CALPT+3

         CALL    TENOUT                ; ### UNCOMMENT TO DEBUG MPC CALCS OVER HOST PORT


         MOV     DPTR,#MPCPTR
         MOVX    A,@DPTR
         INC     A                     ; BUMP MPCPTR AND CHECK FOR ROLLOVER
         CJNE    A,#10,AUTOCA5
         MOV     A,#0
AUTOCA5: MOVX    @DPTR,A

         JMP     AUTODN


AUTOCAL3:CALL    PUTCAL
         MOV     DPTR,#SWAR
         MOV     A,SWARRAY
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,SWARRAY+1
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,SWARRAY+2

         MOVX    @DPTR,A
         CALL    DOSWFAC
         CALL    WAIT50
         CALL    PUTCAL
         MOV     DPTR,#MSG105
         CALL    SHOW

AUTODN:
         CALL    CRLF                 ; ### UNCOMMENT TO DEBUG MPC CALCS OVER HOST PORT
         CALL    CRLF                 ; ### UNCOMMENT TO DEBUG MPC CALCS OVER HOST PORT
         RET


; *****************************************************
; GETCAL GETS THE SWITCH SETTING FOR CURRENT PRCOD
; *****************************************************
GETCAL:  CALL    CALPTR
         MOV     R2,#3
         MOV     R0,#SWARRAY
GETCAL1: MOVX    A,@DPTR
         MOV     @R0,A
         INC     DPTR
         INC     R0
         DJNZ    R2,GETCAL1
         CALL    DOSWFAC
         RET

; *****************************************************
; PUTCAL PUTS THE SWITCH SETTING FOR CURRENT PRCOD
; *****************************************************
PUTCAL:  CALL    CALPTR
         MOV     R2,#3
         MOV     R0,#SWARRAY
PUTCAL1: MOV     A,@R0
         MOVX    @DPTR,A
         INC     DPTR
         INC     R0
         DJNZ    R2,PUTCAL1
         CALL    DOSWFAC
         RET

; ******************************************************
; GETDWL - GETS DWELL FOR CURRENT PRODUCT CODE
; *******************************************************
GETDWL:  CALL    DWLPTR
         MOVX    A,@DPTR
         MOV     S2DWELL,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     S2DWELL+1,A
         RET

; ******************************************************
; PUTDWL - PUTS DWELL FOR CURRENT PRODUCT CODE
; *******************************************************
PUTDWL:  CALL    DWLPTR
         MOV     A,S2DWELL
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,S2DWELL+1
         MOVX    @DPTR,A
         RET

CALPTR:
         MOV     A,PRCOD
         CLR     C
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#3
         MUL     AB
         CLR     C
         MOV     DPTR,#PR1CAL          ; POINT TO FIRST CAL FACTOR
CMPPTR2: ADD     A,DPL
         MOV     DPL,A
         MOV     A,B
         ADDC    A,DPH
         MOV     DPH,A
         RET

DWLPTR:  MOV     A,PRCOD
         CLR     C
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#2
         MUL     AB
         CLR     C
         MOV     DPTR,#PR1DWL          ; POINT TO FIRST DWL FACTOR
         CALL    CMPPTR2
         RET

CMPPTR:
         MOV     A,PRCOD
         CLR     C
         SUBB    A,#1
         MOV     DPTR,#PR1CMP          ; POINT TO FIRST COMP TABLE
CMPPTR1: ADD     A,DPL
         MOV     DPL,A
         MOV     A,#0
         ADDC    A,DPH
         MOV     DPH,A
         RET

CPTPTR:  MOV    DPTR,#MPCPTR
         MOVX   A,@DPTR
         MOV    B,#4                   ; 4 BYTES PER CALPT
         MUL    AB
         MOV    DPTR,#CALPT1
         CLR    C
         CALL   CMPPTR1                ; DPTR MODIFIED HERE
         RET


ADCPTR:  MOV     A,PRCOD
         CLR     C
         SUBB    A,#1
         MOV     B,#2                  ; NOW 2 BYTES PER PRCOD
         MUL     AB
         CLR     C
         MOV     DPTR,#ADDPLCOD        ; POINT TO ADD PROD LABEL CODE
         CALL    CMPPTR1
         RET

ADLPTR:                                ; ACC HAS LINE NUMMBER MODIFIED BY LINE
         CLR     C                     ; COUNTER
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#25
         MUL     AB
         CLR     C
         MOV     DPTR,#ADDPRLBL        ; POINT TO FIRST MESSAGE
         CALL    CMPPTR2
         RET

PCMPTR:  MOV     A,PRCOD
         CLR     C
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#25
         MUL     AB
         CLR     C
         MOV     DPTR,#PMSG1           ; POINT TO FIRST MESSAGE
         CALL    CMPPTR2
         RET

; GET POINTER FOR FIXED CHARGE LABEL AND AMOUNT
FXDMPTR:
         MOV     DPTR,#FIXEDNUM
         MOVX    A,@DPTR
         ANL     A,#0FH                ; MASK MSN TO MAKE BCD 0-9

         CLR     C
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#15
         MUL     AB
         CLR     C
         MOV     DPTR,#FIXEDLBL        ; POINT TO FIRST MESSAGE
         CALL    CMPPTR2
         RET

FXDAPTR:
         MOV     DPTR,#FIXEDNUM
         MOVX    A,@DPTR
         ANL     A,#0FH                ; MASK MSN TO MAKE BCD 0-9

         CLR     C
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#6
         MUL     AB
         CLR     C
         MOV     DPTR,#FIXEDCHG        ; POINT TO FIRST AMOUNT
         CALL    CMPPTR2
         RET


PCTPTR:
         CALL    CMPPTR
         MOVX    A,@DPTR
         CLR     C
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#50
         MUL     AB
         CLR     C
         MOV     DPTR,#PCMSG1          ; POINT TO FIRST MESSAGE
         CALL    CMPPTR2
         RET


PRCPTR:  MOV     A,PRCOD
         CLR     C
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#32
         MUL     AB
         CLR     C
         MOV     DPTR,#PRPRICE         ; POINT TO FIRST MESSAGE
         CALL    CMPPTR2
         RET

QOBPTR:  MOV     A,PRCOD
         CLR     C
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#3
         MUL     AB
         CLR     C
         MOV     DPTR,#QOB1
         CALL    CMPPTR2
         RET


PCVPTR:  MOV     A,PRCOD
         CLR     C
         SUBB    A,#1                  ; ZERO INDEX
         MOV     B,#4
         MUL     AB
         CLR     C
         CALL    CMPPTR2
         RET

; GETS PRICING BY PRODUCT CODE

GETPCPR:
         MOV     R2,#29
         MOV     DPTR,#PRICE
         MOV     R6,DPL
         MOV     R7,DPH                ; SAVE PRICE DPTR

         CALL    PRCPTR                ; GET MATRIX DPTR
GETPC1:
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - SWITCH RAMS
         MOVX    A,@DPTR
         MOV     TEMP,A                ; SAVE IN TEMP
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY - SWITCH RAMS BACK
         PUSH    DPL                   ; SAVE MATRIX DPTR
         PUSH    DPH

         MOV     DPL,R6
         MOV     DPH,R7                ; GET BACK PRICE POINTER
         MOV     A,TEMP
         MOVX    @DPTR,A               ; LOAD FROM TEMP
         INC     DPTR                  ; BUMPO DPTR
         MOV     R6,DPL
         MOV     R7,DPH                ; SAVE IT AGAIN
         POP     DPH
         POP     DPL                   ; RESTORE MATRIX DPTR
         INC     DPTR                  ; BUMP IT TOO
         DJNZ    R2,GETPC1
         RET

; PUTS BACK CURRENT PRICING BY PRCOD

PUTPCPR: MOV     DPTR,#PRICE
         MOV     R1,#ECGALS
         MOV     R2,#29
         CALL    EXTMOV
         CALL    PRCPTR                ; GET POINTER
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     R0,#ECGALS
         MOV     R2,#29
         CALL    CARDMOV
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         RET

COMPQOB:
         MOV     R2,#4
         MOV     DPTR,#NVOL
         MOV     R1,#DATEIMG
         CALL    EXTMOV                ; DATEIMG=NVOL
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    NEGATE                ; DATEIMG= -NVOL
         MOV     R2,#3
         CALL    QOBPTR                ; GET CURRENT QOB POINTER
         MOV     R1,#ECGALS            ; ECGALS=QOB
         CALL    EXTMOV
         MOV     R2,#4
         SETB    C                     ; PREAMT-CGALS
         MOV     R0,#ECGALS
         MOV     R1,#DATEIMG
         CALL    SUMUP                 ; CY==0 IF GCST > PREAMT
         MOV     R2,#3
         CALL    QOBPTR
         MOV     R0,#ECGALS
         CALL    CARDMOV
         RET



;******************************************
; FORM NEGATIVE OF BCD NUMBER  999999 - NUM
; 99 - (R0) ; R2 TIMES
;******************************************
NEGATE:  CLR     C
         MOV     A,#99H
         SUBB    A,@R0
         MOV     @R0,A
         INC     R0
         DJNZ    R2,NEGATE
         RET


;******************************************
; sums into upper memory
;  (DPTR) += (R0)  : R2
;******************************************
SUMUPPER:MOVX    A,@DPTR
         ADDC    A,@R0
         DA      A
         MOVX    @DPTR,A
         INC     R0
         INC     DPTR
         DJNZ    R2,SUMUPPER
         RET

;************************************************
;  MESSSAGE MOVER. MOVE FROM @DPTR INTO @R1
;************************************************
;  EXTENDED MEMORY MOVE FROM @DPTR INTO @R1
;************************************************
EXTMOV:  MOVX    A,@DPTR               ;  FROM
         MOV     @R1,A                 ;    TO
         INC     R1
         INC     DPTR
         DJNZ    R2,EXTMOV
         RET

;*************************************************
; BUMPTKT INCREMENTS THE TKT NUMBER BY ONE
;*************************************************
BUMPTKT: MOV     R2,#3
         MOV     DPTR,#TKTNO
         JMP     DOBUMP

DOBUMP:
         SETB    C
TKCT1:   MOVX    A,@DPTR
         ADDC    A,#0                  ; PROPAGATE CARRY
         DA      A
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R2,TKCT1              ; R2 BYTES
         RET

BUMPTMT: SETB    C
         MOV     R2,#6
         MOV     DPTR,#TMT
TMT1:    MOVX    A,@DPTR
         ADDC    A,#0                  ; PROPAGATE CARRY
;         DA      A
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R2,TMT1               ; 3 BYTES
         RET

BUMPCAL: MOV     R2,#3
         MOV     DPTR,#CALNO
         JMP     DOBUMP

;         SETB    C
;CALT1:   MOVX    A,@DPTR
;         ADDC    A,#0                  ; PROPAGATE CARRY
;         DA      A
;         MOVX    @DPTR,A
;         INC     DPTR
;         DJNZ    R2,CALT1              ; 3 BYTES
;         RET

BUMPCON: MOV     R2,#3
         MOV     DPTR,#CONNO
         JMP     DOBUMP

;         SETB    C
;CONT1:   MOVX    A,@DPTR
;         ADDC    A,#0                  ; PROPAGATE CARRY
;         DA      A
;         MOVX    @DPTR,A
;         INC     DPTR
;         DJNZ    R2,CONT1              ; 3 BYTES
;         RET

; **********************************************
; SELECTP0 - SELECTS COM TO PORT0 -
; **********************************************
SELECTP0:CALL    WAIT3
         MOV     IE,#0
;         MOV     TH1,#-3               ; 9600 BAUD
         CLR     SERSELA
         CLR     SERSELB
         CALL    INPA
         CLR     SCON0_RI
         CLR     C
         RET

; **********************************************
; SELECTP1 - SELECTS COM TO PORT1 -
; **********************************************
SELECTP1:CALL    WAIT3
         MOV     IE,#0
;         MOV     TH1,#-3               ; 9600 BAUD
         SETB    SERSELA
         CLR     SERSELB
         CALL    INPA
         CLR     SCON0_RI
         CLR     C
         RET

; **********************************************
; SELECTP2 - SELECTS COM TO PORT2 -
; **********************************************
SELECTP2:
         MOV     DPTR,#SNDDUP
         MOVX    A,@DPTR
         JNZ     SELECTP2R             ; TEST IF SENDING DUP TICKET TO HOST
         CALL    LODHCMDFLG
         JZ      SELECT2D
         CALL    SELECTHST             ; REDIDECT TO HOST FOR CAL TICKET
SELECTP2R:
         RET

SELECT2D:CALL    SELECTP0

         MOV     A,#1FH
         CALL    OUTA                  ; SIGNAL COMMAND TO COME
         MOV     DPTR,#REGNUM
         MOVX    A,@DPTR
         CJNE    A,#1,SELECT2A
         MOV     A,#12                 ; LOCK COMMAND FIRST
         CALL    OUTA
         MOV     A,#1FH
         CALL    OUTA
         MOV     A,#5                  ; REGULAR COMMAND
         CALL    OUTA
         JMP     SELECT2B
SELECT2A:MOV     A,#13
         CALL    OUTA
         MOV     A,#1FH
         CALL    OUTA
         MOV     A,#6
         CALL    OUTA
SELECT2B:
         CALL    WAIT3               ; SET PRINTER BAUD RATE
         MOV     IE,#0
;         MOV     TH1,#-3               ; 9600 BAUD
         CLR     SERSELA
         SETB    SERSELB
         CALL    INPA
         CLR     SCON0_RI
         CLR     C

         MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#4,SELECT2B1        ; 4 = ZEBRA RW-420 V1 PRINTER = NO BMFF
         JMP     SELECT2BZ
SELECT2B1:
         CJNE    A,#14,SELECT2C        ; 14 = ZEBRA RW-420 V2 = DO BMFF AT AND OF TICKET
SELECT2BZ:

         MOV     R1,#12
         MOV     DPTR,#TMSG3
         CALL    MX1                   ; SET TO JOURNAL MODE ( NO FORM)

         MOV     R1,#17
         MOV     DPTR,#TMSG1           ; SETUP FOR TEXT
         CALL    MX1
         MOV     R1,#19
         MOV     DPTR,#TMSG8           ; SETUP FOR TEXT
         CALL    MX1

         MOV     R1,#15
         MOV     DPTR,#TMSG2
         CALL    MX1

SELECT2C:RET

MX1:     CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,MX1
         CALL    CRLF
         RET


; **********************************************
; SELECTP3 - SELECTS COM TO PORT3 -
; **********************************************
SELECTP3:CALL    WAIT3
         MOV     IE,#0
;         MOV     TH1,#-3               ; 9600 BAUD
         SETB    SERSELA
         SETB    SERSELB
         CALL    INPA
         CLR     SCON0_RI
         CLR     C
         RET

; ******************************************************
; SELECTHST - SELECTS CORRECT HOST COM PORT FOR LT OR EC
; ******************************************************
SELECTHST:
        ;CALL    SELECTP3              ; <<<< LT
         CALL    SELECTP1              ; ECOUNT
         RET

; **************************************************
; CON2HST  CONNNECT E:COUNT TO HOST AFTER PRINTING
; **************************************************
CON2HOST:CALL    SELECTP0
         MOV     A,#1FH
         CALL    OUTA
         MOV     DPTR,#REGNUM
         MOVX    A,@DPTR
         CJNE    A,#1,CON2HSTA
         MOV     A,#2
         JMP     CON2HSTB
CON2HSTA:MOV     A,#3
CON2HSTB:CALL    OUTA
         RET

; **************************************************
; CON2AUX  CONNNECT E:COUNT TO AUXILLARY PORT
; **************************************************
CON2AUX: CALL   SELECTP0
         MOV    A,#1FH
         CALL   OUTA                   ; PCM COMMAND START
         MOV    DPTR,#REGNUM
         MOVX   A,@DPTR
         CJNE   A,#01H,CON2AU1
         MOV    A,#07H
         CALL   OUTA                   ; CONNECT REG1 TO AUX
         JMP    CON2AUZ
CON2AU1: MOV    A,#08H
         CALL   OUTA                   ; CONNECT REG2 TO AUX
CON2AUZ: RET


; **********************************************
; SELECT92 - SELECTS 1092 ADC FOR COMPENSATOR
; REV E
; **********************************************
SELECT92:
;        CLR     NCS
         SETB    PLS6K
         CLR     DPOTCS
         RET

; **********************************************
; SELECTDP - SELECTS DIGITAL POT
; REV E
; **********************************************
SELECTDP:
;        CLR     NCS
         CLR     PLS6K
         SETB    DPOTCS
         RET

; **********************************************
; SELECT48 - SELECTS TLC548 ADC
; NOT USED REV E
; **********************************************
; SELECT48:CLR     NCS
;          CLR     PLS6K
;          CLR     DPOTCS
;          RET

; **********************************************
; SELECTLD - SELECTS LED, DESELECTS CURRENT DEVICE
; SELECTS 0 OUTPUT. LED2 REV E
; **********************************************
SELECTLD:
;        SETB    NCS
         CLR     PLS6K
         CLR     DPOTCS
         RET

;******************************************************
; DISCON: DISCONNECTS PCM CONNECTION
; ******************************************************
DISCON:  CALL    SELECTP0
         MOV     A,#1FH
         CALL    OUTA
         MOV     A,#14                 ; UNLOCK COMMAND
         CALL    OUTA
         MOV     DPTR,#REGNUM
         MOVX    A,@DPTR
         CALL    OUTA                  ; SEND REG ID. MUST MATCH PCM LOCK BYTE
         MOV     A,#0FFH
         CALL    OUTA
         RET

; *****************************************************
; OUTA WAITS FOR NOT BUSY ON '51 UART
; CALLED WITH CHARACTER IN ACC TO BE OUTPUT TO PC
OUTA:    JNB     SCON0_TI,$                  ; RIGHT HERE TILL UART RDY
         CLR     SCON0_TI                    ; S/W MUST CLEAR IT(SEE SCON)
         MOV     SBUF0,A                ; NOW OUTPUT CHARACTER
         RET

; THIS ONE KILLS LEADING ZEROES
OUTAZ:
         JNB     SCON0_TI,$                  ; RIGHT HERE TILL UART RDY
         CLR     SCON0_TI                    ; S/W MUST CLEAR IT(SEE SCON)
         MOV     TEMP,A                ; SAVE A
         CJNE    A,#'0',OUTAZ2         ; CHECK FOR 0
         MOV     A,ZERO                ; CHECK IF LEADING ZERO (0=LEADING)
         JNZ     OUTAZ3
         MOV     A,#' '                ; MAKE ZERO A SPACE
         JMP     OUTAZ1
OUTAZ2:  MOV     ZERO,#1               ; FLAG LAST LEADING ZERO
OUTAZ3:  MOV     A,TEMP
OUTAZ1:  MOV     SBUF0,A                ; NOW OUTPUT CHARACTER
         RET


; *****************************************************
; INPA BYPASSES IF NO CHAR READY.  USES CY TO SIGNIFY STATE
; IF NO CHAR READY THEN RETURN WITH CY=0 ELSE SET CY=1
;  AND PUT CHAR IN ACC AND RETURN
INPA:    CLR     C                     ; CLEAR IS DEFAULT CASE
         JNB     SCON0_RI,INPADONE           ; IF RI=0 NO CHAR READY
         CLR     SCON0_RI                    ; IF SET THEN WE MUST CLEAR
         MOV     A,SBUF0                ;   SO GET IT THEN
         SETB    C                     ;   AND INDICATE
INPADONE:RET                           ; RETURN EITHER WAY

CRLF:    MOV     A,#0DH
         CALL    OUTA
SENDLF:  MOV     A,#0AH
         CALL    OUTA
         RET

DECDPTR: MOV     A,DPL                 ; LOOK AT DPL
         JNZ     DECDPTR1              ; IF ZERO WE ARE AT 1 00
         DEC     DPH                   ; SO DROP DPH
DECDPTR1:DEC     DPL                   ; ALWAYS DROP DPL
         RET

GETUNIT: MOV     DPTR,#UNITS
         MOVX    A,@DPTR
         JZ      GETUNIT1
         SETB    PSW_F0
         JMP     GETUNIT2
GETUNIT1:CLR     PSW_F0
GETUNIT2:RET


;**************************************************
;** PRINT GROSS FUNCTIONS
;**
;** CKPGRSON = CHECK IF PRINT GROSS VOL
;** RETURNS PSW_F0=CLR IF SKIP VOL, ELSE RETURNS PSW_F0=SET
;**************************************************
CKPGRSPGRS:
         CLR     PSW_F0
         MOV     DPTR,#GRSFLG          ; GRSFLG: 0=OFF, 1=ON, 2=NOTEMP (PRINT ALL EXCEPT TEMP), 3=TEMP (SKIP GVOL)
         MOVX    A,@DPTR
         JZ      CKPGRSX               ; CKPGRSPGRS: PSW_F0=CLR=OFF (0,3); PSW_F0=SET=ON (1,2)
         CJNE    A,#3,CKPGRSY          ; CKPGRSPTMP: PSW_F0=CLR=OFF, PSW_F0=SET=ON=PRINT GROSS VOLUMES
         JMP     CKPGRSX
CKPGRSY: SETB    PSW_F0
CKPGRSX: RET

;**************************************************
;** CKPGRSPTMP = CHECK IF PRINT GROSS PRINTS TEMP
;**  OFF(0), NOTEMP(2) SKIP TEMP PRINT
;**  ON(1), TEMP(3) PRINT TEMP
;** RETURNS PSW_F0=SET IF GRSFLG=1, ELSE RETURNS PSW_F0=CLR
;**************************************************
CKPGRSPTMP:
         CLR     PSW_F0
         MOV     DPTR,#GRSFLG          ; GRSFLG: 0=OFF, 1=ON, 2=NOTEMP (PRINT ALL EXCEPT TEMP), 3=TEMP (SKIP GVOL)
         MOVX    A,@DPTR
         JZ      CKPGRSX               ; CKPGRSPTMP: PSW_F0=CLR=OFF (0,2); PSW_F0=SET=ON (1,3)
         CJNE    A,#2,CKPGRSY          ; CKPGRSPTMP: PSW_F0=CLR=OFF, PSW_F0=SET=ON=PRINT TEMP
         JMP     CKPGRSX


; RRF ROTATES THE RES AREA RIGHT FOUR BITS.  ONLY DOES
;  RES+2..RES
RRF:     MOV     R2,#4                 ; DO FOUR BITS WORTH
RRF1:    MOV     R1,#4                 ; DO THREE BYTES
         CLR     C                     ;  INITIALIZE CY BIT
         MOV     R0,#RES+3             ; TOP DOWN POINTER
RRF2:    MOV     A,@R0                 ; GET BIT
         RRC     A                     ; OVER ONE PLACE
         MOV     @R0,A                 ; BACK INTO POSITION
         DEC     R0                    ;
         DJNZ    R1,RRF2               ; THREE BYTES
         DJNZ    R2,RRF1               ;   FOUR TIMES
         RET
; THIS ROTATE DOES 4 BYTES
RRR:     MOV     R2,#4                 ; DO FOUR BITS WORTH
RRR1:    MOV     R1,#4                 ; DO FOUR BYTES
         CLR     C                     ;  INITIALIZE CY BIT
         MOV     R0,#RES+2             ; TOP DOWN POINTER
RRR2:    MOV     A,@R0                 ; GET BIT
         RRC     A                     ; OVER ONE PLACE
         MOV     @R0,A                 ; BACK INTO POSITION
         DEC     R0                    ;
         DJNZ    R1,RRR2               ; THREE BYTES
         DJNZ    R2,RRR1               ;   FOUR TIMES
         RET

SAVE:    MOV     DPTR,#NVOL
         MOV     A,#56H
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,#34H
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,#12H
         MOVX    @DPTR,A

; SAVES DATETIME @DPTR
SAVTIME: MOV     R1,#8
         MOV     R0,#DATEIMG
SAVTIM1: MOV     A,@R0
         MOVX    @DPTR,A
         INC     DPTR
         INC     R0
         DJNZ    R1,SAVTIM1
         RET

; RETREIVES OLD DATETIME
RETTIME: MOV     R1,#8
         MOV     R0,#DATEIMG
RETTIM1: MOVX    A,@DPTR
         MOV     @R0,A
         INC     DPTR
         INC     R0
         DJNZ    R1,RETTIM1
         RET

RTIME:   CALL    ESTART
         MOV     A,#0DEH              ; DUMMY WRITE
         CALL    SHOUT
         MOV     A,#0
         CALL    SHOUT
         CALL    ESTART
         MOV     A,#0DFH
         CALL    SHOUT
         CALL    SHIN
         CALL    ACK
;        MOV     R1,A                  ; SECONDS
         MOV     DATEIMG+1,A
         CALL    SHIN
         CALL    ACK
         MOV     DATEIMG+2,A           ; MINUTE
         CALL    SHIN
         CALL    ACK
         ANL     A,#7FH                ; KILL MIL BIT
         MOV     DATEIMG+3,A           ; HOUR
         CALL    SHIN
         CALL    ACK
         MOV     DATEIMG+5,A           ; DAY
         CALL    SHIN
         CALL    ACK
         MOV     DATEIMG+6,A           ; MONTH
         CALL    SHIN
         MOV     DATEIMG+7,A           ; YEAR
         CALL    EESTOP
         RET

WTIME:   MOV     R3,#SC
         MOV     R4,#0                 ; START SECONDS AT 00
         CALL    WBYTE
         MOV     R3,#MIN
         MOV     R4,DATEIMG+2          ; MINUTE
         CALL    WBYTE

         MOV     R3,#HR
         MOV     A,DATEIMG+3
         ORL     A,#80H
         MOV     R4,A
;         MOV     R4,DATEIMG+3         ; HOUR
         CALL    WBYTE
         MOV     R3,#DT
         MOV     R4,DATEIMG+5          ; DAY
         CALL    WBYTE
         MOV     R3,#MO
         MOV     R4,DATEIMG+6          ; MONTH
         CALL    WBYTE
         MOV     R3,#YR
         MOV     R4,DATEIMG+7          ; YEAR
         CALL    WBYTE
         RET

WBYTE:
         CALL    ESTART
         MOV     A,#0DEH
         CALL    SHOUT
         MOV     A,R3                  ; ADDRESS
         CALL    SHOUT
         MOV     A,R4                  ; DATA
         CALL    SHOUT
         CALL    EESTOP
         RET



; I2C ROUTINES

ESTART:
        SETB    SDA
        SETB    SCL
        JNB     SDA,ESTAR1
        JNB     SCL,ESTAR1    ; VERIFY BUS READY
        CALL    WAIT2U
        CLR     SDA
        CALL    WAIT2U
        CLR     SCL
        CLR     C
        JMP     ESTAR2
ESTAR1: SETB    C
ESTAR2: RET

EESTOP: CLR     SDA
        CALL    WAIT2U
        SETB    SCL
        CALL    WAIT2U
        SETB    SDA
        RET

SHOUT:  MOV     B,#8     ; BIT COUNTER
SHOUT1: RLC     A
        MOV     SDA,C
        CALL    WAIT2U
        SETB    SCL
        CALL    WAIT2U
        CLR     SCL
        DJNZ    B,SHOUT1
        SETB    SDA       ; RELEASE SDA FOR ACK
        CALL    WAIT2U
        SETB    SCL       ; RAISE ACK CLOCK
        CALL    WAIT2U
        MOV     C,SDA
        CLR     SCL
        RET

SHIN:   SETB    SDA       ; MAKE AN INPUT
        MOV     B,#8      ; BIT COUNTER
SHIN1:  CALL    WAIT2U
        SETB    SCL
        CALL    WAIT2U
        MOV     C,SDA
        RLC     A
        CLR     SCL
        DJNZ    B,SHIN1
        RET

ACK:    CLR     SDA       ; ACK BIT
        CALL    WAIT2U
        SETB    SCL       ; RAISE CLOCK
        CALL    WAIT2U
        CLR     SCL       ; DROP CLOCK
        RET

NAK:    SETB    SDA       ; NAK BIT
        CALL    WAIT2U
        SETB    SCL       ; RAISE CLOCK
        CALL    WAIT2U
        CLR     SCL
        RET

;******************************************************
; HELPER FOR OUTAZ REDUNDANCY
DOOUTAZ:
         MOV     A,@R0
DOOUTAZ1:CALL    MAKEASC
         CALL    OUTAZ
         MOV     A,B
         CALL    OUTAZ
         DEC     R0
         RET

;*******************************************************
; PRICECHK CHECKS MONEYFLG TO SEE IF PRICEING IS:
;  0=ALL PRICING OFF
;  1=PRICE CHECK = ONLY DO PRICING OF PRICE>0 (WAS 'ON')
;  2=ALWAYS PRINT PRICING (SO FEES/TAX CHARGED EVEN IF PRICE=0
; RETURNS:
;  RETURNS 0 IN ACC IF PRICE=0 OR IF PRICING OFF
;  RETURNS 1 IN ACC IF PRICE>0 OR IF PRICING ON
;
PRICECHK:
         MOV     DPTR,#MONEYFLG
         MOVX    A,@DPTR
         JZ      PRICEOFF
         CJNE    A,#2,PRICECH0
         JMP     PRICEON
PRICECH0:MOV     R4,#3
         MOV     R0,#CPGAL
         MOV     A,#0
PRICECH1:ORL     A,@R0
         INC     R0                    ; IF CPGAL == 0 SKIP ALL DODELIVER PRINT
         DJNZ    R4,PRICECH1           ; WILL LEAVE 0 IN ACC A IF ALL 3 BYTES,
         JMP     PRICEEXT              ;  OR NON-0 IF PRICE IS NON-0
PRICEOFF:MOV     A,#0                  ; LEAVE 0 IN ACCA = SAME RESULT AS NO PRICE
         JMP     PRICEEXT
PRICEON: MOV     A,#1                  ; LEAVE NON-0 IN ACCA = SAME RESULT AS PRICE OK
PRICEEXT:RET


VOLUMCHK:MOV     TEMP,#0
         MOV     R4,#4
         MOV     DPTR,#NVOL
VOLCHK1: MOVX    A,@DPTR
         ORL     TEMP,A
         INC     DPTR
         DJNZ    R4,VOLCHK1
         MOV     A,TEMP
         RET


DOPRINT: MOV     SECTAX,#0

         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JNZ     PR65N

         CALL    PRICECHK
         CJNE    A,#0,DOPRIN12
         JMP     DOPRINDN

DOPRIN12:CALL    VOLUMCHK
         CJNE    A,#0,DOPRIN10
         JMP     DOPRINDN

PR65N:   MOV     R2,#6
         MOV     R0,#PGCST
         MOV     A,#0
DOP12:   MOV     @R0,A
         INC     R0
         DJNZ    R2,DOP12              ; CLEAR OUT TOTAL

DOPRIN10:MOV     DPTR,#PRINTFLG
         MOVX    A,@DPTR
         JZ      DOPRIN11              ; SKIP TO VOL CALCS IF DOING PRESET CALCULATIONS INSTEAD OF PRINTING


         CALL    GETUNIT
         JNZ     DOP19

         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JZ      P65A
         MOV     DPTR,#PRMSG56
         JMP     P65B
P65A:    MOV     DPTR,#PRMSG39
P65B:    MOV     R1,#16
         CALL    SHDP20
         JMP     DOP20
DOP19:   MOV     DPTR,#PRMSG47
         MOV     R1,#16
         CALL    SHDP20
DOP20:   MOV     R1,#16

         CALL    BLAST                 ; PRICE/UNIT
DOP1:    CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DOP1
         MOV     A,#' '
         CALL    OUTA
         CALL    OUTA

         MOV     ZERO,#0
         MOV     R1,#2
         MOV     DPTR,#PRICE
DOP2:    MOVX    A,@DPTR
         CALL    OUTAZ
         INC     DPTR
         DJNZ    R1,DOP2
         MOV     A,#'.'
         CALL    OUTAZ                 ; OUTAZ SUPPRESSES LEADING ZEROS
         MOV     R1,#4
DOP3:    MOVX    A,@DPTR
         CALL    OUTAZ
         INC     DPTR
         DJNZ    R1,DOP3
         CALL    PLINEEND

DOPRIN11:CALL    CLMRMC                ; CLEAR OUT MLPR AND MCND
         MOV     MLPR,CPGAL            ; MLPR  00 XX.XX XX
         MOV     MLPR+1,CPGAL+1
         MOV     MLPR+2,CPGAL+2
         MOV     MLPR+3,#0
         MOV     R2,#4
         MOV     DPTR,#NVOL
         MOV     R1,#MCND
         CALL    EXTMOV                ; PUT XGALS IN MCND

DOPRIN1E:CALL    XLRRES
         CALL    XLPLY                 ; RESULT = 00 00 XX XX XX XX.XX XX
         CALL    SHIFTR
         CALL    SHIFTR
         CALL    ROUNDEM               ; ROUNDEM PUTS IN FORM  XX--XX.XX
         MOV     R2,#4
         MOV     R0,#RESULT
         MOV     R1,#GCST
         CALL    MOVER                 ; GCST := RESULT

         MOV     DPTR,#PRINTFLG
         MOVX    A,@DPTR
         JZ      SKP2


         MOV     R2,#4
         MOV     R0,#RESULT
         MOV     R1,#DATEIMG
         CALL    MOVER
         MOV     R2,#4
         MOV     R0,#DATEIMG+3
         MOV     DPTR,#ACOST
         CALL    MOVDECASC             ; MOVE FROM RAM TO HIMEM

         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JZ      P65C
         MOV     DPTR,#PRMSG56
         JMP     P65D

P65C:    MOV     DPTR,#PRMSG40
P65D:    MOV     R1,#16
         CALL    BLAST
DOP4:    CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DOP4

         MOV     ZERO,#0
         MOV     R0,#GCST+3
         CALL    DOOUTAZ
         CALL    DOOUTAZ
         MOV     A,@R0
         CALL    MAKEASC
         CALL    OUTAZ
         MOV     A,B
         CALL    OUTAZ
         MOV     A,#'.'
         CALL    OUTAZ
         DEC     R0
         MOV     A,@R0
         CALL    MAKEASC
         CALL    OUTAZ
         MOV     A,B
         CALL    OUTAZ
         CALL    PLINEEND

         MOV     DPTR,#B4FLG
         MOVX    A,@DPTR
         JNZ     SKIP9

         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JNZ     SKP2

         CALL    PFEED

SKP2:
SKIP9:
         MOV     DPTR,#REPRFLG
         MOVX    A,@DPTR
         JZ      SKP10
         CALL    PRCPTR
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     R1,#31
X12:     INC     DPTR
         DJNZ    R1,X12
         MOVX    A,@DPTR
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         MOV     DPTR,#FIXEDTAX
         MOVX    @DPTR,A


         CALL    PRCPTR
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     R1,#29
X11:     INC     DPTR
         DJNZ    R1,X11                ; MOV OVER 29 SPACES
         MOVX    A,@DPTR               ; GET FEE TABLE VALUE
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         MOV     DPTR,#FIXEDNUM
         MOVX    @DPTR,A               ; STORE IT

         MOV     DPTR,#FIXEDTAX
         MOVX    A,@DPTR
         CJNE    A,#'Y',DOP28
         CALL    FIXED                 ; COMPUTE FIXED

         CALL    PRCPTR
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     R1,#30
X13:     INC     DPTR
         DJNZ    R1,X13                ; MOV OVER 30 SPACES
         MOVX    A,@DPTR               ; GET FEE TABLE VALUE
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         MOV     DPTR,#FIXEDNUM
         MOVX    @DPTR,A               ; STORE IT
         CALL    FIXED
SKP10:
DOP28:   CALL    XLRRES                ; CLEAR RESULT AREA
         MOV     DATEIMG,GCST
         MOV     DATEIMG+1,GCST+1
         MOV     DATEIMG+2,GCST+2
         MOV     DATEIMG+3,GCST+3      ; SAVE FOR SECOND TAX

         MOV     PGCST,GCST
         MOV     PGCST+1,GCST+1
         MOV     PGCST+2,GCST+2
         MOV     PGCST+3,GCST+3

         MOV     DPTR,#FIXEDTAX
         MOVX    A,@DPTR
         CJNE    A,#'Y',DOPRIN2        ; SAME AS SKP3

         MOV     DPTR,#PRINTFLG
         MOVX    A,@DPTR
         JZ      SKP3

         MOV     DPTR,#S1FLG
         MOVX    A,@DPTR
         JNZ     SKIP10

         MOV     DPTR,#PRTFLG          ; THE BLASTER PRINTER DOESN'T SUPPORT LEADING SPACES IN TEXT COMMANDS, NEED TO SEND A LINE OF CHARS
         MOVX    A,@DPTR
         CJNE    A,#5,DOP28A           ; 5 = BLASTER V1, USE SOLID ADDITION LINE
         MOV     DPTR,#PRMSG51A        ; ADDITION LINE FOR BLASTER
         JMP     DOP28Z1
DOP28A:
         CJNE    A,#11,DOP28Z          ; 11 = BLASTER V2, USE SOLID ADDITION LINE
         MOV     DPTR,#PRMSG51A        ; ADDITION LINE FOR BLASTER
         JMP     DOP28Z1
DOP28Z:
         MOV     DPTR,#PRMSG51         ; ELSE USE STANDARD ADDITION LINE
DOP28Z1:
         CALL    PRTCMSG

SKIP10:  MOV     DPTR,#PRMSG49         ; SUBTOTAL
         MOV     R1,#14
         CALL    BLAST
DOP25:   CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DOP25

         MOV     ZERO,#0
         MOV     R0,#PGCST+4
         MOV     A,@R0
         ANL     A,#0FH
         CALL    DOOUTAZ1
         CALL    DOOUTAZ
         CALL    DOOUTAZ
         MOV     A,@R0
         CALL    MAKEASC
         CALL    OUTAZ
         MOV     A,B
         CALL    OUTAZ
         MOV     A,#'.'
         CALL    OUTAZ
         DEC     R0
         CALL    DOOUTAZ
         CALL    PLINEEND

         MOV     DPTR,#B5FLG
         MOVX    A,@DPTR
         JNZ     SKIP11

         CALL    PFEED

SKP3:
SKIP11:
DOPRIN2:
         CALL    NOTAX

         MOV     DPTR,#PRINTFLG
         MOVX    A,@DPTR
         JZ      SKP4

         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JNZ     PR65P

         CALL    PRTTR1

SKP4:    MOV     SECTAX,#1
         CALL    SWAPTAX
         CALL    XLRRES
         CALL    NOTAX

         MOV     DPTR,#PRINTFLG
         MOVX    A,@DPTR
         JZ      SKP5

         CALL    PRTTR2

PR65P:   MOV     R2,#4
         MOV     R0,#PGCST
         MOV     R1,#DATEIMG
         CALL    MOVER
         MOV     R2,#4
         MOV     R0,#DATEIMG+3
         MOV     DPTR,#ATCOST
         CALL    MOVDECASC
SKP5:
X10:
         MOV     DPTR,#REPRFLG
         MOVX    A,@DPTR
         JZ      SKP11

         MOV     DPTR,#FIXEDTAX
         MOVX    A,@DPTR
         CJNE    A,#'N',DOP26

         CALL    PRCPTR
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     R1,#29
X14:     INC     DPTR
         DJNZ    R1,X14                ; MOV OVER 29 SPACES
         MOVX    A,@DPTR               ; GET FEE TABLE VALUE
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         MOV     DPTR,#FIXEDNUM
         MOVX    @DPTR,A               ; STORE IT
         CALL    FIXED

         CALL    PRCPTR
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     R1,#30
X15:     INC     DPTR
         DJNZ    R1,X15                ; MOV OVER 30 SPACES
         MOVX    A,@DPTR               ; GET FEE TABLE VALUE
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         MOV     DPTR,#FIXEDNUM
         MOVX    @DPTR,A               ; STORE IT
         CALL    FIXED

SKP11:
DOP26:
         MOV     DPTR,#PRINTFLG
         MOVX    A,@DPTR
         JNZ     SKP6A
         JMP     SKP6

SKP6A:   MOV     DPTR,#S2FLG
         MOVX    A,@DPTR
         JNZ     SKIP12

         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JNZ     SKIP12

         MOV     DPTR,#PRTFLG          ; THE BLASTER PRINTER DOESN'T SUPPORT LEADING SPACES IN TEXT COMMANDS, NEED TO SEND A LINE OF CHARS
         MOVX    A,@DPTR
         CJNE    A,#5,SKP6AA           ; 5 = BLASTER V1, USE SOLID ADDITION LINE
         MOV     DPTR,#PRMSG51A        ; ADDITION LINE FOR BLASTER
         JMP     SKP6AZ1
SKP6AA:
         CJNE    A,#11,SKP6AZ          ; 11 = BLASTER V2, USE SOLID ADDITION LINE
         MOV     DPTR,#PRMSG51A        ; ADDITION LINE FOR BLASTER
         JMP     SKP6AZ1
SKP6AZ:
         MOV     DPTR,#PRMSG51         ; ELSE USE STANDARD ADDITION LINE
SKP6AZ1:
         CALL    PRTCMSG


SKIP12:  MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JZ      PR65G
         MOV     DPTR,#PRMSG56
         JMP     PR65H

PR65G:   MOV     DPTR,#PRMSG43         ; AMOUNT DUE
PR65H:   MOV     R1,#14
         CALL    BLAST
DOP8:    CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DOP8

         MOV     ZERO,#0
         MOV     R0,#PGCST+4
         MOV     A,@R0
         ANL     A,#0FH
         CALL    DOOUTAZ1
         CALL    DOOUTAZ
         CALL    DOOUTAZ
         MOV     A,@R0
         CALL    MAKEASC
         CALL    OUTAZ
         MOV     A,B
         CALL    OUTAZ
         MOV     A,#'.'
         CALL    OUTAZ
         DEC     R0
         CALL    DOOUTAZ
         CALL    PLINEEND

         MOV     DPTR,#DOLTOT          ; ADD $$ TOTAL TO SHIFT TOTAL
         CLR     C
         MOV     R1,#PGCST
         MOV     R2,#5
DOPRIN4: MOVX    A,@DPTR
         ADDC    A,@R1
         DA      A
         MOVX    @DPTR,A
         INC     R1
         INC     DPTR
         DJNZ    R2,DOPRIN4
SKP6:
DOPRINDN:RET


;******************************************
; DOES TAX COMPUTATION IF TAX CODE==0
;******************************************
NOTAX:
         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JNZ     NOTAXOK

         CALL    PRICECHK              ; PRICE=0 THEN FORGET IT.
         CJNE    A,#0,NOTAXOK
         JMP     NTAX

NOTAXOK:
         MOV     R2,#3
         MOV     R0,#FEDTX             ;                %  $
         MOV     R1,#MLPR              ; FEDTAX TO MLPR .XX.XX XX
         CALL    MOVER                 ; MCND STILL HAS XGALS
         MOV     MLPR+3,#0             ; MCND XX XX XX X.X
         MOV     DPTR,#TAXTYPE         ; DETERMINE MODE OF OPERATION
         MOVX    A,@DPTR               ;  FROM TAX TYPE
         CJNE    A,#'$',PCT            ; TAXTYPE IN ASCII
; '0'==DOLLAR  '1'==PCT
; IF $/GAL PRICING :  XGALS = MCND = XX XX XX X.X
;                    FEDTAX = MLPR = 00 XX.XX XX
; AND RESULT OF XLPLY =   00 00 00 0X XX X.X XX XX
         MOV     R2,#3
         MOV     DPTR,#NVOL
         MOV     R1,#MCND
         CALL    EXTMOV
         MOV     MCND+3,#0
         CALL    XLRRES
         CALL    XLPLY                 ; 00...XX X.X XX XX
         CALL    SHIFTR                ; 00...XX XX. XX XX
         CALL    SHIFTR                ; 00...0X XX X.X XX
         SJMP    PREP                  ;  CPG * XGALS (SKIP PCT)
; IF PERCENT ($/$) PRICING GCST = MCND = XX XX XX.XX
;                        FEDTAX = MLPR = 00.XX XX XX
; NOTE DEC PT NOT TREATED SAME FOR FEDTAX HERE
; AND RESULT OF XLPLY =  00 00 00 XX.XX XX XX XX  DOLS
PCT:     MOV     R2,#4                 ; PUT 4 BYTES OF TOTAL COST
         MOV     R0,#GCST              ;  INTO MULTIPLICAND  ..XX.XX
         MOV     R1,#MCND              ;  NOW GCST * FEDTAX
         CALL    MOVER
         CALL    XLRRES
         CALL    XLPLY                 ; RESULT 00--XX.XX XX XX XX  DOLS
         CALL    SHIFTR                ;         ...XX X.X XX XX XX
         CALL    SHIFTR                ; RESULT     00..XX.XX XX XX
         CALL    SHIFTR                ;                  X.X XX XX
         CALL    SHIFTR                ;                   XX.XX XX
         CALL    SHIFTR                ;                   XX X.X XX
PREP:    CALL    ROUNDEM
         MOV     R2,#4
         MOV     R0,#RESULT
         CLR     A
ZEROCHK: ORL     A,@R0
         INC     R0
         DJNZ    R2,ZEROCHK
         JNZ     ZERCHK2

         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JNZ     ZERCHK2

         JMP     NTAX

ZERCHK2:
         MOV     DPTR,#PRINTFLG
         MOVX    A,@DPTR
         JZ      SKP7

         MOV     R2,#4
         MOV     R0,#RESULT
         MOV     R1,#ECGALS
         CALL    MOVER
         MOV     R2,#4
         MOV     R0,#ECGALS+3
         MOV     A,SECTAX
         JNZ     NTAX1
         MOV     DPTR,#ATAX1
         JMP     NTAX2
NTAX1:   MOV     DPTR,#ATAX2
NTAX2:   CALL    MOVDECASC

         MOV     A,SECTAX
         JNZ     DOP6

         MOV     DPTR,#PR65FLG
         MOVX    A,@DPTR
         JZ      PR65E
         MOV     DPTR,#PRMSG56A
         JMP     PR65F

PR65E:   MOV     DPTR,#PRMSG41
PR65F:   JMP     DOP7
DOP6:    MOV     DPTR,#PRMSG42
DOP7:    MOV     R1,#16
         CALL    BLAST
DOP5:    MOVX    A,@DPTR					;Messages are stored in RAM so keep MOVX
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DOP5

         MOV     ZERO,#0
         MOV     R0,#RESULT+3
         CALL    DOOUTAZ
         CALL    DOOUTAZ
         MOV     A,@R0
         CALL    MAKEASC
         CALL    OUTAZ
         MOV     A,B
         CALL    OUTAZ
         MOV     A,#'.'
         CALL    OUTAZ
         DEC     R0
         CALL    DOOUTAZ
         CALL    PLINEEND

SKP7:
         CLR     C
         MOV     R2,#5
         MOV     R0,#RESULT
         MOV     R1,#PGCST
NOTAX1:  MOV     A,@R0                 ; PGCST += TAX
         ADDC    A,@R1
         DA      A
         MOV     @R1,A
         INC     R0
         INC     R1
         DJNZ    R2,NOTAX1
NTAX:    RET

FIXED:   MOV     DPTR,#FIXEDNUM
         MOVX    A,@DPTR
         CJNE    A,#'0',DOP24
         JMP     FIXEDDN               ; DON'T DO IF FIXEDNUM IS ZERO
DOP24:
         MOV     DPTR,#PRINTFLG
         MOVX    A,@DPTR
         JZ      SKP8

         CALL    FXDMPTR
         MOV     R1,#15
         CALL    BLAST
DOP21:   MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DOP21
         MOV     A,#'$'
         CALL    OUTA
         MOV     A,#' '
         CALL    OUTA
         CALL    OUTA

SKP8:
         MOV     R2,#3
         CALL    FXDAPTR
         MOV     R0,#BCDM+2
         CALL    GETDEC
         MOV     R2,#3
         MOV     DPTR,#FIXEDAMT
         MOV     R0,#BCDM
         CALL    CARDMOV

         MOV     DPTR,#PRINTFLG
         MOVX    A,@DPTR
         JZ      SKP9

         MOV     ZERO,#0
         MOV     R1,#4
         CALL    FXDAPTR
DOP22:   MOVX    A,@DPTR
         CALL    OUTAZ
         INC     DPTR
         DJNZ    R1,DOP22
         MOV     A,#'.'
         CALL    OUTAZ
         MOV     R1,#2
DOP23:   MOVX    A,@DPTR
         CALL    OUTAZ
         INC     DPTR
         DJNZ    R1,DOP23
         CALL    PLINEEND
SKP9:
         MOV     DPTR,#FIXEDTAX
         MOVX    A,@DPTR
         CJNE    A,#'Y',DOP27
         CALL    FXDADDTX
         JMP     FIXEDDN
DOP27:   CALL    FXDADDNT
FIXEDDN: RET


;***** CHECK ON STATUS OF MLPR = 0
;  IF SO ACC=0 ELSE ACC=1  TEST WITH  JZ
MLPREQ0: MOV     R2,#3
         MOV     R0,#MLPR
         MOV     A,#0
MLPREQ01:ORL     A,@R0
         INC     R0
         DJNZ    R2,MLPREQ01
         RET

;***** COMPUTE TYPE A DISCOUNT AND PRINT
;             X DAY PAY = TYPE A
;  IF TYPE B IS ON YOU CANNOT HAVE TYPE A
DODISCA:
         CALL    SWAPDSC

         CALL    PRICECHK         ; PRICE=0 THEN FORGET IT.
         CJNE    A,#0,DISCOK
         JMP     NTAX

DISCOK:  MOV     R2,#3            ;
         MOV     R0,#FEDTX        ;                %  $
         MOV     R1,#MLPR         ; FEDTAX TO MLPR .XX.XX XX
         CALL    MOVER            ; MCND STILL HAS XGALS
         MOV     MLPR+3,#0        ; MCND XX XX XX X.X
         MOV     DPTR,#TAXTYPE    ; DETERMINE MODE OF OPERATION
         MOVX    A,@DPTR          ;  FROM TAX TYPE
         CJNE    A,#'$',PCT1      ; TAXTYPE IN ASCII
; '0'==DOLLAR  '1'==PCT
; IF $/GAL PRICING :  XGALS = MCND = XX XX XX X.X
;                    FEDTAX = MLPR = 00 XX.XX XX
; AND RESULT OF XLPLY =   00 00 00 0X XX X.X XX XX
         MOV     R2,#3
         MOV     DPTR,#NVOL
         MOV     R1,#MCND
         CALL    EXTMOV
         MOV     MCND+3,#0
         CALL    XLRRES
         CALL    XLPLY            ; 00...XX X.X XX XX
         CALL    SHIFTR           ; 00...XX XX. XX XX
         CALL    SHIFTR           ; 00...0X XX X.X XX
         SJMP    PREP1            ;  CPG * XGALS (SKIP PCT)
; IF PERCENT ($/$) PRICING GCST = MCND = XX XX XX.XX
;                        FEDTAX = MLPR = 00.XX XX XX
; NOTE DEC PT NOT TREATED SAME FOR FEDTAX HERE
; AND RESULT OF XLPLY =  00 00 00 XX.XX XX XX XX  DOLS
PCT1:    MOV     R2,#4            ; PUT 4 BYTES OF TOTAL COST
         MOV     R0,#GCST         ;  INTO MULTIPLICAND  ..XX.XX
         MOV     R1,#MCND         ;  NOW GCST * FEDTAX
         CALL    MOVER
         CALL    XLRRES
         CALL    XLPLY            ; RESULT 00--XX.XX XX XX XX  DOLS
         CALL    SHIFTR           ;         ...XX X.X XX XX XX
         CALL    SHIFTR           ; RESULT     00..XX.XX XX XX
         CALL    SHIFTR           ;                  X.X XX XX
         CALL    SHIFTR           ;                   XX.XX XX
         CALL    SHIFTR           ;                   XX X.X XX
PREP1:   CALL    ROUNDEM
         MOV     R2,#4
         MOV     R0,#RESULT
         CLR     A
ZEROCHK1:ORL     A,@R0
         INC     R0
         DJNZ    R2,ZEROCHK1
         JNZ     ZERCH1
         JMP     DODISCDN

ZERCH1:
         MOV     DPTR,#B6FLG
         MOVX    A,@DPTR
         JNZ     SKIP13

         CALL    PFEED

SKIP13:  MOV     ZERO,#0
         MOV     DPTR,#PRMSG45
         CALL    BLAST
         MOV     R1,#11
DOP10:   CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DOP10
         MOV     DPTR,#DISCDAY
         MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         MOVX    A,@DPTR
         CALL    OUTA
         MOV     DPTR,#PRMSG45+13
         MOV     R1,#5
DOP11:   CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DOP11
         CALL    PLINEEND

         MOV     R2,#4
         MOV     R0,#RESULT
         MOV     R1,#DATEIMG
         CALL    MOVER
         MOV     R2,#4
         MOV     R0,#DATEIMG+3
         MOV     DPTR,#ADISC
         CALL    MOVDECASC

         MOV     DPTR,#PRMSG46  ; YOU MAY DEDUCT
         CALL    BLAST
         MOV     R1,#16

DOP9:    CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DOP9

         MOV     R0,#RESULT+3
         CALL    DOOUTAZ
         CALL    DOOUTAZ
         MOV     A,@R0
         CALL    MAKEASC
         CALL    OUTAZ
         MOV     A,B
         CALL    OUTAZ
         MOV     A,#'.'
         CALL    OUTAZ
         DEC     R0
         CALL    DOOUTAZ
         CALL    PLINEEND

DODISCDN:RET

;*************************************
; GCST += FIXEDAMT   XX XX XX.XX + XX XX.XX
FXDADDTX:MOV     DPTR,#FIXEDAMT
         MOVX    A,@DPTR
         MOV     R0,#GCST
         CALL    PARTSUM
         RET

;**************************************
; DPTR(2) + @R0(4)
PARTSUM: CLR     C
         MOV     R2,#3
PARTSUM1:MOVX    A,@DPTR               ; GET @DPTR
         ADDC    A,@R0                 ; ADD @R0 AND SAVE BACK
         DA      A
         MOV     @R0,A
         INC     DPTR
         INC     R0
         DJNZ    R2,PARTSUM1
         MOV     A,@R0
         ADDC    A,#0
         DA      A
         MOV     @R0,A
         RET

;**************************************
; PGCST += FIXEDAMT    XX XX XX.XX + XX XX.XX
FXDADDNT:
         MOV     DPTR,#FIXEDAMT
         MOV     R0,#PGCST             ; POINT TO LOW PGCST
         CALL    PARTSUM
         RET


PRODST:  CALL    SELECTHST
         MOV     MSB,PRCOD             ; SAVE CURRENT PRCOD
         MOV     PRCOD,#01H
         MOV     R7,#99
PRODST3: CALL    GETCAL
         MOV     A,#0
         ORL     A,SWARRAY
         ORL     A,SWARRAY+1
         ORL     A,SWARRAY+2
         JNZ     PRODST1
         MOV     A,#'0'
         CALL    OUTA
         CALL    OUTA
         JMP     PRODST2
PRODST1: MOV     TEMP,PRCOD
         CALL    BIN2BCD
         MOV     A,TEMP
         CALL    MAKEASC
         CALL    OUTA
         MOV     A,B
         CALL    OUTA
PRODST2: INC     PRCOD
         DJNZ    R7,PRODST3
         MOV     PRCOD,MSB             ; RETREIVE CURRENT PRCOD
         CALL    GETCAL
         RET


PRINTH:  MOV     DPTR,#HMSG1
         CALL    CHKSP                 ; SEE IF SPACE AND GET OUT
         MOV     DPTR,#HMSG2           ; PRINT IF NOT SPACE
         CALL    CHKSP
         MOV     DPTR,#HMSG3
         CALL    CHKSP
         MOV     DPTR,#HMSG4
         CALL    CHKSP
         MOV     DPTR,#HMSG5
         CALL    CHKSP
         MOV     DPTR,#HMSG6
         CALL    CHKSP
         MOV     DPTR,#HMSG7
         CALL    CHKSP
         MOV     DPTR,#HMSG8
         CALL    CHKSP
         MOV     DPTR,#HMSG9
         CALL    CHKSP
         MOV     DPTR,#HMSG10
         CALL    CHKSP
         MOV     DPTR,#B1FLG
         MOVX    A,@DPTR
         JNZ     SKIP8
         CALL    PFEED
SKIP8:   RET


PRINTF:  MOV     DPTR,#B9FLG
         MOVX    A,@DPTR
         JNZ     SKIP7
         CALL    PFEED
SKIP7:   MOV     DPTR,#FMSG1
         CALL    CHKSP                 ; SEE IF SPACE AND GET OUT
         MOV     DPTR,#FMSG2           ; PRINT IF NOT SPACE
         CALL    CHKSP
         MOV     DPTR,#FMSG3
         CALL    CHKSP
         MOV     DPTR,#FMSG4
         CALL    CHKSP
         RET


CHKSP:   MOVX    A,@DPTR
         CJNE    A,#0,CHKSP1           ; CHECK FOR NULL
         JMP     CHKSP2
CHKSP1:  CALL    PRTMSG
CHKSP2:  RET


DOSLSPLINE:
         MOV     DPTR,#HOSTLNPRT
         MOV     R0,#25
HSTPRTLX:CALL    INPA
         JNC     HSTPRTLX
         CALL    OUTA
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,HSTPRTLX
         CALL    CHKTKT
         JNC     EXSLSPLINE
         MOV     DPTR,#HOSTLNPRT
         CALL    PRTMSG
         CALL    DISCON
         CALL    WAIT3
EXSLSPLINE:
         CALL    WAIT3               ; THE WAITS HERE MAY NOT BE NECESSARY
         CALL    CON2HOST
         CALL    SELECTHST
         CALL    WAIT3
         CALL    SENDPIPE
         CALL    CRLF
         JMP     MAINZ



DOSLSSAVAL:

         MOV     DPTR,#SLSACHUNKS
         MOVX    A,@DPTR
         INC     A                     ; NEED TO SET 1 HIGHER FOR LOOPING
         MOVX    @DPTR,A


         ; ECHO P CRLF  = HOST WILL SEND CHARS
         MOV     A,#'P'
         CALL    OUTA
         CALL    CRLF

         MOV     DPTR,#HOSTLNPRT
         MOV     R0,#25
HSTSAVLX:CALL    INPA
         JNC     HSTSAVLX              ; WAIT FOR NEXT CHAR
         CALL    OUTA                  ; IN DEL SLS DOESN'T ECHO UNTIL COMPLETE, HOWEVER IN NON-DEL SLS DOES ECHO EACH CHAR
         MOVX    @DPTR,A               ; THIS ALWAYS WILL ECHO, MAY NEED 2 FUNCTIONS (1 ECHO, 1 NO ECHO)
         INC     DPTR
         DJNZ    R0,HSTSAVLX


         ; MOVE THE ABYTES PTR OVER TO THE START OF NEXT UNUSED CHUNK
         MOV     DPTR,#SLSACHUNKS      ; WILL BE 1 1ST TIME, 2 2ND, ETC..
         MOVX    A,@DPTR
         MOV     R1,A
         MOV     DPTR,#ABYTES
MVHLCKSTRT:
         DJNZ    R1,MVHLCKNXT          ; ONLY NEED TO MOVE IF >1, LINE 1 AT 0
         JMP     MVHLMOVIT
MVHLCKNXT:
         MOV     R0,#25
MVHLLCHNK:
         INC     DPTR
         DJNZ    R0,MVHLLCHNK
         MOV     R0,#25
         JMP     MVHLCKSTRT


MVHLMOVIT:
         MOV     R1,#25
         MOV     LSB,DPL
         MOV     MSB,DPH
         MOV     DPTR,#HOSTLNPRT
MVHSTLN2:MOVX    A,@DPTR
         PUSH    DPH
         PUSH    DPL

         MOV     DPL,LSB
         MOV     DPH,MSB
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - SWITCH RAMS
         MOVX    @DPTR,A
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY - SWITCH BACK TO LOCAL RAM
         INC     DPTR
         MOV     LSB,DPL
         MOV     MSB,DPH
         POP     DPL
         POP     DPH
         INC     DPTR
         DJNZ    R1,MVHSTLN2


         CALL    WAIT3

         CALL    SENDPIPE
         CALL    CRLF

         RET



GETFLT:  MOV     DPTR,#FLTIME
         MOV     R7,#7                 ; TIME,00000,CHKSUM
GETFLT1: CALL    INPA
         JNC     GETFLT1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R7,GETFLT1
         MOV     TEMP,#0A5H            ; XOR SEED
         XRL     TEMP,#'O'             ; START WITH COMMAND
         MOV     DPTR,#FLTIME
         MOV     R1,#6                 ; TIME + 5 NULLS
GETFLT2: MOVX    A,@DPTR
         XRL     TEMP,A
         INC     DPTR
         DJNZ    R1,GETFLT2
         MOV     DPTR,#FLTIME+6        ; GET CHKSUM
         MOVX    A,@DPTR
         CJNE    A,TEMP,GETFLT3
         MOV     A,#'0'
         CALL    OUTA
         JMP     GETFLT4               ; GOOD TO GO
GETFLT3: MOV     DPTR,#FLTIME
         MOV     A,#0
         MOVX    @DPTR,A               ; FORCE TO NO TIMEOUT
         MOV     A,#'1'
         CALL    OUTA
GETFLT4: CALL    SENDPIPE
         RET


GETHDR:  MOV     DPTR,#HMSG1
         MOV     R7,#10
         JMP     GETHDFT
GETFTR:  MOV     DPTR,#FMSG1
         MOV     R7,#4
GETHDFT: MOV     R6,#25
GETHDFT1:CALL    INPA
         JNC     GETHDFT1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R6,GETHDFT1
         DJNZ    R7,GETHDFT
         CALL    SENDPIPE
         RET


GETBBYTE:
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#BBYTES
GETB1:   CALL    INPA
         JNC     GETB1
         CJNE    A,#0,GETB2
         JMP     GETB3
GETB2:   CJNE    A,#1BH,GETB4          ; CHECK FOR ESC
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         MOV     DPTR,#ESC
         MOV     A,#1
         MOVX    @DPTR,A               ; SET ESC BIT
         JMP     GETB5
GETB4:   MOVX    @DPTR,A
         INC     DPTR
         JMP     GETB1
GETB3:   CALL    SENDPIPE
GETB5:
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         RET


GETABYTE:
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#ABYTES
GETA1:   CALL    INPA
         JNC     GETA1
         CJNE    A,#0,GETA2
         JMP     GETA3
GETA2:   CJNE    A,#1BH,GETA4          ; CHECK FOR ESC
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         MOV     DPTR,#ESC
         MOV     A,#1
         MOVX    @DPTR,A               ; SET ESC BIT
         JMP     GETA5
GETA4:   MOVX    @DPTR,A
         INC     DPTR
         JMP     GETA1
GETA3:   CALL    SENDPIPE
GETA5:
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         RET


CLRBYTES:
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
                                       ; NOTE IN 179 AND OLDER BBYTES WAS DECLARED BEFORE ABYTES
                                       ; AS A RESULT WE COULD JUST CLEAR THE NEXT 1500 BYTES
                                       ; IN E200 AND NEWER ABYTES IS DECLARED BEFORE BBYTES
         MOV     DPTR,#ABYTES          ; POINT TO START OF ARRAY FOR BOTH ABYTES AND BBYTES
         MOV     R0,#15
         MOV     R1,#100               ; 15X100 = 1500 BYTES OF 0'S
         MOV     A,#0
CLRB1:   MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,CLRB1
         MOV     R1,#100               ; 15X100 = 1500 BYTES OF 0'S
         DJNZ    R0,CLRB1

; NEED TO CLEAR 2 MORE BYTES ...
         MOVX    @DPTR,A
         INC     DPTR
         MOVX    @DPTR,A

;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         RET


GETLABEL:
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#PMSG1
         MOV     R0,#0                 ; 256*9 + 171  = 2475
         MOV     R1,#9                 ; 25/PC * 99PC = 2475
GETLBL1: CALL    INPA
         JNC     GETLBL1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,GETLBL1
         DJNZ    R1,GETLBL1
         MOV     R0,#171
GETLBL2: CALL    INPA
         JNC     GETLBL2
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,GETLBL2
         CALL    SENDPIPE
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         RET


GETBLOCK:MOV     DPTR,#APRCOD
         MOV     R0,#161
GETBL1:  CALL    INPA
         JNC     GETBL1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,GETBL1
         CALL    SENDPIPE
         CALL    CONVPRST              ; CONVERT AND APPLY IF FLAG SET
         CALL    CONV2BCD              ; CONVERT PRICE AND TAXES TO BCD
         CALL    PUTPCPR               ; PUT CURRENT PRICING IN ARRAY
         RET

;************************************************************************
; LT: EAT THESE BYTES AND MAKE SURE THE LT 'LOOKS' LIKE AN EC TO A HOST
;  THIS IS USED TO READ RO BYTES FROM HOST
;  WILL SEND PIPE AFTER R0 CHARS ARE READ
;  THE CHARS ARE JUST IGNORED, NEVER STORED
GTBLKR0: CALL    INPA
         JNC     GTBLKR0
         DJNZ    R0,GTBLKR0
         JMP     GTBLKR0X
GTBLKR1R2:
         CALL    INPA
         JNC     GTBLKR1R2
         DJNZ    R1,GTBLKR1R2
         DJNZ    R2,GTBLKR1R2
GTBLKR0X:CALL    SENDPIPE
         RET




;************************************************************************
; SETHCMDFLG SETS HOST CMD FLAG
SETHCMDFLG:
         MOV     DPTR,#ISHSTCMD
         MOV     A,#1
         MOVX    @DPTR,A
         RET

;************************************************************************
; CLRHCMDFLG CLEARS HOST CMD FLAG
CLRHCMDFLG:
         MOV     DPTR,#ISHSTCMD
         MOV     A,#0
         MOVX    @DPTR,A
         RET

;************************************************************************
; LODHCMDFLG LOADS HOST CMD FLG IN ACC FOR TESTING IN CALLER
LODHCMDFLG:
         MOV     DPTR,#ISHSTCMD
         MOVX    A,@DPTR
         RET


;************************************************************************
; SETHSTLNFLG SETS HOST PASS-THROUGH PRINTING FLAG FROM ACC A
SETHSTLNFLG:
         MOV     DPTR,#HOSTLNFLG
         MOVX    @DPTR,A
         RET

;************************************************************************
; LODHSTLNFLG LOADS HOSTLNFLG IN ACC FOR TESTING IN CALLER
LODHSTLNFLG:
         MOV     DPTR,#HOSTLNFLG
         MOVX    A,@DPTR
         RET


;************************************************************************
; HSTPRNTST PRINTS BEGINNING OF TICKET, LOGOS, BLACK MARK SENSOR, ETC
;  BASED ON PRINTER SETTING
; RETURNS TO HOST:
;  0 = OK
;  1 = PAPER ERR
;  2 = OTHER ERR
;  3 = NO PRINTER
HSTPRNTST:
         MOV     A,#1                  ; JUMPS TO PRINTFIN2 IF PRINTER READY (START OF TKT)
         JMP     HPTPCMD1

;************************************************************************
; HSTPRNTFN PRINTS ENDING OF TICKET, FFS, SPACES, CLOSE TICKET, ETC
;  BASED ON PRINTER SETTING
; RETURNS TO HOST:
;  0 = OK
;  1 = PAPER ERR
;  2 = OTHER ERR
;  3 = NO PRINTER
HSTPRNTFN:
         MOV     A,#3                  ; JUMPS TO FINISHTKT IF PRINTER READY (END OF TKT)
         JMP     HPTPCMD1

;************************************************************************
; HSTPRNTLN GETS NEXT 25 CHARACTERS FROM HOST AND SENDS TO PRINTER;
; RETURNS TO HOST:
;  0 = OK
;  1 = PAPER ERR
;  2 = OTHER ERR
;  3 = NO PRINTER
HSTPRTLN:
         MOV     DPTR,#HOSTLNPRT
         MOV     R0,#25
HSTPRTL1:CALL    INPA
         JNC     HSTPRTL1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,HSTPRTL1
         MOV     A,#2                  ; PRINTS 25 CHARS IF PRINTER READY
HPTPCMD1:CALL    SETHSTLNFLG
         CALL    CHKPRTR
         MOV     A,#0
         CALL    SETHSTLNFLG
         RET


GETFLAGS:MOV     DPTR,#CMDFLAG        ; SEND CMDFLAGS TO HOST
         MOV     R0,#10
GETF1:   CALL    INPA
         JNC     GETF1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,GETF1
         CALL    SENDPIPE
         RET

LOADPRST:MOV     DPTR,#APRCOD          ; GET ASCII PRCOD,PRESET AND TIMER
         MOV     R0,#10
LOADP1:  CALL    INPA
         JNC     LOADP1
         CJNE    A,#1BH,LOADP2
         MOV     DPTR,#ESC
         MOV     A,#1
         MOVX    @DPTR,A
         JMP     LOADP3
LOADP2:  MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,LOADP1
         CALL    CONVPRST
         MOV     A,TEMP
         CALL    OUTA
         CALL    SENDPIPE
LOADP3:  RET

SNDR0CHRA:
         CALL    OUTA
         DJNZ    R0,SNDR0CHRA
         RET

SENDBLK: CALL    CONV2ASC
         MOV     DPTR,#APRCOD
         MOV     R0,#161
SENDBL1: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R0,SENDBL1
         MOV     A,#0
         RET


LD6PRST:
         MOV     R0,#DATEIMG
         MOV     R1,#11
LD6A:    CALL    INPA
         JNC     LD6A
         MOV     @R0,A
         INC     R0
         DJNZ    R1,LD6A               ;GET TEMP 11 DIGITS

         MOV     DPTR,#APRCOD
         MOV     A,DATEIMG
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,DATEIMG+1
         MOVX    @DPTR,A               ; PRODUCT CODE PUT AWAY

         MOV     A,DATEIMG+2
         ANL     A,#0FH                ; MAKE IT DECIMAL (STRIP OFF '3'X)
         MOV     DPTR,#PSMSD
         MOVX    @DPTR,A               ; STORE IT TO PRESET MSD

         MOV     R0,#DATEIMG+3
         MOV     DPTR,#APRESET
         MOV     R1,#8                 ; SHOULD ONLY BE 8 LEFT
LD6B:    MOV     A,@R0
         MOVX    @DPTR,A
         INC     DPTR
         INC     R0
         DJNZ    R1,LD6B               ; ALL PUT AWAY NOW

         CALL    CONVPRST
         MOV     A,TEMP
         CALL    OUTA
         CALL    SENDPIPE
         RET


SENDVER: MOV     DPTR,#VMSG2
         MOV     R1,#6
SENDV1:  CLR     A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,SENDV1              ; VERSION NUMBER 6 BYTES

		 CLR     A
         MOV     DPTR,#VDBLK
         MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         CLR     A
         MOVC    A,@A+DPTR
         CALL    OUTA                  ; DATA BLOCK VERSION 2 BYTES

         MOV     DPTR,#REGNUM
         MOVX    A,@DPTR
         ORL     A,#30H                ; CONVERT TO ASCII
         CALL    OUTA                  ; OUT REGNUM 1 BYTE

         MOV     DPTR,#SERNUM+2
         CALL    ASCOUT
         CALL    DASCOUTX2
         CALL    SENDPIPE              ; SERIAL NUMBER THE HARD WAY
         RET                           ; 6 BYTES  15 TOTAL


SENDMPCDAT:                            ; FLOWRATE 0-FFH / SIGN /ADJUSTMENT 0-DD (.00DD)
         MOV     DPTR,#CALPT1
         MOV     R1,#40                ; 40 BYTES
SENMPC1: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,SENMPC1
         CALL    SENDPIPE
         RET

SENDCAL: MOV     DPTR,#PR1CAL
         MOV     R1,#200
         MOV     R2,#3                 ; 600 BYTES  CAL/DWL/CMP
SENCAL1: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,SENCAL1
         MOV     R1,#200               ; RELOAD 200 COUNTER
         DJNZ    R2,SENCAL1
         CALL    SENDPIPE
         RET

SENDDEL:
         CALL    CONV2ASC              ; SEND DELIVERY DATA TO HOST
         MOV     DPTR,#ASTIME
         MOV     R1,#10
         CALL    SENDIT
         MOV     DPTR,#AFTIME
         MOV     R1,#10
         CALL    SENDIT
         MOV     DPTR,#APRCOD
         MOV     R1,#2
         CALL    SENDIT
         MOV     DPTR,#ATRUCK
         MOV     R1,#4
         CALL    SENDIT
         MOV     DPTR,#ADRIVER
         MOV     R1,#4
         CALL    SENDIT
         MOV     DPTR,#ATKTNO
         MOV     R1,#6
         CALL    SENDIT
         MOV     DPTR,#ANVOL
         MOV     R1,#8
         CALL    SENDIT
         MOV     DPTR,#AGVOL
         MOV     R1,#8
         CALL    SENDIT
         MOV	 DPTR,#TOTDIGITS		;LOAD EITHER 8 OR 12 FOR NUMBER OF TOTALIZER DIGITS TO SEND
         MOVX	 A,@DPTR
         MOV	 R1,A
         CLR	 C						;IF ONLY SENDING 8 DIGITS THEN NEED TO SHIFT STARTING ADDRESS BY 4
         MOV	 B,#12
         XCH	 A,B
         SUBB	 A,B					;RESUTS ARE EITHER 0 OR 4
         CLR 	 C
         MOV	 DPTR,#ATNVOL
         ADD	 A,DPL
         MOV	 DPL,A
         MOV 	 A,DPH
         ADDC	 A,#0					;BRING CARRY BIT INTO DPH
         MOV	 DPH,A
		 CALL    SENDIT
         MOV	 DPTR,#TOTDIGITS		;LOAD EITHER 8 OR 12 FOR NUMBER OF TOTALIZER DIGITS TO SEND
         MOVX	 A,@DPTR
         MOV	 R1,A
         CLR	 C						;IF ONLY SENDING 8 DIGITS THEN NEED TO SHIFT STARTING ADDRESS BY 4
         MOV	 B,#12
         XCH	 A,B
         SUBB	 A,B					;RESUTS ARE EITHER 0 OR 4
         CLR 	 C						;DROP NEGATIVE
         MOV	 DPTR,#ATGVOL
         ADD	 A,DPL
         MOV	 DPL,A
         MOV 	 A,DPH
         ADDC	 A,#0					;BRING CARRY BIT INTO DPH
         MOV	 DPH,A
		 CALL    SENDIT
         MOV     DPTR,#ACOMPST
         MOV     R1,#1
         CALL    SENDIT
         MOV     DPTR,#STATUS
         MOV     R1,#3
         CALL    SENDIT
         CALL    SENDPIPE
         RET


SENDIDEL:                              ; FORMAT DELIVERY DATA TO MATCH PA21 FOR SLS MODE


         CALL    CONV2ASC              ; SEND DELIVERY DATA TO HOST

         MOV     DPTR,#AFTIME
         MOV     R1,#6
         CALL    SENDIT

         MOV     DPTR,#AFTIME+6
         MOV     R1,#4
         CALL    SENDIT

         MOV     DPTR,#ATRUCK
         MOV     R1,#4
         CALL    SENDIT

         MOV     DPTR,#ATKTNO
         MOV     R1,#6
         CALL    SENDIT

         MOV     DPTR,#APRCOD
         MOV     R1,#2
         CALL    SENDIT

         MOV     DPTR,#AGVOL+1
         MOV     R1,#7
         CALL    SENDIT0

         MOV     DPTR,#ANVOL+1
         MOV     R1,#7
         CALL    SENDIT0

         ; SEND PC 2
         MOV     A,#'0'
         CALL    OUTA
         CALL    OUTA
         CALL    CRLF

         ; SEND GVR2
         MOV     A,#'0'
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    CRLF

         ; SEND NVR2
         MOV     A,#'0'
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    OUTA
         CALL    CRLF

         CALL    SENDPIPE
         CALL    CRLF
         RET


SENDIT:  MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,SENDIT
         CALL    CRLF
         RET

SENDITP: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,SENDITP
         RET

SENDIT0: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,SENDIT0
         MOV     A,#'0'                ; SLSMODE - PA21 VOLS ARE IN HUNDREDTHS, FAKE IT
         CALL    OUTA
         CALL    CRLF
         RET

SENDSTAT:MOV     TEMP,#00H             ; STARTING STATUS
SENDST1: MOV     DPTR,#ACTIVE          ; CHECK BYTES AND ORL R0
         MOVX    A,@DPTR
         JZ      SST1
         MOV     A,#20H
         ORL     TEMP,A
SST1:    MOV     DPTR,#SSFLAG
         MOVX    A,@DPTR
         JZ      SST2
         MOV     A,#08H
         ORL     TEMP,A
SST2:    MOV     DPTR,#PRSSET
         MOVX    A,@DPTR
         JZ      SST3
         MOV     A,#04H
         ORL     TEMP,A
SST3:    MOV     DPTR,#PRTPRSD
         MOVX    A,@DPTR
         JZ      SST4
         MOV     A,#02H
         ORL     TEMP,A
SST4:    MOV     DPTR,#TIMEOUT
         MOVX    A,@DPTR
         JZ      SST5
         MOV     A,#01H
         ORL     TEMP,A
SST5:    MOV     DPTR,#TKTPND
         MOVX    A,@DPTR
         JZ      SST6
         MOV     A,#40H
         ORL     TEMP,A
SST6:    MOV     DPTR,#HOST
         MOVX    A,@DPTR
         JZ      SST7
         MOV     A,#80H
         ORL     TEMP,A
SST7:    MOV     DPTR,#GETSTAT
         MOVX    A,@DPTR
         JNZ     SST8
SST9:    MOV     A,TEMP
         CALL    OUTA
SST8:    MOV     A,TEMP
         MOV     DPTR,#GETSTAT
         MOV     A,#0
         MOVX    @DPTR,A               ; RESET FLAG
         RET

SENDPD:  CALL    INPA
         JNC     SENDPD
         MOV     DATEIMG,PRCOD
         MOV     PRCOD,A
         CALL    PCMPTR
         MOV     R2,#25
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
SENDPD1: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R2,SENDPD1
         MOV     PRCOD,DATEIMG
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         RET


SENDQOB: MOV     DPTR,#QOB1
         MOV     R2,#99
SENDQB1: INC     DPTR
         INC     DPTR
         CALL    ASCOUT
         CALL    DASCOUTX2
         INC     DPTR
         INC     DPTR
         INC     DPTR
         DJNZ    R2,SENDQB1
         CALL    SENDPIPE
         RET


SENDPCPR:MOV     TEMP,PRCOD            ; SEND PRCOD AND LAST PRESET FOR "m" AND "n" HOST COMMANDS
         CALL    BIN2BCD
         MOV     A,TEMP
         CALL    MAKEASC
         CALL    OUTA
         MOV     A,B
         CALL    OUTA
         MOV     DPTR,#PS6GET          ; IF SENDING THE 6-DIGITS PRESETS NEED TO SEND LEADING DIGIT = "n" COMMAND
         MOVX    A,@DPTR               ; 1=SEND, 0=DON'T SEND
         JZ      SENDPCPN
         MOV     DPTR,#PSMSD
         MOVX    A,@DPTR
         ORL     A,#30H                ; MAKE ASCII
         CALL    OUTA
SENDPCPN:MOV     DPTR,#LASTPRST
         INC     DPTR
         INC     DPTR
         MOVX    A,@DPTR
         CALL    MAKEASC
         MOV     A,B
         CALL    OUTA
         CALL    DASCOUTX2
         RET


GETAQOB: MOV     TEMP,PRCOD
         MOV     R6,#99
         MOV     PRCOD,#1
GETAQ2:  MOV     R7,#6
         MOV     DPTR,#BUFFER
GETAQ1:  CALL    INPA
         JNC     GETAQ1

         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R7,GETAQ1             ; GET 6 BYTES

         MOV     R2,#3                 ; FOUR BYTES
         MOV     DPTR,#BUFFER          ; GET ACCTNO
         MOV     R0,#DATEIMG+2         ; MSB - 4 BYTES
         CALL    GETDEC                ; GET ASCII TO HEX

         MOV     R2,#3                 ; INTO DATEIMG
         CALL    QOBPTR
         MOV     R0,#DATEIMG
         CALL    CARDMOV

         INC     PRCOD
         DJNZ    R6,GETAQ2
         CALL    SENDPIPE
         MOV     PRCOD,TEMP
         RET


SENDVOL:
         MOV     R1,#4
         MOV     R0,#CGALS+4
SENDVOL1:MOV     A,@R0
         CALL    OUTA
         DEC     R0
         DJNZ    R1,SENDVOL1
         RET

CHKSUM:  MOV     A,TEMP
         XRL     A,CGALS+4
         XRL     A,CGALS+3
         XRL     A,CGALS+2
         XRL     A,CGALS+1
         CALL    OUTA
         RET


DUMP:
         MOV     DPTR,#PRPRICE
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     R1,#100
         MOV     R2,#32                ; 3200 BYTES FOR PRICING DATA FOR 99 PC (32X100)
DUMP1:   MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMP1
         MOV     R1,#100               ; RELOAD 100 COUNTER
         DJNZ    R2,DUMP1
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY

         MOV     DPTR,#FIXEDCHG
         MOV     R1,#54                ; 54 BYTES FOR FIXED CHARGE AMOUNTS FOR 9 FIXED CHARGES (9X6)
DUMP2:   MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMP2

         MOV     DPTR,#FIXEDLBL
         MOV     R1,#135               ; 135 BYTES FOR FIXED CHARGE LABELS FOR 9 FIXED CHARGES (9X15)
DUMP3:   MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMP3

;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#PMSG1
         MOV     R0,#0
         MOV     R1,#9
DUMP4:   MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R0,DUMP4
         DJNZ    R1,DUMP4

         MOV     R1,#171               ; 2475 BYTES TOTAL, 9X256 + 171 = 2475 BYTES = ALL LABELS FOR 99 PC (99X25=2475)
DUMP4A:  MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMP4A
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY

         CALL    DUMPHF

         MOV     DPTR,#B1FLG
         MOV     R1,#15                ; 15 BYTES FOR LINE PRINTING FLAGS FOR METER TICKET
DUMP5:   MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMP5

;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#ADDPLCOD
         MOV     R1,#198               ; 198 BYTES (99 PC X 2 BYTES/PC) FOR ADDITIONAL DESCRIPTION LINE FLAGS FOR EACH PC, LT: ALL 0'S
DUMP6:   MOVX    A,@DPTR
         ADD     A,#20H
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMP6

         MOV     DPTR,#ADDPRLBL
         MOV     R1,#0
         MOV     R2,#9                 ; TOTAL OF 2500 BYTES FOR ADDITIONAL PRODUCT DESCRIPTIONS, 9X256 + 196 = 2500, 100PC X 25 BYTES/PC (#100 ISN'T USED), LT: SEND ALL SPACES
DUMP7:   MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMP7
         DJNZ    R2,DUMP7

         MOV     R1,#196               ; SEND FINAL 196 BYTES OF ADDITIONAL DESCRIPTIONS TO MAKE A TOTAL OF 2500
DUMP7A:  MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMP7A
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY

         CALL    SENDPIPE              ; "DUMP" COMPLETE
         RET


DUMPHF:  MOV     DPTR,#HMSG1
         MOV     R1,#250               ; 250 BYTES (10 LINES @ 25 BYTES/LINE) FOR PUMP & PRINT HEADER
DUMPHFA: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMPHFA

         MOV     DPTR,#FMSG1
         MOV     R1,#100               ; 100 BYTES (4 LINES @ 25 BYTES/LINE) FOR PUMP & PRINT FOOTER
DUMPHFB: MOVX    A,@DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R1,DUMPHFB
         RET


SENDTMP: MOV     DPTR,#NVOL            ; ONLY CALLED IN STATE 2, DELIVERY ACTIVE && NO FLOW
         MOV     R1,#DATEIMG
         MOV     R2,#4
         CALL    EXTMOV
         MOV     DPTR,#NVTMP
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV

         MOV     DPTR,#GVOL
         MOV     R1,#DATEIMG
         MOV     R2,#4
         CALL    EXTMOV
         MOV     DPTR,#GVTMP
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV

         MOV     DPTR,#TNVOL
         MOV     R1,#DATEIMG
         MOV     R2,#4
         CALL    EXTMOV
         MOV     DPTR,#TNVTMP
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV

         MOV     DPTR,#TGVOL
         MOV     R1,#DATEIMG
         MOV     R2,#4
         CALL    EXTMOV
         MOV     DPTR,#TGVTMP
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV               ; SAVE CURRENT VALUES

         MOV     DPTR,#GRTOTN
         MOV     R1,#DATEIMG
         MOV     R2,#6
         CALL    EXTMOV
         MOV     DPTR,#GRTNTMP
         MOV     R0,#DATEIMG
         MOV     R2,#6
         CALL    CARDMOV

         MOV     DPTR,#GRTOTG
         MOV     R1,#DATEIMG
         MOV     R2,#6
         CALL    EXTMOV
         MOV     DPTR,#GRTGTMP
         MOV     R0,#DATEIMG
         MOV     R2,#6
         CALL    CARDMOV

XXX:     MOV     DPTR,#NVOL
         CALL    ROTVOL
         MOV     DPTR,#NVOL
         CALL    RLVOL
         MOV     DPTR,#GVOL
         CALL    ROTVOL
         MOV     DPTR,#GVOL
         CALL    RLVOL

         CALL    ADDTOT                ; CALL 2

         MOV     DPTR,#GETSTAT
         MOV     A,#1
         MOVX    @DPTR,A
         CALL    SENDSTAT
         MOV     A,TEMP
         MOV     DPTR,#STATUS
         MOVX    @DPTR,A               ; SAVE LAST STATUS

         CALL    RTIME                 ; WITHOUT THIS CALL HERE, THE T DATA DURING THE DELIVERY
         MOV     DPTR,#FTIME           ;  RETURN THE DELIVERY START TIME
         CALL    SAVTIME               ; SET FINISH TIME TO CURRENT TIME WHILE DELIVERY ACTIVE

         CALL    CONV2ASC              ; SEND DELIVERY DATA TO HOST
         MOV     DPTR,#ASTIME
         MOV     R1,#10
         CALL    SENDIT
         MOV     DPTR,#AFTIME
         MOV     R1,#10
         CALL    SENDIT
         MOV     DPTR,#APRCOD
         MOV     R1,#2
         CALL    SENDIT
         MOV     DPTR,#ATRUCK
         MOV     R1,#4
         CALL    SENDIT
         MOV     DPTR,#ADRIVER
         MOV     R1,#4
         CALL    SENDIT
         MOV     DPTR,#ATKTNO
         MOV     R1,#6
         CALL    SENDIT
         MOV     DPTR,#ANVOL
         MOV     R1,#8
         CALL    SENDIT
         MOV     DPTR,#AGVOL
         MOV     R1,#8
         CALL    SENDIT
         MOV     DPTR,#ATNVOL
         MOV     R1,#8
         CALL    SENDIT
         MOV     DPTR,#ATGVOL
         MOV     R1,#8
         CALL    SENDIT
         MOV     DPTR,#ACOMPST
         MOV     R1,#1
         CALL    SENDIT
         MOV     DPTR,#STATUS
         MOV     R1,#3
         CALL    SENDIT
         CALL    SENDPIPE

         MOV     DPTR,#NVTMP
         MOV     R1,#DATEIMG
         MOV     R2,#4
         CALL    EXTMOV
         MOV     DPTR,#NVOL
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV

         MOV     DPTR,#GVTMP
         MOV     R1,#DATEIMG
         MOV     R2,#4
         CALL    EXTMOV
         MOV     DPTR,#GVOL
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV

         MOV     DPTR,#TNVTMP
         MOV     R1,#DATEIMG
         MOV     R2,#4
         CALL    EXTMOV
         MOV     DPTR,#TNVOL
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV

         MOV     DPTR,#TGVTMP
         MOV     R1,#DATEIMG
         MOV     R2,#4
         CALL    EXTMOV
         MOV     DPTR,#TGVOL
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV               ; RESTORE CURRENT VALUES

         MOV     DPTR,#GRTNTMP
         MOV     R1,#DATEIMG
         MOV     R2,#6
         CALL    EXTMOV
         MOV     DPTR,#GRTOTN
         MOV     R0,#DATEIMG
         MOV     R2,#6
         CALL    CARDMOV

         MOV     DPTR,#GRTGTMP
         MOV     R1,#DATEIMG
         MOV     R2,#6
         CALL    EXTMOV
         MOV     DPTR,#GRTOTG
         MOV     R0,#DATEIMG
         MOV     R2,#6
         CALL    CARDMOV

         RET                           ; FROM WHENCE WE CAME


GETTTLZR:
         MOV     DPTR,#TTLZRFLG        ; DO ONLY WHEN VALID
         MOVX    A,@DPTR               ; FLAG SET GOING INTO CAL MODE AND ALSO GOING INTO SETUP CODE
         JNZ     GETTOTAL2A            ; THAT WAY CAN UPDATE TOTALIZERS WITHOUT BREAKING SEAL

;       NEED TO SIT&SPIN SO NEXT 6 CHARS GET EATEN
         MOV     R1,#6                 ; 6 BYTES NET=6, PACKED BCD, IN ORDER
GETTOTAL1:
         CALL    INPA
         JNC     GETTOTAL1
         DJNZ    R1,GETTOTAL1
         MOV     A,#'X'                ; REPORT CAN'T DO
         CALL    OUTA
         JMP     GETTOTAL4

GETTOTAL2A:
         MOV     DPTR,#GRTOTN
         MOV     R1,#6                 ; 6 BYTES NET=6, PACKED BCD, IN ORDER
GETTOTAL2B:
         CALL    INPA
         JNC     GETTOTAL2B
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,GETTOTAL2B
                                       ; UPDATE ALL TOTALIZERS HERE ...

         MOV     DPTR,#GRTOTN
         MOV     R1,#DATEIMG
         MOV     R2,#6
         CALL    EXTMOV
         MOV     DPTR,#GRTOTG
         MOV     R0,#DATEIMG
         MOV     R2,#6
         CALL    CARDMOV

         MOV     DPTR,#GRTOTSTRT
         MOV     R0,#DATEIMG
         MOV     R2,#6
         CALL    CARDMOV

         MOV     DPTR,#TNVOL
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV

         MOV     DPTR,#TGVOL
         MOV     R0,#DATEIMG
         MOV     R2,#4
         CALL    CARDMOV


GETTOTAL4:
         CALL    SENDPIPE
         RET


GETMPCDAT:
         MOV     DPTR,#CALFLG          ; DO ONLY IN CAL MODE
         MOVX    A,@DPTR
         JNZ     GETMPCD2
; NEED TO SIT&SPIN SO NEXT 40 CHARS GET EATEN
         MOV     R1,#40                ; 40 BYTES MPCAL DATA
GETMPCD4:CALL    INPA
         JNC     GETMPCD4
         DJNZ    R1,GETMPCD4
         MOV     A,#'X'                ; REPORT CAN'T DO
         CALL    OUTA
         JMP     GETMPCD3              ; JUMP TO EXIT
GETMPCD2:MOV     DPTR,#CALPT1
         MOV     R1,#40                ; 40 BYTES MPCAL DATA
GETMPCD1:CALL    INPA                  ; SAVE ALL 40 BYTES TO MEMORY
         JNC     GETMPCD1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,GETMPCD1
GETMPCD3:CALL    SENDPIPE
         RET                           ; EXIT RX OF MPC DATA FROM HOST



GETCALDAT:
         MOV     DPTR,#CALFLG          ; DO ONLY IN CAL MODE
         MOVX    A,@DPTR
         JNZ     GETCALD2
; NEED TO SIT&SPIN SO NEXT 600 CHARS GET EATEN
         MOV     R1,#200
         MOV     R2,#3                 ; 600 BYTES  CAL/DWL/CMP
GETCALD4:CALL    INPA
         JNC     GETCALD4
         DJNZ    R1,GETCALD4
         MOV     R1,#200               ; RELOAD 200 COUNTER
         DJNZ    R2,GETCALD4
         MOV     A,#'X'                ; REPORT CAN'T DO
         CALL    OUTA
         JMP     GETCALD3              ; JUMP TO EXIT
GETCALD2:MOV     DPTR,#PR1CAL
         MOV     R1,#200
         MOV     R2,#3                 ; 600 BYTES  CAL/DWL/CMP
GETCALD1:CALL    INPA                  ; SAVE ALL 600 BYTES TO MEMORY
         JNC     GETCALD1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,GETCALD1
         MOV     R1,#200               ; RELOAD 200 COUNTER
         DJNZ    R2,GETCALD1
GETCALD3:CALL    SENDPIPE
         RET                           ; EXIT RX OF CAL DATA FROM HOST


GETLINE: MOV     R1,#15
         MOV     DPTR,#B1FLG
GETLIN1: CALL    INPA
         JNC     GETLIN1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,GETLIN1
         CALL    SENDPIPE
         RET

GETPLC:  MOV     R1,#198               ; 2 BYTES PER. LOCATION+#LINES
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - LOOK TO 2ND RAM
         MOV     DPTR,#ADDPLCOD
GETPLC1: CALL    INPA
         JNC     GETPLC1
         CLR     C
         SUBB    A,#20H
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,GETPLC1
         CALL    SENDPIPE
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         RET

GETADPL: MOV     R1,#0
         MOV     R2,#9                 ; 100 X 25 = 2500
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - 9 X 256 + 196 = 2500
         MOV     DPTR,#ADDPRLBL
GETADP1: CALL    INPA
         JNC     GETADP1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,GETADP1
         DJNZ    R2,GETADP1
         MOV     R1,#196
GETADP2: CALL    INPA
         JNC     GETADP2
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,GETADP2
         CALL    SENDPIPE
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         RET

GETTXLB: MOV     DPTR,#PRMSG41
         MOV     R0,#32
GETTX1:  CALL    INPA
         JNC     GETTX1
         CJNE    R0,#17,GETTX1B			; 16th and 32nd character should be $, so ignore from Matrix
         MOV	 A,#'$'
         JMP     GETTX1D
GETTX1B: CJNE	 R0,#1,GETTX1D			; 16th and 32nd character should be $, so ignore from Matrix
         MOV	 A,#'$'
GETTX1D: MOVX    @DPTR,A
		 INC     DPTR
         DJNZ    R0,GETTX1


GETTX2:  CALL    INPA
         JNC     GETTX2
         MOV     DPTR,#TAXSUB
         CJNE    A,#'Y',GETTX3
         MOV     A,#1
         JMP     GETTX4
GETTX3:  MOV     A,#0
GETTX4:  MOVX    @DPTR,A
         CALL    SENDPIPE
         RET


; GETS PRICING MATRIX FROM PC. 3168 BYTES 32 X 99

GETPRMX: MOV     DPTR,#PRPRICE         ; POINT TO TOP OF ARRAY
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     R0,#0
         MOV     R1,#12
GETPRMX1:CALL    INPA
         JNC     GETPRMX1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,GETPRMX1
         DJNZ    R1,GETPRMX1
         MOV     R0,#96
GETPRMX2:CALL    INPA
         JNC     GETPRMX2
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,GETPRMX2
         CALL    SENDPIPE
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
         RET

; GETFIXED GETS 9 CHARGES AND LABELS

GETFIXED:MOV     DPTR,#FIXEDCHG
         MOV     R0,#189
GETFIX1: CALL    INPA
         JNC     GETFIX1
         MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R0,GETFIX1
         CALL    SENDPIPE
         RET


; CONVERTS ASCII PRESET AND PRODUCT CODE TO USABLE VALUES
CONVPRST:MOV     DPTR,#APRSSET
         MOVX    A,@DPTR               ; CHECK IF TO BE APPLIED
         CJNE    A,#'0',CONVPR1
         JMP     CONVPR5
CONVPR1: MOV     DPTR,#APRESET
         MOV     R2,#1
         MOV     R0,#PRESET+2
         CALL    GETDEC
         MOV     DPTR,#APRESET+2
         MOV     R2,#1
         MOV     R0,#PRESET+1
         CALL    GETDEC
         MOV     DPTR,#APRESET+4
         MOV     R2,#1
         MOV     R0,#PRESET            ; ADDED TENTHS (.01'S =0)
         CALL    GETDEC
         ANL     PRESET,#0F0H          ; MASK LSN. NOT VALID

         CALL    SHIFR

         MOV     DPTR,#LASTPRST
         MOV     A,PRESET
         MOV     VOLUME,PRESET
         MOVX    @DPTR,A
         MOV     DPTR,#LASTPRST+1
         MOV     A,PRESET+1
         MOV     VOLUME+1,PRESET+1
         MOVX    @DPTR,A
         MOV     DPTR,#LASTPRST+2
         MOV     A,PRESET+2
         MOV     VOLUME+2,PRESET+2
         MOVX    @DPTR,A               ; .1'S

         MOV     DPTR,#CONVFLG
         MOV     A,#1
         MOVX    @DPTR,A               ; FLAG NOT TO SHOW 'PRESET OK'
         CALL    PRESET4

         MOV     DPTR,#PRSSET
         MOV     A,#1
         MOVX    @DPTR,A

CONVPR5: MOV     DPTR,#ACTIVE
         MOVX    A,@DPTR
         JZ      CONVPR5X
         MOV     TEMP,#'1'
         JMP     CONVPRDN

CONVPR5X:MOV     MSB,PRCOD             ; SAVE CURRENT PRCOD
         MOV     R2,#1
         MOV     DPTR,#APRCOD
         MOV     R0,#PRCOD
         CALL    GETDEC
         MOV     TEMP,PRCOD
         CALL    BCD2BIN
         MOV     PRCOD,TEMP

         MOV     A,PRCOD
         JZ      CONVPR4Z

         CALL    GETCAL
         MOV     A,#0
         MOV     TEMP,#'1'              ; ASSUME PRCOD OK
         ORL     A,SWARRAY
         ORL     A,SWARRAY+1
         ORL     A,SWARRAY+2
         JNZ     CONVPRDN
CONVPR4Z:MOV     PRCOD,MSB              ; RESTORE OLD PRCOD. NEW INVALID.
         MOV     TEMP,#'0'              ; FLAG PRCOD OK OR NOT
CONVPRDN:RET


CONVERT: MOV     R2,#3
         MOV     DPTR,#PRICE
         MOV     R0,#CPGAL+2
         CALL    GETDEC

         MOV     DPTR,#TAX1T
         MOVX    A,@DPTR
         MOV     DPTR,#TAXTYPE
         MOVX    @DPTR,A

         MOV     R2,#3
         MOV     DPTR,#TAX1
         MOV     R0,#FEDTX+2
         CALL    GETDEC
         RET


CONV2ASC:MOV     R2,#4
         MOV     DPTR,#GVOL
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#4
         MOV     R0,#DATEIMG+3
         MOV     DPTR,#AGVOL
         CALL    MOVDECASC

         MOV     R2,#4
         MOV     DPTR,#NVOL
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#4
         MOV     R0,#DATEIMG+3
         MOV     DPTR,#ANVOL
         CALL    MOVDECASC

         MOV     R2,#6				;ADDED JAC TO STORE 12 DIGIT GROSS TOTALIZER
         MOV     DPTR,#GRTOTG
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#6
         MOV     R0,#DATEIMG+5
         MOV     DPTR,#ATGVOL
         CALL    MOVDECASC

         MOV     R2,#6				;ADDED JAC TO STORE 12 DIGIT NET TOTALIZER
         MOV     DPTR,#GRTOTN
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#6
         MOV     R0,#DATEIMG+5
         MOV     DPTR,#ATNVOL
         CALL    MOVDECASC

         MOV     A,PRCOD
         MOV     TEMP,A
         CALL    BIN2BCD
         MOV     A,TEMP
         CALL    MAKEASC
         MOV     DPTR,#APRCOD
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,B
         MOVX    @DPTR,A

         MOV     R2,#2
         MOV     DPTR,#TRUCK
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#2
         MOV     R0,#DATEIMG+1
         MOV     DPTR,#ATRUCK
         CALL    MOVDECASC             ; TRUCK NUMBER

         MOV     R2,#2
         MOV     DPTR,#DRIVER
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#2
         MOV     R0,#DATEIMG+1
         MOV     DPTR,#ADRIVER         ; DRIVER NUMBER
         CALL    MOVDECASC

         MOV     R2,#3
         MOV     DPTR,#TKTNO
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#3
         MOV     R0,#DATEIMG+2
         MOV     DPTR,#ATKTNO
         CALL    MOVDECASC

         MOV     R2,#3
         MOV     DPTR,#SERNUM
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#3
         MOV     R0,#DATEIMG+2
         MOV     DPTR,#ASERNUM
         CALL    MOVDECASC

         MOV     R2,#3                            ; TANKID AS UNIQUE IDENTIFIER FOR TDATA STORAGE IN NAVRAM
         MOV     DPTR,#TANKID
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#3
         MOV     R0,#DATEIMG+2
         MOV     DPTR,#ATANKID
         CALL    MOVDECASC

         MOV     DPTR,#STTIME
         CALL    RETTIME
         MOV     R2,#1
         MOV     R0,#DATEIMG+6
         MOV     DPTR,#ASTIME
         CALL    MOVDECASC
         MOV     R0,#DATEIMG+5
         MOV     R2,#1
         CALL    MOVDECASC
         MOV     R0,#DATEIMG+7
         MOV     R2,#1
         CALL    MOVDECASC
         MOV     R0,#DATEIMG+3
         MOV     R2,#1
         CALL    MOVDECASC
         MOV     R0,#DATEIMG+2
         MOV     R2,#1
         CALL    MOVDECASC

         MOV     DPTR,#FTIME
         CALL    RETTIME
         MOV     R2,#1
         MOV     R0,#DATEIMG+6
         MOV     DPTR,#AFTIME
         CALL    MOVDECASC
         MOV     R0,#DATEIMG+5
         MOV     R2,#1
         CALL    MOVDECASC
         MOV     R0,#DATEIMG+7
         MOV     R2,#1
         CALL    MOVDECASC
         MOV     R0,#DATEIMG+3
         MOV     R2,#1
         CALL    MOVDECASC
         MOV     R0,#DATEIMG+2
         MOV     R2,#1
         CALL    MOVDECASC

         MOV     DPTR,#ACOMPST
         MOV     A,COMP
         JNZ     CONV1
         MOV     A,#'0'
         JMP     CONV2
CONV1:   MOV     A,#'1'
CONV2:   MOVX    @DPTR,A

         MOV     DPTR,#VMSG2
         MOV     R1,#6
         MOV     R0,#DATEIMG
CONV4:   CLR 	 A
		 MOVC    A,@A+DPTR
         MOV     @R0,A
         INC     DPTR
         INC     R0
         DJNZ    R1,CONV4
         MOV     DPTR,#AVERNO
         MOV     R2,#6
         MOV     R0,#DATEIMG
         CALL    CARDMOV
CONV3:   RET


CONV2BCD:MOV     R2,#3
         MOV     DPTR,#PRICE
         MOV     R0,#DATEIMG+2
         CALL    GETDEC
         MOV     R2,#3
         MOV     DPTR,#PCEBCD
         MOV     R0,#DATEIMG
         CALL    CARDMOV

         MOV     R2,#3
         MOV     DPTR,#TAX1
         MOV     R0,#DATEIMG+2
         CALL    GETDEC
         MOV     R2,#3
         MOV     DPTR,#TAX1BCD
         MOV     R0,#DATEIMG
         CALL    CARDMOV

         MOV     R2,#3
         MOV     DPTR,#TAX2
         MOV     R0,#DATEIMG+2
         CALL    GETDEC
         MOV     R2,#3
         MOV     DPTR,#TAX2BCD
         MOV     R0,#DATEIMG
         CALL    CARDMOV

         MOV     R2,#3
         MOV     DPTR,#DISCRATE
         MOV     R0,#DATEIMG+2
         CALL    GETDEC
         MOV     R2,#3
         MOV     DPTR,#DSCBCD
         MOV     R0,#DATEIMG
         CALL    CARDMOV

         MOV     R2,#1
         MOV     DPTR,#DISCDAY
         MOV     R0,#DATEIMG
         CALL    GETDEC
         MOV     R2,#1
         MOV     DPTR,#DAYSBIN
         MOV     R0,#DATEIMG
         CALL    CARDMOV
         RET

SWAPTAX: MOV     DPTR,#TAX2T
         MOVX    A,@DPTR
         MOV     DPTR,#TAXTYPE
         MOVX    @DPTR,A

         MOV     R2,#3
         MOV     DPTR,#TAX2
         MOV     R0,#FEDTX+2
         CALL    GETDEC

         MOV     DPTR,#TAXSUB
         MOVX    A,@DPTR
         JZ      SWAPTAX1              ; TAX ON SUBTOTAL IF HIGH

         MOV     GCST,PGCST
         MOV     GCST+1,PGCST+1
         MOV     GCST+2,PGCST+2
         MOV     GCST+3,PGCST+3
         JMP     SWAPTAX2

SWAPTAX1:MOV     GCST,DATEIMG
         MOV     GCST+1,DATEIMG+1
         MOV     GCST+2,DATEIMG+2
         MOV     GCST+3,DATEIMG+3
SWAPTAX2:RET

SWAPDSC: MOV     DPTR,#DISCAKEY
         MOVX    A,@DPTR
         MOV     DPTR,#TAXTYPE
         MOVX    @DPTR,A

         MOV     R2,#3
         MOV     DPTR,#DISCRATE
         MOV     R0,#FEDTX+2
         CALL    GETDEC

         MOV     GCST,PGCST
         MOV     GCST+1,PGCST+1
         MOV     GCST+2,PGCST+2
         MOV     GCST+3,PGCST+3
         RET


;******************************************************
; CARDMOV MOVES FROM @R0 TO @DPTR , R2 TIMES
CARDMOV: MOV     A,@R0
         MOVX    @DPTR,A
         INC     R0
         INC     DPTR
         DJNZ    R2,CARDMOV
         RET


;************************************************
; MOVES FROM @R0    TO    @R1     R2 TIMES
;************************************************
MOVER:   MOV     A,@R0
         MOV     @R1,A
         INC     R1
         INC     R0
         DJNZ    R2,MOVER
         RET

;***************************************************
; ROUNDS AND SHIFTS RESULT TO PUT IN FORM BELOW
;
ROUNDEM: MOV     R2,#5                 ; RESULT    ... XX X.X XX
         MOV     A,#5                  ;  +                    5
         ADD     A,RESULT
         DA      A
         MOV     RESULT,A
         MOV     R1,#RESULT+1
ROUNDEM1:MOV     A,@R1
         ADDC    A,#0
         DA      A
         MOV     @R1,A
         INC     R1
         DJNZ    R2,ROUNDEM1
         CALL    SHIFTR                ; RESULT:  ... XX XX.XX  DOLS
         RET

;************************************************
; DOES LOGICAL SHIFT RIGHT OF RESULT AREA ONE NIBBLE
;************************************************
SHIFTR:  CLR     A                     ; SHIFT RESULT AREA DOWN
         MOV     R2,#8                 ; IE. 00 00 00 00 0X.XX XX XY
         MOV     R1,#RESULT+7          ; >>  00 00 00 00 00 X.X XX XX
SHIFTR1: XCH     A,@R1
         SWAP    A
         XCHD    A,@R1
         DEC     R1
         DJNZ    R2,SHIFTR1
         RET


SHDP6:   PUSH    DPL
         PUSH    DPH
         MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JZ      SHDP6B
         POP     DPH
         POP     DPL
         MOV     R1,#6
SHDP6A:  INC     DPTR
         DJNZ    R1,SHDP6A
         JMP     SHDP6C
SHDP6B:  POP     DPH
         POP     DPL
SHDP6C:  RET

SHDP20:  PUSH    DPL
         PUSH    DPH
         MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JZ      SHDP20B
         POP     DPH
         POP     DPL
;        MOV     R1,#25
SHDP20A: INC     DPTR
         DJNZ    R1,SHDP20A
         JMP     SHDP20C
SHDP20B: POP     DPH
         POP     DPL
SHDP20C: RET

SHDP18:  PUSH    DPL
         PUSH    DPH
         MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JZ      SHDP18B
         POP     DPH
         POP     DPL
         MOV     R1,#18
SHDP18A: INC     DPTR
         DJNZ    R1,SHDP18A
         JMP     SHDP18C
SHDP18B: POP     DPH
         POP     DPL
SHDP18C: RET


;******************************************************
; GETDEC  WILL GET TWO BYTES POINTED TO BY DPTR AND
;    DPTR+1 INTO A, INTO @R0, R2 TIMES. NOTE R0 POINTS
;                                 AT TOP END OF DESTINATION
GETDEC:  MOVX    A,@DPTR               ; MSB FIRST
         INC     DPTR
         XCH     A,B
         MOVX    A,@DPTR
         XCH     A,B                   ; HAD TO REVERSE THEM
         CALL    ASCHEX                ; CONVERT TO HEX
         INC     DPTR                  ; READY FOR NEXT TIME
         MOV     @R0,A
         DEC     R0
         DJNZ    R2,GETDEC
         RET


;************************************************
; CLEAR OUT THE RESULT AREA
;************************************************
XLRRES:  MOV     R2,#8                 ; CLEAR THE RESULT AREA
         MOV     R1,#RESULT
XLRRE1:  MOV     @R1,#0
         INC     R1
         DJNZ    R2,XLRRE1
         RET

;************************************************
; CLEAR OUT BOTH MLPR AND MCND
;************************************************
CLMRMC:  MOV     R2,#8                 ; CLEAR BOTH MLPR AND MCND
         MOV     R1,#MCND
CLMRM1:  MOV     @R1,#0
         INC     R1
         DJNZ    R2,CLMRM1
         RET


;**************************************
; MOVE A DECIMAL FIELD INTO AN ASCII FIELD
; R0 INPUT   DPTR OUTPUT  R2 TIMES
;   ALSO REVERSES ORDER.  R0 AT MSB END
MOVDECASC:
         MOV     A,@R0
         CALL    MAKEASC
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,B
         MOVX    @DPTR,A
         INC     DPTR
         DEC     R0
         DJNZ    R2,MOVDECASC
         RET
; DOES THE OPPOSITE OF ASCHEX

HEXASC:  MOV     TEMP,A                ; SAVE ACC
         ANL     A,#0FH                ; GET LSN
         ORL     A,#30H                ; ADD THE 3 FOR ASCII
         MOV     B,A                   ; LSB IN B
         MOV     A,TEMP
         SWAP    A                     ; GOOD # IN LSN
         ANL     A,#0FH                ; GET LSN
         ORL     A,#30h                ; ADD 3
         RET


;******************************************************
; ASCHEX CONVERTS ASCII DIGITS IN A:B INTO HEX IN A
ASCHEX:  SWAP    A                     ; SWAP NIBBLES
         ANL     A,#0F0H               ; KILL LSN
         XCH     A,B                   ; ACCA<==>ACCB
         ANL     A,#0FH                ; KILL MSN
         ORL     A,B                   ; PUT EM TOGETHER
         RET

;******************************************************
; CHKTBLS LOOKS FOR COMP TABLES PRESENT IN SECOND 8K
CHKTBLS: SETB	  C						;TODO - no longer needed because tables are in code space can be removed but have to fix calls to function
		 RET

		  CLR     C                     ; IF C SET ON RETURN THEN TABLES OK

          MOV     DPTR,#PROPANE         ; TEST FOR 13H @1000H IN 2ND 32K
;;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
;         MOVX    A,@DPTR
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY
;         CJNE    A,#13H,CHKTBL2

         CALL    VCF
         MOV     A,TEMP
         CJNE    A,#13H,CHKTBL2


         SETB    C                     ; TABLES SEEM GOOD
         JMP     CHKTBL1               ; CARRY = TABLES LOADED
CHKTBL2:
         CLR     C                     ; NO CARRY = NO TABLES
         MOV     COMP,#0               ; KILL COMP IF TABLE NOT PRESENT
CHKTBL1:
; I THINK THIS CODE WAS JUST TEMPORARY FOR RICK TO CONFIRM THAT VCF WAS WORKING DURING DEVELOPMENT
;         CALL    SELECTP3
;         CALL    OUTA
        RET


SETEMCFG:MOV     TEMP,#00H
         MOV     DPTR,#EMPLSREV
         MOVX    A,@DPTR
         JZ      SETEMCF1
         MOV     A,#01H
         ORL     TEMP,A
SETEMCF1:MOV     DPTR,#EMCHANNL
         MOVX    A,@DPTR
         JZ      SETEMCF2
         MOV     A,#02H
         ORL     TEMP,A
SETEMCF2:MOV     DPTR,#EMFLODIR
         MOVX    A,@DPTR
         JZ      SETEMCFX
         MOV     A,#04H
         ORL     TEMP,A
SETEMCFX:MOV     DPTR,#EMCONFIG
         MOV     A,TEMP
         MOVX    @DPTR,A
         RET


; PRESET ON DOLLARS ROUTINES -----------------------------------------------------------------------------------

SAVEVAR: MOV     R2,#28
         MOV     R0,#ECGALS
         MOV     DPTR,#SECGALS
         CALL    CARDMOV

         MOV     R2,#15
         MOV     R0,#SWFCTR
         MOV     DPTR,#SSWFCTR
         CALL    CARDMOV

         MOV     A,TNEGH
         MOV     DPTR,#STNEGH
         MOVX    @DPTR,A

         MOV     A,TVALL
         MOV     DPTR,#STVALL
         MOVX    @DPTR,A
         RET

RESTVAR: MOV     R2,#28
         MOV     R1,#ECGALS
         MOV     DPTR,#SECGALS
         CALL    EXTMOV

         MOV     R2,#15
         MOV     R1,#SWFCTR
         MOV     DPTR,#SSWFCTR
         CALL    EXTMOV

         MOV     DPTR,#STVALL
         MOVX    A,@DPTR
         MOV     TVALL,A

         MOV     DPTR,#STNEGH
         MOVX    A,@DPTR
         MOV     TNEGH,A
         RET

;****************************************
; COMPUTE PRESET GALLONS GIVEN PRESET $$
COMPPRES:
         MOV     R2,#3
         MOV     DPTR,#PRICE
         MOV     R0,#CPGAL+2
         CALL    GETDEC

         CALL    PRICECHK
         JNZ     COMPPRE5
         JMP     COMPPRE4
COMPPRE5:
         CALL    PRETEST
                                       ; TEST FOR PREAMT==0
         JNZ     COMPPRE3              ; SKIP OVER IF NOT ZERO
         JMP     COMPPRE4
COMPPRE3:CALL    CLRGCST               ; GCST = 0
         CALL    CLROCGALS             ; OLDCGALS  =  0
         CALL    IGALS100              ; IGALS = 10 0.0
         CALL    CLRDEL

COMPPRE1:MOV     DPTR,#IGALS
         MOVX    A,@DPTR               ; A=LSB(IGALS)
         MOV     B,A                   ; WHILE(IGALS>0)
         INC     DPTR
         MOVX    A,@DPTR               ; TWO BYTES
         ORL     A,B
         JZ      COMPPRDN              ; IF IGALS == 00 0.0

         CALL    DOCALC7               ; NVOL=OLDCGALS+IGALS

         MOV     DPTR,#PRINTFLG        ; DISALLOW ALL PRINTING & STORING
         MOV     A,#0
         MOVX    @DPTR,A
         CALL    CLRPGCST
         CALL    CONVERT
         CALL    DOPRIN11              ;  TO GET TRIAL PRESET AMOUNT  PREAMT

         MOV     DPTR,#PRINTFLG        ; RE ENABLE PRINTING AND STORING
         MOV     A,#1
         MOVX    @DPTR,A

         CALL    IFPALEGC              ; IF PREAMT < GCST SKIP
         JC      COMPPRE2              ; C==0 IFF <

         MOV     R2,#4                 ; IF HERE GCST>PREAMT
         MOV     DPTR,#OLDCGALS
         MOV     R1,#DATEIMG           ; DATEIMG=OLDCGALS
         CALL    EXTMOV
         MOV     R2,#4                 ; THIS RESTORES LAST NVOL
         MOV     DPTR,#NVOL
         MOV     R0,#DATEIMG
         CALL    CARDMOV               ; NVOL = DATEIMG

         MOV     R2,#4
         MOV     DPTR,#IGALS
         MOV     R1,#RESULT
         CALL    EXTMOV                ; RESULT=IGALS
         CALL    SHIFTR                ; ONE DIGIT RIGHT
         MOV     RESULT+3,#0           ; JUST INSURANCE
         MOV     R2,#4
         MOV     DPTR,#IGALS
         MOV     R0,#RESULT
         CALL    CARDMOV               ; BACK TO IGALS

COMPPRE2:
         MOV     R2,#4
         MOV     DPTR,#NVOL            ; MOVES NVOL TO OLDCGALS
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#4
         MOV     DPTR,#OLDCGALS
         MOV     R0,#DATEIMG
         CALL    CARDMOV               ; OLDCGALS=CGALS
         JMP     COMPPRE1              ; BACK TO TEST

COMPPRDN:MOV     R2,#4
         MOV     DPTR,#NVOL
         MOV     R1,#DATEIMG
         CALL    EXTMOV                ; DATEIMG = CGALS
         MOV     R2,#3
         MOV     R0,#DATEIMG
         MOV     DPTR,#LASTPRST        ; PRESET=CGALS
         CALL    CARDMOV
         CALL    CLRCGALS
COMPPRE4:RET

;*********** PRETEST FOR PREAMT==0 ******
PRETEST: MOV     R2,#3                 ; PREAMT XXXX.XX
         MOV     DPTR,#PREAMT          ; IN HI RWM
         MOV     B,#0                  ; RUNNING AMT
PRETEST1:MOVX    A,@DPTR               ; GET PREAMT
         ORL     A,B                   ; OR B WITH IT
         MOV     B,A                   ; PUT IT BACK
         INC     DPTR                  ; MOVE UP ONE IN PREAMT
         DJNZ    R2,PRETEST1           ; SEE IF THREE BYTES DONE
         MOV     A,B                   ; PUT RESULT IN A
         RET


CLRPGCST:MOV     PGCST,#0
         MOV     PGCST+1,#0
         MOV     PGCST+2,#0
         MOV     PGCST+3,#0
         MOV     GCST,#0
         MOV     GCST+1,#0
         MOV     GCST+2,#0
         MOV     GCST+3,#0
         RET


;*********** IF(GCST>PREAMT) CY=0 *******
IFPALEGC:MOV     R2,#3
         MOV     R0,#PGCST
         MOV     R1,#DATEIMG
         CALL    MOVER                 ; DATEIMG[2..0]=GCST
         MOV     R0,#DATEIMG
         MOV     R2,#3
         CALL    NEGATE                ; DATEIMG= -GCST
         MOV     R2,#3
         MOV     DPTR,#PREAMT
         MOV     R1,#DATEIMG+3         ; DATEIMG[5..3]=PREAMT
         CALL    EXTMOV
         MOV     R2,#3
         SETB    C                     ; PREAMT-CGALS
         MOV     R0,#DATEIMG+3
         MOV     R1,#DATEIMG
         CALL    SUMUP                 ; CY==0 IF GCST > PREAMT
         RET

;*****************************************
; CGALS = OLDCGALS+IGALS
DOCALC7: MOV     R2,#4                 ; DATEIMG[4]=OLDCGALS
         MOV     DPTR,#OLDCGALS
         MOV     R1,#DATEIMG
         CALL    EXTMOV
         MOV     R2,#4
         MOV     DPTR,#NVOL
         MOV     R0,#DATEIMG
         CALL    CARDMOV               ; CGALS = OLDCGALS
         MOV     R2,#4
         MOV     DPTR,#IGALS
         MOV     R1,#DATEIMG
         CALL    EXTMOV                ; DATEIMG = IGALS
         MOV     R2,#4
         CLR     C
         MOV     DPTR,#NVOL
         MOV     R0,#DATEIMG
         CALL    SUMUPPER              ; CGALS=OLDCGALS+IGALS
         RET

;*********** CLEAR OLD CGALS ************
CLROCGALS:
         MOV     R2,#4
         MOV     DPTR,#OLDCGALS        ; 4 BYTES
         MOV     A,#0
CLROCGA1:MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R2,CLROCGA1
         RET

;*********** CLEAR GAS COST *************
CLRGCST: MOV     R2,#5
         MOV     R1,#PGCST
         MOV     A,#0
CLRGCST1:MOV     @R1,A
         INC     R1
         DJNZ    R2,CLRGCST1
         RET

;***********INITIALIZE IGALS TO 100.0
IGALS100:MOV     DPTR,#IGALS
         MOV     A,#0
         MOVX    @DPTR,A               ; 00 00 10 0.0
         INC     DPTR
         MOV     A,#10H
         MOVX    @DPTR,A
         INC     DPTR
         MOV     A,#0
         MOVX    @DPTR,A
         INC     DPTR
         MOVX    @DPTR,A
         RET



CHKFR:
         CALL    WAIT50
         MOV     DPTR,#FRFLAG
         //Since P4 is not bit addressable, we have to use TEMPBIT to address bits
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         MOV     TEMPBIT, P4
		 MOV     SFRPAGE,#00H          ;Switch SFR page
         JB      TEMPBIT.2,CHKFR1      ; ;P4.2 is EMPULSE
         MOV     A,#0
         JMP     CHKFR2
CHKFR1:  MOV     A,#1
CHKFR2:  MOVX    @DPTR,A
         CALL    WAIT50                ; WAIT OUT EM DELAY
         CALL    WAIT50
         RET


SETBLDG1FLG:
         MOV     DPTR,#PRBLSTDEG
         MOV     A,#1                  ; SET THE BLASTER DEGREE SYMBOL FLAG FOR THE VOL CORR LINE DEG SYMBOL FOR BLASTER V1/V2
         MOVX    @DPTR,A
         RET
SETBLDG2FLG:
         MOV     DPTR,#PRBLSTDEG
         MOV     A,#2                  ; SET THE BLASTER DEGREE SYMBOL FLAG FOR THE FINAL TEMP LINE SYMBOLS FOR PRINT GROSS
         MOVX    @DPTR,A
         RET
CLRBLDGFLG:
         MOV     DPTR,#PRBLSTDEG
         MOV     A,#0                  ; CLEAR THE BLASTER DEGREE SYMBOL FLAG, IT SHOULD HAVE BEEN CLEARED IN BLAST BUT CALLING THIS IS INSURANCE
         MOVX    @DPTR,A
         RET


BLAST:   PUSH    DPH
         PUSH    DPL

         CALL    LODHCMDFLG            ; DON'T BLAST IF GOING TO HOST
         JZ      BLAST1
         JMP     BLASTX

BLAST1:  MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#5,BLAST2           ; 5 = BLASTER V1, USE NEW FONT BLOCK
         JMP     BLAST3
BLAST2:  CJNE    A,#11,BLAST3B         ; 11 = BLASTER V2, USE NEW FONT BLOCK
BLAST3:  MOV     DPTR,#BMSG32          ; BLASTERS ALWAYS USE V2 BLAST BLOCK SINCE FONT IS NOW FIXED
         MOV     R0,#59
BLAST3A: CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R0,BLAST3A
         JMP     BLASTX
BLAST3B: CJNE    A,#13,BLASTX          ; 13 = FOR DATAMAX M4TE THAT HAS TO BE SENT ON EVERY LINE WHEN PRINTING IN LINE PRINTER MODE
         MOV     A,#1BH
         CALL    OUTA
         MOV     A,#21H
         CALL    OUTA
         MOV     A,#30H                ; 1BH 21H 30H = DOUBLE-HEIGHT DOUBLE-WIDTH FOR ENTIRE LINE, APPLIES UNTIL NEXT CR/LF/CRLF
         CALL    OUTA
         MOV     R0,#8
         MOV     A,#' '
BLAST3C: CALL    OUTA                  ; SEND 8 SPACES TO SET THE LEFT MARGIN OVER A LITTLE
         DJNZ    R0,BLAST3C
         JMP     BLASTX

BLASTX:  POP     DPL
         POP     DPH
         RET



BLASTN:
         CALL    LODHCMDFLG
         JZ      BLASTN2
         CALL    CRLF
         JMP     BLASTN1               ; DON'T BLASTN IF ISHSTCMD SET

BLASTN2: MOV     DPTR,#PRTFLG
         MOVX    A,@DPTR
         CJNE    A,#5,BLASTN2A         ; 5 = BLASTR V1
         JMP     BLASTN2B
BLASTN2A:CJNE    A,#11,BLASTN1         ; 11 = BLAST2 V2
BLASTN2B:CALL    CRLF                  ; NEED TO SEND THIS CRLF AFTER THE TEXT TO COMPLETE THE 'T' COMMAND AFTER TEXT SENT!

         MOV     DPTR,#PRBLSTDEG
         MOVX    A,@DPTR
         CJNE    A,#1,BLASTN3C         ; 1 = BLASTER V1/V2, NEED TO PRINT THE DEGREE SYMBOL FOR THE VOL CORR MESSAGE
                                       ; WE CAN USE OTHER #'S TO MEAN OTHER LOCATIONS WHEN WE NEED THE SYMBOL
         CALL    CLRBLDGFLG            ; CLEAR THE BLASTER DEGREE SYMBOL FLAG
         MOV     DPTR,#BMSG37          ; SEND THE LINE TO PRINT THE DEGREE SYMBOL ON THIS BLASTER V1/V2 LABEL
         MOV     R0,#13
BLASTN3B:CLR 	 A
	     MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R0,BLASTN3B
         JMP     BLASTNX               ; ONLY PRINTING 1 SYMBOL FOR VOL CORR LINE, GET OUT

BLASTN3C:CJNE    A,#2,BLASTNX          ; 2 = BLASTER V1/V2, NEED TO PRINT THE DEGREE SYMBOLS FOR THE FINAL TEMP LINE IN 'PRINT GROSS'
         CALL    CLRBLDGFLG            ; CLEAR THE BLASTER DEGREE SYMBOL FLAG
         MOV     DPTR,#BMSG38          ; SEND THE LINE TO PRINT THE F DEGREE SYMBOL ON THIS BLASTER V1/V2 LABEL
         MOV     R0,#13                ; BOTH C & F SYMBOLS ARE NEEDED ON THE SAME LINE
BLASTN3D:CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R0,BLASTN3D

         MOV     DPTR,#BMSG39          ; SEND THE LINE TO PRINT THE C DEGREE SYMBOL ON THIS BLASTER V1/V2 LABEL
         MOV     R0,#13                ; BOTH C & F SYMBOLS ARE NEEDED ON THE SAME LINE
BLASTN3E:CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R0,BLASTN3E
         JMP     BLASTNX               ; ONLY PRINTING 2 SYMBOLS FOR PRTGROSS LINE, GET OUT

BLASTNX: MOV     DPTR,#BMSG93          ; SEND END+CRLF ONLY ... 5 CHARS
         MOV     R0,#5
BLASTN4A:CLR	 A
		 MOVC    A,@A+DPTR
         CALL    OUTA
         INC     DPTR
         DJNZ    R0,BLASTN4A
BLASTN1: RET


PSLABEL: MOV     DPTR,#PRSSET
         MOVX    A,@DPTR
         JZ      PSL1
         MOV     DPTR,#LGDFLG2
         MOVX    A,@DPTR
         ORL     A,#40H                ; SET PRESET LEGEND ON
         MOVX    @DPTR,A
         JMP     PSL2
PSL1:    MOV     DPTR,#LGDFLG2
         MOVX    A,@DPTR
         ANL     A,#0BFH
         MOVX    @DPTR,A
PSL2:    RET


; INITIALIZE PORTS AND CONTROL REGISTERS
INIT:

;------------------------------------------------------------------------------
; TURN OFF WATCHDOG TIMER BEFORE ANYTHING ELSE
;------------------------------------------------------------------------------

         ANL     PCA0MD,  #NOT(40h)   ; Clear Watchdog Enable bit

;------------------------------------------------------------------------------
; SETUP CROSSBAR
;------------------------------------------------------------------------------

         MOV     XBR0,#01h			   ;Enable UART on P0.4(TX) and P0.5(RX)
   		 MOV     XBR2,#40h			   ;Enable crossbar and weak pull-ups

;------------------------------------------------------------------------------
; Initialize the Port I/O
;------------------------------------------------------------------------------

		 MOV     P0MDIN,#11111111B     ;All I/O set for digital in
		 MOV     P1MDIN,#11111111B     ;All I/O set for digital in
		 MOV     P2MDIN,#11111111B     ;All I/O set for digital in

		 MOV     SFRPAGE,#0FH          ;Switch SFR page
		 MOV     P3MDIN,#11111111B     ;All I/O set for digital in
		 MOV     P4MDIN,#11111111B     ;All I/O set for digital in
		 MOV     P5MDIN,#11111111B     ;All I/O set for digital in
		 MOV     P6MDIN,#11111111B     ;All I/O set for digital in
		 MOV	 P3MDOUT,#11100011B	   ;P3.5-7 push pull, P3.0-1 push pull, all others open drain
		 MOV	 P4MDOUT,#00000000B	   ;All open drain
		 MOV	 P5MDOUT,#11111111B	   ;All push-pull for external memory
		 MOV	 P6MDOUT,#11111111B	   ;All push-pull for external memory
		 MOV	 P7MDOUT,#00000000B	   ;All open drain

		 MOV     SFRPAGE, #00H         ;Switch back to main SFR page
		 MOV	 P1MDOUT,#00000000B	   ;All open drain
		 MOV	 P2MDOUT,#11000000B	   ;P2.6 and P2.7 push pull, all other open drain

		 MOV     P0,#11000000B
         MOV     P1,#10001011B
         MOV     P2,#11000111B
        ;MOV     P3,#01100111B         ; P3.4 = DATAOUT = DATA FROM SIP TO MEM DONGLE + COMMANDS TO DONGLE
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         MOV     P4,#01100110B         ; P3.4 = DATAOUT = DATA FROM SIP TO MEM DONGLE + COMMANDS TO DONGLE
		 MOV     SFRPAGE,#00H          ;Switch SFR page
;*-*         ORL     MCON,#4               ; ALL DATA MEMORY IN FAR RAM
;------------------------------------------------------------------------------
; Initialize clock
;------------------------------------------------------------------------------
CLKINIT: MOV     CKCON,#00h			   ;T0 AND T1 USE SYSTEM CLOCK/12
   		 ORL     REG0CN,#10h           ;Enable the precision osc. bias
   		 ORL     OSCICN,#80h           ;Enable the precision internal osc.
		 MOV     RSTSRC,#06h           ;Enable missing clock detector and leave VDD Monitor enabled.
		 MOV     CLKSEL,#00            ;Select precision internal osc and system clock / 1
;------------------------------------------------------------------------------
; Initialize UART
;------------------------------------------------------------------------------
SERSET:  MOV     SCON0,#00010000B       ; 8 BIT, NO STOP, RCV ENABLE, XMIT INTERRUPT ENABLED
         MOV     TCON,#11010000B       ; T1OVF,T1RUN,T0OVF=0,T0RUN
         MOV     TMOD,#00100001B       ; T1 8 BIT AUOT-RELOAD, T0 16 BIT
         MOV     TH1,#0x96               ; 9600 BAUD WITH TIMER CLOCK SOURCE = SYSCLK/12
         MOV     IE,#00H               ; ENABLE ONLY SERIAL INTERUPT
         SETB SCON0_TI				   ; Indicate TX0 ready

;------------------------------------------------------------------------------
; Initialize external memory
;------------------------------------------------------------------------------
MEMINIT: MOV 	 EMI0CF, #0Ch		   ;MULTIPLEXED, EXTERNAL ONLY, ALE=1 SYSCLK CYCLE
		 MOV  	 EMI0TC,#00h		   ;ADDRESS SETUP 0 SYSCLK CYCLES, RD/WR 1 SYSCLK CYCLES, ADDRESS HOLD 0 SYSCLK CYCLES

;------------------------------------------------------------------------------
; Initialize Flash memory banking
;------------------------------------------------------------------------------
;		 MOV	 PSBANK,#0x21		   ; IFBANK (Instruction Fetches for addresses 0x8000-0xFFFF) from bank 1
		 							   ; COBANK (Constant fetches from bank 2

;------------------------------------------------------------------------------
; Load defaults if product code =0 (new chip)
;------------------------------------------------------------------------------
		MOV		DPTR,#LASTPRC		; LAST PRODUCT CODE STORED, WILL BE 0 IF NEW CHIP
		MOVX	A,@DPTR
		JNZ		LOADDEFX			;IF PRCOD=0 THEN THIS IS A NEW CHIP AND WE NEED TO LOAD DEFAULTS
LOADDEF:MOV     A,#2
		MOV     DPTR,#PRTFLG		; 2=THERMAL FOR EC BY DEFAULT

		MOVX    @DPTR,A
		MOV     A,#1

		MOV     DPTR,#DIVISOR		;SET MRATIO TO 1
		MOVX    @DPTR,A

		MOV     DPTR,#COPIES		;SET PRINT COPIES TO 1
		MOVX    @DPTR,A

		MOV     DPTR,#CPYCPY		;COPIES FOR HOST CONTROL SET TO 1
		MOVX    @DPTR,A

		MOV		DPTR,#REGNUM		;SET REGISTER NUMBER TO 1
		MOVX	@DPTR,A

		MOV     DPTR,#LASTPRC		;LAST PRODUCT CODE SET TO 1
		MOVX    @DPTR,A

LOADDEF1:
		MOV     DPTR,#MSG72           ; RESET
		CALL    SHOW
		MOV     DPTR,#MSG102          ; CYCLE
		CALL    SHOW
		MOV     DPTR,#MSG103          ; POWER
		CALL    SHOW
		JMP     LOADDEF1

LOADDEFX:
;------------------------------------------------------------------------------
; Initialize eCount
;------------------------------------------------------------------------------


         CLR     RRCNT                 ; INIT REMOTE READOUT COUNT LINE
         CLR     RRCLR                 ; INIT CLEAR
		 MOV     SFRPAGE,#0FH          ;Switch SFR page
         ANL     AIRSOL,#0xF7			; CLR AIRSOL AIRSOL CLEAR = OPTIC WET
		 MOV     SFRPAGE,#00H          ;Switch SFR page


         MOV     DPTR,#SWAUTHFLG       ; CHECK S/W AUTHORIZATION
         MOVX    A,@DPTR
         JZ      INIT10

         MOV     DPTR,#RSTARM
         MOV     A,#0
         MOVX    @DPTR,A               ; DISARM SSRST ON POWER UP FOR FMS
         JMP     INIT11                ; IF SWAUTHFLG IS ON
INIT10:  MOV     DPTR,#RSTARM
         MOV     A,#1                  ; IF SWAUTHFLG OFF, ARM SSRST
         MOVX    @DPTR,A

INIT11:
        MOV   R3,#SRADD			; THIS STUFF NEEDED TO GET THE
        MOV   R4,#SRVAL			; RTC REAL TIME CLOCK RUNNING SEZ
        CALL  WBYTE
        MOV   R3,#INTADD
        MOV   R4,#INTVAL
        CALL  WBYTE           		; SET STATUS AND CONTROL REGISTERS.


INIT5:
         CALL    SELECTHST             ; INITIAL DEFAULT COM PORT
         CALL    LOADPOT               ; LOAD THE DIGITAL COMP POT
         CALL    WAIT2000                ; TIME FOR DMICRO TO FINISH UP
;         SETB    DMCLK                 ; LET GO HIGH AND SEE IF STILL
;         CALL    WAIT1                ; LOW FROM DM11 OR ABOVE
;         JNB     DMCLK,INIT5A


; IF WE DON'T GET HANDSHAKE IT MEANS DMICRO IS POWERED UP AND READY = IGNORE
INIT5A:
;         CLR     DMCLK
;         SETB    DMMODE
;         CALL    WAIT50                ; WAIT 150 MS FOR DM TO DECIDE IF OLD ECX
;         CALL    WAIT50
;         CALL    WAIT50
;         CLR     DMMODE
;         CALL    WAIT1




INIT5AB:
;         CALL    CLRBYTES              ; CLEAR BEFORE (U) AND AFTER (W) PRINT BYTES FROM HOST
         CALL    CHKTBLS               ; CHECK FOR LP IN TABLE1 AND ETHANOL IN TABLE9
                                       ; WILL SET COMP=0 IF TABLE ERR



         MOV     DPTR,#SWAR            ; RELOAD SWARRAY WITH LAST SETTING
         MOVX    A,@DPTR
         MOV     SWARRAY,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     SWARRAY+1,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     SWARRAY+2,A
         CALL    DOSWFAC               ; PUT IT IN PLACE FOR CALCS

         MOV     DPTR,#LASTPRC
         MOVX    A,@DPTR
         MOV     PRCOD,A               ; RELOAD LAST PRODUCT CODE.

         MOV     DPTR,#S1LAST
         MOVX    A,@DPTR
         MOV     S1SHOFF,A
         MOV     DPTR,#S2LAST
         MOVX    A,@DPTR
         MOV     S2DWELL,A
         INC     DPTR
         MOVX    A,@DPTR
         MOV     S2DWELL+1,A           ; RESTORE LAST VALVE SETTINGS

         MOV     DPTR,#PRINTFLG        ; PRESET ON $ HOLDOFF VARIABLE
         MOV     A,#1
         MOVX    @DPTR,A               ; ALLOW PRINTING

         CALL    CLRHCMDFLG            ; CLEAR FLAG MEANING IN HOST COMMS

         MOV     DPTR,#EMULATEFLG      ; 0=OFF=DEFAULT, 1=TEMP OFF, 2=PA21, 3=E141
         MOVX    A,@DPTR
         CJNE    A,#1,INIT5AC

         MOV     DPTR,#LASTEMUFLG
         MOVX    A,@DPTR
         MOV     DPTR,#EMULATEFLG      ; RESTORE LAST EMULATION FLAG
         MOVX    @DPTR,A

INIT5AC:
         MOV     A,#0
         CALL    SETHSTLNFLG           ; CLEAR FLAG MEANING HAVE HOST LINE TO PRINT

         MOV     DPTR,#FLTIME
         MOVX    @DPTR,A               ; CLEAR FLEET TIME
         MOV     DPTR,#FLTIME+1
         MOVX    @DPTR,A               ; CLEAR FLEET TIME AND TIMER OVERRIDE
                                       ; AGAIN AT END OF DELIVERY
         MOV     A,#0                  ; ADDED FOR INSURANCE

         MOV     DPTR,#HOST            ; START NOT IN HOST MODE
         MOVX    @DPTR,A

         MOV     DPTR,#EMVARCHGD       ; MAKE SURE FLAG INDICATING EM CONFIG CHANGED IS CLEARED ON POIWERUP
         MOVX    @DPTR,A

         MOV     DPTR,#RETURN          ; CLEAR FLAG TO INDICATE EVENT INITIATED BY HOST
         MOVX    @DPTR,A

         MOV     DPTR,#STORFLG
         MOVX    @DPTR,A               ; CLEAR TANK FLAG

         MOV     DPTR,#TORFLG
         MOV     R1,#24
         CALL    RESETLOOP             ; TIMER OVERRIDE OFF, NEXT 24 VARIABLES CLEARED AT ONCE


         CALL    CLRCMNDFLG            ; CLEAR ALL HOST 'i' COMMAND FLAGS

;		MOVED INITIALIZATION OF VARIABLES TO LOADDEF
;         MOV     DPTR,#COPIES
;         MOVX    A,@DPTR
;         JNZ     INIT3
;         MOV     A,#1
;         MOVX    @DPTR,A               ; DEFAULT TO 1 COPY IF ZERO

INIT3:   MOV     DPTR,#UTYPE
         MOVX    A,@DPTR
         JZ      INIT4A					;SKIPPING INIT8 BECAUSE MOVED TO LOADDEF
         MOV     COMP,#0               ; ENSURE COMP OFF IN MASS MODE

;INIT8:   MOV     DPTR,#REGNUM
;         MOVX    A,@DPTR
;         JNZ     INIT4A
;         MOV     A,#1                  ; DEFAULT TO 1 IF REGNUM IS ZERO
;         MOVX    @DPTR,A

INIT4A:
		 MOV	 R1,#50
		 MOV	 DPTR,#PRMSG55
		 MOV	 R2,DPL
		 MOV	 R3,DPH
		 MOV	 DPTR,#PRMSG55T
LOADTXT: CLR	 A
		 MOVC	 A,@A+DPTR
		 INC	 DPTR
		 PUSH	 ACC					;SAVE VALUE IN ACC
		 CALL	 SWAPADDR
		 POP	 ACC					;RETRIEVE ACC
		 MOVX	 @DPTR,A
		 INC	 DPTR
		 CALL	 SWAPADDR
		 DJNZ	 R1,LOADTXT
		 JMP	 HOOK
SWAPADDR:MOV	 A,DPL					;SWAP CODESPACE ADDRESS WITH XRAM ADDRESS
		 XCH	 A,R2
		 MOV	 DPL,A
		 MOV	 A,DPH
		 XCH	 A,R3
		 MOV	 DPH,A
		 RET

HOOK:
         CLR     DMCLK
         MOV     DPTR,#MSG9            ; "MIDCOM"
         CALL    SHOW
         MOV     DPTR,#MSG37           ; SW VER
         CALL    SHOW
         MOV     DPTR,#VMSG1           ; M  NNN
         CALL    SHOW
         MOV     DPTR,#VMSG2           ; U  NNN
         CALL    SHOW
         CALL    WAIT4000



; IF WE'RE EMULATING ANOTHER VERSION WE NEED TO TELL THEM
INIT4B:  MOV     DPTR,#EMULATEFLG      ; 0=OFF=DEFAULT, 1=TEMP OFF, 2=PA21, 3=E141
         MOVX    A,@DPTR
         CJNE    A,#2,INIT4C
         MOV     DPTR,#MSG226          ; "EMULAT"
         CALL    SHOW
         MOV     DPTR,#MSG227          ; "PA21"
         CALL    SHOW
         JMP     INIT6
INIT4C:  CJNE    A,#3,INIT6
         MOV     DPTR,#MSG226          ; "EMULAT"
         CALL    SHOW
         MOV     DPTR,#MSG232          ; "E141E"
         CALL    SHOW
         JMP     INIT6

INIT6:   CALL    KPRESS                ; TRAP DOOR INTO ACCESS CODE
         MOV     A,KEY
         JNZ     INIT6A                ; IF KEY!=0 THEN CHECK KEY


INIT6X:  JMP     EXIT                  ; IF NO KEY GET OUT

INIT6A:
         CJNE    A,#RIGHTKEY,HOOK      ; RIGHT WILL BE ONLY KEY WE ALLOW ON BOOTUP, OTHERS CAUSE INFINITE LOOP OF VERSION SCREENS
                                       ; CAN BE USEFUL TO HELP DETECT FAILED KEYBOARD ...
         CALL    NEWCODE

INIT6B:                                ; CLEAR FLAGS NORMALLY CLEARED WITH SUCCESSFUL PRINT
         MOV     A,#0                  ; IF YOU ADD/CHANGE VARS HERE UPDATE BOTH INIT6B + PRTFX1

         MOV     DPTR,#DELFLG          ; RESET DELIVERY FLAG WITH NO PRINTER
         MOVX    @DPTR,A

         MOV     DPTR,#POWFAIL         ; RESET POWFAIL TO DELIVERY PRINTED
         MOVX    @DPTR,A

         CALL    DISCON                ; CALL #9: AFTER BYPASS NEED TO MAKE SURE POWER/COMMS UNLATCHED IN PCM
         CALL    UNLATCH

         JMP     EXIT



//	CLRTOT4 is no longer used but code was left in for reference or future use. Could be deleted to free up space. Appears to clear all settings
//which can also be done in the bootloader
CLRTOT4: MOV     DPTR,#TGVOL           ; CLEAR 4 4-BYTE DELIVERY TOTALIZERS
         MOV     R1,#16
         CALL    RESETLOOP

         MOV     DPTR,#GRTOTG          ; CLEAR 2 6-BYTE OVERALL TOTALIZERS
         MOV     R1,#12
         CALL    RESETLOOP

         MOV     DPTR,#GRTOTSTRT       ; CLEAR 6-BYTE PRE-DELIVERY TOTALIZER
         MOV     R1,#6
         CALL    RESETLOOP

         MOV     DPTR,#SETUPCOD
         MOV     R1,#6
         CALL    RESETLOOP

         MOV     PRESET,#0
         MOV     PRESET+1,#0
         MOV     PRESET+2,#0           ; CLEAR PRESET

         MOV     DPTR,#CONFIG
         MOV     A,#0
         MOVX    @DPTR,A
         INC     DPTR
         MOVX    @DPTR,A               ; CLEAR AUDIT BUMP FLAGS

         MOV     S1SHOFF,#0

         MOV     R0,#100
         MOV     R1,#14
         MOV     A,#0
         MOV     DPTR,#PCNVOL          ; CLEAR ALL CAL & DWL'S
CLRCAL:  MOVX    @DPTR,A               ; AND CMPS 1400 WORTH
         INC     DPTR                  ; SHFT VOLS
         DJNZ    R0,CLRCAL
         MOV     R0,#100
         DJNZ    R1,CLRCAL


         MOV     DPTR,#QOB1            ; CLEAR QOB'S, 300 BYTES
         MOV     R0,#3
         MOV     R1,#100
CLRQOB:  CALL    RESETLOOP
         MOV     R1,#100
         DJNZ    R0,CLRQOB

         MOV     PRCOD,#1              ; START WITH 1

         MOV     DPTR,#CALNO           ; CLEAR AUDIT COUNTERS (TEMP ONLY)
         MOV     R1,#6
         CALL    RESETLOOP


CLRPLC:  MOV     R1,#198               ; 2 BYTES PER. LOCATION+#LINES
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - LOOK TO 2ND RAM
         MOV     DPTR,#ADDPLCOD
         CALL    RESETLOOP
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY - RESTORE MCON




CLRADPL:
         MOV     R2,#9                 ; 100 X 25 = 2500
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - 9 X 256 + 196 = 2500
         MOV     DPTR,#ADDPRLBL

         MOV     R1,#0
CLRADP1: CALL    RESETLOOP
         DJNZ    R2,CLRADP1
         MOV     R1,#196
         CALL    RESETLOOP

;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY

REST:
         MOV     DPTR,#PRPRICE
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     R2,#32                ; 3200 BYTES
         MOV     R1,#100
REST1:   CALL    RESETLOOP
         MOV     R1,#100               ; RELOAD 100 COUNTER
         DJNZ    R2,REST1
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY

         MOV     DPTR,#FIXEDCHG
         MOV     R1,#54                ; 54 BYTES
         CALL    RESETLOOP

         MOV     DPTR,#FIXEDLBL
         MOV     R1,#135               ; 135 BYTES
         CALL    RESETLOOP

;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY
         MOV     DPTR,#PMSG10
         MOV     R0,#8                 ; 8*256 + 202 = 2250 (9 FIXED TABLES)
         MOV     R1,#0                 ;
REST5:   CALL    RESETLOOP
         DJNZ    R0,REST5

         MOV     R1,#202               ; 2250 BYTES TOTAL, CLEARS FROM PMSG10 DOWN
         CALL    RESETLOOP
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY

         MOV     DPTR,#HMSG1
         MOV     R1,#250               ; 250 BYTES
         CALL    RESETLOOP

         MOV     DPTR,#FMSG1
         MOV     R1,#100               ; 100 BYTES
         CALL    RESETLOOP

         MOV     DPTR,#B1FLG
         MOV     R1,#15
         CALL    RESETLOOP

         CALL    CLRBYTES              ; CLEAR BEFORE (U) AND AFTER (W) PRINT BYTES FROM HOST

                                       ; PMSG1 = ADDRESS OF FIRST BYTES FOR PROD DESC
                                       ;  FOR ALL 99 PRODUCTS, IN FARMEMORY
                                       ; DMSG1 = ADDRESS OF DEFAULT LABELS FOR FIRST 9
                                       ;  PRODUCTS, THEY ARE COPIED TO PMSG1 ON RESET

         MOV     R1,#225               ; SHIFT 1ST 9 PRODUCT LABELS TO EXT RAM
         MOV     DPTR,#PMSG1           ; FOR DEFAULT PRODUCT DESCRIPTIONS
         MOV     LSB,DPL
         MOV     MSB,DPH               ; SAVE PMSG1 DPTR
         MOV     DPTR,#DMSG1
RESTX:   MOVX    A,@DPTR
         PUSH    DPH
         PUSH    DPL
         MOV     DPL,LSB
         MOV     DPH,MSB               ; RESTORE PMSG1 DPTR
;*-*         ORL     MCON,#4               ; ONLY CALL IF EC, SET MCON TO FAR MEMORY - SWITCH RAMS
         MOVX    @DPTR,A
;         ANL     MCON,#0FBH            ; ONLY CALL IF EC, SET MCON TO LOCAL MEMORY - SWITCH BACK TO LOCAL RAM
         INC     DPTR
         MOV     LSB,DPL
         MOV     MSB,DPH               ; SAVE BACK AGAIN
         POP     DPL
         POP     DPH
         INC     DPTR                  ; NEXT DMSG
         DJNZ    R1,RESTX

         MOV     A,#0

         MOV     DPTR,#TRUNC
         MOVX    @DPTR,A

         MOV     DPTR,#EMPLSREV        ; 0=256,1=100
         MOVX    @DPTR,A
         MOV     DPTR,#EMCHANNL        ; 0=2,1=1
         MOVX    @DPTR,A
         MOV     DPTR,#EMFLODIR        ; 0=CW,1=CCW
         MOVX    @DPTR,A
         MOV     DPTR,#EMCONFIG        ; CONFIG BYTE FOR EM
         MOVX    @DPTR,A

         MOV     DPTR,#QOBMODE         ; 0=QOB FUNCTIONS OFF (DEFAULT), 1=QOB FUNCTIONS ON
         MOVX    @DPTR,A

         MOV     DPTR,#EMULATEFLG      ; 0=OFF=DEFAULT, 1=TEMP OFF, 2=PA21, 3=E141
         MOVX    @DPTR,A
         MOV     DPTR,#LASTEMUFLG      ; FOR RESTORING EMULATEFLG IF EMULATEFLG = 1 ON REBOOT
         MOVX    @DPTR,A

         MOV     DPTR,#HOSTFIX         ; CLEAR FLAG MEANING HOST COMMANDS REQUIRE TILDES PREFIXED
         MOVX    @DPTR,A

         MOV     DPTR,#PR65FLG
         MOVX    @DPTR,A               ; 6501 PRINT FORMAT OFF

         MOV     DPTR,#BROKNVLV        ; 179_A, DISABLE BROKEN VALVE DETECTION BY DEFAULT
         MOVX    @DPTR,A

         MOV     DPTR,#SWAUTHFLG
         MOVX    @DPTR,A               ; SET SOFTWARE AUTHORIZATION DISABLED

         MOV     DPTR,#MPCFLG          ; MP CAL FLAG
         MOVX    @DPTR,A

         MOV     DPTR,#CPMFLG          ; CAL PT MODE FLAG
         MOVX    @DPTR,A

         MOV     DPTR,#SSRSTFLG
         MOVX    @DPTR,A               ; S/S BUTTON ENABLED

         MOV     DPTR,#PPBTNENBL       ; 3=START WITH PRINT AND PRESET BUTTONS ENABLED BY DEFAULT
         MOVX    @DPTR,A


         MOV     A,#1

         MOV     DPTR,#DIVISOR
         MOVX    @DPTR,A

         MOV     DPTR,#COPIES
         MOVX    @DPTR,A

         MOV     DPTR,#CPYCPY
         MOVX    @DPTR,A

         MOV     DPTR,#LASTPRC
         MOVX    @DPTR,A
         MOV     PRCOD,#01H


         MOV     A,#2

         MOV     DPTR,#PRTFLG          ; 2=THERMAL FOR EC BY DEFAULT
         MOVX    @DPTR,A

         CALL    CLRHCMDFLG            ; CLEAR FLAG MEANING IN HOST COMMS

         CALL    DISCON                ; CALL #10: ON RE-PROGRAM GO AHEAD AND CLEAR THEM = INSURANCE
         CALL    UNLATCH

REST10:  MOV     DPTR,#MSG72           ; TELL EM ALL RESET , RESET
         CALL    SHOW
         MOV     DPTR,#MSG102          ; CYCLE
         CALL    SHOW
         MOV     DPTR,#MSG103          ; POWER
         CALL    SHOW
         JMP     REST10

//CLRLINES is still used, so if removing the CLRTOT4 code, this must not be deleted
CLRLINES:MOV     DPTR,#LINES
         MOV     A,#0
         MOVX    @DPTR,A

EXIT:    RET


; FREE UP SOME CODE SPACE
RESETLOOP:
         MOV     A,#0
RESETLO1:MOVX    @DPTR,A
         INC     DPTR
         DJNZ    R1,RESETLO1
         RET


LGD100:  MOV     DPTR,#LGDFLG1
         MOV     A,#00H
         MOVX    @DPTR,A
         RET
LGD101:  MOV     DPTR,#LGDFLG1
         MOV     A,#01H
         MOVX    @DPTR,A
         RET
LGD102:  MOV     DPTR,#LGDFLG1
         MOV     A,#02H
         MOVX    @DPTR,A
         RET
LGD104:  MOV     DPTR,#LGDFLG1
         MOV     A,#04H
         MOVX    @DPTR,A
         RET
LGD108:  MOV     DPTR,#LGDFLG1
         MOV     A,#08H
         MOVX    @DPTR,A
         RET
LGD10A:  MOV     DPTR,#LGDFLG1
         MOV     A,#0AH
         MOVX    @DPTR,A
         RET

LGD200:  MOV     DPTR,#LGDFLG2
         MOV     A,#00H
         MOVX    @DPTR,A
         RET
LGD210:  MOV     DPTR,#LGDFLG2
         MOV     A,#10H
         MOVX    @DPTR,A
         RET
LGD218:  MOV     DPTR,#LGDFLG2
         MOV     A,#18H
         MOVX    @DPTR,A
         RET
LGD220:  MOV     DPTR,#LGDFLG2
         MOV     A,#20H
         MOVX    @DPTR,A
         RET
LGD240:  MOV     DPTR,#LGDFLG2
         MOV     A,#40H
         MOVX    @DPTR,A
         RET

LF0000:  CALL    LGD100                ; NO ICONS: GETDRV, NEWCODE, SETTYP1, SETTYP2, SETDSC2, GETDAYS, SETTXSUB, SETTOVR
         CALL    LGD200
         RET
LF0010:  CALL    LGD100                ; SETUP ICON ONLY: GETTRK, GETFIRST, GETCODE, TKTNUM, CALNUM, GETCOPYS, SETHOSPK, SETBATCH, GTHSTFIX, SETSSRST, SET65PRT, SETMONY
         CALL    LGD210
         RET
LF0018:  CALL    LGD100                ; CALIBRATE AND SETUP ICONS
         CALL    LGD218
         RET
LF0020:  CALL    LGD100                ; ONLY CALLED IN GETPRCOD ...
         CALL    LGD220
         RET
LF0100:  CALL    LGD101                ; SHOWING TEMP IN NON-CAL MODE, ONLY CALLED IN TMPOUT
         CALL    LGD200
         RET
LF0110:  CALL    LGD101                ; GETSECND, GETQOB, GETENT ...
         CALL    LGD210
         RET
LF0118:  CALL    LGD101                ; SHOWING TEMP IN CAL MODE, ONLY CALLED IN TMPOUT, COMPLEMENT OF LF 0100 ABOVE ..
         CALL    LGD218
         RET
LF0210:  CALL    LGD102
         CALL    LGD210
         RET
LF0240:  CALL    LGD102
         CALL    LGD240
         RET
LF0410:  CALL    LGD104
         CALL    LGD210
         RET
LF0800:  CALL    LGD108
         CALL    LGD200
         RET
LF0818:  CALL    LGD108
         CALL    LGD218
         RET
LF0A10:  CALL    LGD10A
         CALL    LGD210
         RET

ALLSIX:  MOV     DGTPOS,#1             ; START WITH DIGIT1 BLINK
         MOV     DATEIMG+1,#1          ; TEMP SAVE FOR BLANKING
         MOV     DATEIMG,#0FFH         ; ALL 6 DIGITS
         RET

TWODIGAT5:
         MOV     DGTPOS,#5             ; STARTING DIGIT POSITION
         MOV     DATEIMG+1,#5
         MOV     DATEIMG,#0FH          ; BLANKED DIGITS [00001111]
         RET

DGT00FFHD:
         MOV     DGTPOS,#0
         MOV     DATEIMG,#0FFH
         CALL    DISPSET
         RET
DGT00FHD:
         MOV     DGTPOS,#0
         MOV     DATEIMG,#0FH
         CALL    DISPSET
         RET

; Force to bootloader mode with button press, must be run each time through main loop to watch for button press
BootloaderEvent:
		  JB ENTER_BOOT_MODE_BTN, BLE_EXIT    ;Jump if Bit Set and Clear Bit
		  MOV     DPTR,#MSG161            ; "PRGRM "
		  CALL    SHOW
          MOV RSTSRC ,#0x10          ; make reset will boot into bootloader and check for button status
BLE_EXIT: RET


$include (MLPLY.INC)
$include (XLPLY.INC)
$include (E300EA-C.INC)
$include (CMPTBLS.INC)

         END


