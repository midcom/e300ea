# README #

E300EA is the intial version for the DS2 released in March 2022. The seed version was E202EA.
The code was created in Simplicity Studio. The SDKs are in a shared folder - G:\shared\SiLabs\sdks\8051


### Repos ###
Jerff's local repo - G:\jeff\EC\E300EA

### Files ###

* E300EA - Main source code
* DS2HDR - Include with all internal RAM setup as well as port pins
* CMPTBLS - Include file containing temperature compensation tables
* E300EA-M - Include file defining external battery baccked memory layout
* MLPLY, XlPLY - Include file for handling multiplication

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact